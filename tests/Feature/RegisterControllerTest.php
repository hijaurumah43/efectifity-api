<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Ramsey\Uuid\Uuid;

class RegisterControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCanRegister()
    {
        $key = 'Member';
        \App\Models\Role::firstOrCreate([
            'id' => Uuid::uuid4(),
            'name' => $key,
            'display_name' => ucwords(str_replace('_', ' ', $key)),
            'description' => ucwords(str_replace('_', ' ', $key))
        ]);
        
        $this->postJson(route('api.auth.register'), [
            'name'                  => $this->faker->firstName,
            'email'                 => $this->faker->unique()->safeEmail,
            'password'              => 'password',
            'password_confirmation' => 'password',
        ])
            ->assertStatus(Response::HTTP_CREATED);
    }

    public function testCannotRegisterBeacusePasswordIsTooShrot()
    {
        $this->postJson(route('api.auth.register'), [
            'name'                  => $this->faker->firstName,
            'email'                 => $this->faker->unique()->safeEmail,
            'password'              => 'pass',
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testCannotRegisterBeacuseEmailAlreadyRegistered()
    {
        User::create([
            'email' => 'test@test.com',
            'password' => bcrypt('password')
        ]);

        $this->postJson(route('api.auth.register'), [
            'name'                  => 'test',
            'email'                 => 'test@test.com',
            'password'              => 'password',
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testVerifyEmail()
    {
        $user = User::create([
            'email'                     => $this->faker->unique()->safeEmail,
            'email_verified_at'         => null,
            'email_token_confirmation'  => Uuid::uuid4(),
            'password'                  => bcrypt('password'),
            'is_active'                 => 1,
        ]);

        $this->post(route('api.email.verify', [$user->email_token_confirmation]))
            ->assertOk();
    }

    public function testInvalidVerifyEmail()
    {
        $user = User::create([
            'email'                     => $this->faker->unique()->safeEmail,
            'email_verified_at'         => null,
            'email_token_confirmation'  => null,
            'password'                  => bcrypt('password'),
            'is_active'                 => 1,
        ]);

        $this->post(route('api.email.verify', [Uuid::uuid4()]))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertSee('Invalid token for email verification');
    }
}
