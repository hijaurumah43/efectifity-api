<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResetPasswordControllerTest extends TestCase
{
    private $user;

    public function setUp(): void
    {
        parent::setUp();
    
        $this->user = User::factory()->create();
    }

    public function testSubmitPasswordReset()
    {
        $token = Password::broker()->createToken($this->user);
        $password = $this->faker->password(12);

        $this
            ->post(route('reset.password'), [
                'token'                 => $token,
                'email'                 => $this->user->email,
                'password'              => $password,
                'password_confirmation' => $password,
            ])
            ->assertSuccessful();
    }

    /* public function testSubmitPasswordResetRequestInvalidEmail()
    {
        $this
            ->post(route('reset.email-link'), [
                'email' => $this->faker->word,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    } */

    public function testSubmitPasswordResetRequestEmailNotFound()
    {
        $this
            ->post(route('api.reset.email-link'), [
                'email' => $this->faker->unique()->safeEmail,
            ])
            ->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /**
     * Testing submitting a password reset request.
     */
    public function testSubmitPasswordResetRequest()
    {
        $this
            ->post(route('api.reset.email-link'), [
                'email' =>$this->user->email
            ])
            ->assertOk();

        Notification::assertSentTo($this->user, ResetPasswordNotification::class);
    }

    /**
     * Testing submitting the password reset page with an invalid
     * email address.
     */
    /* public function testSubmitPasswordResetInvalidEmail()
    {
        $token = Password::broker()->createToken($this->user);

        $password = $this->faker->password(12);

        $this
            ->post(route('reset.password'), [
                'token'                 => $token,
                'email'                 => $this->faker->word,
                'password'              => $password,
                'password_confirmation' => $password,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    } */

    /**
     * Testing submitting the password reset page with an email
     * address not in the database.
     */
    public function testSubmitPasswordResetEmailNotFound()
    {
        $token = Password::broker()->createToken($this->user);

        $password = $this->faker->password(12);

        $this
            ->post(route('reset.password'), [
                'token'                 => $token,
                'email'                 => $this->faker->unique()->safeEmail,
                'password'              => $password,
                'password_confirmation' => $password,
            ])
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /* public function testSubmitPasswordResetPasswordMismatch()
    {
        $token = Password::broker()->createToken($this->user);
        $password = 'password';
        $password_confirmation = 'secretxxx';

        $this
            ->post(route('reset.password'), [
                'token'                 => $token,
                'email'                 => $this->user->email,
                'password'              => $password,
                'password_confirmation' => $password_confirmation,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    } */

    /* public function testSubmitPasswordResetPasswordTooShort()
    {
        $token = Password::broker()->createToken($this->user);
        $password = 'pass';

        $this
            ->post(route('reset.password'), [
                'token'                 => $token,
                'email'                 => $this->user->email,
                'password'              => $password,
                'password_confirmation' => $password,
            ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    } */
}
