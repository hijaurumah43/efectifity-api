<?php

namespace Tests\Feature;

use App\Models\Common\Address\City;
use Tests\TestCase;
use App\Models\User;
use App\Models\Common\Company\Company;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyControllerTest extends TestCase
{
    private $user;

    public function setUp(): void
    {
        parent::setUp();
    
        $this->user = User::factory()->create();
    }
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFetchCompanies()
    {
        $this
            ->getJson(route('api.companies.index'))
            ->assertStatus(Response::HTTP_OK);
    }

    public function testFetchCompanyById()
    {
        $city = new City();
        $city->title = $this->faker->city;
        $city->save();

        $company = new Company();
        $company->user_id       = $this->user->id;
        $company->title         = $this->faker->firstName;
        $company->city_id       = $city->id;
        $company->is_published  = 1;
        $company->is_active     = 1;
        $company->save();

        $this
            ->getJson(route('api.companies.show', $company->id))
            ->assertStatus(Response::HTTP_OK);
    }

    public function testFetchServicesByInvalidId()
    {
        $this
            ->getJson(route('api.companies.show', $this->faker->randomNumber(2)))
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
