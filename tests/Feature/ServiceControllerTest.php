<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Str;
use App\Models\Common\Service\Service;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiceControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testFetchServices()
    {
        $this
            ->getJson(route('api.services.index'))
            ->assertStatus(Response::HTTP_OK);
    }

    public function testFetchServicesBySlug()
    {
        $title   = $this->faker->word();

        $service = new Service();
        $service->title         = $title;
        $service->slug          = Str::slug($title);
        $service->image         = 'image.jpg';
        $service->is_published  = 1;
        $service->save();

        $this
            ->getJson(route('api.services.show', $service->slug))
            ->assertStatus(Response::HTTP_OK);
    }

    public function testFetchServicesByInvalidSlug()
    {
        $this
            ->getJson(route('api.services.show', $this->faker->word()))
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
