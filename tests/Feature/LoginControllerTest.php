<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginControllerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $user = User::create([
            'email' => $this->faker->unique()->safeEmail,
            'password' => bcrypt('password')
        ]);

        $this
            ->postJson(route('api.auth.login'), [
                'email' => $user->email,
                'password' => 'password',
            ])
            ->assertSuccessful()
            ->assertJsonStructure([
                "data" => [
                    "token",
                    "tokenType",
                ],
                "meta" => [
                    "code",
                    "status",
                    "timestamp",
                ]
            ]);
    }
}
