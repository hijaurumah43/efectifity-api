<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Client\Member\Member;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MemberControllerTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    private $user;

    public function setUp(): void
    {
        parent::setUp();
    
        $this->user = User::factory()->create();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testMe()
    {
        $member = new Member();
        $member->user_id = $this->user->id;
        $member->name    = $this->faker->name;
        $member->save();

        $this->actingAs($this->user)
            ->getJson(route('api.me'))
            ->assertOk();
    }
}
