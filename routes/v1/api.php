<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppVersionController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Webhook\XenditController;
use App\Http\Controllers\Auth\LoginSocialController;
use App\Http\Controllers\Common\Home\HomeController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Internal\Bank\BankController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Common\Address\CityController;
use App\Http\Controllers\Common\Branch\BranchController;
use App\Http\Controllers\Common\Classes\ClassController;
use App\Http\Controllers\Common\Search\SearchController;
use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Common\Company\CompanyController;
use App\Http\Controllers\Common\Service\ServiceController;
use App\Http\Controllers\Common\Voucher\VoucherController;
use App\Http\Controllers\Auth\ResetPasswordPartnerController;
use App\Http\Controllers\Common\Branch\BranchGalleryController;
use App\Http\Controllers\Common\Branch\BranchPackageController;
use App\Http\Controllers\Common\Branch\BranchScheduleController;
use App\Http\Controllers\Common\Payment\PaymentMethodController;
use App\Http\Controllers\Common\Branch\BranchClassLevelController;
use App\Http\Controllers\Common\Branch\BranchClassCategoryController;
use App\Http\Controllers\Common\Branch\BranchScheduleDetailController;
use App\Http\Controllers\Common\Branch\BranchOperationalTimeController;

###################
# GUEST
###################
Route::get('app/version', [AppVersionController::class, 'index'])->name('api.version.index');
Route::get('app/time/{date_time}', [AppVersionController::class, 'time'])->name('api.version.time');
Route::patch('app/version', [AppVersionController::class, 'update'])->name('api.version.update');
Route::get('nearby/{services}', [HomeController::class, 'index'])->name('api.home');
Route::post('apple/login', [LoginSocialController::class, 'verifyApple'])->name('api.auth.social-media-apple');
Route::post('{provider}/login', [LoginSocialController::class, 'verify'])->name('api.auth.social-media');
Route::post('login', [LoginController::class, 'login'])->name('api.auth.login');
Route::post('register', [RegisterController::class, 'register'])->name('api.auth.register');
Route::post('email/verify/{token}', [EmailVerificationController::class, 'verify'])
    ->middleware('throttle:5,1')
    ->name('api.email.verify');

Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail'])
    ->middleware('throttle:5,1')
    ->name('api.reset.email-link');

// OLD
/* Route::post('password/reset', [ResetPasswordController::class, 'reset'])
    ->middleware('throttle:5,1')
    ->name('reset.password'); */

Route::post('password/reset', [ResetPasswordPartnerController::class, 'reset'])
    ->middleware('throttle:5,1')
    ->name('reset.password');

Route::apiResource('companies', CompanyController::class)
    ->only([
        'index',
        'show',
    ])
    ->names([
        'index' => 'api.companies.index',
        'show' => 'api.companies.show'
    ]);

###################
# Service / Product
###################
Route::apiResource('services', ServiceController::class)
    ->only([
        'index',
        'show',
    ])
    ->names([
        'index' => 'api.services.index',
        'show' => 'api.services.show'
    ]);

###################
# Search
###################
Route::get('search/{category}', [SearchController::class, 'collection'])
    ->name('api.search.collection');
###################
# Branches
###################
Route::apiResource('branches', BranchController::class)
    ->only([
        'index',
        'show',
    ])
    ->names([
        'index' => 'api.branches.index',
        'show' => 'api.branches.show'
    ]);
Route::get('branches/{id}/others', [BranchController::class, 'location'])
    ->name('api.branches.other');
Route::get('branches/{id}/operational-times', [BranchOperationalTimeController::class, 'collection'])
    ->name('api.branch-operational-time.collection');
Route::get('branches/{id}/operational-times/date/{date}', [BranchOperationalTimeController::class, 'showByDate'])
    ->name('api.branch-operational-time.show.byDate');
Route::get('branches/{id}/schedules', [BranchScheduleController::class, 'collection'])
    ->name('api.branch-schdules.collection');
Route::get('branches/{id}/packages', [BranchPackageController::class, 'collection'])
    ->name('api.branch-packages.collection');
Route::get('branches/{id}/vouchers', [VoucherController::class, 'index'])
    ->name('api.branch.voucher');
Route::get('branches/{id}/agreements', [BranchController::class, 'agreements'])
    ->name('api.branch.agreements');
Route::get('branches/{id}/classes/{class}/{date}', [BranchScheduleDetailController::class, 'show'])
    ->name('api.branch-classes.show');
Route::get('branches/{id}/galleries', [BranchGalleryController::class, 'index'])
    ->name('api.branch.galleries');
Route::get('branches/{id}/galleries/collections', [BranchGalleryController::class, 'collection'])
    ->name('api.branch.galleries.collection');

Route::apiResource('branch-packages', BranchPackageController::class)
    ->only([
        'show'
    ])
    ->names([
        'show' => 'api.branch-packages.show'
    ]);

###################
# Classes
###################
Route::get('classes', [ClassController::class, 'index'])->name('api.classes.index');
Route::get('classes/levels', [ClassController::class, 'level'])->name('api.classes.level');
Route::get('classes/categories', [ClassController::class, 'category'])->name('api.classes.category');
Route::get('classes/{schedule_detail_id}', [ClassController::class, 'show'])->name('api.classes.show');

Route::get('payment-methods', [PaymentMethodController::class, 'index'])->name('api.payment-type.index');
###################
# Cities
###################
Route::prefix('address')->group(function () {
    Route::get('cities', [CityController::class, 'index'])->name('api.address-city.index');
});
###################
# Banks
###################
Route::get('banks', [BankController::class, 'index'])->name('api.banks.index');

Route::prefix('callback')->group(function () {
    Route::prefix('xendit')->group(function () {
        Route::post('va', [XenditController::class, 'va'])->name('api.webhook.xendit.va');
        Route::post('ewallets', [XenditController::class, 'eWallets'])->name('api.webhook.xendit.eWallets');
        Route::post('ovo', [XenditController::class, 'ovo'])->name('api.webhook.xendit.ovo');
        Route::post('qrcode', [XenditController::class, 'qrCode'])->name('api.webhook.xendit.qrCode');
        Route::post('disbursement', [XenditController::class, 'disbursement'])->name('api.webhook.xendit.disbursement');
    });
});

Route::get('vouchers/{voucher_id}', [VoucherController::class, 'show'])
    ->name('api.voucher.show');


###################
# LOGOUT
###################
Route::middleware(['auth:api'])->group(function () {
    Route::post('logout', [LoginController::class, 'logout'])->name('api.auth.logout');
});
