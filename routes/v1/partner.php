<?php

use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\RegisterPartnerController;
use App\Http\Controllers\Client\Member\MemberController;
use App\Http\Controllers\Client\Partner\PartnerController;
use App\Http\Controllers\Auth\ResetPasswordPartnerController;
use App\Http\Controllers\Client\Partner\Bank\MemberBankController;
use App\Http\Controllers\Client\Partner\Branch\PlatformController;
use App\Http\Controllers\Client\Partner\Branch\PlatformLogController;
use App\Http\Controllers\Client\Partner\Branch\PlatformManagerController;
use App\Http\Controllers\Client\Partner\Branch\PlatformRequireController;
use App\Http\Controllers\Client\Partner\Company\CompanyDocumentController;
use App\Http\Controllers\Client\Partner\Branch\PlatformAgreementController;
use App\Http\Controllers\Client\Partner\Branch\PlatformRefundPolicyController;
use App\Http\Controllers\Client\Partner\Company\CompanyDocumentLogController;
use App\Http\Controllers\Client\Partner\Transaction\TransactionDetailController;

Route::post('register', [RegisterPartnerController::class, 'register'])
    ->name('auth.partner-register');

Route::get('password/email/check', [ForgotPasswordController::class, 'checkToken'])
    ->middleware('throttle:5,1')
    ->name('check.email-link');

Route::post('password/email/link', [ForgotPasswordController::class, 'sendLinkForgotPassword'])
    ->middleware('throttle:5,1')
    ->name('send.email-link');

Route::post('password/reset', [ResetPasswordPartnerController::class, 'reset'])
    ->middleware('throttle:5,1')
    ->name('reset.password');

Route::middleware(['auth:api', 'role:partner'])->group(function () {
    Route::get('me', [PartnerController::class, 'profile'])->name('partner.me');
    Route::patch('me', [PartnerController::class, 'updateMe'])->name('partner.me.update');
    Route::patch('password', [MemberController::class, 'updatePassword'])->name('me.password');

    Route::prefix('pin')->group(function () {
        Route::post('/', [UserController::class, 'setPin'])->name('pin.partners.set');
        Route::patch('/', [UserController::class, 'changePin'])->name('pin.partners.update');
        Route::get('email', [UserController::class, 'sendEmailResetPin'])->name('reset.pin.email-link');
        Route::post('verify/{token}', [UserController::class, 'verifyPin'])->middleware('throttle:5,1')->name('pin.partners.verify');
        Route::get('check/{pin}', [UserController::class, 'checkPin'])->name('pin.partners.check');
    });

    Route::get('phone/otp', [UserController::class, 'phoneNotification'])->name('phone.partner.notif-otp');
    Route::post('phone/verify/{token}', [UserController::class, 'verifyPhone'])
        ->middleware('throttle:5,1')
        ->name('phone.partner.verify');

    ###################
    # Company Document
    ###################
    Route::get('company-document/{id}/log', [CompanyDocumentLogController::class, 'index'])->name('partner.company-document-log.index');
    Route::apiResource('company-document', CompanyDocumentController::class)
        ->only([
            'index',
            'show',
            'store',
        ])
        ->names([
            'index' => 'partner.company-document.index',
            'show' => 'partner.company-document.show',
            'store' => 'partner.company-document.store',
        ]);

    ###################
    # Member Bank
    ###################
    Route::apiResource('member-bank', MemberBankController::class)
        ->only([
            'index',
            'show',
            'store',
            'update',
            'destroy',
        ])
        ->names([
            'index' => 'partner.member-bank.index',
            'show' => 'partner.member-bank.show',
            'store' => 'partner.member-bank.store',
            'update' => 'partner.member-bank.update',
            'destroy' => 'partner.member-bank.destroy',
        ]);

    ###################
    # Platform
    ###################
    Route::prefix('platforms')->group(function () {
        Route::get('/', [PlatformController::class, 'index'])
            ->name('partner.platforms.index');
        Route::get('required', [PlatformRequireController::class, 'index'])
            ->name('partner.platforms.require.index');
        Route::post('required', [PlatformRequireController::class, 'store'])
            ->name('partner.platforms.require.store');
        Route::get('{platform_id}', [PlatformController::class, 'show'])
            ->name('partner.platforms.show');

        Route::get('{service_slug}/document', [PlatformController::class, 'checkLegalDocument'])
            ->name('partner.platforms.check');
        Route::get('{platform_id}/log', [PlatformLogController::class, 'index'])
            ->name('partner.platforms.status');

        Route::post('submit', [PlatformController::class, 'submit'])
            ->name('partner.platforms.submit');
        Route::post('/', [PlatformController::class, 'store'])
            ->name('partner.platforms.store');
        Route::post('{platform_id}/update', [PlatformController::class, 'update'])
            ->name('partner.platforms.update');
        Route::post('{platform_id}/logo', [PlatformController::class, 'uploadLogo'])
            ->name('partner.platforms.uploadLogo');
        Route::post('{platform_id}/thumbnail', [PlatformController::class, 'uploadThumbnail'])
            ->name('partner.platforms.uploadThumbnail');

        // Agreements
        Route::get('{platform_id}/agreements', [PlatformAgreementController::class, 'index'])
            ->name('partner.platforms.require.index');
        Route::get('{platform_id}/agreements/{id}', [PlatformAgreementController::class, 'show'])
            ->name('partner.platforms.require.show');
        Route::post('{platform_id}/agreements', [PlatformAgreementController::class, 'store'])
            ->name('partner.platforms.require.store');

        // RefundPolicy
        Route::get('{platform_id}/refund-policies', [PlatformRefundPolicyController::class, 'index'])
            ->name('partner.platforms.refund-policy.index');
        Route::get('{platform_id}/refund-policies/{id}', [PlatformRefundPolicyController::class, 'show'])
            ->name('partner.platforms.refund-policy.show');
        Route::post('{platform_id}/refund-policies', [PlatformRefundPolicyController::class, 'store'])
            ->name('partner.platforms.refund-policy.store');

        ###################
        # Manager
        ###################
        Route::post('{platform_id}/invitation', [PlatformManagerController::class, 'store'])
            ->name('partner.platforms.store');
        Route::post('{platform_id}/re-invitation', [PlatformManagerController::class, 'reInvite'])
            ->name('partner.platforms.reInvite');
        Route::get('{platform_id}/manager', [PlatformManagerController::class, 'index'])
            ->name('partner.platforms.index');
        Route::delete('{platform_id}/manager/{member_id}', [PlatformManagerController::class, 'destroy'])
            ->name('partner.platforms.destroy');
    });

    Route::prefix('finance')->group(function () {
        Route::prefix('transactions')->group(function () {
            // CRUD
            Route::get('/', [TransactionDetailController::class, 'index'])->name('fc.transaction.index');
            Route::get('/summary', [TransactionDetailController::class, 'summary'])->name('fc.transaction.summary');
            Route::get('/banks', [TransactionDetailController::class, 'bank'])->name('fc.transaction.bank');
            Route::post('/', [TransactionDetailController::class, 'store'])->name('fc.transaction.store');
        });
        
        Route::get('/balance', [TransactionDetailController::class, 'getBalance'])->name('fc.transaction.balance');
    });
});
