<?php

use App\Http\Controllers\Client\Staff\StaffSchedulesController;
use App\Http\Controllers\Client\Member\Classes\MemberBranchController;
use App\Http\Controllers\Client\Staff\Attendance\ClassAttendanceController;
use App\Http\Controllers\Client\Staff\Attendance\MemberAttendanceController;
use App\Http\Controllers\Client\Staff\Attendance\StaffAttendanceController;
use App\Http\Controllers\Client\Staff\ClientScheduleController;

###################
# Staff
###################
Route::middleware(['auth:api', 'role:customer_service|sales_marketing|finance_accounting|coach|personal_trainer'])->group(function () {
    Route::apiResource('branches', MemberBranchController::class)
    ->only([
        'index',
        'show',
    ])
    ->names([
        'index' => 'api.staff.branches.index',
        'show' => 'api.staff.branches.show',
    ]);

    Route::get('branches/{member_branch_id}/schedules', [StaffSchedulesController::class, 'index'])->name('api.staff.schedules.index');
    Route::get('activity/attendances/{member_branch_id}', [StaffAttendanceController::class, 'staffScheduleActivity'])->name('api.staff.attendances.activity');
    Route::post('attendances/clockin', [StaffAttendanceController::class, 'clockin'])->name('api.staff.attendances.store');
    Route::post('attendances/clockout', [StaffAttendanceController::class, 'clockout'])->name('api.staff.attendances.update');
    Route::get('/attendances/status/{member_branch_id}', [StaffAttendanceController::class, 'status'])->name('api.staff.attendances.status');

    // scan
    Route::get('branches/{branch_id}/member/{member_id}', [ClassAttendanceController::class, 'scanned'])->name('api.staff.classes');
    Route::post('branches/{branch_id}/class-attendance/{class_attendance_id}', [ClassAttendanceController::class, 'update'])->name('api.staff.classes.update');
    
    Route::post('/clockin/member', [MemberAttendanceController::class, 'clockin'])->name('api.staff.attendances-member.store');
    Route::post('/clockout/member', [MemberAttendanceController::class, 'clockout'])->name('api.staff.attendances-member.udpate');

    // Work Schedule
    Route::get('/branches/{member_branch_id}/schedules/today', [StaffSchedulesController::class, 'checkSchedule'])->name('api.staff.schedules.check'); 
    Route::get('/branches/{member_branch_id}/schedules/{month}/{year}', [StaffSchedulesController::class, 'workerSchedule'])->name('api.staff.schedules.work'); 

    // Client Schedule
    Route::get('/branches/{member_branch_id}/clients/session/{pt_attendance_id}', [ClientScheduleController::class, 'session'])->name('api.staff.client.session'); 
    Route::post('/branches/{member_branch_id}/clients/session/{pt_attendance_id}', [ClientScheduleController::class, 'getPin'])->name('api.staff.client.getPin'); 
    Route::patch('/branches/{member_branch_id}/clients/session/{pt_attendance_id}', [ClientScheduleController::class, 'submit'])->name('api.staff.client.submit'); 
    Route::patch('/branches/{member_branch_id}/clients/summary/{pt_attendance_id}', [ClientScheduleController::class, 'summary'])->name('api.staff.client.summary'); 
    Route::get('/branches/{member_branch_id}/clients/view/{pt_attendance_id}', [ClientScheduleController::class, 'show'])->name('api.staff.client.show'); 
    Route::get('/branches/{member_branch_id}/clients/{month}/{year}', [ClientScheduleController::class, 'index'])->name('api.staff.client.index'); 
    Route::get('/branches/{member_branch_id}/current-clients', [ClientScheduleController::class, 'currentClient'])->name('api.staff.client.current'); 

    // Route::post('/clockout/member', [StaffAttendanceController::class, 'clockout'])->name('api.staff.attendances-member.udpate');
});