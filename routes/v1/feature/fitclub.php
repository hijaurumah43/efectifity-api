<?php

use App\Http\Controllers\Client\Staff\StaffController as AppStaffController;
use App\Http\Controllers\Common\Branch\BranchOperationalTimeController;
use App\Http\Controllers\Feature\Fitclub\ClassController;
use App\Http\Controllers\Feature\Fitclub\StaffController;
use App\Http\Controllers\Feature\Fitclub\BranchController;
use App\Http\Controllers\Feature\Fitclub\CustomerController;
use App\Http\Controllers\Feature\Fitclub\DashboardController;
use App\Http\Controllers\Feature\Fitclub\ClassScheduleController;
use App\Http\Controllers\Feature\Fitclub\Settings\ShiftController;
use App\Http\Controllers\Feature\Fitclub\CustomerPackageController;
use App\Http\Controllers\Feature\Fitclub\CustomerActivityController;
use App\Http\Controllers\Feature\Fitclub\Promo\PromoController;
use App\Http\Controllers\Feature\Fitclub\PTController;
use App\Http\Controllers\Feature\Fitclub\Rules\RuleController;
use App\Http\Controllers\Feature\Fitclub\Settings\PackageController;
use App\Http\Controllers\Feature\Fitclub\Settings\PositionController;
use App\Http\Controllers\Feature\Fitclub\Settings\ScheduleController;
use App\Http\Controllers\Feature\Fitclub\Settings\StaffScheduleController;
use App\Http\Controllers\Feature\Fitclub\Settings\ClassController as SettingClassController;
use App\Http\Controllers\Feature\Fitclub\Settings\ClassScheduleController as SettingClassSchedule;
use App\Http\Controllers\Feature\Fitclub\Settings\ClubInfoController;
use App\Http\Controllers\Feature\Fitclub\Settings\GalleryController;
use App\Http\Controllers\Feature\Fitclub\Settings\OperatioanlTimeController;
use App\Http\Controllers\Feature\Fitclub\Settings\PersonalTrainerController;
use App\Http\Controllers\Feature\Fitclub\StaffCollectionController;
use App\Http\Controllers\Feature\Fitclub\Transaction\TransactionDetailController;

Route::middleware(['role:partner|manager|customer_service|sales_marketing|finance_accounting|coach|personal_trainer'])->group(function () {
    Route::get('branches', [BranchController::class, 'index'])->name('fc.branch.index');
    Route::get('me', [AppStaffController::class, 'profile'])->name('api.me');

    // Branch ID
    Route::prefix('{branch_id}')->group(function () {


        // ROLE:: DASHBOAARD
        Route::middleware(['role:partner|manager|customer_service|coach|personal_trainer'])->group(function () {
            Route::get('/', [DashboardController::class, 'branch'])->name('fc.dashboard.branch');
            Route::prefix('dashboard')->group(function () {
                Route::get('visitors', [DashboardController::class, 'visitor'])->name('fc.dashboard.visitor');
                Route::get('upcoming-classes', [DashboardController::class, 'classes'])->name('fc.dashboard.classes');
            });
        });

        // ROLE:: MEMBER ATTENDANCE
        Route::middleware(['role:partner|manager|customer_service|sales_marketing'])->group(function () {
            Route::get('members/{member_id}/program-history', [CustomerPackageController::class, 'history'])->name('fc.programs.history');
            Route::get('members/{member_id}/activity-history', [CustomerActivityController::class, 'history'])->name('fc.activity.history');
            Route::get('members', [CustomerController::class, 'index'])->name('fc.members.index');
            Route::get('members/{member_id}', [CustomerController::class, 'show'])->name('fc.members.show');
            Route::get('managers', [ClassController::class, 'getManager'])->name('fc.manager');
            Route::post('managers/{manager_id}/{pin}', [ClassController::class, 'pinManager'])->name('fc.manager.pin');
            Route::get('member-attendances', [CustomerController::class, 'attendance'])->name('fc.members.attendance');
        });


        // ROLE:: CLASS SESSION
        Route::middleware(['role:partner|manager|customer_service'])->group(function () {
            Route::get('classes/schedules/{schedule_detail_id}', [ClassScheduleController::class, 'index'])->name('fc.classes.schedule');
            Route::post('classes/schedules/{schedule_detail_id}', [ClassScheduleController::class, 'cancel'])->name('fc.classes.cancel');
            Route::apiResource('classes', ClassController::class)
                ->only([
                    'index',
                    'show',
                ])
                ->names([
                    'index' => 'fc.classes.index',
                    'show' => 'fc.classes.show',
                ]);
        });

        // ROLE:: STAFF ATTENDANCE
        Route::middleware(['role:partner|manager|customer_service|finance_accounting'])->group(function () {
            Route::get('all-staff', [StaffCollectionController::class, 'index'])->name('fc.staff-collection.index');
            Route::get('staff', [StaffController::class, 'index'])->name('fc.staff.index');
            Route::get('staff/schedule/{schedule_id}', [StaffController::class, 'showByScheduelId'])->name('fc.staff.scheduleId');
            Route::post('staff/schedule/{schedule_id}', [StaffController::class, 'note'])->name('fc.staff.note');
            Route::get('staff/{member_branch_id}', [StaffController::class, 'show'])->name('fc.staff.show');
        });

        // ROLE:: PT SESSION
        Route::middleware(['role:partner|manager|customer_service|finance_accounting'])->group(function () {
            Route::prefix('personal-trainer')->group(function () {
                // CRUD
                Route::get('/', [PTController::class, 'index'])->name('fc.pt.index');
                Route::patch('/schedule/cancelled/{member_package_id}/{pt_attendance_id}', [PTController::class, 'cancelled'])->name('fc.pt.cancelled');
                Route::get('/schedule/{id}/{member_package_id}', [PTController::class, 'schedule'])->name('fc.pt.schedule');
                Route::get('/schedule-marked/{id}/{member_package_id}', [PTController::class, 'dateMarked'])->name('fc.pt.dateMarked');
                Route::post('/schedule/{id}/{member_package_id}', [PTController::class, 'store'])->name('fc.pt.schedule.store');
                Route::patch('/schedule/{id}/{member_package_id}/{pt_attendance_id}', [PTController::class, 'update'])->name('fc.pt.schedule.update');
                Route::get('/schedule/show/{member_package_id}/{pt_attendance_id}', [PTController::class, 'showSchedule'])->name('fc.pt.showSchedule');
                Route::get('/list/{id}/{member_package_id}/{date}', [PTController::class, 'bookedList'])->name('fc.pt.bookedList');
                Route::get('/{id}', [PTController::class, 'show'])->name('fc.pt.show');
            });
        });

        Route::prefix('me')->group(function () {
            Route::get('attendance', [DashboardController::class, 'clockIn'])->name('fc.dashboard.clockIn');
        });

        // ROLE:: STAFF ATTENDANCE
        Route::middleware(['role:partner|manager|finance_accounting'])->group(function () {
            Route::prefix('transactions')->group(function () {
                // CRUD
                Route::get('/', [TransactionDetailController::class, 'index'])->name('fc.transaction.index');
                Route::get('/balance', [TransactionDetailController::class, 'getBalance'])->name('fc.transaction.balance');
                Route::get('/summary', [TransactionDetailController::class, 'summary'])->name('fc.transaction.summary');
                Route::get('/banks', [TransactionDetailController::class, 'bank'])->name('fc.transaction.bank');
                Route::post('/', [TransactionDetailController::class, 'store'])->name('fc.transaction.store');
            });
        });

        Route::middleware(['role:partner|manager'])->group(function () {
            Route::prefix('settings')->group(function () {
                Route::prefix('staff')->group(function () {
                    Route::get('/', [StaffScheduleController::class, 'index'])->name('fc.staff-schedule.index');
                    Route::get('/search', [PositionController::class, 'search'])->name('fc.position.serach');
                    Route::get('/{member_branch_id}', [StaffScheduleController::class, 'show'])->name('fc.staff-schedule.show');
                    Route::delete('/{member_branch_id}', [StaffScheduleController::class, 'destroy'])->name('fc.staff-schedule.destroy');
                });

                Route::get('roles', [PositionController::class, 'role'])->name('fc.position.role');
                Route::get('permissions', [PositionController::class, 'permission'])->name('fc.position.permission');
                Route::get('positions/invite', [PositionController::class, 'index'])->name('fc.position.index');
                Route::post('positions/cancelled/{member_id}', [PositionController::class, 'cancelled'])->name('fc.position.cancelled');
                Route::post('positions/invite/{member_id}', [PositionController::class, 'invite'])->name('fc.position.invite');
                Route::post('positions/re-invite/{member_id}', [PositionController::class, 'reInvite'])->name('fc.position.reInvite');
                Route::get('positions/{member_id}', [PositionController::class, 'show'])->name('fc.position.show');
                Route::post('positions/{member_id}', [PositionController::class, 'store'])->name('fc.position.store');

                Route::prefix('schedules')->group(function () {
                    Route::get('/', [ScheduleController::class, 'index'])->name('fc.schedule.index');
                    Route::post('/', [ScheduleController::class, 'store'])->name('fc.schedule.store');
                    Route::get('/staffs', [ScheduleController::class, 'staffs'])->name('fc.schedule.staff');
                    Route::get('/available-months/{year}', [ScheduleController::class, 'availableMonth'])->name('fc.schedule.available');
                    Route::get('/marked/{month}/{year}', [ScheduleController::class, 'dateMarked'])->name('fc.schedule.marked');
                });

                Route::prefix('shifts')->group(function () {
                    Route::get('/', [ShiftController::class, 'index'])->name('fc.shifts.index');
                    Route::post('/', [ShiftController::class, 'store'])->name('fc.shifts.store');
                    Route::patch('/{id}', [ShiftController::class, 'update'])->name('fc.shifts.update');
                });

                // Schedule
                Route::get('/class-schedules', [SettingClassSchedule::class, 'index'])->name('fc.classes.schedule.index');
                Route::get('/class-schedules/marked/{month}/{year}', [SettingClassSchedule::class, 'dateMarked'])->name('fc.classes.schedule.marked');
                Route::get('/class-schedules/available/{date}', [SettingClassSchedule::class, 'checkAvailableClass'])->name('fc.classes.schedule.available');
                Route::get('/class-schedules/collections/{date}', [SettingClassSchedule::class, 'classScheduled'])->name('fc.classes.schedule.collections');
                Route::get('/class-schedules/coaches', [SettingClassSchedule::class, 'coaches'])->name('fc.classes.schedule.coaches');
                Route::get('/class-schedules/{date}/{schedule_id}', [SettingClassSchedule::class, 'show'])->name('fc.classes.schedule.show');
                Route::post('/class-schedules', [SettingClassSchedule::class, 'store'])->name('fc.classes.schedule.store');
                Route::patch('/class-schedules/{date}/{schedule_id}', [SettingClassSchedule::class, 'update'])->name('fc.classes.schedule.update');
                Route::post('/class-schedules/collections', [SettingClassSchedule::class, 'storeCollection'])->name('fc.classes.schedule.stroreCollections');


                // Operational Times
                Route::prefix('operational-times')->group(function () {
                    Route::get('/', [OperatioanlTimeController::class, 'index'])->name('fc.opt.index');
                    Route::post('/', [OperatioanlTimeController::class, 'store'])->name('fc.opt.store');
                    Route::patch('/published', [OperatioanlTimeController::class, 'update'])->name('fc.opt.update');
                    Route::get('/options', [OperatioanlTimeController::class, 'getTime'])->name('fc.opt.getTime');
                    Route::get('/available-months/{year}', [OperatioanlTimeController::class, 'availableMonth'])->name('fc.opt.available');
                    Route::get('/marked/{month}/{year}', [OperatioanlTimeController::class, 'dateMarked'])->name('fc.opt.marked');
                });

                Route::prefix('classes')->group(function () {

                    // Additional
                    Route::get('/levels', [SettingClassController::class, 'level'])->name('fc.classes.level');
                    Route::get('/categories', [SettingClassController::class, 'categories'])->name('fc.classes.categories');

                    // CRUD
                    Route::get('/', [SettingClassController::class, 'index'])->name('fc.classes.index');
                    Route::get('/{id}', [SettingClassController::class, 'show'])->name('fc.classes.show');
                    Route::post('/', [SettingClassController::class, 'store'])->name('fc.classes.store');
                    Route::post('/{id}', [SettingClassController::class, 'update'])->name('fc.classes.update');
                });

                Route::prefix('packages')->group(function () {

                    // CRUD
                    Route::get('/', [PackageController::class, 'index'])->name('fc.packages.index');
                    Route::get('/{id}', [PackageController::class, 'show'])->name('fc.packages.show');
                    Route::post('/', [PackageController::class, 'store'])->name('fc.packages.store');
                    Route::patch('/{id}', [PackageController::class, 'update'])->name('fc.packages.update');
                });

                Route::prefix('promos')->group(function () {

                    // CRUD
                    Route::get('/', [PromoController::class, 'index'])->name('fc.promos.index');
                    Route::get('/{id}', [PromoController::class, 'show'])->name('fc.promos.show');
                    Route::post('/', [PromoController::class, 'store'])->name('fc.promos.store');
                    Route::patch('/{id}', [PromoController::class, 'update'])->name('fc.promos.update');
                });

                Route::prefix('club')->group(function () {

                    // CRUD
                    Route::get('/services', [ClubInfoController::class, 'getCategories'])->name('fc.club.getCategories');
                    Route::get('/amenities', [ClubInfoController::class, 'getAmenity'])->name('fc.club.getAmenity');
                    Route::get('/safety-cleanliness', [ClubInfoController::class, 'getSafetyClean'])->name('fc.club.getSafetyClean');

                    Route::patch('/services', [ClubInfoController::class, 'categories'])->name('fc.club.categories');
                    Route::patch('/amenities', [ClubInfoController::class, 'amenity'])->name('fc.club.amenity');
                    Route::patch('/safety-cleanliness', [ClubInfoController::class, 'safetyClean'])->name('fc.club.safetyClean');
                    Route::patch('/description', [ClubInfoController::class, 'clubDescription'])->name('fc.club.description');
                    Route::patch('/soscial-network', [ClubInfoController::class, 'clubSocialMedia'])->name('fc.club.social-network');
                });

                Route::prefix('galleries')->group(function () {

                    // CRUD
                    Route::get('/', [GalleryController::class, 'index'])->name('fc.gallery.index');
                    Route::get('/{id}', [GalleryController::class, 'show'])->name('fc.gallery.show');
                    Route::post('/{id}', [GalleryController::class, 'store'])->name('fc.gallery.store');
                    Route::delete('/{id}/{image_id}', [GalleryController::class, 'destroy'])->name('fc.gallery.destroy');
                });

                Route::prefix('personal-trainer')->group(function () {

                    // CRUD
                    Route::get('/', [PersonalTrainerController::class, 'index'])->name('fc.personal-trainer.index');
                    // Route::get('/{id}', [PersonalTrainerController::class, 'show'])->name('fc.personal-trainer.show');
                    Route::post('/', [PersonalTrainerController::class, 'store'])->name('fc.personal-trainer.store');
                    Route::post('/{id}', [PersonalTrainerController::class, 'update'])->name('fc.personal-trainer.update');
                });

                Route::prefix('rules')->group(function () {

                    // CRUD
                    Route::get('/', [RuleController::class, 'index'])->name('fc.rule.index');
                    Route::patch('/reset', [RuleController::class, 'reset'])->name('fc.rule.reset');
                    Route::post('/', [RuleController::class, 'store'])->name('fc.rule.store');
                });
            });
        });
    });
});
