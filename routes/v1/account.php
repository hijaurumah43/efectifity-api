<?php

use App\Http\Controllers\User\Account\AccountController;

Route::middleware(['auth:api'])->group(function () {
    Route::get('access', [AccountController::class, 'access'])->name('account.access'); 
});