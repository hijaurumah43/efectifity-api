<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Internal\Admin\AdminController;
use App\Http\Controllers\Internal\Branch\DocumentController;
use App\Http\Controllers\Internal\Branch\PlatformController;
use App\Http\Controllers\Internal\Company\CompanyController;
use App\Http\Controllers\Internal\Branch\BranchFeeController;
use App\Http\Controllers\Internal\Company\CompanyTempController;
use App\Http\Controllers\Internal\Question\QuestionTypeController;
use App\Http\Controllers\Internal\Question\QuestionDetailController;
use App\Http\Controllers\Internal\Question\QuestionCategoryController;
use App\Http\Controllers\Internal\Settings\Club\ClubAmenityController;
use App\Http\Controllers\Internal\Settings\Club\ClubServiceController;
use App\Http\Controllers\Internal\Settings\Classes\ClassLevelController;
use App\Http\Controllers\Internal\Settings\Club\ClubInformationController;
use App\Http\Controllers\Internal\Settings\Classes\ClassCategoryController;
use App\Http\Controllers\Internal\Settings\Club\ClubSectionController;

###################
# Question
###################
Route::post('signin', [LoginController::class, 'login'])->name('internal.auth.login');
Route::middleware(['role:administrator'])->group(function () {
    Route::get('me', [AdminController::class, 'profile'])->name('internal.admin.me');
    Route::get('platforms/reviewer/status', [PlatformController::class, 'reviewerStatus'])->name('internal.platforms.reviewer-status');
    Route::post('platforms/review', [PlatformController::class, 'review'])->name('internal.platforms.review');
    Route::get('platforms/status/{status}', [PlatformController::class, 'status'])->name('internal.platforms.status');
    Route::apiResource('platforms', PlatformController::class)
        ->only([
            'index',
            'show',
        ])
        ->names([
            'index' => 'internal.platforms.index',
            'show' => 'internal.platforms.show',
        ]);
    Route::get('documents/reviewer/status', [DocumentController::class, 'reviewerStatus'])->name('internal.documents.reviewer-status');
    Route::post('documents/review', [DocumentController::class, 'review'])->name('internal.documents.review');
    Route::get('documents/status/{status}', [DocumentController::class, 'status'])->name('internal.documents.status');
    Route::apiResource('documents', DocumentController::class)
        ->only([
            'index',
            'show',
        ])
        ->names([
            'index' => 'internal.documents.index',
            'show' => 'internal.documents.show',
        ]);
    Route::apiResource('company-temp', CompanyTempController::class)
        ->only([
            'index',
            'show',
        ])
        ->names([
            'index' => 'internal.company-temp.index',
            'show' => 'internal.company-temp.show',
        ]);
    Route::apiResource('companies', CompanyController::class)
        ->only([
            'index',
            'show',
        ])
        ->names([
            'index' => 'internal.company.index',
            'show' => 'internal.company.show',
        ]);
    Route::apiResource('question-types', QuestionTypeController::class)
        ->only([
            'index',
            'store',
        ])
        ->names([
            'index' => 'internal.question-type.index',
            'store' => 'internal.question-type.store',
        ]);

    Route::apiResource('question-categories', QuestionCategoryController::class)
        ->only([
            'index',
            'store',
        ])
        ->names([
            'index' => 'internal.question-categories.index',
            'store' => 'internal.question-categories.store',
        ]);

    Route::apiResource('question-detail', QuestionDetailController::class)
        ->only([
            'store',
        ])
        ->names([
            'store' => 'internal.questions.store',
        ]);

    Route::apiResource('platform-fee', BranchFeeController::class)
        ->only([
            'index',
            'show',
            'store',
            'update',
        ])
        ->names([
            'index' => 'internal.branch-fee.index',
            'show' => 'internal.branch-fee.show',
            'store' => 'internal.branch-fee.store',
            'update' => 'internal.branch-fee.update',
        ]);

    Route::prefix('class-settings')->group(function () {
        Route::apiResource('levels', ClassLevelController::class)
            ->only([
                'index',
                'show',
                'store',
                'update',
            ])
            ->names([
                'index' => 'internal.settings.class.level.index',
                'show' => 'internal.settings.class.level.show',
                'store' => 'internal.settings.class.level.store',
                'update' => 'internal.settings.class.level.update',
            ]);

        Route::apiResource('categories', ClassCategoryController::class)
            ->only([
                'index',
                'show',
                'store',
            ])
            ->names([
                'index' => 'internal.settings.class.category.index',
                'show' => 'internal.settings.class.category.show',
                'store' => 'internal.settings.class.category.store',
            ]);
        Route::post('categories/{category_id}', [ClassCategoryController::class, 'update'])
            ->name('internal.settings.class.category.update');
    });

    Route::prefix('club-settings')->group(function () {
        Route::apiResource('services', ClubServiceController::class)
            ->only([
                'index',
                'show',
                'store',
            ])
            ->names([
                'index' => 'internal.settings.club.services.index',
                'show' => 'internal.settings.club.services.show',
                'store' => 'internal.settings.club.services.store',
            ]);
        Route::post('services/{service_id}', [ClubServiceController::class, 'update'])
            ->name('internal.settings.club.services.update');

        Route::apiResource('amenities', ClubAmenityController::class)
            ->only([
                'index',
                'show',
                'store',
            ])
            ->names([
                'index' => 'internal.settings.club.amenities.index',
                'show' => 'internal.settings.club.amenities.show',
                'store' => 'internal.settings.club.amenities.store',
            ]);
        Route::post('amenities/{amenity_id}', [ClubAmenityController::class, 'update'])
            ->name('internal.settings.club.amenities.update');

        Route::apiResource('informations', ClubInformationController::class)
            ->only([
                'index',
                'show',
                'store',
            ])
            ->names([
                'index' => 'internal.settings.club.informations.index',
                'show' => 'internal.settings.club.informations.show',
                'store' => 'internal.settings.club.informations.store',
            ]);
        Route::post('informations/{information_id}', [ClubInformationController::class, 'update'])
            ->name('internal.settings.club.informations.update');

        Route::apiResource('sections', ClubSectionController::class)
            ->only([
                'index',
                'show',
                'store',
                'update',
            ])
            ->names([
                'index' => 'internal.settings.club.sections.index',
                'show' => 'internal.settings.club.sections.show',
                'store' => 'internal.settings.club.sections.store',
                'update' => 'internal.settings.club.sections.update',
            ]);
    });
});
