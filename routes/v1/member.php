<?php

use App\Models\Client\Member\Log\MemberLog;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\Client\Member\MemberController;
use App\Http\Controllers\Client\Member\Log\MemberLogController;
use App\Http\Controllers\Client\Member\Programs\ProgramController;
use App\Http\Controllers\Client\Member\PT\PersonalTrainerController;
use App\Http\Controllers\Client\Member\PT\PersonalTrainerReviewController;
use App\Http\Controllers\Client\Member\Questions\QuestionAnswerController;
use App\Http\Controllers\Client\Member\Settings\DeliveryAddressController;
use App\Http\Controllers\Client\Member\Transactions\TransactionController;
use App\Http\Controllers\Client\Member\Settings\EmergencyContactController;
use App\Http\Controllers\Client\Member\Notifications\NotificationController;
use App\Http\Controllers\Client\Member\Attendances\ClassAttendanceController;
use App\Http\Controllers\Client\Member\Attendances\PersonalAttendanceController;
use App\Http\Controllers\Client\Member\Notifications\NotificationPTController;
use App\Http\Controllers\Client\Member\Notifications\NotificationLimitController;

Route::middleware(['auth:api'])->group(function () {

    Route::get('access', [MemberController::class, 'access'])->name('api.access');
    Route::get('me', [MemberController::class, 'profile'])->name('api.me');
    Route::patch('me', [MemberController::class, 'updateMe'])->name('api.me.update');
    Route::patch('password', [MemberController::class, 'updatePassword'])->name('api.me.password');
    Route::post('avatar', [MemberController::class, 'avatar'])->name('api.avatar');
    Route::post('avatar2', [MemberController::class, 'avatar2'])->name('api.avatar2');

    ###################
    # Notifications
    ###################
    Route::get('notifications', [NotificationController::class, 'index'])->name('api.notifications.index'); //DT
    Route::get('notifications/read/{notif_id}', [NotificationController::class, 'readAt'])->name('api.notifications.readtAt');
    Route::get('notifications/open/{notif_id}', [NotificationController::class, 'show'])->name('api.notifications.show'); //DT
    Route::patch('notifications/invitation/{notif_id}', [NotificationController::class, 'accepted'])->name('api.notifications.accepted');
    Route::delete('notifications/invitation/{notif_id}', [NotificationController::class, 'rejected'])->name('api.notifications.rejected');
    Route::get('notifications/collections', [NotificationController::class, 'collection'])->name('api.notifications.collection');
    ###################
    # Notifications - PT
    ###################
    Route::get('notifications/pt-reservation/{notif_id}', [NotificationPTController::class, 'show'])->name('api.notifications-pt.show');
    Route::patch('notifications/pt-reservation/{notif_id}', [NotificationPTController::class, 'accepted'])->name('api.notifications-pt.accepted');
    Route::delete('notifications/pt-reservation/{notif_id}', [NotificationPTController::class, 'rejected'])->name('api.notifications-pt.rejected');


    Route::get('notifications/{type}', [NotificationLimitController::class, 'collection'])->name('api.notifications-limit.collection');



    ###################
    # Delivery Address
    ###################
    Route::apiResource('delivery-address', DeliveryAddressController::class)
        ->only([
            'index',
            'show',
            'store',
            'update',
            'destroy',
        ])
        ->names([
            'index' => 'api.member.delivery-address.index',
            'show' => 'api.member.delivery-address.show',
            'store' => 'api.member.delivery-address.store',
            'update' => 'api.member.delivery-address.update',
            'destroy' => 'api.member.delivery-address.destroy',
        ]);

    ###################
    # Emergency Contact
    ###################
    Route::apiResource('emergency-contact', EmergencyContactController::class)
        ->only([
            'index',
            'show',
            'store',
            'update',
            'destroy',
        ])
        ->names([
            'index' => 'api.member.emergency-contact.index',
            'show' => 'api.member.emergency-contact.show',
            'store' => 'api.member.emergency-contact.store',
            'update' => 'api.member.emergency-contact.update',
            'destroy' => 'api.member.emergency-contact.destroy',
        ]);

    ###################
    # Pin
    ###################
    Route::prefix('pin')->group(function () {
        Route::post('/', [UserController::class, 'setPin'])->name('api.pin.set');
        Route::patch('/', [UserController::class, 'changePin'])->name('api.pin.update');
        Route::get('email', [UserController::class, 'sendEmailResetPin'])->name('api.reset.pin.email-link');
        Route::post('verify/{token}', [UserController::class, 'verifyPin'])->middleware('throttle:5,1')->name('api.pin.verify');
        Route::get('check/{pin}', [UserController::class, 'checkPin'])->name('api.pin.check');
    });

    Route::get('phone/otp', [UserController::class, 'phoneNotification'])->name('api.phone.notif-otp');
    Route::patch('username', [UserController::class, 'updateUsername'])->name('api.username.update');
    Route::post('phone/verify/{token}', [UserController::class, 'verifyPhone'])
        ->middleware('throttle:5,1')
        ->name('api.phone.verify');

    ###################
    # Transactions
    ###################
    // DT Inex
    Route::apiResource('transactions', TransactionController::class)
        ->only([
            'index',
            'store',
            'show',
        ])
        ->names([
            'index' => 'api.member.transactions.index',
            'store' => 'api.member.transactions.store',
            'show' => 'api.member.transactions.show',
        ]);

    ###################
    # Program
    ###################
    // DT Inex
    Route::apiResource('programs', ProgramController::class)
        ->only([
            'index',
            'show',
        ])
        ->names([
            'index' => 'api.member.program.index',
            'show' => 'api.member.program.show',
        ]);

    ###################
    # Upcoming
    ###################
    Route::apiResource('classes', ClassAttendanceController::class)
        ->only([
            'index',
            'show',
            'store',
        ])
        ->names([
            'index' => 'api.member.classes.index',
            'show' => 'api.member.classes.show',
            'store' => 'api.member.classes.store',
        ]);
    Route::get('classes/{class_id}/attend', [ClassAttendanceController::class, 'attendanceClass'])->name('api.member.classes.attend');
    Route::get('classes/{class_id}/cancelled', [ClassAttendanceController::class, 'cancelledClass'])->name('api.member.classes.cancelled');
    Route::get('upcoming/personal-trainer/{pt_id}', [PersonalAttendanceController::class, 'show'])->name('api.member.pt.show'); // DT
    Route::apiResource('questions', QuestionAnswerController::class)
        ->only([
            'index',
            'store',
        ])
        ->names([
            'index' => 'api.member.questions.index',
            'store' => 'api.member.questions.store',
        ]);
    ###################
    # PT (Personal Trainer)
    ###################
    Route::get('personal-trainers/fetch/{package_id}', [PersonalTrainerController::class, 'index'])->name('api.member.pt.index');
    Route::get('personal-trainers/reviews/{pt_id}', [PersonalTrainerReviewController::class, 'index'])->name('api.member.pt.reviews');
    Route::get('personal-trainers/{pt_id}', [PersonalTrainerController::class, 'show'])->name('api.member.pt.show');
    ###################
    # Log
    ###################
    Route::get('branches/{branch_id}/histories', [MemberLogController::class, 'branchLog'])->name('api.member.log.branch');
});
