<?php

/* use Berkayk\OneSignal\OneSignalFacade;
use OneSignal; */

use Sujip\Ipstack\Ipstack;
use Illuminate\Http\Request;

Route::get('phpinfo', function () {
    phpinfo();
});

Route::prefix('debug')->group(function () {
    Route::get('sentry', function () {
        throw new Exception('My first Sentry error! why?');
    });
});

Route::get('peyment/receipt', function () {
    return view('emails.payment.receipt');
});

Route::get('ip', function (Request $request) {
    $ip = $request->ip();
    $ip = geoip()->getLocation($ip)->timezone;
    return $ip;
});
