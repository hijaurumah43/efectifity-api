<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchClassCoachesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_class_coaches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_class_id');
            $table->unsignedInteger('coach_id');
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('coach_id')->references('id')->on('coaches');
            $table->foreign('branch_class_id')->references('id')->on('branch_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_class_coaches');
    }
}
