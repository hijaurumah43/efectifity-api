<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type');
            $table->unsignedInteger('branch_package_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('member_type');
            $table->string('title', 50);
            $table->string('duration', 50);
            $table->string('periode', 50);
            $table->unsignedInteger('gym_access')->default(0);
            $table->unsignedInteger('session')->default(0);
            $table->unsignedInteger('pt_session')->default(0);
            $table->unsignedInteger('price')->default(0);
            $table->string('start_time');
            $table->string('end_time');
            $table->unsignedInteger('status')->default(0);
            $table->uuid('member_id')->nullable();
            $table->timestamps();

            $table->foreign('branch_package_id')->references('id')->on('branch_packages');
            $table->foreign('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
    }
}
