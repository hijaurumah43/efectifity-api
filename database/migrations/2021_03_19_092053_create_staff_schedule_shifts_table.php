<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffScheduleShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_schedule_shifts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('shift_id');
            $table->unsignedInteger('staff_schedule_id');
            $table->string('title');
            $table->string('start_time');
            $table->string('end_time');
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('staff_schedule_id')->references('id')->on('staff_schedules');
            $table->foreign('shift_id')->references('id')->on('shifts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_schedule_shifts');
    }
}
