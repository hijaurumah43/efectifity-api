<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchScheduleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_schedule_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_schedule_id');
            $table->unsignedInteger('branch_class_id');
            $table->string('start_time');
            $table->string('end_time');
            $table->unsignedInteger('max_quota')->nullable();
            $table->boolean('is_quota')->default(0);
            $table->boolean('is_cancelled')->default(0);
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('branch_schedule_id')->references('id')->on('branch_schedules');
            $table->foreign('branch_class_id')->references('id')->on('branch_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_schedule_details');
    }
}
