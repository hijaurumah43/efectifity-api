<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTrainerReviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_trainer_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('personal_trainer_id');
            $table->uuid('member_id');
            $table->unsignedInteger('star')->nullable();
            $table->text('review')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('personal_trainer_id')->references('id')->on('personal_trainer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_trainer_review');
    }
}
