<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_attendances', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('member_id');
            $table->uuid('member_package_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('branch_schedule_id');
            $table->unsignedInteger('branch_schedule_detail_id');
            $table->unsignedInteger('class_attendance_status_id')->default(1); // 1:book, 2:attend, 3:cancel, 4:absen
            $table->string('scan_time')->nullable(); // customer datang pada saat class
            $table->string('cancel_time')->nullable(); // customer cancel class
            $table->text('note')->nullable(); // diisi kita class cancel by partner
            $table->boolean('is_active')->default(1); // 1: class active, 0: class past
            $table->uuid('user_id')->nullable(); // diisi kita class cancel by partner
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('branch_schedule_id')->references('id')->on('branch_schedules');
            $table->foreign('branch_schedule_detail_id')->references('id')->on('branch_schedule_details');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('class_attendance_status_id')->references('id')->on('class_attendance_statuses');
            $table->foreign('member_package_id')->references('id')->on('member_packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_attendances');
    }
}
