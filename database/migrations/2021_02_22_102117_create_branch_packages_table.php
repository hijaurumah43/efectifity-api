<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->string('title');
            $table->string('duration', 50);
            $table->string('periode', 50);
            $table->unsignedInteger('pt_session');
            $table->unsignedInteger('session');
            $table->text('description')->nullable();
            $table->text('about')->nullable();
            $table->text('benefit')->nullable();
            $table->text('policy')->nullable();
            $table->unsignedInteger('price');
            $table->unsignedInteger('gym_access')->default(0);
            $table->boolean('is_published');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_packages');
    }
}
