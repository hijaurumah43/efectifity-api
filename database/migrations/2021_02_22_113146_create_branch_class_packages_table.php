<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchClassPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_class_packages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_class_id');
            $table->unsignedInteger('branch_pacakge_id');
            $table->timestamps();

            $table->foreign('branch_class_id')->references('id')->on('branch_classes');
            $table->foreign('branch_pacakge_id')->references('id')->on('branch_packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_class_package');
    }
}
