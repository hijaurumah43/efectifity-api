<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('order_number');
            $table->uuid('member_id');
            $table->unsignedInteger('service_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('branch_package_id');
            $table->unsignedInteger('branch_agreement_id')->nullable();
            $table->unsignedInteger('transaction_status_id')->default(1);
            $table->unsignedInteger('payment_type_id');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->unsignedInteger('branch_schedule_detail_id')->nullable();
            $table->unsignedBigInteger('total_payment')->default(0);
            $table->string('channels')->nullable();
            $table->mediumText('action_to')->nullable();
            $table->mediumText('old_values')->nullable(); // store value order
            $table->mediumText('callback_values')->nullable(); // store data callback
            $table->string('callback_at')->nullable(); // return callback at from payment gateway
            $table->boolean('is_callback')->default(0); // return callback from payment gateway
            $table->boolean('is_published')->default(1); 
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('branch_package_id')->references('id')->on('branch_packages');
            $table->foreign('branch_agreement_id')->references('id')->on('branch_agreements');
            $table->foreign('transaction_status_id')->references('id')->on('transaction_statuses');
            $table->foreign('payment_type_id')->references('id')->on('payment_types');
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('branch_schedule_detail_id')->references('id')->on('branch_schedule_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
