<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_packages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('member_id');
            $table->uuid('transaction_id');
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('branch_agreement_id')->nullable();
            $table->unsignedInteger('branch_package_id');
            $table->unsignedInteger('session');
            $table->unsignedInteger('session_remaining');
            $table->string('expiry_date');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('branch_agreement_id')->references('id')->on('branch_agreements');
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('branch_package_id')->references('id')->on('branch_packages');
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_packages');
    }
}
