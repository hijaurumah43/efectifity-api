<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_branches', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('member_id');
            $table->unsignedInteger('branch_id');
            $table->string('checkin')->nullable();
            $table->string('checkout')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->unsignedInteger('type')->nullable();
            $table->boolean('is_published')->default(0);
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_branches');
    }
}
