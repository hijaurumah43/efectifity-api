<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('service_id');
            $table->string('title', 100);
            $table->string('slug', 100);
            $table->string('phone_number', 20);
            $table->text('address');
            $table->string('logo')->nullable();
            $table->string('promo')->nullable();
            $table->string('thumbnail')->nullable();
            $table->string('front_side')->nullable();
            $table->string('inside')->nullable();
            $table->string('website')->nullable();
            $table->string('whatsapp')->nullable();
            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('tiktok')->nullable();
            $table->double('latitude');
            $table->double('longitude');
            $table->text('description')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('region')->nullable();
            $table->boolean('is_published')->default(0);
            $table->boolean('is_active')->default(0);
            $table->timestamps();
            
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branchs');
    }
}
