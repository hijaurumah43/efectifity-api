<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('member_id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->uuid('class_attendance_id')->nullable();
            $table->uuid('member_package_id')->nullable();
            $table->unsignedInteger('member_attendance_id')->nullable();
            $table->unsignedInteger('status')->nullable();
            $table->string('message')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('class_attendance_id')->references('id')->on('class_attendances');
            $table->foreign('member_package_id')->references('id')->on('member_packages');
            $table->foreign('member_attendance_id')->references('id')->on('member_attendances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_logs');
    }
}
