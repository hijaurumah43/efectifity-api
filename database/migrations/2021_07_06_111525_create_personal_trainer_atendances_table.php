<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTrainerAtendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_trainer_attendances', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedInteger('personal_trainer_id');
            $table->uuid('member_id');
            $table->uuid('member_package_id');
            $table->unsignedInteger('branch_id');
            $table->string('date');
            $table->string('start_time');
            $table->string('end_time');
            $table->unsignedInteger('personal_trainer_attendance_status_id');
            $table->string('scan_time')->nullable();
            $table->string('cancel_time')->nullable();
            $table->string('note')->nullable();
            $table->boolean('is_active')->default(1);
            $table->uuid('user_id')->nullable();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('member_package_id')->references('id')->on('member_packages');
            $table->foreign('branch_id')->references('id')->on('branches');
            // $table->foreign('personal_trainer_attendance_status_id')->references('id')->on('personal_trainer_atendance_statuses');
            // $table->foreign('personal_trainer_id')->references('id')->on('personal_trainers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_trainer_atendances');
    }
}
