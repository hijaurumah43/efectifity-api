<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('member_id');
            $table->string('label')->nullable();
            $table->string('receiver')->nullable();
            $table->string('phone_number')->nullable();
            $table->text('address')->nullable();
            $table->string('zip_code')->nullable();
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
            $table->boolean('is_published')->default(1);
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_addresses');
    }
}
