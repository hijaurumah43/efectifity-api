<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffScheduleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_schedule_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('staff_schedule_id');
            $table->unsignedInteger('staff_schedule_shift_id');
            $table->string('title')->nullable();
            $table->string('start_time');
            $table->string('end_time');
            $table->string('description')->nullable();
            $table->text('note')->nullable();
            $table->uuid('created_by')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('staff_schedule_id')->references('id')->on('staff_schedules');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('staff_schedule_shift_id')->references('id')->on('staff_schedule_shifts');
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_schedule_details');
    }
}
