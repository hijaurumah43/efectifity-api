<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('transaction_id')->nullable();
            $table->string('order_number');
            $table->uuid('member_id')->nullable();
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('transaction_type_id')->default(1);
            $table->unsignedInteger('status')->default(0);
            $table->unsignedInteger('amount')->default(0);
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('transaction_id')->references('id')->on('transactions');
            $table->foreign('transaction_type_id')->references('id')->on('transaction_types');
            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
