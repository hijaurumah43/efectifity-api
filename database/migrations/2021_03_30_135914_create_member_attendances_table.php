<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('member_id');
            $table->unsignedInteger('branch_id');
            $table->string('time');
            $table->uuid('scanned_by')->nullable();
            $table->boolean('status'); //1 check in - 2 check out
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('scanned_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_attendances');
    }
}
