<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('question_category_id');
            $table->unsignedInteger('question_type_id');
            $table->unsignedInteger('question_group_id');
            $table->string('title');
            $table->boolean('with_note')->default(0);
            $table->string('question_note')->nullable();
            $table->boolean('is_published')->default(1);
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('question_category_id')->references('id')->on('question_categories');
            $table->foreign('question_type_id')->references('id')->on('question_types');
            $table->foreign('question_group_id')->references('id')->on('question_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
