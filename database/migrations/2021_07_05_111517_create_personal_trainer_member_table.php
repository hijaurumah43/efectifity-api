<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTrainerMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_trainer_member', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('personal_trainer_id');
            $table->uuid('member_id');
            $table->uuid('member_package_id');
            $table->boolean('status')->default(1);
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('members');
            $table->foreign('member_package_id')->references('id')->on('member_packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_trainer_member');
    }
}
