<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('image')->nullable();
            $table->unsignedInteger('type');
            $table->unsignedInteger('branch_id')->nullable();
            $table->unsignedInteger('qty')->nullable();
            $table->unsignedInteger('discount')->nullable();
            $table->string('expired')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
