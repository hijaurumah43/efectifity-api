<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->string('title');
            $table->string('image')->nullable();
            $table->unsignedInteger('branch_class_category_id')->nullable();
            $table->unsignedInteger('delay_time')->nullable();
            $table->unsignedInteger('max_book_time')->nullable();
            $table->unsignedInteger('max_cancel_time')->nullable();
            $table->unsignedInteger('branch_class_level_id')->nullable();
            $table->boolean('is_online')->default(0);
            $table->boolean('is_published');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('branch_class_level_id')->references('id')->on('branch_class_levels');
            $table->foreign('branch_class_category_id')->references('id')->on('branch_class_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_class');
    }
}
