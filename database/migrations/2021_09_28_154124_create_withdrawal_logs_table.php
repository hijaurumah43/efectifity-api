<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('branch_id');
            $table->string('order_number');
            $table->string('account_holder_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('bank_code')->nullable();
            $table->string('amount')->nullable();
            $table->mediumText('old_values')->nullable(); // store value order
            $table->mediumText('callback_values')->nullable(); // store data callback
            $table->string('callback_at')->nullable(); // return callback at from payment gateway
            $table->boolean('is_callback')->default(0); // return callback from payment gateway
            $table->boolean('is_published')->default(1);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdrawal_logs');
    }
}
