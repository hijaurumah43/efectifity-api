<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Common\Company\Company;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        $user = User::where('email', 'partner@app.com')->first();
        $c = new Company();
        
        $c->title        = 'Partner Company';
        $c->user_id      = $user->id;
        $c->phone        = $faker->phoneNumber;
        $c->address      = $faker->address;
        $c->city_id      = 1;
        $c->is_published = 1;
        $c->is_active    = 1;
        $c->save();
    }
}
