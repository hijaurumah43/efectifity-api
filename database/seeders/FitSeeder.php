<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\Common\Coach\Coach;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;
use App\Models\Common\Branch\BranchClass;
use App\Models\Common\Branch\BranchGallery;
use App\Models\Common\Branch\BranchPackage;
use App\Models\Common\Branch\BranchCategory;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Company\CompanyService;
use App\Models\Common\Branch\BranchClassCoach;
use App\Models\Common\Branch\BranchClassLevel;
use App\Models\Common\Service\ServiceCategory;
use App\Models\Common\Branch\BranchClassPackage;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Common\Branch\BranchOperationalTime;

class FitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for($i = 1; $i <=  date('t'); $i++)
        {
            $dates[] = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        $levels = [
            [
                'title' => 'Beginner',
                'description' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
            ],
            [
                'title' => 'Middle',
                'description' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
            ],
            [
                'title' => 'Master',
                'description' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
            ],
        ];

        foreach ($levels as $level) {
            $bcl = new BranchClassLevel();
            $bcl->title = $level['title'];
            $bcl->description = $level['description'];
            $bcl->is_active = 1;
            $bcl->save();
        }

        $companies = [
            'Empire Fit Club',
            'Bikram Yoga',
        ];

        foreach ($companies as $company) {

            $user = User::factory()->create();
                        
            $c = new Company();
            $c->title        = $company;
            $c->user_id      = $user->id;
            $c->phone        = $faker->phoneNumber;
            $c->address      = $faker->address;
            $c->city_id      = 1;
            $c->is_published = 1;
            $c->is_active    = 1;
            $c->save();

            $cs = new CompanyService();
            $cs->company_id = $c->id;
            $cs->service_id = 1;
            $cs->save();            
        }

        $serviceCategories = [
            'Gym',
            'Classs',
            'Personal Trainer',
        ];

        foreach($serviceCategories as $serviceCategory) {
            $sc = new ServiceCategory();
            $sc->title      = $serviceCategory;
            $sc->service_id = 1;
            $sc->save();
        }

        $packages = [
            [
                'title' => 'Single Pass',
                'duration' => '1',
                'periode' => 'Day',
                'session' => 1,
                'description' => 'access to only 1 class available',
                'about' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'benefit' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'policy' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'price' => 150000,
            ],
            [
                'title' => '5 Pass',
                'duration' => '1',
                'periode' => 'Month',
                'session' => 5,
                'description' => 'access to only 5 class available',
                'about' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'benefit' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'policy' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'price' => 875000,
            ],
            [
                'title' => '10 Pass',
                'duration' => '1',
                'periode' => 'Month',
                'session' => 10,
                'description' => 'access to only 10 class available',
                'about' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'benefit' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'policy' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'price' => 1690000,
            ],
            [
                'title' => '1 Month Unlimited',
                'duration' => '1',
                'periode' => 'Month',
                'session' => 0,
                'description' => 'access to unlimited class available',
                'about' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'benefit' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'policy' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.",
                'price' => 1925000,
            ],
        ];

        $classes = [
            [
                'title' => 'Bikram Yoga 90',
                'delay_time' => 15, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '08:00',
                'end_time' => '09:30',
            ],
            [
                'title' => 'Bikram Express',
                'delay_time' => 15, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '11:00',
                'end_time' => '12:00',
            ],
            [
                'title' => 'Online Zoom - YIN',
                'delay_time' => 0, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '17:00',
                'end_time' => '20:00',
            ],
        ];

        for ($i=0; $i < 35; $i++) { 
            switch ($i) {
                case '0':
                    // Jayadata Indonesia
                    $title = 'Jayadata Indonesia';
                    $lat = '-6.1595315';
                    $lng = '106.8904416';
                    break;
                
                case '1':
                    // Monas
                    $title = 'Monumen Nasional';
                    $lat = '-6.1753871';
                    $lng = '106.8249641';
                    break;

                case '2':
                    // King Cross
                    $title = 'King Cross';
                    $lat = '-6.1589226';
                    $lng = '106.8918302';
                    break;

                case '3':
                    // Rumah Puji
                    $title = 'Rumah Puji';
                    $lat = '-6.1395857';
                    $lng = '106.9051773';
                    break;

                case '4':
                    // Rumah Sakit Cibubur
                    $title = 'Rumah Sakit Cibubur';
                    $lat = '-6.36987';
                    $lng = '106.8905098';
                    break;
                
                default:

                    if($i % 2 == 0) {
                        $title = $companies[0].' - '.$faker->city;
                    } else {
                        $title = $companies[1].' - '.$faker->city;
                    }

                    $lat = $faker->latitude;
                    $lng = $faker->longitude;
                    break;
            }

            $b = new Branch();
            $b->company_id   = $faker->numberBetween(1,2);
            $b->service_id   = $faker->numberBetween(1,2);
            $b->title        = $title;
            $b->slug         = Str::slug($title);
            $b->phone_number = $faker->phoneNumber;
            $b->address      = $faker->address;
            $b->region       = $faker->city;
            $b->thumbnail    = 'https://aje.bz/containers/efectifity/branch/SS72-1.jpeg';
            $b->whatsapp     = $faker->phoneNumber;
            $b->instagram    = $faker->userName;
            $b->facebook     = $faker->userName;
            $b->tiktok       = $faker->userName;
            $b->youtube      = $faker->userName;
            $b->latitude     = $lat;
            $b->longitude    = $lng;
            $b->is_published = 1;
            $b->is_active    = 1;
            $b->promo        = 'Offer up to 30%';
            $b->description  = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tempor magna nec massa dignissim, sit amet vulputate ipsum interdum. Nam finibus lectus diam, non tempor nulla bibendum sed. Nulla semper et ex in gravida. Curabitur non mi facilisis, sagittis sem a, ultrices ipsum. Pellentesque molestie, leo ac eleifend dapibus, metus augue molestie mi, in vulputate nisl urna non sem. Nulla tincidunt ullamcorper dui, vel porta enim varius tincidunt. Vestibulum eu rutrum elit. Ut at nisi consequat, commodo sapien condimentum, tincidunt neque.';
            $b->save();

            $bcat = new BranchCategory();
            $bcat->branch_id            = $b->id;
            $bcat->service_category_id  = 1;
            $bcat->save();

            $c = new Coach();
            $c->name      = $faker->firstName;
            $c->branch_id = $b->id;
            $c->save();

            if($i % 2 == 0) {
                $bcat = new BranchCategory();
                $bcat->branch_id            = $b->id;
                $bcat->service_category_id  = 2;
                $bcat->save();

                $c = new Coach();
                $c->name      = $faker->firstName;
                $c->branch_id = $b->id;
                $c->save();
            }

            for ($x=0; $x < 5; $x++) { 
                $bg = new BranchGallery();
                $bg->branch_id  = $b->id;
                $bg->name       = 'https://picsum.photos/id/237/200/300';
                $bg->save();
            }

            foreach ($dates as $date) {

                $bot = new BranchOperationalTime();
                $bot->branch_id     = $b->id;
                $bot->title         = "06.00 to 21.00";
                $bot->date          = $date;
                $bot->save();
            }

            foreach ($dates as $date) {
                $bs = new BranchSchedule();
                $bs->branch_id      = $b->id;
                $bs->date           = $date;
                $bs->is_published   = 1;
                $bs->save();
            }

            foreach ($packages as $package) {
                $bp = new BranchPackage();
                $bp->branch_id      = $b->id;
                $bp->title          = $package['title'];
                $bp->duration       = $package['duration'];
                $bp->periode        = $package['periode'];
                $bp->session        = $package['session'];
                $bp->description    = $package['description'];
                $bp->about          = $package['about'];
                $bp->benefit        = $package['benefit'];
                $bp->policy         = $package['policy'];
                $bp->price          = $package['price'];
                $bp->is_published   = 1;
                $bp->save();
            }

            foreach ($classes as $class) {
                $bc = new BranchClass();
                $bc->branch_id                  = $b->id;
                $bc->title                      = $class['title'];
                $bc->image                      = 'https://aje.bz/containers/efectifity/branch/SS72-1.jpeg';
                $bc->branch_class_category_id   = rand(1,7);
                $bc->delay_time                 = $class['delay_time'];
                $bc->max_book_time              = $class['max_book_time'];
                $bc->max_cancel_time            = $class['max_cancel_time'];
                $bc->branch_class_level_id      = rand(1,3);
                $bc->is_published               = 1;
                $bc->is_published               = rand(1,2);
                $bc->save();

                $coaches = Coach::where('branch_id', $b->id)->get();
                foreach($coaches as $coach) {
                    $bcc = new BranchClassCoach();
                    $bcc->branch_class_id = $bc->id;
                    $bcc->coach_id        = $coach->id;
                    $bcc->save();
                }

                $branchPackages = BranchPackage::where('branch_id', $b->id)->get();

                foreach($branchPackages as $bp) {
                    $bcp = new BranchClassPackage();
                    $bcp->branch_class_id   = $bc->id;
                    $bcp->branch_pacakge_id = $bp->id;
                    $bcp->save();
                }

                $branchSchedules = BranchSchedule::where('branch_id', $b->id)->get();

                foreach($branchSchedules as $bs) {
                    $bsd = new BranchScheduleDetail();
                    $bsd->branch_schedule_id = $bs->id;
                    $bsd->branch_class_id    = $bc->id;
                    $bsd->start_time         = $class['start_time'];
                    $bsd->end_time           = $class['end_time'];
                    $bsd->max_quota          = 50;
                    $bsd->is_quota           = 1;
                    $bsd->is_published       = 1;
                    $bsd->save();
                }
            }
        }
    }
}
