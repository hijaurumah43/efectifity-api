<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Internal\Attendance\ClassAttendanceStatus;

class ClassAttendanceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            'booking',
            'attend',
            'cancel',
            'not show',
        ];

        foreach ($collections as $collection) {
            $att = new ClassAttendanceStatus();
            $att->title = $collection;
            $att->save();
        }
    }
}
