<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Common\Coach\Coach;
use App\Models\Common\Branch\BranchClass;
use App\Models\Common\Branch\BranchPackage;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Branch\BranchClassCoach;
use App\Models\Common\Branch\BranchClassPackage;
use App\Models\Common\Branch\BranchScheduleDetail;

class MonthlySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <=  date('t'); $i++)
        {
            $dates[] = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        $classes = [
            [
                'title' => 'Bikram Yoga 90',
                'delay_time' => 15, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '08:00',
                'end_time' => '09:30',
            ],
            [
                'title' => 'Bikram Express',
                'delay_time' => 15, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '11:00',
                'end_time' => '12:00',
            ],
            [
                'title' => 'Online Zoom - YIN',
                'delay_time' => 0, //minute
                'max_book_time' => 30, //minute
                'max_cancel_time' => 30, //minute
                'start_time' => '17:00',
                'end_time' => '20:00',
            ],
        ];

        for ($i=1; $i < 35; $i++) { 

            foreach ($dates as $date) {
                $bs = new BranchSchedule();
                $bs->branch_id      = $i;
                $bs->date           = $date;
                $bs->is_published   = 1;
                $bs->save();
            }

            foreach ($classes as $class) {
                $bc = new BranchClass();
                $bc->branch_id                  = $i;
                $bc->title                      = $class['title'];
                $bc->image                      = 'https://aje.bz/containers/efectifity/branch/SS72-1.jpeg';
                $bc->branch_class_category_id   = rand(1,7);
                $bc->delay_time                 = $class['delay_time'];
                $bc->max_book_time              = $class['max_book_time'];
                $bc->max_cancel_time            = $class['max_cancel_time'];
                $bc->branch_class_level_id      = rand(1,3);
                $bc->is_published               = 1;
                $bc->is_published               = rand(1,2);
                $bc->save();
    
                $coaches = Coach::where('branch_id', $i)->get();
                foreach($coaches as $coach) {
                    $bcc = new BranchClassCoach();
                    $bcc->branch_class_id = $bc->id;
                    $bcc->coach_id        = $coach->id;
                    $bcc->save();
                }
    
                $branchPackages = BranchPackage::where('branch_id', $i)->get();
    
                foreach($branchPackages as $bp) {
                    $bcp = new BranchClassPackage();
                    $bcp->branch_class_id   = $bc->id;
                    $bcp->branch_pacakge_id = $bp->id;
                    $bcp->save();
                }
    
                $branchSchedules = BranchSchedule::where('branch_id', $i)->get();
    
                foreach($branchSchedules as $bs) {
                    $bsd = new BranchScheduleDetail();
                    $bsd->branch_schedule_id = $bs->id;
                    $bsd->branch_class_id    = $bc->id;
                    $bsd->start_time         = $class['start_time'];
                    $bsd->end_time           = $class['end_time'];
                    $bsd->max_quota          = 50;
                    $bsd->is_quota           = 1;
                    $bsd->is_published       = 1;
                    $bsd->save();
                }
            }
        }
    }
}
