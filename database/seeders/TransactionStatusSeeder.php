<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Client\Member\Transactions\TransactionStatus;

class TransactionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            'awaiting',
            'success',
            'cancelled'
        ];

        foreach ($collections as $collection) {
            # code...
            $transaction = new TransactionStatus();
            $transaction->title = $collection;
            $transaction->save();
        }
    }
}
