<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Models\Common\Service\Service;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            [
                'title' => 'fitClub',
                'image' => 'https://aje.bz/containers/efectifity/image/ICON-FITCLUB.svg',
            ],
            [
                'title' => 'fitKitchen',
                'image' => 'https://aje.bz/containers/efectifity/image/ICON-FITKITCHEN.svg',
            ],
        ];

        foreach ($collections as $collection) {
            $service = new Service();
            $service->title = $collection['title'];
            $service->image = $collection['image'];
            $service->slug = Str::slug($collection['title']);
            $service->is_published = 1;
            $service->save();
        }
    }
}
