<?php

namespace Database\Seeders;

use App\Models\Internal\Addtional\AddtionalInformation;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(BranchStatusSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ClassCategorySeeder::class);
        $this->call(FitSeeder::class);
        $this->call(TransactionStatusSeeder::class);
        $this->call(ClassAttendanceStatusSeeder::class);
        $this->call(PaymentTypeSeeder::class);
        $this->call(TransactionSeeder::class);
        // $this->call(StaffSeeder::class);
        $this->call(PartnerSeeder::class);
        $this->call(BankSeeder::class);
        $this->call(AmenitySeeder::class);
        $this->call(AdditionalInformationSeeder::class);
        $this->call(VoucherSeeder::class);
        $this->call(PermissioSeeder::class);
        $this->call(MonthlySeeder::class);
        $this->call(OperationalTimeSeeder::class);
        $this->call(RolePermissionSeeder::class);
        // $this->call(WorkScheduleSeeder::class);
    }
}
