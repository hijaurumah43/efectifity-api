<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Internal\Bank\Bank;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            'BCA',
            'MANDIRI',
            'BNI',
        ];

        foreach ($banks as $bank) {
            Bank::create([
                'title' => $bank,
                'code' => rand('111', '999'),
            ]);
        }
    }
}
