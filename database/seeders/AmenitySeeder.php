<?php

namespace Database\Seeders;

use App\Models\Common\Branch\BranchAmenity;
use App\Models\Internal\Amenity\Amenity;
use Illuminate\Database\Seeder;

class AmenitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $aminities = [
            'Shower',
            'Lockers',
            'Towel',
            'Cafetaria',
            'Parking',
        ];

        foreach ($aminities as $aminity) {
            $a = Amenity::create([
                'title' => $aminity
            ]);

            BranchAmenity::create([
                'branch_id' => 1,
                'amenity_id' => $a->id
            ]);
        }
    }
}
