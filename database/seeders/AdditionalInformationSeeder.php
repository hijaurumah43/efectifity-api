<?php

namespace Database\Seeders;

use App\Models\Common\Branch\BranchAdditionalInformation;
use App\Models\Internal\Additional\AdditionalInformation;
use Illuminate\Database\Seeder;

class AdditionalInformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $as = [
            [
                'title' => 'Temperature Checks',
                'description' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some',
                'image' => 'https://picsum.photos/id/237/200/300'
            ],
            [
                'title' => 'Social Distancing Measures',
                'description' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some',
                'image' => 'https://picsum.photos/id/237/200/300'
            ],[
                'title' => 'Ventilation System',
                'description' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some',
                'image' => 'https://picsum.photos/id/237/200/300'
            ],
        ];

        foreach ($as as $item) {
            $a = AdditionalInformation::create([
                'title' => $item['title'],
                'description' => $item['description'],
                'image' => $item['image'],
            ]);

            BranchAdditionalInformation::create([
                'branch_id' => 1,
                'additional_information_id' => $a->id
            ]);
        }
    }
}
