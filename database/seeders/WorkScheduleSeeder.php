<?php

namespace Database\Seeders;

use App\Models\Client\Staff\Shift;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Staff\StaffScheduleShift;
use Illuminate\Database\Seeder;

class WorkScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $y = Shift::where('branch_id', 4)->truncate();
        $s = StaffSchedule::where('member_branch_id', 34)->first();
        if ($s) {
            StaffScheduleShift::where('staff_schedule_id', $s->id)->delete();
            StaffScheduleDetail::where('staff_schedule_id', $s->id)->delete();
            StaffSchedule::where('member_branch_id', 34)->delete();
        }

        // Create Schedule
        $schedules = StaffSchedule::create([
            'schedule_id' => 1,
            'title' => 'Mely Indah',
            'member_branch_id' => 34,
            'date' => date('Y-m-d'),
            'is_published' => 1
        ]);

        $times = [
            [
                'start_time' => '06:00:00',
                'end_time' => '19:00:00',
            ],
            /* [
                'start_time' => '12:00:00',
                'end_time' => '23:00:00',
            ], */
        ];

        $x = 1;
        foreach ($times as $time) {
            $shift = Shift::create([
                'branch_id' => 4,
                'title' => 'SHIFT ' . $x,
                'start_time' => $time['start_time'],
                'end_time' => $time['end_time'],
                'created_by' => 'c65e63a9-2c2a-4cec-a2e8-d804647ee84c',
                'status' => 1,
            ]);

            $ss = StaffScheduleShift::create([
                'shift_id' => $shift->id,
                'staff_schedule_id' => $schedules->id,
                'title' => 'Shift M' . $x,
                'start_at' => $shift->start_time,
                'end_at' => $shift->end_time,
                'is_active' => 1,
            ]);

            $x++;
        }

        for ($i = 1; $i <=  date('t'); $i++) {
            $start_date1 = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . ' ' . $shift->start_time;
            $end_date1 = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT) . ' ' . $shift->end_time;

            StaffScheduleDetail::create([
                'branch_id' => 4,
                'staff_schedule_id' => $schedules->id,
                'staff_schedule_shift_id' => $ss->id,
                'title' => 'SHIFT M' . $x,
                'start_time' => $start_date1,
                'end_time' => $end_date1,
                'is_published' => 1,
            ]);
        }
    }
}
