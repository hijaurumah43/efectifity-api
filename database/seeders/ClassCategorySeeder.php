<?php

namespace Database\Seeders;

use App\Models\Common\Branch\BranchClassCategory;
use Illuminate\Database\Seeder;

class ClassCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'PHYSICAL',
            'MIND & BODY',
            'CYCLING',
            'DANCE',
            'MARTIAL ARTS',
            'CARDIO',
            'STRENGTH & CONDITIONING',
        ];

        foreach ($categories as $item) {
            BranchClassCategory::create([
                'title' => strtolower($item),
            ]);
        }
    }
}
