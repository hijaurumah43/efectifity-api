<?php

namespace Database\Seeders;

use App\Models\Common\Payment\PaymentMethod;
use Illuminate\Database\Seeder;
use App\Models\Common\Payment\PaymentType;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $methods = [
            'Virtual Accounts',
            'Retail Outlets',
            'eWallets',
            'Cards',
            'QR Codes',
        ];

        foreach ($methods as $method) {
            PaymentMethod::create([
                'title' => $method
            ]);
        }

        $channels = [
            [
                'methode' => '1',
                'title' => 'BRI',
                'channel_code' => 'BRI',
                'image' => 'https://aje.bz/containers/efectifity/banks/bri-logo.svg',
            ],
            [
                'methode' => '1',
                'title' => 'BNI',
                'channel_code' => 'BNI',
                'image' => 'https://aje.bz/containers/efectifity/banks/bni-logo.svg',
            ],
            [
                'methode' => '1',
                'title' => 'Mandiri',
                'channel_code' => 'MANDIRI',
                'image' => 'https://aje.bz/containers/efectifity/banks/mandiri-logo.svg',
            ],
            [
                'methode' => '1',
                'title' => 'Permata',
                'channel_code' => 'PERMATA',
                'image' => 'https://aje.bz/containers/efectifity/banks/permata-logo.svg',
            ],
            [
                'methode' => '2',
                'title' => 'Alfamart',
                'channel_code' => 'ALFAMART',
                'image' => 'https://aje.bz/containers/efectifity/banks/alfamart-logo.svg',
            ],
            [
                'methode' => '3',
                'title' => 'OVO',
                'channel_code' => 'ID_OVO',
                'image' => 'https://aje.bz/containers/efectifity/banks/ovo-logo.svg',
            ],
            [
                'methode' => '3',
                'title' => 'DANA',
                'channel_code' => 'ID_DANA',
                'image' => 'https://aje.bz/containers/efectifity/banks/dana-logo.svg',
            ],
            [
                'methode' => '3',
                'title' => 'LinkAja',
                'channel_code' => 'ID_LINKAJA',
                'image' => 'https://aje.bz/containers/efectifity/banks/linkaja-logo.svg',
            ],
            [
                'methode' => '3',
                'title' => 'Shopee Pay',
                'channel_code' => 'ID_SHOPEEPAY',
                'image' => 'https://aje.bz/containers/efectifity/banks/shopeepay-logo.svg',
            ],
            [
                'methode' => '4',
                'title' => 'Visa',
                'channel_code' => 'VISA',
                'image' => 'https://aje.bz/containers/efectifity/banks/VISA-settings.png',
            ],
            [
                'methode' => '4',
                'title' => 'Mastercards',
                'channel_code' => 'MASTERCARD',
                'image' => 'https://aje.bz/containers/efectifity/banks/MASTERCARD-settings.png',
            ],
            [
                'methode' => '4',
                'title' => 'JCBs',
                'channel_code' => 'JCB',
                'image' => 'https://aje.bz/containers/efectifity/banks/JCB-settings.png',
            ],
            [
                'methode' => '5',
                'title' => 'QRIS',
                'channel_code' => 'QRIS',
                'image' => 'https://aje.bz/containers/efectifity/banks/qris-logo.svg',
            ],
        ];

        foreach ($channels as $channel) {
            PaymentType::create([
                'methode' => $channel['methode'],
                'title' => $channel['title'],
                'channel_code' => $channel['channel_code'],
                'image' => $channel['image'],
            ]);
        }
    }
}
