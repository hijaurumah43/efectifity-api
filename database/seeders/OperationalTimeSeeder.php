<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Common\Branch\BranchOperationalTime;

class OperationalTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <=  date('t'); $i++)
        {
            $dates[] = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
        }

        for ($i=1; $i < 35; $i++) { 
            foreach ($dates as $date) {

                $bot = new BranchOperationalTime();
                $bot->branch_id     = $i;
                $bot->title         = "06.00 to 21.00";
                $bot->date          = $date;
                $bot->save();
            }
        }
    }
}
