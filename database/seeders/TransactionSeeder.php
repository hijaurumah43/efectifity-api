<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Client\Member\Member;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Member\Transactions\TransactionHistory;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i=1; $i <4; $i++) { 
            $user = User::factory()->create();

            $member = new Member();
            $member->user_id = $user->id;
            $member->name = $faker->firstName;
            $member->last_name = $faker->lastName;
            $member->avatar = 'https://picsum.photos/id/237/200/300';
            $member->save();

            $t = new Transaction();
            $t->member_id = $member->id;
            $t->service_id = 1;
            $t->branch_id = 1;
            if($i == 2) {
                $t->branch_package_id = 3;
            } else {
                $t->branch_package_id = 1;
            }
            $t->transaction_status_id = $i;
            $t->total_payment = 1500000;
            $t->old_values = 'datajson';
            $t->callback_values = 'datajson';

            if($i == 2) {
                $t->callback_at = date('Y-md-d H:i:s');
                $t->is_callback = 1;
            }

            $t->payment_type_id = 1;
            $t->save();

            if($i == 1) {
                
                // Pending

                $th = new TransactionHistory();
                $th->transaction_id = $t->id;
                $th->transaction_status_id = $i;
                $th->save();

            } elseif($i == 2) {

                // Success

                $th = new TransactionHistory();
                $th->transaction_id = $t->id;
                $th->transaction_status_id = 1;
                $th->save();

                $th = new TransactionHistory();
                $th->transaction_id = $t->id;
                $th->transaction_status_id = $i;
                $th->save();

                $mp = new MemberPackage();
                $mp->member_id = $member->id;
                $mp->branch_package_id = $t->branch_package_id;
                $mp->transaction_id = $t->id;
                $mp->branch_id = $t->branch_id;
                $mp->session = $t->branchPackage->session;
                $mp->session_remaining = $t->branchPackage->session;
                $mp->expiry_date = Carbon::now()->addMonths(1);
                $mp->save();

                $att = new ClassAttendance();
                $att->member_package_id = $mp->id;
                $att->member_id = $member->id;
                $att->branch_id = 1;
                $att->branch_schedule_id = 1;
                $att->branch_schedule_detail_id = 1;
                $att->save();

                $updateUser = User::where('id', $member->user->id)->first();
                $updateUser->email = 'faisal.murkadi@gmail.com';
                $updateUser->save();

                $decMp = MemberPackage::where('id', $mp->id)->first();
                $decMp->decrement('session_remaining', 1);
                $decMp->save();

            } elseif($i == 3) {

                // Cancel
                
                $th = new TransactionHistory();
                $th->transaction_id = $t->id;
                $th->transaction_status_id = 1;
                $th->save();

                $th = new TransactionHistory();
                $th->transaction_id = $t->id;
                $th->transaction_status_id = $i;
                $th->save();

            }
        }
    }
}
