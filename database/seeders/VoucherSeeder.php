<?php

namespace Database\Seeders;

use App\Models\Internal\Voucher\Voucher;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VoucherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vouchers = [
            [
                'title' => 'PROMO20 (20% Discount)',
                'type' => 1,
                'branch_id' => NULL,
                'image' => 'https://picsum.photos/id/237/200/300',
                'qty' => 10,
                'discount' => 20,
                'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis dolorem odit, maxime nemo provident iusto tempore aspernatur ipsum? Molestiae laudantium alias vero ex veritatis nisi delectus fugit dicta deserunt est!',
                'expired' => Carbon::now()->addHour(5),
            ],
            [
                'title' => 'PERMATA10 (10% Discount)',
                'type' => 1,
                'branch_id' => NULL,
                'image' => 'https://picsum.photos/id/237/200/300',
                'qty' => 3,
                'discount' => 10,
                'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis dolorem odit, maxime nemo provident iusto tempore aspernatur ipsum? Molestiae laudantium alias vero ex veritatis nisi delectus fugit dicta deserunt est!',
                'expired' => Carbon::now()->addDay(1),
            ],
            [
                'title' => 'MANDIRICC5 (5% Discount)',
                'type' => 2, // Merchant
                'branch_id' => 1,
                'image' => 'https://picsum.photos/id/237/200/300',
                'qty' => 5,
                'discount' => 5,
                'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis dolorem odit, maxime nemo provident iusto tempore aspernatur ipsum? Molestiae laudantium alias vero ex veritatis nisi delectus fugit dicta deserunt est!',
                'expired' => Carbon::now()->addDay(2),
            ],
        ];

        foreach ($vouchers as $item) {
            Voucher::create($item);
        }
    }
}
