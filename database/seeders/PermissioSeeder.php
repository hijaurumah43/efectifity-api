<?php

namespace Database\Seeders;

use App\Models\Role;
use Ramsey\Uuid\Uuid;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Database\Seeder;

class PermissioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collections = [
            'dashboard',
            'member',
            'class',
            'staff',
            'transaction',
            'settings',
            'attendance',
            'scanning',
            'schedule',
        ];

        $permissions = [];

        foreach ($collections as $key => $item) {

            $name = 'fitclub-' . $item;
            if ($key > 5) {
                $name = 'app-' . $item;
            }
            $permissions[] = \App\Models\Permission::create([
                'id' => Uuid::uuid4(),
                'name' => $name,
                'display_name' => 'Fitclub ' . ucfirst($item),
                'description' => 'Fitclub ' . ucfirst($item),
            ])->id;
        }

        $roles = Role::all();
        foreach ($roles as $role) {
            switch ($role->name) {
                case 'gilgamesh':
                    $role->permissions()->attach($permissions);
                    break;

                case 'superadmin':
                    $role->permissions()->attach($permissions);
                    break;

                case 'partner':
                    $role->permissions()->attach($permissions);
                    break;

                case 'manager':
                    $role->permissions()->attach($permissions);
                    break;

                default:
                    # code...
                    break;
            }
        }

        // PermissionRole::where('')->update(['role', 1]);
    }
}
