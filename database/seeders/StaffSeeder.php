<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Client\Member\Member;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use Carbon\Carbon;

class StaffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $user = User::factory()->create();

        $member = Member::create([
            'user_id' => $user->id,
            'name' => $faker->firstName,
        ]);

        $user->attachRole(Role::MEMBER);
        $user->attachRole(Role::STAFF);

        $mb = MemberBranch::create([
            'member_id' => $member->id,
            'branch_id' => 1,
            'is_published' => 1,
        ]);

        $ss = StaffSchedule::create([
            'member_branch_id' => $mb->id,
            'title' => 'periode seeder',
            'date' => date('Y-m-d')
        ]);

        $ssd = StaffScheduleDetail::create([
            'staff_schedule_id' => $ss->id,
            'title' => 'Morning Shift',
            'start_time' => date('Y-m-d').' 08:00:00',
            'end_time' => date('Y-m-d').' 17:00:00',
        ]);
    
    }
}
