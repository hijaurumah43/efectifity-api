<?php

namespace Database\Seeders;

use App\Models\Internal\Branch\BranchStatus;
use Illuminate\Database\Seeder;

class BranchStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            'draft',
            'pending',
            'in-review',
            'rejected',
            'approved',
            'active',
        ];

        foreach($statuses as $status){
            BranchStatus::create([
                'title' => $status
            ]);
        }
    }
}
