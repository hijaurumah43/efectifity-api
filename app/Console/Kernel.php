<?php

namespace App\Console;

use App\Console\Commands\Client\Member\ClassAttendanceExpired;
use App\Console\Commands\Client\Member\TransactionExpired;
use App\Console\Commands\Client\Staff\StaffClockOutAutomation;
use App\Console\Commands\Client\Member\MemberClockOutAutomation;
use App\Console\Commands\Client\Member\MemberPackageExpired;
use App\Console\Commands\Client\Member\PtAttendanceExpired;
use App\Console\Commands\Feature\Fitclub\PersonalTrainerMemberNotShow;
use App\Console\Commands\Feature\Fitclub\TransactionAutoCompleted;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ClassAttendanceExpired::class,
        TransactionExpired::class,
        StaffClockOutAutomation::class,
        MemberClockOutAutomation::class,
        TransactionAutoCompleted::class,
        StaffClockOutAutomation::class,
        MemberPackageExpired::class,
        PersonalTrainerMemberNotShow::class,
        PtAttendanceExpired::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('classAttendance:expired')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('transaction:expired')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('staffAttendance:clockOut')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('memberAttendance:clockOut')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('transaction:completed')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('staffAttendance:notClockIn')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('memberPackage:expired')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('perosnalTrainerMemberNotShow')
            ->cron('* * * * *')
            ->runInBackground();

        $schedule->command('ptAttendance:expired')
            ->cron('* * * * *')
            ->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
