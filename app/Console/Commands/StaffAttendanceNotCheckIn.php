<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Client\Staff\StaffAttendance;
use App\Models\Client\Staff\StaffScheduleDetail;

class StaffAttendanceNotCheckIn extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staffAttendance:notClockIn';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = date('Y-m-d H:i:s');
        $nowTimeStamp = ConvertToTimestamp($now);
        $details = StaffScheduleDetail::where('end_timestamp', '<=', $nowTimeStamp)
            ->whereNull('note')
            ->where('is_published', 1)
            ->orderBy('start_time', 'ASC')
            ->get();

        if (count($details) > 0) {
            foreach ($details as $detail) {

                // $convertDate = Carbon::parse($detail->shift->end_time)->format('Y-m-d H:i:s');
                $convertDate = ConvertToTimeServer($detail->end_timestamp);

                $staff = StaffAttendance::where([
                    ['member_id', $detail->member_id],
                    ['member_branch_id', $detail->member_branch_id],
                ])
                    ->whereDate('time', $convertDate)
                    ->orderBy('time', 'DESC')
                    ->first();

                if (!$staff) {
                    // $expired = Carbon::parse($detail->shift->end_time)->addMinute(30)->format('Y-m-d H:i:s');
                    $converToTimeServer = ConvertToTimeServer($detail->end_timestamp);
                    $expired = Carbon::parse($converToTimeServer)->addMinute(30)->format('Y-m-d H:i:s');
                    if ($now > $expired) {
                        $detail->note = 'not-checked-in';
                        $detail->is_published = 0;

                        $detail->save();
                        echo 201;
                    } else {
                        echo 202;
                    }
                }
            }
        } else {
            echo 203;
        }
        echo 205;
    }
}
