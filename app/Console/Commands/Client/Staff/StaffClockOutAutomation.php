<?php

namespace App\Console\Commands\Client\Staff;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffAttendance;
use App\Models\Client\Staff\StaffScheduleDetail;

class StaffClockOutAutomation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'staffAttendance:clockOut';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clock out automation for staff attendance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d');
        // Check attendance Clock in
        $attendances = StaffAttendance::whereDate('time', $date)->select('member_id')->groupBy('member_id')->get();
        foreach ($attendances as $attendance) {
            $staff = StaffAttendance::where('member_id', $attendance->member_id)->orderBy('time', 'DESC')->first();
            if ($staff) {
                if ($staff->status == 1) {
                    $now = date('Y-m-d H:i:s');

                    // $schedule = StaffSchedule::where('member_branch_id', $staff->member_branch_id)->orderBy('created_at', 'DESC')->where('is_published', 1)->first();
                    $details = StaffScheduleDetail::where('start_time', '<=', $now)
                        // where('staff_schedule_id', $schedule->id)
                        ->whereNull('note')
                        ->where('member_branch_id', $staff->member_branch_id)
                        ->where('is_published', 1)
                        ->orderBy('start_time', 'ASC')
                        // ->first();
                        ->get();
                    // TODO:: cek kalau 1 hari 2 shift

                    if (count($details) > 0) {
                        foreach ($details as $detail) {
                            // $expired = Carbon::parse($detail->shift->end_time)->addMinute(5)->format('Y-m-d H:i:s');

                            // Conver to date time string
                            $converToTimeServer = ConvertToTimeServer($detail->end_timestamp);
                            $expired = Carbon::parse($converToTimeServer)->addMinute(5)->format('Y-m-d H:i:s');

                            if ($now > $expired) {
                                $detail->note = 'Checkout by system';
                                $detail->is_published = 0;
                                $detail->save();

                                if ($staff->status == 1) {
                                    StaffAttendance::create([
                                        'member_id' => $staff->member_id,
                                        'member_branch_id' => $staff->member_branch_id,
                                        'time' => $now,
                                        'status' => 0,
                                        'is_active' => 1,
                                    ]);
                                }
                                echo 201;
                            } else {
                                echo 202;
                            }
                        }
                    } else {
                        echo 203;
                    }
                } else {
                    echo 204;
                }
            }
        }

        echo 205;
    }
}
