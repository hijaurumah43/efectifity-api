<?php

namespace App\Console\Commands\Client\Member;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Member\Classes\ClassExpired;
use App\Models\Client\Member\Log\MemberLog;
use App\Models\Client\Member\Attendances\ClassAttendance;

class ClassAttendanceExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'classAttendance:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status class attendance to expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collections = ClassAttendance::where([
            ['is_active', 1],
            ['class_attendance_status_id', 1],
        ])->get();

        foreach ($collections as $collection) {
            $now = date('Y-m-d H:i:s');

            // Convert to date format of resource time
            $classEndTime = $collection->scheduleDetail->end_time;
            $classEndTimeFormat = Carbon::parse((date($collection->schedule->date . ' ' . $classEndTime . '.00')))->format('Y-m-d H:i:s');

            // Conver to date time string
            $converToTimeServer = ConvertToTimeServer($collection->scheduleDetail->end_timestamp);

            // Change status to expired
            // if ($now > $classEndTimeFormat) {
            if ($now > $converToTimeServer) {
                $collection->class_attendance_status_id = 4;
                $collection->is_active = 0;
                $collection->save();
                echo 'success';

                /**
                 * Create History
                 */
                $className = $collection->scheduleDetail->class->title;
                $classCoach = $collection->scheduleDetail->class->branchCoach->first()->member->name . ' ' . $collection->scheduleDetail->class->branchCoach->first()->member->last_name;
                $classBranch = $collection->branch->title;
                $classTime = Carbon::parse($collection->scheduleDetail->schedule->date)->format('d F Y') . ' ' . $collection->scheduleDetail->start_time;
                $message = "You've missed " .  $className . " class with coach " . ucwords($classCoach) . " at " . $classBranch . " on " . $classTime;

                $memberLog = MemberLog::create([
                    'member_id' => $collection->member_id,
                    'branch_id' => $collection->branch_id,
                    'class_attendance_id' => $collection->id,
                    'status' => 7, // 7 = not show
                    'message' => $message,
                ]);

                $dataFb = [
                    'slug' => 'upcoming', // slug == path
                    'param' => [
                        'key' => 'status',
                        'value' => 'expired',
                    ]
                ];

                event(new ClassExpired($memberLog));
                sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);
            }
            echo 'nope';
        }
    }
}
