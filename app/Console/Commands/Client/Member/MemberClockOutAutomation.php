<?php

namespace App\Console\Commands\Client\Member;

use App\Models\Client\Staff\Attendance\MemberAttendance;
use Illuminate\Console\Command;

class MemberClockOutAutomation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'memberAttendance:clockOut';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clock out automation for staff attendance';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = date('Y-m-d');
        // Check attendance Clock in
        $attendances = MemberAttendance::whereDate('time', $date)->select('member_id')->groupBy('member_id')->get();
        foreach ($attendances as $attendance) {
            $member = MemberAttendance::where('member_id', $attendance->member_id)->orderBy('time', 'DESC')->first();
            if ($member) {
                if ($member->status == 1) {
                    $now = date('Y-m-d H:i:s');
                    $expired = date('Y-m-d 23:15:00');
                    if ($now > $expired) {
                        MemberAttendance::create([
                            'member_id' => $member->member_id,
                            'branch_id' => $member->branch_id,
                            'time' => $now,
                            'status' => 0,
                            'scanned_by' => NULL,
                        ]);
                        echo 201;
                    } else {
                        echo 202;
                    }
                }
            }
        }

        echo 205;
    }
}
