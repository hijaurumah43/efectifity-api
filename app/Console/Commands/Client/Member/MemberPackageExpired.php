<?php

namespace App\Console\Commands\Client\Member;

use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Console\Command;

class MemberPackageExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'memberPackage:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status memberpackge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now  = date('Y-m-d H:i:s');
        $collections = MemberPackage::where('status', 1)->get();
        foreach ($collections as $collection) {
            if ($now > $collection->expiry_date) {
                $collection->status = 0;
                $collection->save();

                return 200;
            }
        }
    }
}
