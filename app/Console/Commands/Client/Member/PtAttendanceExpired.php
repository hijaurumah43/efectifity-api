<?php

namespace App\Console\Commands\Client\Member;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Member\PT\PtClassExpired;
use App\Events\Member\Classes\ClassExpired;
use App\Models\Client\Member\Log\MemberLog;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Member\Attendances\ClassAttendance;

class PtAttendanceExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ptAttendance:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status pt attendance to not show';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collections = PersonalTrainerAttendance::where([
            ['is_active', 1],
            ['personal_trainer_attendance_status_id', 1],
        ])->get();

        foreach ($collections as $collection) {

            // $now = date('Y-m-d H:i:s', strtotime('+7 hour'));
            $now = date('Y-m-d H:i:s');

            // Convert to date format of resource time
            $classEndTime = $collection->end_time;
            $classEndTimeFormat = Carbon::parse((date($collection->date . ' ' . $classEndTime . '.00')))->format('Y-m-d H:i:s');

            // Conver to date time string
            $converToTimeServer = ConvertToTimeServer($collection->end_timestamp);

            // Change status to expired
            if ($now > $converToTimeServer) {
                $collection->personal_trainer_attendance_status_id = 3;
                $collection->is_active = 0;
                $collection->save();
                echo 'success';

                /**
                 * Create History
                 */
                $message = 'We’re sorry not to see you at class Personal Training” at ' . $collection->branch->title;
                $memberLog = MemberLog::create([
                    'member_id' => $collection->member_id,
                    'branch_id' => $collection->branch_id,
                    'personal_trainer_attendance_id' => $collection->id,
                    'status' => 7, // 7 = not show
                    'message' => $message,
                ]);

                $dataFb = [
                    'slug' => 'upcoming',
                    'param' => [
                        'key' => 'status',
                        'value' => 'expired',
                    ]
                ];

                event(new PtClassExpired($memberLog));
                //NOTE:: NOTIF FIREBASE
                sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);
            }
            echo 'nope';
        }
    }
}
