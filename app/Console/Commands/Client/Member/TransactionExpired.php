<?php

namespace App\Console\Commands\Client\Member;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Client\Member\Transactions\Transaction;

class TransactionExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status transaction to expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collections = Transaction::where('transaction_status_id', 1)->get();
        $now = date('Y-m-d H:i:s');
        foreach ($collections as $collection) {
            // OVO
            if($collection->payment_type_id == 6) {
                $expired = Carbon::parse($collection->created_at)->addMinute(1)->format('Y-m-d H:i:s');
            } else {
                $expired = Carbon::parse($collection->created_at)->addDay(1)->format('Y-m-d H:i:s');
            }
            if ($now > $expired) {
                $collection->transaction_status_id = 3;
                $collection->save();
            }
        }
    }
}
