<?php

namespace App\Console\Commands\Feature\Fitclub;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Client\Staff\PersonalTrainerAttendance;

class PersonalTrainerMemberNotShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'perosnalTrainerMemberNotShow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'in personal member if customer not accept or reject reservation pt then status expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = date('Y-m-d H:i:s');
        $collections = PersonalTrainerAttendance::where('personal_trainer_attendance_status_id', 7)->get();

        foreach ($collections as $collection) {
            $expired = Carbon::parse($collection->created_at)->addMinute(2)->format('Y-m-d H:i:s');

            if ($now > $expired) {
                $collection->personal_trainer_attendance_status_id = 9;
                $collection->save();

                return 200;
            }
        }
    }
}
