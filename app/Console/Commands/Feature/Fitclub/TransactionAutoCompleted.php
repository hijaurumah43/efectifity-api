<?php

namespace App\Console\Commands\Feature\Fitclub;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Client\Staff\Transaction\TransactionDetail;
use App\Models\Internal\Branch\BranchFee;

class TransactionAutoCompleted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:completed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jika selama 24 jam ada REFUND atau TIDAK maka INCOMING PAYMENT diubah menjadi COMPLETED';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collections = TransactionDetail::where('status', 0)->where('transaction_type_id', 1)->where('is_published', 1)->get();
        foreach ($collections as $collection) {
            $now = date('Y-m-d H:i:s');
            $dateNow = date('Y-m-d');
            $expired = Carbon::parse($collection->created_at)->addDays(1)->format('Y-m-d H:i:s');
            if ($now >= $expired) {
                // NOTE:: Update Resource

                DB::beginTransaction();
                $resource = TransactionDetail::where('id', $collection->id)->first();
                $resource->status = 1;
                $resource->save();

                $efectifityFee = ENV('FEE');
                $branchFee = BranchFee::where('branch_id', $resource->branch_id)
                    ->where('start_date', '>=', $dateNow)
                    ->where('end_date', '<=', $dateNow)
                    ->where('is_active', 1)
                    ->orderBy('created_at', 'DESC')
                    ->first();

                if ($branchFee) {
                    $efectifityFee = $branchFee->fee;
                }

                // NOTE:: Store Efectifity Fee
                $fee = new TransactionDetail();
                $fee->transaction_id = $resource->transaction_id;
                $fee->order_number = $resource->order_number;
                $fee->member_id = $resource->member_id;
                $fee->branch_id = $resource->branch_id;
                $fee->transaction_type_id = 2;
                $fee->status = $resource->status;
                $fee->amount = ($resource->amount * $efectifityFee) / 100; // Bagi Per 10
                $fee->is_published = $resource->is_published;
                $fee->save();

                DB::commit();
            }
        }

        return 200;
    }
}
