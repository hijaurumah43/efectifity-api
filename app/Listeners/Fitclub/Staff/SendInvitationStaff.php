<?php

namespace App\Listeners\Fitclub\Staff;

use App\Models\User;
use App\Notifications\Fitclub\Staff\InvitationStaffNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class SendInvitationStaff
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = User::where('id', $event->data->member->user->id)->first();

        Notification::send($user, new InvitationStaffNotification($event->data, $event->messageNotification));
    }
}
