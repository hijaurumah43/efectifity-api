<?php

namespace App\Listeners;

use Illuminate\Bus\Queueable;
use App\Contracts\UserRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PasswordChangedNotification;

class PasswordResetListener implements ShouldQueue
{
    use Queueable;
    
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle($event)
    {
        $this->userRepository->setNewEmailTokenConfirmation($event->user->id);
        
        Notification::send($event->user, new PasswordChangedNotification());
    }
}
