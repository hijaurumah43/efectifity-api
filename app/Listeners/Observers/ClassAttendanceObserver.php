<?php

namespace App\Listeners\Observers;

use Illuminate\Database\Eloquent\Model;
use Neves\Events\Contracts\TransactionalEvent;
use Ramsey\Uuid\Uuid;

class ClassAttendanceObserver implements TransactionalEvent
{
    public function creating(Model $model)
    {
        $model->setAttribute('id', $model->getAttribute('id') ?? Uuid::uuid4()->toString());
    }
}
