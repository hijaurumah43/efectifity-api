<?php

namespace App\Listeners\Observers;

use Ramsey\Uuid\Nonstandard\Uuid;
use App\Models\Client\Member\Member;
use Illuminate\Database\Eloquent\Model;
use Neves\Events\Contracts\TransactionalEvent;

class MemberObserver implements TransactionalEvent
{
    public function creating(Model $model) 
    {
        $model->setAttribute('id', $model->getAttribute('id') ?? Uuid::uuid4()->toString());
        $model->setAttribute('member_id', $model->getAttribute('member_id') ?? $this->generateMemberID());
    }

    private function generateMemberID()
    {
        $cek = Member::selectRaw('count(id) as total')->get();
        if ($cek[0]->total == 0) {
            
            $bulan_sekarang     = date('m');
            $tahun_sekarang     = date('y');
            $tanggal_sekarang   = date('d');

            return $tahun_sekarang . $bulan_sekarang . $tanggal_sekarang . str_pad(1, 4, 0, STR_PAD_LEFT);

        } else {

            $ticket             = Member::take(1)->orderBy('created_at', 'desc')->first();
            
            $code               = $ticket->member_id;
            $bulan_sekarang     = date('m');
            $tahun_sekarang     = date('y');
            $tanggal_sekarang   = date('d');
            $tahun              = substr($code, 0, 2);

            $bulan              = substr($code, 2, 2);
            $tanggal            = substr($code, 4, 2);
            $urutan             = intval(substr($code, -4));

            if ($bulan_sekarang != $bulan || $tahun_sekarang != $tahun || $tanggal_sekarang != $tanggal) {
                $next       = 1;
            } else {
                $next       = $urutan + 1;
            }
            
            return $tahun_sekarang . $bulan_sekarang . $tanggal_sekarang . str_pad($next, 4, 0, STR_PAD_LEFT);
        }
    }
}
