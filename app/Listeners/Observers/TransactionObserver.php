<?php

namespace App\Listeners\Observers;

use Ramsey\Uuid\Nonstandard\Uuid;
use Illuminate\Database\Eloquent\Model;
use Neves\Events\Contracts\TransactionalEvent;
use App\Models\Client\Member\Transactions\Transaction;

class TransactionObserver implements TransactionalEvent
{
    public function creating(Model $model) 
    {
        $model->setAttribute('id', $model->getAttribute('id') ?? Uuid::uuid4()->toString());
        $model->setAttribute('order_number', $model->getAttribute('order_number') ?? $this->generateMemberID());
    }

    private function generateMemberID()
    {
        $cek = Transaction::selectRaw('count(id) as total')->get();
        if ($cek[0]->total == 0) {
            
            $bulan_sekarang     = date('m');
            $tahun_sekarang     = date('Y');
            $tanggal_sekarang   = date('d');

            return 'PR/'.$tahun_sekarang . $bulan_sekarang . $tanggal_sekarang .'/'. str_pad(1, 6, 0, STR_PAD_LEFT);

        } else {

            $ticket             = Transaction::take(1)->orderBy('created_at', 'desc')->first();
            
            $code               = $ticket->order_number;
            $bulan_sekarang     = date('m');
            $tahun_sekarang     = date('Y');
            $tanggal_sekarang   = date('d');
            $tahun              = substr($code, 3, 4);

            $bulan              = substr($code, 7, 2);
            $tanggal            = substr($code, 9, 2);
            $urutan             = intval(substr($code, -6));

            if ($bulan_sekarang != $bulan || $tahun_sekarang != $tahun || $tanggal_sekarang != $tanggal) {
                $next       = 1;
            } else {
                $next       = $urutan + 1;
            }
            
            return 'PR/'.$tahun_sekarang . $bulan_sekarang . $tanggal_sekarang .'/'. str_pad($next, 6, 0, STR_PAD_LEFT);
        }
    }
}
