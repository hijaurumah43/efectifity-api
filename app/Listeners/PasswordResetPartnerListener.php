<?php

namespace App\Listeners;

use App\Notifications\Partner\ForgotPasswordNotification;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\RegisterPartnerEmailNotification;

class PasswordResetPartnerListener implements ShouldQueue
{
    use Queueable;

    public function __construct()
    {
        $this->onQueue('notifications');
    }

    public function handle($event)
    {
        $token = Str::random(64);
        DB::table('password_resets')->where('email', $event->user->email)->delete();
        DB::table('password_resets')->insert([
            'email' => $event->user->email,
            'token' => bcrypt($token),
            'created_at' => Carbon::now()
        ]);

        Notification::send($event->user, new ForgotPasswordNotification($event->user, $token));
    }
}
