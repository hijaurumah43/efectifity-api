<?php

namespace App\Listeners\Member\Attendance;

use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Member\Attendance\MemberAttendanceClockOutNotification;

class MemberAttendanceClockOutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = User::where('id', $event->data->member->user->id)->first();
        Notification::send($user, new MemberAttendanceClockOutNotification($event->data));
    }
}
