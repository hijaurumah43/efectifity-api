<?php

namespace App\Listeners\Member\Classes;

use App\Models\User;
use App\Notifications\Member\Classes\ClassExpiredNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class ClassExpiredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $user = User::where('id', $event->data->member->user->id)->first();
        Notification::send($user, new ClassExpiredNotification($event->data));
    }
}
