<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\RegisterPartnerEmailNotification;

class UserRegisterPartnerListener implements ShouldQueue
{
    use Queueable;

    public function __construct()
    {
        $this->onQueue('notifications');
    }

    public function handle($event)
    {
        $token = Str::random(64);
        DB::table('password_resets')->where('email', $event->companyTemp->email)->delete();
        DB::table('password_resets')->insert([
            'email' => $event->companyTemp->email,
            'token' => bcrypt($token),
            'created_at' => Carbon::now()
        ]);

        Notification::send($event->companyTemp, new RegisterPartnerEmailNotification($event->companyTemp, $token));
    }
}
