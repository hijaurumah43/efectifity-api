<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Audit;
use App\Models\LoginHistory;
use App\Events\PT\SessionDeduct;
use App\Models\Client\Member\Member;
use App\Events\Partner\RemoveManager;
use Illuminate\Auth\Events\Registered;
use App\Events\Member\PT\PtClassCancel;
use App\Events\Partner\RegisterPartner;
use App\Events\Member\PT\PtClassExpired;
use App\Listeners\PasswordResetListener;
use App\Events\Partner\InvitationManager;
use App\Listeners\Observers\UserObserver;
use App\Listeners\UserRegisteredListener;
use Illuminate\Auth\Events\PasswordReset;
use App\Listeners\Observers\AuditObserver;
use App\Events\Fitclub\Classes\ClassCancel;
use App\Events\Member\Classes\ClassExpired;
use App\Events\Member\PT\PtAttendanceEvent;
use App\Listeners\Observers\MemberObserver;
use App\Listeners\PT\SessionDeductListener;
use App\Events\Member\PT\PtClassBookedEvent;
use App\Events\Fitclub\PT\PTReservationEvent;
use App\Events\Fitclub\Staff\InvitationStaff;
use App\Events\Partner\ForgotPasswordPartner;
use App\Listeners\UserRegisterPartnerListener;
use App\Events\Member\Classes\ClassBookedEvent;
use App\Listeners\PasswordResetPartnerListener;
use App\Listeners\Observers\TransactionObserver;
use App\Listeners\Partner\RemoveManagerListener;
use App\Listeners\Partner\SendInvitationManager;
use App\Events\Member\Program\ReservationProgram;
use App\Listeners\Member\PT\PtAttendanceListener;
use App\Listeners\Observers\LoginHistoryObserver;
use App\Listeners\Member\PT\PtClassBookedListener;
use App\Listeners\Member\PT\PtClassCancelListener;
use App\Listeners\Observers\MemberPackageObserver;
use App\Events\Member\Classes\ClassAttendanceEvent;
use App\Listeners\Fitclub\PT\PTReservationListiner;
use App\Listeners\Member\PT\PtClassExpiredListener;
use App\Listeners\Fitclub\Staff\SendInvitationStaff;
use App\Listeners\Observers\ClassAttendanceObserver;
use App\Listeners\Observers\CompanyDocumentObserver;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Listeners\Member\Classes\ClassBookedListener;
use App\Listeners\Fitclub\Classes\ClassCancelListener;
use App\Listeners\Member\Classes\ClassExpiredListener;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Listeners\Member\Classes\ClassAttendanceListener;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Events\Member\Classes\ClassCancel as AppClassCancel;
use App\Listeners\Member\Program\ReservationProgramListener;
use App\Events\Member\Attendance\MemberAttendanceClockInEvent;
use App\Listeners\Observers\PersonalTrainerAttendanceObserver;
use App\Events\Member\Attendance\MemberAttendanceClockOutEvent;
use App\Listeners\Member\Attendance\MemberAttendanceClockInListener;
use App\Listeners\Member\Attendance\MemberAttendanceClockOutListener;
use App\Listeners\Member\Classes\ClassCancelListener as AppClassCancelListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            UserRegisteredListener::class,
        ],

        RegisterPartner::class => [
            UserRegisterPartnerListener::class,
        ],

        PasswordReset::class => [
            PasswordResetListener::class,
        ],

        ForgotPasswordPartner::class => [
            PasswordResetPartnerListener::class,
        ],

        InvitationManager::class => [
            SendInvitationManager::class,
        ],

        RemoveManager::class => [
            RemoveManagerListener::class,
        ],

        ClassCancel::class => [
            ClassCancelListener::class
        ],

        InvitationStaff::class => [
            SendInvitationStaff::class,
        ],

        ReservationProgram::class => [
            ReservationProgramListener::class,
        ],

        ClassBookedEvent::class => [
            ClassBookedListener::class,
        ],

        ClassExpired::class => [
            ClassExpiredListener::class,
        ],

        AppClassCancel::class => [
            AppClassCancelListener::class,
        ],

        ClassAttendanceEvent::class => [
            ClassAttendanceListener::class,
        ],

        MemberAttendanceClockInEvent::class => [
            MemberAttendanceClockInListener::class,
        ],

        MemberAttendanceClockOutEvent::class => [
            MemberAttendanceClockOutListener::class,
        ],

        SessionDeduct::class => [
            SessionDeductListener::class,
        ],

        PTReservationEvent::class => [
            PTReservationListiner::class,
        ],

        PtAttendanceEvent::class => [
            PtAttendanceListener::class,
        ],

        PtClassExpired::class => [
            PtClassExpiredListener::class,
        ],

        PtClassBookedEvent::class => [
            PtClassBookedListener::class,
        ],

        PtClassCancel::class => [
            PtClassCancelListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Audit::observe(AuditObserver::class);
        User::observe(UserObserver::class);
        LoginHistory::observe(LoginHistoryObserver::class);
        Member::observe(MemberObserver::class);
        Transaction::observe(TransactionObserver::class);
        ClassAttendance::observe(ClassAttendanceObserver::class);
        MemberPackage::observe(MemberPackageObserver::class);
        CompanyDocument::observe(CompanyDocumentObserver::class);
        PersonalTrainerAttendance::observe(PersonalTrainerAttendanceObserver::class);
    }
}
