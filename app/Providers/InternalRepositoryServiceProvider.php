<?php

namespace App\Providers;

use App\Models\Internal\Address\City;
use Illuminate\Support\ServiceProvider;
use App\Models\Internal\Question\Question;
use App\Models\Internal\Question\QuestionType;
use App\Models\Internal\Question\QuestionGroup;
use App\Models\Internal\Question\QuestionDetail;
use App\Contracts\Internal\Address\CityRepository;
use App\Models\Internal\Question\QuestionCategory;
use App\Contracts\Internal\Question\QuestionRepository;
use App\Contracts\Internal\Question\QuestionTypeRepository;
use App\Contracts\Internal\Question\QuestionGroupRepository;
use App\Models\Client\Member\Transactions\TransactionStatus;
use App\Contracts\Internal\Question\QuestionDetailRepository;
use App\Repositories\Internal\Address\EloquentCityRepository;
use App\Contracts\Internal\Question\QuestionCategoryRepository;
use App\Repositories\Internal\Question\EloquentQuestionRepository;
use App\Repositories\Internal\Question\EloquentQuestionTypeRepository;
use App\Repositories\Internal\Question\EloquentQuestionGroupRepository;
use App\Repositories\Internal\Question\EloquentQuestionDetailRepository;
use App\Contracts\Client\Member\Transactions\TransactionStatusRepository;
use App\Repositories\Internal\Question\EloquentQuestionCategoryRepository;
use App\Repositories\Client\Member\Transactions\EloquentTransactionStatusRepository;

class InternalRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(QuestionTypeRepository::class, function () {
            return new EloquentQuestionTypeRepository(new QuestionType());
        });

        $this->app->singleton(QuestionCategoryRepository::class, function () {
            return new EloquentQuestionCategoryRepository(new QuestionCategory());
        });

        $this->app->singleton(QuestionRepository::class, function () {
            return new EloquentQuestionRepository(new Question());
        });

        $this->app->singleton(QuestionDetailRepository::class, function () {
            return new EloquentQuestionDetailRepository(new QuestionDetail());
        });

        $this->app->singleton(QuestionGroupRepository::class, function () {
            return new EloquentQuestionGroupRepository(new QuestionGroup());
        });

        $this->app->singleton(CityRepository::class, function () {
            return new EloquentCityRepository(new City());
        });

        $this->app->singleton(TransactionStatusRepository::class, function () {
            return new EloquentTransactionStatusRepository(new TransactionStatus());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function provides()
    {
        return [
            QuestionTypeRepository::class,
            QuestionCategoryRepository::class,
            QuestionRepository::class,
            QuestionDetailRepository::class,
            QuestionGroupRepository::class,
            CityRepository::class,
            TransactionStatusRepository::class,
        ];
    }
}
