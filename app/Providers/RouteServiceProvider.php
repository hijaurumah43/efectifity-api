<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('v1')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/api.php'));

            Route::prefix('v1/members')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/member.php'));

            Route::prefix('v1/staff')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/staff.php'));

            Route::prefix('v1/partners')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/partner.php'));

            Route::prefix('v1/myaccount')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/account.php'));

            Route::prefix('v1/fc/')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/feature/fitclub.php'));

            Route::prefix('v1/internal')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/v1/internal.php'));

            Route::prefix('dev')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/development.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/members', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/staff', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/partners', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/myaccount', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/fc', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });

        RateLimiter::for('v1/internal', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
