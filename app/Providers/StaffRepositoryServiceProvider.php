<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Contracts\Client\Staff\Schedule\StaffScheduleRepository;
use App\Repositories\Client\Staff\EloquentStaffScheduleRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleDetailRepository;
use App\Repositories\Client\Staff\EloquentStaffScheduleDetailRepository;

class StaffRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StaffScheduleRepository::class, function () {
            return new EloquentStaffScheduleRepository(new StaffSchedule());
        });

        $this->app->singleton(StaffScheduleDetailRepository::class, function () {
            return new EloquentStaffScheduleDetailRepository(new StaffScheduleDetail());
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function provides()
    {
        return [
            StaffScheduleRepository::class,
            StaffScheduleDetailRepository::class,
        ];
    }
}
