<?php

namespace App\Providers;

use App\Models\Client\Member\Member;
use Illuminate\Support\ServiceProvider;
use App\Contracts\Client\Member\MemberRepository;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Questions\QuestionAnswer;
use App\Models\Client\Member\Settings\DeliveryAddress;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Member\Settings\EmergencyContact;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Repositories\Client\Member\EloquentMemberRepository;
use App\Models\Client\Member\Notifications\NotificationLimit;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;
use App\Contracts\Client\Member\Questions\QuestionAnswerRepository;
use App\Contracts\Client\Member\Settings\DeliveryAddressRepository;
use App\Contracts\Client\Member\Transactions\TransactionRepository;
use App\Contracts\Client\Member\Settings\EmergencyContactRepository;
use App\Contracts\Client\Member\Attendances\ClassAttendanceRepository;
use App\Contracts\Client\Member\Notifications\NotificationLimitRepository;
use App\Repositories\Client\Member\Classes\EloquentMemberBranchRepository;
use App\Repositories\Client\Member\Programs\EloquentMemberPackageRepository;
use App\Repositories\Client\Member\Questions\EloquentQuestionAnswerRepository;
use App\Repositories\Client\Member\Settings\EloquentDeliveryAddressRepository;
use App\Repositories\Client\Member\Transactions\EloquentTransactionRepository;
use App\Repositories\Client\Member\Settings\EloquentEmergencyContactRepository;
use App\Repositories\Client\Member\Attendances\EloquentClassAttendanceRepository;
use App\Repositories\Client\Member\Notifications\EloquentNotificationLimitRepository;

class MemberRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MemberRepository::class, function () {
            return new EloquentMemberRepository(new Member());
        });

        $this->app->singleton(MemberBranchRepository::class, function () {
            return new EloquentMemberBranchRepository(new MemberBranch());
        });

        $this->app->singleton(MemberPackageRepository::class, function () {
            return new EloquentMemberPackageRepository(new MemberPackage());
        });

        $this->app->singleton(DeliveryAddressRepository::class, function () {
            return new EloquentDeliveryAddressRepository(new DeliveryAddress());
        });

        $this->app->singleton(TransactionRepository::class, function () {
            return new EloquentTransactionRepository(new Transaction());
        });

        $this->app->singleton(ClassAttendanceRepository::class, function () {
            return new EloquentClassAttendanceRepository(new ClassAttendance());
        });

        $this->app->singleton(QuestionAnswerRepository::class, function () {
            return new EloquentQuestionAnswerRepository(new QuestionAnswer());
        });

        $this->app->singleton(EmergencyContactRepository::class, function () {
            return new EloquentEmergencyContactRepository(new EmergencyContact());
        });

        $this->app->singleton(NotificationLimitRepository::class, function () {
            return new EloquentNotificationLimitRepository(new NotificationLimit());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function provides()
    {
        return [
            MemberRepository::class,
            MemberBranchRepository::class,
            MemberPackageRepository::class,
            DeliveryAddressRepository::class,
            TransactionRepository::class,
            ClassAttendanceRepository::class,
            QuestionAnswerRepository::class,
            EmergencyContactRepository::class,
            NotificationLimitRepository::class,
        ];
    }
}
