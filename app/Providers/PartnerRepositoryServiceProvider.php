<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PartnerRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return void
     */
    public function provides()
    {
        //
    }
}
