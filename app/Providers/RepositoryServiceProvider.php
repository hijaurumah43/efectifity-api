<?php

namespace App\Providers;

use App\Models\User;
use App\Models\LoginHistory;
use App\Contracts\UserRepository;
use App\Models\Common\Address\City;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;
use App\Models\Common\Service\Service;
use Illuminate\Support\ServiceProvider;
use App\Contracts\LoginHistoryRepository;
use App\Models\Common\Branch\BranchClass;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Common\Payment\PaymentType;
use App\Models\Internal\Question\Question;
use App\Models\Common\Branch\BranchPackage;
use App\Models\Client\Staff\StaffAttendance;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Payment\PaymentMethod;
use App\Repositories\EloquentUserRepository;
use App\Models\Common\Company\CompanyService;
use App\Models\Client\Partner\Bank\MemberBank;
use App\Models\Internal\Question\QuestionType;
use App\Models\Internal\Question\QuestionGroup;
use App\Contracts\Common\Address\CityRepository;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Internal\Question\QuestionDetail;
use App\Contracts\Client\Member\MemberRepository;
use App\Contracts\Common\Branch\BranchRepository;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Internal\Question\QuestionCategory;
use App\Contracts\Common\Company\CompanyRepository;
use App\Contracts\Common\Service\ServiceRepository;
use App\Models\Common\Branch\BranchOperationalTime;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Repositories\EloquentLoginHistoryRepository;
use App\Contracts\Common\Branch\BranchClassRepository;
// MEMBER //
use App\Models\Client\Member\Questions\QuestionAnswer;
use App\Models\Client\Member\Settings\DeliveryAddress;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Contracts\Common\Payment\PaymentTypeRepository;
use App\Contracts\Internal\Question\QuestionRepository;
use App\Models\Client\Member\Settings\EmergencyContact;
use App\Contracts\Common\Branch\BranchPackageRepository;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Contracts\Common\Branch\BranchScheduleRepository;
use App\Contracts\Common\Payment\PaymentMethodRepository;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Contracts\Common\Company\CompanyServiceRepository;
use App\Contracts\Client\Partner\Bank\MemberBankRepository;
use App\Contracts\Internal\Question\QuestionTypeRepository;
use App\Repositories\Common\Address\EloquentCityRepository;
use App\Contracts\Internal\Question\QuestionGroupRepository;
use App\Models\Client\Member\Transactions\TransactionStatus;
use App\Repositories\Client\Member\EloquentMemberRepository;
use App\Repositories\Common\Branch\EloquentBranchRepository;
use App\Contracts\Internal\Question\QuestionDetailRepository;
use App\Models\Client\Member\Notifications\NotificationLimit;
use App\Repositories\Common\Company\EloquentCompanyRepository;
use App\Repositories\Common\Service\EloquentServiceRepository;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Contracts\Common\Branch\BranchScheduleDetailRepository;
// STAFF //
use App\Contracts\Internal\Question\QuestionCategoryRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleRepository;
use App\Contracts\Common\Branch\BranchOperationalTimeRepository;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;
use App\Repositories\Common\Branch\EloquentBranchClassRepository;
use App\Repositories\Client\Staff\EloquentStaffScheduleRepository;
use App\Repositories\Common\Payment\EloquentPaymentTypeRepository;
use App\Repositories\Internal\Question\EloquentQuestionRepository;
// INTERNAL //
use App\Contracts\Client\Member\Questions\QuestionAnswerRepository;
use App\Contracts\Client\Member\Settings\DeliveryAddressRepository;
use App\Contracts\Client\Member\Transactions\TransactionRepository;
use App\Contracts\Client\Partner\Company\CompanyDocumentRepository;
use App\Repositories\Common\Branch\EloquentBranchPackageRepository;
use App\Contracts\Client\Member\Settings\EmergencyContactRepository;
use App\Contracts\Client\Staff\Attendance\StaffAttendanceRepository;
use App\Repositories\Common\Branch\EloquentBranchScheduleRepository;
use App\Repositories\Common\Payment\EloquentPaymentMethodRepository;
use App\Contracts\Client\Staff\Attendance\MemberAttendanceRepository;
use App\Repositories\Common\Company\EloquentCompanyServiceRepository;
use App\Contracts\Client\Member\Attendances\ClassAttendanceRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleDetailRepository;
use App\Repositories\Client\Partner\Bank\EloquentMemberBankRepository;
use App\Repositories\Internal\Question\EloquentQuestionTypeRepository;
use App\Repositories\Internal\Question\EloquentQuestionGroupRepository;
use App\Repositories\Client\Staff\EloquentStaffScheduleDetailRepository;
use App\Repositories\Internal\Question\EloquentQuestionDetailRepository;
use App\Contracts\Client\Member\Transactions\TransactionStatusRepository;
use App\Contracts\Client\Member\Notifications\NotificationLimitRepository;
use App\Repositories\Client\Member\Classes\EloquentMemberBranchRepository;
use App\Repositories\Common\Branch\EloquentBranchScheduleDetailRepository;
use App\Repositories\Internal\Question\EloquentQuestionCategoryRepository;
use App\Repositories\Common\Branch\EloquentBranchOperationalTimeRepository;
use App\Repositories\Client\Member\Programs\EloquentMemberPackageRepository;
use App\Repositories\Client\Member\Questions\EloquentQuestionAnswerRepository;
use App\Repositories\Client\Member\Settings\EloquentDeliveryAddressRepository;
use App\Repositories\Client\Member\Transactions\EloquentTransactionRepository;
use App\Repositories\Client\Partner\Company\EloquentCompanyDocumentRepository;
use App\Repositories\Client\Member\Settings\EloquentEmergencyContactRepository;
use App\Repositories\Client\Staff\Attendance\EloquentStaffAttendanceRepository;
use App\Repositories\Client\Staff\Attendance\EloquentMemberAttendanceRepository;
use App\Repositories\Client\Member\Attendances\EloquentClassAttendanceRepository;
use App\Repositories\Client\Member\Transactions\EloquentTransactionStatusRepository;
use App\Repositories\Client\Member\Notifications\EloquentNotificationLimitRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LoginHistoryRepository::class, function () {
            return new EloquentLoginHistoryRepository(new LoginHistory());
        });

        $this->app->singleton(UserRepository::class, function () {
            return new EloquentUserRepository(new User());
        });

        $this->app->singleton(ServiceRepository::class, function () {
            return new EloquentServiceRepository(new Service());
        });

        $this->app->singleton(CompanyRepository::class, function () {
            return new EloquentCompanyRepository(new Company());
        });

        $this->app->singleton(CompanyServiceRepository::class, function () {
            return new EloquentCompanyServiceRepository(new CompanyService());
        });

        $this->app->singleton(BranchRepository::class, function () {
            return new EloquentBranchRepository(new Branch());
        });

        $this->app->singleton(BranchOperationalTimeRepository::class, function () {
            return new EloquentBranchOperationalTimeRepository(new BranchOperationalTime());
        });

        $this->app->singleton(BranchScheduleRepository::class, function () {
            return new EloquentBranchScheduleRepository(new BranchSchedule());
        });

        $this->app->singleton(BranchPackageRepository::class, function () {
            return new EloquentBranchPackageRepository(new BranchPackage());
        });

        $this->app->singleton(PaymentTypeRepository::class, function () {
            return new EloquentPaymentTypeRepository(new PaymentType());
        });

        $this->app->singleton(BranchClassRepository::class, function () {
            return new EloquentBranchClassRepository(new BranchClass());
        });

        $this->app->singleton(BranchScheduleDetailRepository::class, function () {
            return new EloquentBranchScheduleDetailRepository(new BranchScheduleDetail());
        });

        $this->app->singleton(PaymentMethodRepository::class, function () {
            return new EloquentPaymentMethodRepository(new PaymentMethod());
        });

        // MEMBER //
        $this->app->singleton(MemberRepository::class, function () {
            return new EloquentMemberRepository(new Member());
        });

        $this->app->singleton(MemberBranchRepository::class, function () {
            return new EloquentMemberBranchRepository(new MemberBranch());
        });

        $this->app->singleton(MemberPackageRepository::class, function () {
            return new EloquentMemberPackageRepository(new MemberPackage());
        });

        $this->app->singleton(DeliveryAddressRepository::class, function () {
            return new EloquentDeliveryAddressRepository(new DeliveryAddress());
        });

        $this->app->singleton(TransactionRepository::class, function () {
            return new EloquentTransactionRepository(new Transaction());
        });

        $this->app->singleton(ClassAttendanceRepository::class, function () {
            return new EloquentClassAttendanceRepository(new ClassAttendance());
        });

        $this->app->singleton(QuestionAnswerRepository::class, function () {
            return new EloquentQuestionAnswerRepository(new QuestionAnswer());
        });

        $this->app->singleton(EmergencyContactRepository::class, function () {
            return new EloquentEmergencyContactRepository(new EmergencyContact());
        });

        $this->app->singleton(NotificationLimitRepository::class, function () {
            return new EloquentNotificationLimitRepository(new NotificationLimit());
        });

        // STAFF //
        $this->app->singleton(StaffScheduleRepository::class, function () {
            return new EloquentStaffScheduleRepository(new StaffSchedule());
        });

        $this->app->singleton(StaffScheduleDetailRepository::class, function () {
            return new EloquentStaffScheduleDetailRepository(new StaffScheduleDetail());
        });

        $this->app->singleton(StaffAttendanceRepository::class, function () {
            return new EloquentStaffAttendanceRepository(new StaffAttendance());
        });

        $this->app->singleton(MemberAttendanceRepository::class, function () {
            return new EloquentMemberAttendanceRepository(new MemberAttendance());
        });

        // PARTNER //
        $this->app->singleton(CompanyDocumentRepository::class, function () {
            return new EloquentCompanyDocumentRepository(new CompanyDocument());
        });

        $this->app->singleton(MemberBankRepository::class, function () {
            return new EloquentMemberBankRepository(new MemberBank());
        });

        // INTERNAL //
        $this->app->singleton(QuestionTypeRepository::class, function () {
            return new EloquentQuestionTypeRepository(new QuestionType());
        });

        $this->app->singleton(QuestionCategoryRepository::class, function () {
            return new EloquentQuestionCategoryRepository(new QuestionCategory());
        });

        $this->app->singleton(QuestionRepository::class, function () {
            return new EloquentQuestionRepository(new Question());
        });

        $this->app->singleton(QuestionDetailRepository::class, function () {
            return new EloquentQuestionDetailRepository(new QuestionDetail());
        });

        $this->app->singleton(QuestionGroupRepository::class, function () {
            return new EloquentQuestionGroupRepository(new QuestionGroup());
        });

        $this->app->singleton(CityRepository::class, function () {
            return new EloquentCityRepository(new City());
        });

        $this->app->singleton(TransactionStatusRepository::class, function () {
            return new EloquentTransactionStatusRepository(new TransactionStatus());
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            LoginHistoryRepository::class,
            UserRepository::class,
            ServiceRepository::class,
            CompanyRepository::class,
            CompanyServiceRepository::class,  
            BranchRepository::class,
            BranchOperationalTimeRepository::class,
            BranchScheduleRepository::class,
            BranchPackageRepository::class,
            PaymentTypeRepository::class,
            BranchClassRepository::class,
            BranchScheduleDetailRepository::class,
            PaymentMethodRepository::class,
            // MEMBER //
            MemberRepository::class,
            MemberBranchRepository::class,
            MemberPackageRepository::class,
            DeliveryAddressRepository::class,
            TransactionRepository::class,
            ClassAttendanceRepository::class,
            QuestionAnswerRepository::class,
            EmergencyContactRepository::class,
            NotificationLimitRepository::class,
            // STAFF //
            StaffScheduleRepository::class,
            StaffScheduleDetailRepository::class,
            StaffAttendanceRepository::class,
            MemberAttendanceRepository::class,
            // PARTNER //
            CompanyDocumentRepository::class,
            MemberBankRepository::class,
            // INTERNAL //
            QuestionTypeRepository::class,
            QuestionCategoryRepository::class,
            QuestionRepository::class,
            QuestionDetailRepository::class,
            QuestionGroupRepository::class,
            CityRepository::class,
            TransactionStatusRepository::class,
        ];
    }
}
