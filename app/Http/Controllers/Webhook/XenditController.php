<?php

namespace App\Http\Controllers\Webhook;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use Berkayk\OneSignal\OneSignalClient;
use App\Models\Client\Staff\PersonalTrainer;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\PersonalTrainerMember;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Partner\Branch\BranchAgreement;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Staff\Transaction\TransactionDetail;
use App\Models\WithdrawalLog;

class XenditController extends Controller
{

    public function disbursement()
    {
        $data  = file_get_contents("php://input");
        // $data = file_get_contents('http://localhost/wd.json');
        $result = json_decode($data, true);

        $id = $result['external_id'];
        // $id = $result['reference_id'];
        $transaction = WithdrawalLog::where('order_number', $id)->first();
        if (!$transaction) {
            return $this->respondWithCustomData(['message' => 'transaction not found'], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        if ($transaction->is_callback == 1) {
            return $this->respondWithCustomData(["message" => "Transaction has been callback"], Response::HTTP_BAD_REQUEST);
        }

        // COMPLETED
        $status = 1;
        $isFailed = 0;

        if (strtolower($result['status']) !=  strtolower('COMPLETED')) {

            // FAILED
            // $status = 2;
            $isFailed = 1;
        }

        $transaction->callback_values = $result;
        $transaction->callback_at = date('Y-m-d H:i:s');
        $transaction->is_callback = 1;
        $transaction->save();

        $transactionDetail = TransactionDetail::where([
            ['order_number', $transaction->order_number],
            ['withdrawal_log_id', $transaction->id]
        ])->first();

        if (!$transactionDetail) {
            return $this->respondWithCustomData(["message" => "Order Number or Withdrawal history not found"], Response::HTTP_BAD_REQUEST);
        }

        $transactionDetail->status = $status;
        $transactionDetail->is_failed = $isFailed;
        $transactionDetail->save();

        // Store Income
        /* $income = new TransactionDetail();
        $income->branch_id = $transaction->branch_id;
        $income->order_number = $transaction->order_number;
        $income->transaction_type_id = 4;
        $income->status = 1;
        $income->amount = $transaction->amount;
        $income->is_published = 1;
        $income->withdrawal_log_id = $transaction->id;
        $income->save(); */

        DB::commit();
        return $this->respondWithCustomData(["message" => "Success"], Response::HTTP_OK, 'success');
    }

    public function va()
    {
        $data  = file_get_contents("php://input");
        $result = json_decode($data, true);

        $id = $result['external_id'];
        $transaction = Transaction::where('id', $id)->firstOrFail();

        DB::beginTransaction();
        if ($transaction->is_callback == 1) {
            return $this->respondWithCustomData(["message" => "Transaction has been callback"], Response::HTTP_BAD_REQUEST, 'success');
        }

        $transaction->callback_values = $result;
        $transaction->callback_at = date('Y-m-d H:i:s');
        $transaction->transaction_status_id = 2;
        $transaction->is_callback = 1;
        $transaction->save();

        // Store Income
        $income = new TransactionDetail();
        $income->transaction_id = $transaction->id;
        $income->order_number = $transaction->order_number;
        $income->member_id = $transaction->member_id;
        $income->branch_id = $transaction->branch_id;
        $income->service_id = $transaction->branch->service_id;
        $income->transaction_type_id = 1;
        $income->status = 0;
        $income->amount = $transaction->total_payment;
        $income->is_published = 1;
        $income->save();

        $duration = $transaction->branchPackage->duration;
        $periode = $transaction->branchPackage->periode;

        $expiryDate = date('Y-m-d H:i:s', strtotime('+' . $duration . ' ' . $periode . ''));
        // $expiryDate = date($expiryDate, strtotime('+7 hour'));
        $expiryDate = date($expiryDate);

        $branch_agreement_id = NULL;
        $ba = BranchAgreement::where([
            ['is_published', 1],
            ['branch_id', $transaction->branch_id],
            ['is_draft', 0],
        ])->orderBy('created_at', 'DESC')->first();

        if ($ba) {
            $branch_agreement_id = $ba->id;
        }

        $mp = MemberPackage::create([
            'member_id' => $transaction->member_id,
            'transaction_id' => $transaction->id,
            'expiry_date' => $expiryDate,
            'branch_id' => $transaction->branch_id,
            'branch_agreement_id' => $branch_agreement_id,
            'branch_package_id' => $transaction->branch_package_id,
            'branch_agreement_id' => $transaction->branch_agreement_id,
            'session' => $transaction->branchPackage->session,
            'session_remaining' => $transaction->branchPackage->session,
            'pt_session' => $transaction->branchPackage->pt_session,
            'pt_session_remaining' => $transaction->branchPackage->pt_session,
        ]);

        if ($transaction->branch_schedule_detail_id != NULL) {

            // Get Schedule ID
            $bsd = BranchScheduleDetail::find($transaction->branch_schedule_detail_id);

            // Store Class
            $att = new ClassAttendance();
            $att->member_package_id = $mp->id;
            $att->member_id = $mp->member_id;
            $att->branch_id = $mp->branch_id;
            $att->branch_schedule_id = $bsd->branch_schedule_id;
            $att->branch_schedule_detail_id = $transaction->branch_schedule_detail_id;
            $att->save();

            if ($transaction->personal_trainer_id == NULL) {
                // Kurangin MemberPackage
                $mpdecrement = MemberPackage::find($mp->id);
                $mpdecrement->decrement('session_remaining', 1);
                $mpdecrement->save();
            }
        }

        $package = $transaction->branchPackage;
        if ($package->pt_session > 0) {
            $ptMp = MemberPackage::find($mp->id);
            PersonalTrainerMember::create([
                'personal_trainer_id' => $transaction->personal_trainer_id,
                'member_id' => $transaction->member_id,
                'member_package_id' => $ptMp->id,
            ]);
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Success"], Response::HTTP_OK, 'success');
    }

    public function eWallets()
    {
        $data  = file_get_contents("php://input");
        // $data = file_get_contents('http://localhost/dana.json');
        $body = json_decode($data, true);
        $result = $body['data'];

        if (strtolower($result['status']) !=  strtolower('SUCCEEDED')) {
            return $this->respondWithCustomData(["message" => "Transaction status not successded"], Response::HTTP_BAD_REQUEST, 'failed');
        }
        $id = $result['reference_id'];
        $transaction = Transaction::where('id', $id)->firstOrFail();

        DB::beginTransaction();
        if ($transaction->is_callback == 1) {
            return $this->respondWithCustomData(["message" => "Transaction has been callback"], Response::HTTP_BAD_REQUEST, 'failed');
        }

        $transaction->callback_values = $body;
        $transaction->callback_at = date('Y-m-d H:i:s');
        $transaction->transaction_status_id = 2;
        $transaction->is_callback = 1;
        $transaction->save();

        // Store Income
        $income = new TransactionDetail();
        $income->transaction_id = $transaction->id;
        $income->order_number = $transaction->order_number;
        $income->member_id = $transaction->member_id;
        $income->branch_id = $transaction->branch_id;
        $income->service_id = $transaction->branch->service_id;
        $income->transaction_type_id = 1;
        $income->status = 0;
        $income->amount = $transaction->total_payment;
        $income->is_published = 1;
        $income->save();

        $duration = $transaction->branchPackage->duration;
        $periode = $transaction->branchPackage->periode;

        $expiryDate = date('Y-m-d H:i:s', strtotime('+' . $duration . ' ' . $periode . ''));
        // $expiryDate = date($expiryDate, strtotime('+7 hour'));
        $expiryDate = date($expiryDate);

        $mp = MemberPackage::create([
            'member_id' => $transaction->member_id,
            'transaction_id' => $transaction->id,
            'branch_id' => $transaction->branch_id,
            'expiry_date' => $expiryDate,
            'branch_package_id' => $transaction->branch_package_id,
            'branch_agreement_id' => $transaction->branch_agreement_id,
            'session' => $transaction->branchPackage->session,
            'session_remaining' => $transaction->branchPackage->session,
            'pt_session' => $transaction->branchPackage->pt_session,
            'pt_session_remaining' => $transaction->branchPackage->pt_session,
        ]);

        if ($transaction->branch_schedule_detail_id != NULL) {

            // Get Schedule ID
            $bsd = BranchScheduleDetail::find($transaction->branch_schedule_detail_id);

            // Store Class
            $att = new ClassAttendance();
            $att->member_package_id = $mp->id;
            $att->member_id = $mp->member_id;
            $att->branch_id = $mp->branch_id;
            $att->branch_schedule_id = $bsd->branch_schedule_id;
            $att->branch_schedule_detail_id = $transaction->branch_schedule_detail_id;
            $att->save();

            if ($transaction->personal_trainer_id == NULL) {
                // Kurangin MemberPackage
                $mpdecrement = MemberPackage::find($mp->id);
                $mpdecrement->decrement('session_remaining', 1);
                $mpdecrement->save();
            }
        }

        $package = $transaction->branchPackage;
        if ($package->pt_session > 0) {
            $ptMp = MemberPackage::find($mp->id);
            PersonalTrainerMember::create([
                'personal_trainer_id' => $transaction->personal_trainer_id,
                'member_id' => $transaction->member_id,
                'member_package_id' => $ptMp->id,
            ]);
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Success"], Response::HTTP_OK, 'success');
    }

    public function ovo()
    {
        $data  = file_get_contents("php://input");
        $body = json_decode($data, true);
        $result = $body;

        if (strtolower($result['status']) !=  strtolower('COMPLETED')) {
            return $this->respondWithCustomData(["message" => "Transaction status not successded"], Response::HTTP_BAD_REQUEST, 'failed');
        }
        $id = $result['external_id'];
        $transaction = Transaction::where('id', $id)->firstOrFail();

        DB::beginTransaction();
        if ($transaction->is_callback == 1) {
            return $this->respondWithCustomData(["message" => "Transaction has been callback"], Response::HTTP_BAD_REQUEST, 'failed');
        }

        $transaction->callback_values = $body;
        $transaction->callback_at = date('Y-m-d H:i:s');
        $transaction->transaction_status_id = 2;
        $transaction->is_callback = 1;
        $transaction->save();

        // Store Income
        $income = new TransactionDetail();
        $income->transaction_id = $transaction->id;
        $income->order_number = $transaction->order_number;
        $income->member_id = $transaction->member_id;
        $income->branch_id = $transaction->branch_id;
        $income->service_id = $transaction->branch->service_id;
        $income->transaction_type_id = 1;
        $income->status = 0;
        $income->amount = $transaction->total_payment;
        $income->is_published = 1;
        $income->save();

        $duration = $transaction->branchPackage->duration;
        $periode = $transaction->branchPackage->periode;

        $expiryDate = date('Y-m-d H:i:s', strtotime('+' . $duration . ' ' . $periode . ''));
        // $expiryDate = date($expiryDate, strtotime('+7 hour'));
        $expiryDate = date($expiryDate);

        $mp = MemberPackage::create([
            'member_id' => $transaction->member_id,
            'transaction_id' => $transaction->id,
            'branch_id' => $transaction->branch_id,
            'expiry_date' => $expiryDate,
            'branch_package_id' => $transaction->branch_package_id,
            'branch_agreement_id' => $transaction->branch_agreement_id,
            'session' => $transaction->branchPackage->session,
            'session_remaining' => $transaction->branchPackage->session,
            'pt_session' => $transaction->branchPackage->pt_session,
            'pt_session_remaining' => $transaction->branchPackage->pt_session,
        ]);

        if ($transaction->branch_schedule_detail_id != NULL) {

            // Get Schedule ID
            $bsd = BranchScheduleDetail::find($transaction->branch_schedule_detail_id);

            // Store Class
            $att = new ClassAttendance();
            $att->member_package_id = $mp->id;
            $att->member_id = $mp->member_id;
            $att->branch_id = $mp->branch_id;
            $att->branch_schedule_id = $bsd->branch_schedule_id;
            $att->branch_schedule_detail_id = $transaction->branch_schedule_detail_id;
            $att->save();

            if ($transaction->personal_trainer_id == NULL) {
                // Kurangin MemberPackage
                $mpdecrement = MemberPackage::find($mp->id);
                $mpdecrement->decrement('session_remaining', 1);
                $mpdecrement->save();
            }
        }

        $package = $transaction->branchPackage;
        if ($package->pt_session > 0) {
            $ptMp = MemberPackage::find($mp->id);
            PersonalTrainerMember::create([
                'personal_trainer_id' => $transaction->personal_trainer_id,
                'member_id' => $transaction->member_id,
                'member_package_id' => $ptMp->id,
            ]);
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Success"], Response::HTTP_OK, 'success');
    }

    public function qrCode()
    {
        return 200;
    }
}
