<?php

namespace App\Http\Controllers\User\Account;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\Account\AccountAccessCollection;
use App\Http\Resources\User\Account\AccountAccessResource;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;
use App\Models\Common\Service\Service;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = AccountAccessCollection::class;
        $this->resourceItem = AccountAccessResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function access()
    {
        $user = auth()->user();
        $collection = [];
        $owner = Company::where([
            'user_id' => $user->id
        ])->first();

        // return $owner;

        if ($owner) {

            $collection[] = [
                'userID' => $owner->user_id,
                'title' => strtoupper($owner->title),
                'logo' => $owner->image,
                'type' => 'FITBIZ',
                'link' => 'https://fitbiz.efectifity.com'
            ];

            $companyDocuments = $owner->document->where('status', 1);

            foreach ($companyDocuments as $item) {
                $collection[] = [
                    'userID' => $item->user_id,
                    'title' => strtoupper($item->company->title),
                    'logo' => strtoupper($item->company->image),
                    'type' => strtoupper($item->service->title),
                    'link' => 'https://' . $item->service->slug . '.efectifity.com',
                ];
            }
        } else {
            $collections = MemberBranch::where([
                'member_id' => $user->member->id,
                'is_published' => 1,
                'is_active' => 1,
            ])
                ->select('branch_id')
                ->get();

            $services =  Branch::whereIn('id', $collections)->distinct()->select('service_id', 'company_id')->get();
            foreach ($services as $item) {
                $collection[] = [
                    'title' => strtoupper($item->company->title),
                    'logo' => strtoupper($item->company->image),
                    'type' => strtoupper($item->service->title),
                    'link' => 'https://' . $item->service->slug . '.efectifity.com',
                ];
            }
        }
        return $this->respondWithCustomData($collection, Response::HTTP_OK, 'success');
    }
}
