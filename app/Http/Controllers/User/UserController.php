<?php

namespace App\Http\Controllers\User;

use Exception;
use App\Models\User;
use App\Contracts\UserRepository;
use App\Http\Controllers\Controller;
use App\Notifications\ResetPinNotification;
use Illuminate\Support\Facades\Notification;
use App\Http\Requests\User\UserUsernameUpdate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\User\UserPinCreateRequest;
use App\Http\Requests\User\UserPinUpdateRequest;
use App\Models\Client\Member\Notifications\NotificationLimit;
use App\Contracts\Client\Member\Notifications\NotificationLimitRepository;
use Carbon\Carbon;

class UserController extends Controller
{
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Check Token Pin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPin($token)
    {
        $user = auth()->user();

        try {
            $user = app(UserRepository::class)->findOneBy(['id' => $user->id, 'pin_token_confirmation' => $token]);
        } catch (Exception $exception) {
            $message = __('Invalid token for pin verification');

            return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
        }

        if ($user) {
            $message = __('PIN successfully verified');
            $data['pin'] = NULL;
            $data['pin_token_confirmation'] = NULL;
            $this->userRepository->update($user, $data);
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, "success");
        }

        $message = __('Invalid token for pin verification');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
    }

    /**
     * Create user Pin
     *
     * @param UserPinCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setPin(UserPinCreateRequest $request)
    {
        $user = auth()->user();
        $user = $this->userRepository->findOneById($user->id);
        $data = $request->validated();

        $this->userRepository->update($user, $data);
        return $this->respondWithCustomData([
            'message' => 'PIN Successfuly Created!'
        ], Response::HTTP_CREATED, 'success');
    }

    /**
     * Change user pin
     *
     * @param UserPinUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePin(UserPinUpdateRequest $request)
    {
        $user = auth()->user();
        if ($user->pin != $request->current_pin) {
            return $this->respondWithCustomData(['message' => 'Wrong PIN, please re-enter!'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $user = $this->userRepository->findOneById($user->id);
        $data = $request->validated();

        $this->userRepository->update($user, $data);
        return $this->respondWithCustomData(['message' => 'PIN Successfuly Changed!'], Response::HTTP_CREATED, 'success');
    }

    /**
     * Send Email Reset PIN
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEmailResetPin()
    {
        $user = auth()->user();
        $user = $this->userRepository->findOneById($user->id);

        $notificationLimit = NotificationLimit::whereDate('created_at', date('Y-m-d'))->where([
            ['user_id', $user->id],
            ['type', 2],
        ])->count();

        if ($notificationLimit > 0) {
            return $this->respondWithCustomData(['message' => 'Request has been limited'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        };

        $data['pin_token_confirmation'] = rand(000000, 999999);

        $data['value'] = $data['pin_token_confirmation'];
        $data['user_id'] = $user->id;
        $data['type'] = 2;

        app(NotificationLimitRepository::class)->store($data);
        $user = $this->userRepository->update($user, $data);

        Notification::route('mail', $user->email)->notify(new ResetPinNotification($user));

        return $this->respondWithCustomData(['message' => 'Email sent, please check your email to continue'], Response::HTTP_OK, 'success');
    }

    /**
     * Send Phone Notification 
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function phoneNotification()
    {
        $user = auth()->user();

        if ($user->phone == '') {
            return $this->respondWithCustomData(['message' => 'Please insert your phone number'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $notificationLimit = NotificationLimit::whereDate('created_at', date('Y-m-d'))->where([
            ['user_id', $user->id],
            ['type', 1],
        ])->count();

        if ($notificationLimit > 0) {
            return $this->respondWithCustomData(['message' => 'Request has been limited'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        };

        $user = $this->userRepository->findOneById($user->id);
        $data['phone_token_confirmation'] = random_int(100000, 999999);

        $data['value'] = $data['phone_token_confirmation'];
        $data['user_id'] = $user->id;
        $data['type'] = 1;

        app(NotificationLimitRepository::class)->store($data);
        $user = $this->userRepository->update($user, $data);

        $status = sendPhoneNotification($user->phone, $data['phone_token_confirmation']);

        if ($status == 'false') {
            return $this->respondWithCustomData(['message' => 'something wrong'], Response::HTTP_INTERNAL_SERVER_ERROR, 'error');
        }

        return $this->respondWithCustomData(['message' => 'success'], Response::HTTP_OK, 'success');
    }

    private function sendPhoneNotificationOld($to, $content)
    {
        // Change to +62
        if (substr($to, 0, 1) == '0') {
            $from = '/' . preg_quote(substr($to, 0, 1), '/') . '/';
            $to = preg_replace($from, '62', $to, 1);
        } else if (substr($to, 0, 2) == '62') {
            $to = $to;
        } else {
            $to = '62' . $to;
        }

        $to       = str_replace('+', '', $to);
        $from     = "EFECTIFITY"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        $message  = "Halo Fitters! Jangan informasikan angka berikut ini kepada siapapun : " . $content . " - Layanan CS : 021-29385381";
        $username = "mask_efectifity"; //your username
        $password = "123456"; //your password
        $getUrl   = "http://websms.tcastsms.net/sendsms?";
        $apiUrl   = $getUrl . 'account=' . $username . '&password=' . $password . '&sender=' . $from . '&numbers=' . $to . '&content=' . rawurlencode($message);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Accept:application/json'
            )
        );

        $response = curl_exec($ch);
        return $response;
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $responseBody = json_decode($response, true);

        curl_close($ch);

        return $responseBody;
    }

    /**
     * Check Token Otp
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPhone($token)
    {
        $user = auth()->user();

        try {
            $user = app(UserRepository::class)->findOneBy(['id' => $user->id, 'phone_token_confirmation' => $token]);
        } catch (Exception $exception) {
            $message = __('Invalid token for phone verification');

            return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
        }

        if ($user) {
            $message = __('Mobile number successfully verified!');
            $this->userRepository->update($user, ['phone_verified' => 1]);
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, "success");
        }

        $message = __('Invalid token for phone verification');
        return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
    }

    public function checkPin($pin)
    {
        $user = auth()->user();
        try {
            $user = app(UserRepository::class)->findOneBy(['id' => $user->id, 'pin' => $pin]);
        } catch (Exception $exception) {
            $message = __('Invalid pin');

            return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
        }

        if ($user) {
            $message = __('ok');
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, "success");
        }

        $message = __('Invalid pin');
        return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
    }

    /**
     * Update username
     *
     * @param UserUsernameUpdate $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUsername(UserUsernameUpdate $request)
    {
        $user = auth()->user();
        // $lastUpdate = date('Y-m-d');

        // Check user
        $users = User::where('id', '!=', $user->id)->get();
        foreach ($users as $item) {
            if ($item->username == $request->username) {
                return $this->respondWithCustomData(['message' => 'Sorry, This Username Has Taken'],  Response::HTTP_OK);
            }
        }
        // Get User
        $user = User::where('id', $user->id)->first();

        // Check 14 days last update
        if ($user->username_change_at != NULL) {
            $lastUpdate = $user->username_change_at;
            $lastUpdate = Carbon::parse($lastUpdate)->addDay(14)->toDateString();

            if ($lastUpdate >= date('Y-m-d')) {
                return $this->respondWithCustomData(['message' => 'Sorry, You have to wait 14 days to change your username'],  Response::HTTP_OK);
            }
        }

        $user->username = $request->username;
        $user->username_change_at = date('Y-m-d');
        $user->save();

        return $this->respondWithCustomData(['message' => 'Successfully Updated'],  Response::HTTP_OK);
    }
}
