<?php

namespace App\Http\Controllers\Client\Staff;

use App\Http\Controllers\Controller;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Contracts\Client\Staff\Schedule\StaffScheduleDetailRepository;
use App\Http\Resources\Client\Staff\Schedule\StaffScheduleDetailResource;
use App\Http\Resources\Client\Staff\Schedule\StaffScheduleDetailCollection;

class StaffScheduleDetailController extends Controller
{
    private $staffScheduleDetailRepository;

    /**
     * @param StaffScheduleDetailRepository $staffScheduleDetailRepository
     */
    public function __construct(StaffScheduleDetailRepository $staffScheduleDetailRepository)
    {
        $this->staffScheduleDetailRepository = $staffScheduleDetailRepository;
        $this->resourceCollection = StaffScheduleDetailCollection::class;
        $this->resourceItem = StaffScheduleDetailResource::class;
    }

    /**
     * @param StaffScheduleDetail $staffScheduleDetail
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(StaffScheduleDetail $staffScheduleDetail)
    {
        return $this->respondWithItem($staffScheduleDetail);
    }
}
