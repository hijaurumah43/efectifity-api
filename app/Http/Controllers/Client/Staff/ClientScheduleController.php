<?php

namespace App\Http\Controllers\Client\Staff;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\PT\SessionDeduct;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Events\Member\PT\PtAttendanceEvent;
use App\Models\Client\Member\Log\MemberLog;
use App\Models\Client\Staff\PersonalTrainer;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Staff\PersonalTrainerMember;
use App\Events\Member\Classes\ClassAttendanceEvent;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Http\Requests\Client\Staff\PT\SessionDeductRequest;
use App\Http\Requests\Client\Staff\PT\SummaryUpdateRequest;

class ClientScheduleController extends Controller
{
    public function __construct()
    { }

    /**
     * Display a listing of the resource
     *
     * @param int $member_branch_id
     * @param int $month
     * @param int $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($member_branch_id, $month, $year)
    {
        $user = auth()->user();

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        // Find Collection
        $collections = PersonalTrainerAttendance::where([
            ['personal_trainer_id', $pt->id],
            // ['is_active', 1],
        ])
            ->whereIn('personal_trainer_attendance_status_id', [1, 2, 3, 5])
            ->whereMonth('date', $month)
            ->whereYear('date', $year)
            ->get()->groupBy(function ($query) {
                return Carbon::parse($query->date)->format('Y-m-d');
            });

        $tempCollection = [];
        foreach ($collections as $key => $collection) {
            if ($key >= date('Y-m-d')) {
                $tempItem = [];
                foreach ($collection as $item) {
                    $time = Carbon::parse($item->start_time)->format('H.i') . '-' . Carbon::parse($item->end_time)->format('H.i');
                    $tempItem[] = [
                        'packageID' => $item->member_package_id,
                        'member' => [
                            'avatar' => Member::avatarStorage($item->member->avatar),
                            'firstName' => ucwords($item->member->name),
                            'lastName' => ucwords($item->member->last_name),
                        ],
                        'status' => $this->statusPT($item->personal_trainer_attendance_status_id),
                        'date' => $time,
                    ];
                }

                $today = $key == date('Y-m-d') ? 'today' : Carbon::parse($key)->format('D');

                $tempCollection[] = [
                    'day' => $today,
                    'date' => Carbon::parse($key)->format('d F Y'),
                    'collection' => $tempItem
                ];

                $tempItem = [];
            }
        }

        return $this->respondWithCustomData($tempCollection, Response::HTTP_OK);
    }

    /**
     * Display a listing of current membership personal trainer
     *
     * @param int $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function currentClient($member_branch_id)
    {
        $user = auth()->user();

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        $members = PersonalTrainerMember::where([
            ['personal_trainer_id', $pt->id],
            ['status', 1]
        ])->get();

        $collection = [];
        if (count($members) > 0) {

            foreach ($members as $item) {
                $collection[] = [
                    'packageID' => $item->member_package_id,
                    'member' => [
                        'avatar' => Member::avatarStorage($item->member->avatar),
                        'firstName' => ucwords($item->member->name),
                        'lastName' => ucwords($item->member->last_name),
                    ],
                    'package' => [
                        'packageID' => $item->member_package_id,
                        'title' => ucwords($item->memberPackage->package->title),
                        'date' => Carbon::parse($item->memberPackage->created_at)->format('d F Y') . ' - ' . Carbon::parse($item->memberPackage->expiry_date)->format('d F Y'),
                        'usage' => (int) ($item->memberPackage->pt_session - $item->memberPackage->pt_session_remaining),
                        'total' => $item->memberPackage->pt_session,
                    ],
                ];
            }
        }

        return $this->respondWithCustomData($collection, Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $member_branch_id
     * @param int $memberPackageID
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($member_branch_id, $memberPackageID)
    {
        $user = auth()->user();

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        $resource = PersonalTrainerMember::where([
            ['member_package_id', $memberPackageID],
            ['status', 1],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => "resource not found"], Response::HTTP_NOT_FOUND);
        }

        $records = PersonalTrainerAttendance::where([
            ['member_id', $resource->member->id],
            ['member_package_id', $resource->member_package_id],
            // ['is_active', 1],
        ])
            ->whereIn('personal_trainer_attendance_status_id', [1, 2, 3, 5])
            ->orderBy('date', 'DESC')->get();

        $collection = [];
        if (count($records) > 0) {
            foreach ($records as $item) {
                $collection[] = [
                    'attendanceID' => $item->id,
                    'date' => Carbon::parse($item->date)->format('d F Y'),
                    'time' => $item->start_time . ' - ' . $item->end_time,
                    'status' => $this->statusPT($item->personal_trainer_attendance_status_id),
                ];
            }
        }

        $checked = false;
        // CheckIn
        $memberAtt = MemberAttendance::where('member_id', $resource->member_id)->orderBy('time', 'DESC')->first();
        if (isset($memberAtt)) {
            if ($memberAtt->status == 1 && Carbon::parse($memberAtt->time)->toDateString() == date('Y-m-d')) {
                $checked = true;
            }
        }

        $data = [
            'checkedIn' => $checked,
            'member' => [
                'avatar' => Member::avatarStorage($resource->member->avatar),
                'firstName' => ucwords($resource->member->name),
                'lastName' => ucwords($resource->member->last_name),
            ],
            'package' => [
                'packageID' => $resource->member_package_id,
                'title' => ucwords($resource->memberPackage->package->title),
                'date' => Carbon::parse($resource->memberPackage->created_at)->format('d F Y') . ' - ' . Carbon::parse($resource->memberPackage->expiry_date)->format('d F Y'),
                'usage' => (int) ($resource->memberPackage->pt_session - $resource->memberPackage->pt_session_remaining),
                'total' => $resource->memberPackage->pt_session,
            ],
            'sessionRecord' => $collection
        ];

        return $this->respondWithCustomData($data, Response::HTTP_OK);
    }

    /**
     * Display a specified session
     *
     * @param int $member_branch_id
     * @param int $attendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function session($member_branch_id, $attendanceID)
    {
        $user = auth()->user();

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        $resource = PersonalTrainerAttendance::where([
            ['id', $attendanceID],
            // ['is_active', 1],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => "resource not found"], Response::HTTP_NOT_FOUND);
        }


        $data = [
            'id' => $resource->id,
            'date' => Carbon::parse($resource->date)->format('d F Y'),
            'time' => $resource->start_time . ' - ' . $resource->end_time,
            'status' => $this->statusPT($resource->personal_trainer_attendance_status_id),
            'description' => $resource->note,
            'isActive' => $resource->personal_trainer_attendance_status_id == 1 ? true : false,
        ];

        return $this->respondWithCustomData($data, Response::HTTP_OK);
    }

    /**
     * get Pin
     *
     * @param int $member_branch_id
     * @param int $attendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPin($member_branch_id, $attendanceID)
    {
        $user = auth()->user();

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        $resource = PersonalTrainerAttendance::where([
            ['id', $attendanceID],
            ['personal_trainer_attendance_status_id', 1],
            ['is_active', 1],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => "resource not found"], Response::HTTP_NOT_FOUND);
        }

        // Generate PIN
        $resource->pin = random_int(100000, 999999);;
        $resource->save();

        event(new SessionDeduct($resource));

        return $this->respondWithCustomData(['message' =>  'ok'], Response::HTTP_OK);
    }

    /**
     * get Pin
     *
     * @param int $member_branch_id
     * @param int $attendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(Request $request, $branch_id, $attendanceID)
    {
        $user = auth()->user();

        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['branch_id', $branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        DB::beginTransaction();
        $resource = PersonalTrainerAttendance::where([
            ['id', $attendanceID],
            ['is_active', 1],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => "resource not found"], Response::HTTP_NOT_FOUND);
        }

        // GET RULES
        $branchRules = Branch::rules($branch_id, 3);
        // NOTE:: SET TIME
        $maxScanTime = '';
        $delayTime = '';
        // $now = date('Y-m-d H:i:s');

        foreach ($branchRules as $rule) {
            if ($rule['id'] == 10) {
                $maxScanTime = $rule['duration'];
                $maxScanTimePeriode = strtolower(ruleNamePeriode($rule['period']));
            }
        }

        // debug
        // $date = date('Y-m-d H:i:s');
        // $timeClient = Carbon::parse($date)->addHour(7)->format("Y-m-d H:i:s");
        // $timeClient = ConvertToTimestamp($timeClient);
        $timeClient = $request->timestamp;
        $timeClient =  ConvertToTimeServer($timeClient);

        // $now = date('Y-m-d H:i:s');
        $formatTimeClient = Carbon::createFromFormat('Y-m-d H:i:s', $timeClient);
        $formatTimeClient =  $formatTimeClient->format('Y-m-d');

        $classTime = $resource->start_time;
        $classTimeFormat = Carbon::parse($formatTimeClient.' ' . $classTime . '.00')->format('Y-m-d H:i:s');

        // Convert to date format
        $delayTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '+' . $maxScanTime . ' ' . $maxScanTimePeriode));
        $now = $timeClient;

        // Disabled class
        if ($now <= $delayTime) {

            $resource->user_id = $user->id;
            $resource->is_active = 0;
            $resource->scan_time = date('Y-m-d H:i:s');
            $resource->personal_trainer_attendance_status_id = 2;
            $resource->save();

            /**
             * History
             */
            $ptName = $resource->pt->member->name . ' ' . $resource->pt->member->last_name;
            $ptBranch = $resource->branch->title;
            $message = 'Enjoy your Personal Training session with ' . ucwords($ptName) . ' at ' . ucwords($ptBranch);
            $memberLog = MemberLog::create([
                'member_id' => $resource->member_id,
                'branch_id' => $resource->branch_id,
                'personal_trainer_attendance_id' => $resource->id,
                'status' => 4, // 4 = attend classes
                'message' => $message,
            ]);

            $dataFb = [
                'slug' => 'upcoming',
                'param' => [
                    'key' => 'status',
                    'value' =>  'expired' //$memberLog->personal_trainer_attendance_id,
                ]
            ];
            event(new PtAttendanceEvent($memberLog));
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

            DB::commit();

            return $this->respondWithCustomData(['message' =>  'Personal Trainer Attended'], Response::HTTP_OK);
        } else {

            DB::commit();

            return $this->respondWithCustomData(['message' =>  'PT Session Closed. Your arrival is exceeded the scan latest time at ' . $maxScanTime . ' ' . $maxScanTimePeriode], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Update Summary
     *
     * @param SummaryUpdateRequest $request
     * @param int $member_branch_id
     * @param int $attendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function summary(SummaryUpdateRequest $request, $member_branch_id, $attendanceID)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        // Find Branch
        $branch = MemberBranch::where([
            ['member_id', $user->member->id],
            ['id', $member_branch_id]
        ])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => "you're not staff our branch"], Response::HTTP_NOT_FOUND);
        }

        // Find PT
        $pt = PersonalTrainer::where([
            ['status', 1],
            ['is_published', 1],
            ['branch_id', $branch->branch_id],
            ['member_id', $user->member->id],
        ])->first();
        if (!$pt) {
            return $this->respondWithCustomData(['message' => "you're not personal trainer our branch"], Response::HTTP_FORBIDDEN);
        }

        $resource = PersonalTrainerAttendance::where([
            ['id', $attendanceID],
            ['personal_trainer_attendance_status_id', 2],
            // ['is_active', 1],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => "resource not found"], Response::HTTP_NOT_FOUND);
        }

        // Generate PIN
        $resource->note = $data['note'];
        $resource->save();

        return $this->respondWithCustomData(['message' =>  'ok'], Response::HTTP_OK);
    }

    private function statusPT($status)
    {
        switch ($status) {
            case '1':
                return 'attend';
                break;

            case '2':
                return 'attended';
                break;

            case '3':
                return 'not attended';
                break;

            case '4':
                return 'reschedule';
                break;

            case '5':
                return 'cancelled';
                break;

            default:
                return 'error';
                break;
        }
    }
}
