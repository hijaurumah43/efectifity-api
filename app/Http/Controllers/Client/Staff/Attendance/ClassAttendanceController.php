<?php

namespace App\Http\Controllers\Client\Staff\Attendance;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Client\Member\MemberRepository;
use App\Events\Member\Classes\ClassAttendanceEvent;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Contracts\Common\Branch\BranchScheduleRepository;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Contracts\Client\Staff\Attendance\MemberAttendanceRepository;
use App\Contracts\Client\Member\Attendances\ClassAttendanceRepository;
use App\Http\Resources\Client\Staff\Attendance\ClassAttendanceResource;
use App\Http\Resources\Client\Staff\Attendance\ClassAttendanceCollection;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Branch\BranchScheduleDetail;

class ClassAttendanceController extends Controller
{
    private $classAttendanceRepository;

    /**
     * @param ClassAttendanceRepository $classAttendanceRepository
     */
    public function __construct(ClassAttendanceRepository $classAttendanceRepository)
    {
        $this->classAttendanceRepository = $classAttendanceRepository;
        $this->resourceCollection = ClassAttendanceCollection::class;
        $this->resourceItem = ClassAttendanceResource::class;
    }

    /**
     * Scanned Member
     *
     * @param int $branch_id
     * @param string $identitify
     * @return \Illuminate\Http\JsonResponse
     */
    public function scanned($branch_id, $identitify)
    {
        $user = auth()->user();

        // type {barcode / phone}
        $type = (string) request()->get('type');
        switch ($type) {
            case 'phone':
                $phone = $this->phoneFormat($identitify);
                $data = User::where('phone', $phone)->firstOrFail();
                $member_id = $data->member->id;
                break;

            case 'barcode':
                $member_id = $identitify;
                break;

            default:
                return $this->respondWithCustomData(['message' => 'Type for identitify is not available'], Response::HTTP_NOT_FOUND, 'failed');
                break;
        }
        // Check karyawan bukan
        $memberBranch = app(MemberBranchRepository::class)->checkBy([
            'member_id' => $user->member->id,
            'branch_id' => $branch_id,
        ]);

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'You are not own staff!'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $isDisabled = true;
        $expired = false;


        $timeZone = getIp();
        $now = date('Y-m-d');
        $temp = [];

        // Check Class
        // Branch & Schedule
        $nowTimeStamp = ConvertToTimestamp($now . '00:00:00');
        /* $branchSchedule = app(BranchScheduleRepository::class)->checkBy([
            'branch_id' => $branch_id,
            'date' => date("Y-m-d"),
        ]);
         */
        $branchSchedule = BranchSchedule::where('branch_id', $branch_id)->where('timestamp', '>=', $nowTimeStamp)->orderBy('timestamp', 'asc')->first();

        if ($branchSchedule) {
            // return $this->respondWithCustomData(['message' => 'Not Today!'], Response::HTTP_NOT_FOUND, 'failed');
            $collections = ClassAttendance::where([
                'member_id' => $member_id,
                'branch_id' => $branch_id,
                'branch_schedule_id' => $branchSchedule->id,
            ])
                ->whereIn('class_attendance_status_id', [1, 2, 3, 4])
                ->whereHas('scheduleDetail', function ($q) {
                    return $q->orderBy('start_time', 'ASC');
                })
                ->get();

            foreach ($collections as $collection) {

                // CEK CLASS
                $typeRule = 1;
                $class = $collection->scheduleDetail->class;
                if ($class->is_online == 1) {
                    $typeRule = 2;
                }

                // GET RULES
                $branchRules = Branch::rules($branch_id, $typeRule);
                // NOTE:: SET TIME
                $maxScanTime = '';
                $delayTime = '';

                foreach ($branchRules as $rule) {
                    if ($rule['id'] == 5 || $rule['id'] == 15) {
                        $maxScanTime = $rule['duration'];
                        $maxScanTimePeriode = strtolower(ruleNamePeriode($rule['period']));
                    }

                    if ($rule['id'] == 4 || $rule['id'] == 14) {
                        $delayTime = $rule['duration'];
                        $delayTimePeriode = strtolower(ruleNamePeriode($rule['period']));
                    }
                }

                $classStatus = $collection->class_attendance_status_id;
                // $delayTime = $collection->scheduleDetail->class->delay_time;

                // Convert to date format of resource time
                $classTime = $collection->scheduleDetail->start_time;
                $classTimeFormat = Carbon::parse((date('Y-m-d ' . $classTime . '.00')))->format('Y-m-d H:i:s');

                // Convert to date format
                $maxScanTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $maxScanTime . ' ' . $maxScanTimePeriode));
                $delayTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '+' . $delayTime . ' ' . $delayTimePeriode));

                // Disabled class
                if ($now >= $maxScanTime && $now <= $delayTime) {
                    $isDisabled = false;
                }

                // Expired
                if ($now > $delayTime) {
                    $expired = true;
                }

                if ($classStatus == 2 || $classStatus == 3 || $classStatus == 4) {
                    $isDisabled = true;
                    $expired = true;
                }

                $classCoach = NULL;
                if ($class->branchCoach) {
                    $coachMember = $class->branchCoach->first()->member;
                    $classCoach = ucwords($coachMember->name . ' ' . $coachMember->last_name);
                }

                $temp[] = [
                    'id' => $collection->id,
                    'time' => $classTime,
                    'title' => ucwords($class->title),
                    'branch' => [
                        'id' => $collection->branch->id,
                        'title' => $collection->branch->title,
                    ],
                    'scannedAt' => $collection->scan_time != null ? 'Check In at Class : ' . Carbon::parse($collection->scan_time)->format('H.i') : null,
                    'scannedAtTime' => $collection->scan_time != null ? ConvertToTimestamp($collection->scan_time) : null,
                    'isDisabled' => $isDisabled,
                    'expired' => $expired,
                    'status' => Self::statusClassAttendance($collection->class_attendance_status_id),
                    'coach' => $classCoach,
                    'cancelled' => $classStatus == 3 ? true : false,
                    'type' => [
                        'isClass' => true,
                        'isPt' => false,
                    ],
                ];
            }
        }


        // PT Attendance
        $pTcollections = PersonalTrainerAttendance::where([
            ['member_id', $member_id],
            // ['date', date('Y-m-d')],
            ['branch_id', $branch_id],
        ])
            ->where('end_timestamp', '>=', $nowTimeStamp)
            ->whereIn('personal_trainer_attendance_status_id', [1, 2, 3, 5])
            ->orderBy('start_time', 'ASC')
            ->get();

        $pTtemp = [];
        foreach ($pTcollections as $item) {
            $isDisabledPT = true;
            $expiredPT = false;

            // GET RULES
            $branchRules = Branch::rules($branch_id, 3);
            // NOTE:: SET TIME
            $maxScanTime = '';
            $delayTime = '';

            foreach ($branchRules as $rule) {
                if ($rule['id'] == 10) {
                    $maxScanTime = $rule['duration'];
                    $maxScanTimePeriode = strtolower(ruleNamePeriode($rule['period']));
                }
            }

            $classStatus = $item->personal_trainer_attendance_status_id;
            $classTime = $item->start_time;
            $classTimeFormat = Carbon::parse((date('Y-m-d ' . $classTime . '.00')))->format('Y-m-d H:i:s');

            // Convert to date format
            $delayTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '+' . $maxScanTime . ' ' . $maxScanTimePeriode));

            // Disabled class
            if ($now <= $delayTime) {
                $isDisabledPT = false;
            }

            // Expired
            if ($now > $delayTime) {
                $expiredPT = true;
            }

            if ($classStatus > 1 && $classStatus < 7) {
                $isDisabled = true;
                $expiredPT = true;
            }

            $pTtemp[] = [
                'id' => $item->id,
                'time' => $item->start_time,
                'title' => 'Personal Training',
                'branch' => [
                    'id' => $item->branch->id,
                    'title' => $item->branch->title,
                ],
                'scannedAt' => $item->scan_time != null ? 'Check In at Class : ' . Carbon::parse($item->scan_time)->format('H.i') : null,
                'scannedAtTime' => $item->scan_time != null ? ConvertToTimestamp($item->scan_time) : null,
                'isDisabled' => $isDisabledPT,
                'expired' => $expiredPT,
                'status' => Self::statusPtClassAttendance($item->personal_trainer_attendance_status_id),
                'coach' => $item->pt->member->name . ' ' . $item->pt->member->last_name,
                'cancelled' => $item->personal_trainer_attendance_status_id == 5 ? true : false,
                'type' => [
                    'isClass' => false,
                    'isPt' => true,
                ],
            ];
        }

        $collection = collect([$temp, $pTtemp])->sortBy('time');
        $collection = $collection->collapse();
        $sorted = $collection->sortBy('time');
        $temp = $sorted->values()->all();

        $member = app(MemberRepository::class)->findOneById($member_id);
        $log = app(MemberAttendanceRepository::class)->checkBy([
            'member_id' => $member_id,
            'branch_id' => $branch_id,
        ]);

        $message = NULL;
        $status = NULL;

        if (isset($log) && $log->status == 1) {
            $message    = __(
                'Check In at :branch. :time',
                // ['branch' => $log->branch->service->title, 'time' => date("H.i", strtotime($log->created_at . '+7 hour'))]
                ['branch' => $log->branch->service->title, 'time' => date("H.i", strtotime($log->created_at))]
            );
            $status = true;
            $logTime = $log->created_at;
        }

        // ACTIVE PROGRAM
        $tempPrograms = [];
        $memberPrograms = MemberPackage::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id]
        ])
            ->where('expiry_date', '>=', date('Y-m-d H:i:s'))
            // ->where('session_remaining', '>', 0)
            ->orderBy('expiry_date', 'ASC')
            ->get();

        foreach ($memberPrograms as $memberProgram) {
            $tempPrograms[] = [
                'id' => $memberProgram->id,
                'title' => $memberProgram->package->title,
                "type" => ucwords($memberProgram->branch->service->title),
                "gymAccess" => $memberProgram->package->gym_access == 1 ? true : false,
                "classSessions" => $memberProgram->session_remaining . " of " . $memberProgram->session,
                "isUnlimited" => $memberProgram->package->isUnlimited == 1 ? true : false,
                "ptSessions" => ($memberProgram->pt_session - $memberProgram->pt_session_remaining) . " of " . $memberProgram->pt_session,
                'expiryDate' => Carbon::parse($memberProgram->created_at)->format('Y-m-d') . ' until ' . Carbon::parse($memberProgram->expiry_date)->format('Y-m-d'),
                'startDate' => ConvertToTimestamp($memberProgram->created_at),
                'ednDate' => ConvertToTimestamp($memberProgram->expiry_date),
            ];
        }

        // LAST VISIT
        $tempLastVisit = [];
        $yesterday = date("Y-m-d", strtotime('-1 days'));
        $memberAttendances = MemberAttendance::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id],
        ])
            ->whereDate('created_at', $yesterday)
            ->get();

        $lastVisit = [];
        if (count($memberAttendances) < 1) {
            $dateLastVisit  = Carbon::parse($yesterday)->format('Y-m-d H:i:s');
            $lastVisit = [
                'status' => 0,
                // 'date' => Carbon::parse($yesterday)->format('D, d F Y'),
                'date' => ConvertToTimestamp($dateLastVisit),
                'data' => $tempLastVisit
            ];
        } else {
            foreach ($memberAttendances as $memberAttendance) {

                $lastVisitScannedBy = 'By System';
                if (isset($memberAttendance->user)) {
                    $lastVisitScannedBy = 'Scan By ' . ucwords($memberAttendance->user->member->name);
                }

                $tempLastVisit[] = [
                    // 'time' => Carbon::parse($memberAttendance->time)->format('H.i'),
                    'time' => ConvertToTimestamp($memberAttendance->time),
                    'type' => $memberAttendance->status == 1 ? 'Check in' : 'Check out',
                    'scannedBy' => $lastVisitScannedBy,
                ];
            }

            $dateLastVisit  = Carbon::parse($yesterday)->format('Y-m-d H:i:s');
            $lastVisit = [
                'status' => 1,
                // 'date' => Carbon::parse($yesterday)->format('D, d F Y'),
                'date' => ConvertToTimestamp($dateLastVisit),
                'data' => $tempLastVisit
            ];
        }

        // TODAY VISIT
        $tempTodayVisit = [];
        $memberTodayAttendances = MemberAttendance::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id],
        ])
            ->whereDate('created_at', date("Y-m-d"))
            ->get();

        $todayVisit = [];
        if (count($memberTodayAttendances) < 1) {
            $dateTodayVisit  = Carbon::now()->format('Y-m-d H:i:s');
            $todayVisit = [
                'status' => 0,
                // 'date' => Carbon::now()->format('D, d F Y'),
                'date' => ConvertToTimestamp($dateTodayVisit),
                'data' => $tempTodayVisit
            ];
        } else {
            foreach ($memberTodayAttendances as $memberTodayAttendance) {

                $TodayVisitScannedBy = 'By System';
                if (isset($memberTodayAttendance->user)) {
                    $TodayVisitScannedBy = 'Scan By ' . ucwords($memberTodayAttendance->user->member->name);
                }

                $tempTodayVisit[] = [
                    // 'time' => Carbon::parse($memberTodayAttendance->time)->format('H.i'),
                    'time' => ConvertToTimestamp($memberTodayAttendance->time),
                    'type' => $memberTodayAttendance->status == 1 ? 'Check in' : 'Check out',
                    'scannedBy' => $TodayVisitScannedBy,
                ];

                $status = $memberTodayAttendance->status;
            }

            $dateTodayVisit  = Carbon::now()->format('Y-m-d H:i:s');
            $todayVisit = [
                'status' => $status,
                // 'date' => Carbon::now()->format('D, d F Y'),
                'date' => ConvertToTimestamp($dateTodayVisit),
                'data' => $tempTodayVisit
            ];
        }

        // $temp = collect($temp)->sortBy('time');

        return $this->respondWithCustomData([
            'member' => [
                'id' => $member->id,
                'membership' => $member->member_id,
                'name' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
                'phone' => $member->user->phone,
                'email' => $member->user->email,
            ],
            'branchId' => $branch_id,
            'class' => $temp,
            'log' => [
                'message' => $message,
                'messageTime' => isset($logTime) ? ConvertToTimestamp($logTime) : null,
                'status' => $status
            ],
            'todayVisit' => $todayVisit,
            'lastVisit' => $lastVisit,
            'activeProgram' => $tempPrograms
        ]);
    }

    public function update(Request $request, $branch_id, $classAttendance)
    {
        // Employee
        $user = auth()->user();

        $classAttendance = $this->classAttendanceRepository->findOneBy([
            'id' => $classAttendance,
            'class_attendance_status_id' => 1,
        ]);

        // CEK CLASS
        $typeRule = 1;
        $class = $classAttendance->scheduleDetail->class;
        if ($class->is_online == 1) {
            $typeRule = 2;
        }

        // GET RULES
        $branchRules = Branch::rules($branch_id, $typeRule);
        // NOTE:: SET TIME
        $maxScanTime = '';
        $delayTime = '';

        foreach ($branchRules as $rule) {
            if ($rule['id'] == 5 || $rule['id'] == 15) {
                $maxScanTime = $rule['duration'];
                $maxScanTimePeriode = strtolower(ruleNamePeriode($rule['period']));
                $maxScanTimeNotif = $rule['duration'];
                $maxScanTimePeriodeNotif = strtolower(ruleNamePeriode($rule['period']));
            }

            if ($rule['id'] == 4 || $rule['id'] == 14) {
                $delayTime = $rule['duration'];
                $delayTimePeriode = strtolower(ruleNamePeriode($rule['period']));
                $delayTimeNotif = $rule['duration'];
                $delayTimePeriodeNotif = strtolower(ruleNamePeriode($rule['period']));
            }
        }

        // debug
        // $date = date('Y-m-d H:i:s');
        // $timeClient = Carbon::parse($date)->addHour(7)->format("Y-m-d H:i:s");
        // $timeClient = ConvertToTimestamp($timeClient);
        $timeClient = $request->timestamp;
        $timeClient =  ConvertToTimeServer($timeClient);

        // $now = date('Y-m-d H:i:s');
        $formatTimeClient = Carbon::createFromFormat('Y-m-d H:i:s', $timeClient);
        $formatTimeClient =  $formatTimeClient->format('Y-m-d');


        $classTime = $classAttendance->scheduleDetail->start_time;
        // OLD
        // $classTimeFormat = Carbon::parse((date('Y-m-d ' . $classTime . '.00')))->format('Y-m-d H:i:s');

        $classTimeFormat = Carbon::parse($formatTimeClient . ' ' . $classTime . '.00')->format('Y-m-d H:i:s');
        $maxScanTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $maxScanTime . ' ' . $maxScanTimePeriode));
        $delayTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '+' . $delayTime . ' ' . $delayTimePeriode));


        $memberBranch = app(MemberBranchRepository::class)->checkBy([
            'member_id' => $user->member->id,
            'branch_id' => $branch_id,
        ]);

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'You are not own staff!'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $now = $timeClient;
        // return $now .' / '. $maxScanTime .' / '. $delayTime;
        // Disabled class
        if ($now >= $maxScanTime && $now <= $delayTime) {

            $data['user_id'] = $user->id;
            $data['class_attendance_status_id'] = 2;
            $data['is_active'] = 0;
            $data['scan_time'] = date('Y-m-d H:i:s');
            DB::beginTransaction();
            $this->classAttendanceRepository->update($classAttendance, $data);
            /**
             * History
             */
            $className = $classAttendance->scheduleDetail->class->title;
            $classCoach = $classAttendance->scheduleDetail->class->branchCoach->first()->member->name . ' ' . $classAttendance->scheduleDetail->class->branchCoach->first()->member->last_name;
            $classBranch = $classAttendance->branch->title;
            $message = "Enjoy your " . ucwords($className) . " class with " . ucwords($classCoach) . " at " . ucwords($classBranch);
            $memberLog = MemberLog::create([
                'member_id' => $classAttendance->member_id,
                'branch_id' => $classAttendance->branch_id,
                'class_attendance_id' => $classAttendance->id,
                'status' => 4, // 4 = attend classes
                'message' => $message,
            ]);

            $dataFb = [
                'slug' => 'upcoming',
                'param' => [
                    'key' => 'status',
                    'value' => 'expired',
                ]
            ];
            event(new ClassAttendanceEvent($memberLog));
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

            DB::commit();

            return $this->respondWithCustomData(['message' => 'Class has been scanned'], Response::HTTP_CREATED, 'success');
        } else {


            $messageExpired = 'Class attendance closed. Open from ' . $maxScanTimeNotif . ' ' . $maxScanTimePeriodeNotif . ' before class started until ' . $delayTimeNotif . ' ' . $delayTimePeriodeNotif . ' after the class started';
            // Expired
            if ($now > $delayTime) {
                return $this->respondWithCustomData(['message' => $messageExpired], Response::HTTP_BAD_REQUEST, 'failed');
            }
            return $this->respondWithCustomData([
                'message' => $messageExpired
            ], Response::HTTP_BAD_REQUEST, 'failed');
        }
    }

    /**
     * Change phone to format +62
     *
     * @param string $phone
     * @return void
     */
    private function phoneFormat($phone)
    {
        if (substr($phone, 0, 1) == '0') {
            $from = '/' . preg_quote(substr($phone, 0, 1), '/') . '/';
            $phone = preg_replace($from, '62', $phone, 1);
        } else if (substr($phone, 0, 2) == '62') {
            $phone = $phone;
        } else {
            $phone = preg_replace('/[^0-9]/', '', $phone);
            if (substr($phone, 0, 2) == '62') {
                $phone = $phone;
            } else {
                $phone = '62' . $phone;
            }
        }
        return $phone;
    }

    private function statusClassAttendance($id)
    {
        switch ($id) {
            case '1':
                $status = 'attend';
                break;
            case '2':
                $status = 'attended';
                break;
            case '3':
                $status = 'canceled';
                break;
            case '4':
                $status = 'not attended';
                break;


            default:
                $status = 'error';
                break;
        }

        return $status;
    }

    private function statusPtClassAttendance($id)
    {
        switch ($id) {
            case '1':
                $status = 'upcoming';
                break;
            case '2':
                $status = 'attended';
                break;
            case '3':
                $status = 'not attended';
                break;
            case '4':
                $status = 'rescheduled';
                break;
            case '5':
                $status = 'canceled';
                break;
            case '6':
                $status = 'started';
                break;
            case '7':
                $status = 'pending';
            case '8':
                $status = 'rejected';
                break;


            default:
                $status = 'error';
                break;
        }

        return $status;
    }
}
