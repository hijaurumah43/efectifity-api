<?php

namespace App\Http\Controllers\Client\Staff\Attendance;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Services\MemberBranchService;
use App\Models\Client\Staff\StaffAttendance;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleRepository;
use App\Contracts\Client\Staff\Attendance\StaffAttendanceRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleDetailRepository;
use App\Http\Requests\Client\Staff\Attendance\StaffAttendanceCreateRequest;
use App\Http\Requests\Client\Staff\Attendance\StaffAttendanceUpdateRequest;

class StaffAttendanceController extends Controller
{
    private $staffAttendanceRepository;

    /**
     * @param StaffAttendanceRepository $staffAttendanceRepository
     */
    public function __construct(StaffAttendanceRepository $staffAttendanceRepository)
    {
        $this->staffAttendanceRepository = $staffAttendanceRepository;
    }

    public function index(Request $request)
    {
        $user = auth()->user();

        $st = StaffAttendance::where([
            ['member_id', $user->member->id],
        ])->paginate();
    }

    /**
     * Store a newly resource
     *
     * @param StaffAttendanceCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clockin(StaffAttendanceCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        // Check last status 
        $lastStatus = $this->staffAttendanceRepository->checkBy([
            'member_id' => $user->member->id,
            'member_branch_id' => $request->member_branch_id,
        ]);

        if (isset($lastStatus) && $lastStatus->status == 1) {
            return $this->respondWithCustomData(['message' => 'Access Denied, Logout first'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Check member branch 
        $memberBranch = app(MemberBranchRepository::class)->findOneBy([
            'member_id' => $user->member->id,
            'id' => $request->member_branch_id,
            'is_active' => 1,
        ]);

        // return $memberBranch->branch->title;
        // return $memberBranch->branch->longitude;

        // Range of attendance
        $memberArea = $this->location($memberBranch->branch->id, $data['latitude'], $data['longitude']);
        // return $memberArea;
        // return count($memberArea);

        if (count($memberArea) < 1) {
            return $this->respondWithCustomData(['message' => 'Access Denied, You’re too Far away'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Check Schedule aktif
        /* $schedule = app(StaffScheduleRepository::class)->checkBy([
            // 'date' => date('Y-m-d'),
            'is_published' => 1,
            'member_branch_id' => $memberBranch->id,
        ]);
        if (!$schedule) {
            return $this->respondWithCustomData(['message' => 'Access Denied, You’re not have schedule'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        } */

        // Check Schedule on the day 
        // $scheduleDetail = app(StaffScheduleDetailRepository::class)->checkBy(['staff_schedule_id' => $schedule->id]);
        // $scheduleDetail = StaffScheduleDetail::where('staff_schedule_id', $schedule->id)->whereDate('start_time', date('Y-m-d'))->whereNull('note')->first();
        // $scheduleDetail = StaffScheduleDetail::where('member_branch_id', $memberBranch->id)->whereDate('start_time', date('Y-m-d'))->whereNull('note')->where('is_published', 1)->first();
        $date = date('Y-m-d');
        $nowTimeStamp = ConvertToTimestamp($date);
        $scheduleDetail = StaffScheduleDetail::where('member_branch_id', $memberBranch->id)
        ->where('end_timestamp', '>=', $nowTimeStamp)
        ->whereNull('note')->where('is_published', 1)->first();

        if (!$scheduleDetail) {
            return $this->respondWithCustomData(['message' => 'Access Denied, You’re on day off'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Check Schedule Time
        // TODO:: re-check
        /**
         * case GMT+7
         * staff punya jadwal, 14.00 - 18.00
         * staff check in di 15.00
         * 
         * di server ngecek,
         * jam sekarang ketika staff klik tombol checkin. 15.00 (-7 jam) = 08.00 (waktu server)
         * 08.00 >= 18.00 (seharusnya angka 18.00 disesuaikan jadi waktu server)
         */

        $now = date('Y-m-d H:i:s');
        $maxAttendance = date("Y-m-d H:i:s", strtotime($scheduleDetail->shift->end_time));

        if ($now >= $maxAttendance) {
            return $this->respondWithCustomData(['message' => 'Access Denied, Sign In Window Still Closed'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Temporary Time Zone
        DB::beginTransaction();
        $data['member_id'] = $user->member->id;
        $data['status'] = 1;
        $data['time'] = date('Y-m-d H:i:s');
        $data['checkin'] = date('Y-m-d H:i:s');
        $this->staffAttendanceRepository->store($data);

        /**
         * @param MemberBranchRepository $memberBranchRepository
         * @param Array $data
         */
        $arr['checkin'] = date('Y-m-d H:i:s');
        app(MemberBranchService::class)->update($memberBranch, $arr);

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Attendance is successfully'], Response::HTTP_CREATED, 'success');
    }

    private function location($branch_id, $latitude, $longitude)
    {
        $location =  DB::select("
            SELECT 
                ( 6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians(latitude) ) * cos( radians(longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians(latitude) ) ) ) AS distance
            FROM 
                branches 
            WHERE 
                id = " . $branch_id . "
            HAVING distance < 0.4
        ");

        return $location;
    }

    public function clockout(StaffAttendanceUpdateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        // Check last status 
        $lastStatus = $this->staffAttendanceRepository->checkBy([
            'member_id' => $user->member->id,
            'member_branch_id' => $request->member_branch_id,
        ]);
        if (isset($lastStatus) && $lastStatus->status == 0) {
            return $this->respondWithCustomData(['message' => 'Access Denied, Login first'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Check member branch 
        $memberBranch = app(MemberBranchRepository::class)->findOneBy([
            'member_id' => $user->member->id,
            'id' => $request->member_branch_id,
            'is_active' => 1,
        ]);

        // Check Status
        /* $sameDayAttendance = MemberBranch::where([
            ['member_id', $user->member->id],
            ['branch_id', $request->member_branch_id],
            ['is_active', 1],
        ])->whereDate('checkout', date('Y-m-d'))->count();
        if($sameDayAttendance > 0) {
            return $this->respondWithCustomData(['message' => 'Today, You have been out'], Response::HTTP_OK, 'success');
        } */

        // Temporary Time Zone
        DB::beginTransaction();
        $data['member_id'] =  $user->member->id;
        $data['status'] = 0;
        $data['time'] = date('Y-m-d H:i:s');
        $data['checkout'] = date('Y-m-d H:i:s');
        $this->staffAttendanceRepository->store($data);

        /**
         * @param MemberBranchRepository $memberBranchRepository
         * @param Array $data
         */
        $arr['checkout'] = date('Y-m-d H:i:s');
        app(MemberBranchService::class)->update($memberBranch, $arr);

        $scheduleDetail = StaffScheduleDetail::where('member_branch_id', $request->member_branch_id)
            ->whereDate('start_time', date('Y-m-d'))
            ->whereNull('note')
            ->where('is_published', 1)
            ->first();

        if ($scheduleDetail) {
            $endTime = Carbon::parse($scheduleDetail->end_time)->format('Y-m-d');
            $endTime = $endTime . ' ' . $scheduleDetail->shift->end_time . ':00';

            if (date('Y-m-d H:i:s') >= $endTime) {
                $scheduleDetail->note = 'Checkout by user';
                $scheduleDetail->is_published = 0;
                $scheduleDetail->save();
            }
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Today, You have been out'], Response::HTTP_CREATED, 'success');
    }

    public function status($member_branch_id)
    {
        $user = auth()->user();

        // Check member branch 
        $memberBranch = app(MemberBranchRepository::class)->findOneBy([
            'member_id' => $user->member->id,
            'id' => $member_branch_id,
            'is_active' => 1,
        ]);
        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Access Denied'], Response::HTTP_BAD_REQUEST, 'failed');
        }

        // Check last status 
        $lastStatus = $this->staffAttendanceRepository->findOneBy([
            'member_id' => $user->member->id,
            'member_branch_id' => $member_branch_id,
        ]);
        if (isset($lastStatus)) {
            if ($lastStatus->status == 1) {
                return $this->respondWithCustomData(['status' => true], Response::HTTP_OK, 'success');
            }

            return $this->respondWithCustomData(['status' => false], Response::HTTP_OK, 'success');
        }
    }

    public function staffScheduleActivity($member_branch_id)
    {
        $user = auth()->user();
        $checkMemberBranch = MemberBranch::where([
            ['id', $member_branch_id],
            ['member_id', $user->member->id],
        ])->first();

        if (!$checkMemberBranch) {
            return $this->respondWithCustomData(['message' => 'access denied'], Response::HTTP_FORBIDDEN);
        }

        $temp = [];
        $staffAttendances = StaffAttendance::where([
            ['member_id', $user->member->id],
            ['member_branch_id', $member_branch_id],
        ])
            ->whereDate('created_at', date('Y-m-d'))
            ->get();

        if (count($staffAttendances) < 1) {
            return $this->respondWithCustomData([
                'status' => 0,
                'date' => Carbon::parse(date('Y-m-d'))->format('D, d F Y'),
                'data' => $temp
            ], Response::HTTP_OK);
        }

        foreach ($staffAttendances as $item) {
            $temp[] = [
                // 'time' => Carbon::parse($item->time)->format('H.i'),
                'time' => ConvertToTimestamp($item->time),
                'type' => $item->status == 1 ? 'Clock in' : 'Clock out',
            ];
        }

        return $this->respondWithCustomData([
            'status' => 1,
            // 'date' => Carbon::parse(date('Y-m-d'))->format('D, d F Y'),
            'date' => ConvertToTimestamp(date('Y-m-d H:i:s')),
            'data' => $temp
        ], Response::HTTP_OK);
    }
}
