<?php

namespace App\Http\Controllers\Client\Staff\Attendance;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Events\Member\Attendance\MemberAttendanceClockInEvent;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Events\Member\Attendance\MemberAttendanceClockOutEvent;
use App\Contracts\Client\Staff\Attendance\MemberAttendanceRepository;
use App\Http\Requests\Client\Staff\Attendance\MemberAttendanceCreateRequest;

class MemberAttendanceController extends Controller
{
    private $memberAttendanceRepository;

    /**
     * @param MemberAttendanceRepository $memberAttendanceRepository
     */
    public function __construct(MemberAttendanceRepository $memberAttendanceRepository)
    {
        $this->memberAttendanceRepository = $memberAttendanceRepository;
    }

    /**
     * Store a newly resource
     *
     * @param MemberAttendanceCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clockin(MemberAttendanceCreateRequest $request)
    {
        // Employee
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        // Check karyawan bukan
        $memberBranch = app(MemberBranchRepository::class)->checkBy([
            'member_id' => $user->member->id,
            'branch_id' => $data['branch_id'],
        ]);
        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'You are not own staff!'], Response::HTTP_FORBIDDEN, 'failed');
        }

        // Check last status 
        // $lastStatus = $this->memberAttendanceRepository->checkBy([
        //     'member_id' => $data['member_id'],
        //     'branch_id' => $data['branch_id'],
        // ]);
        $lastStatus = MemberAttendance::where([
            ['member_id', $data['member_id']],
            ['branch_id', $data['branch_id']],
        ])
            ->whereDate('created_at', date("Y-m-d"))
            ->orderBy('created_at', "DESC")
            ->first();
        if (isset($lastStatus) && $lastStatus->status == 1) {
            return $this->respondWithCustomData(['message' => 'Access Denied, Logout first'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $data['status'] = 1;
        $data['time'] = date('Y-m-d H:i:s');
        $data['scanned_by'] = $user->id;

        DB::beginTransaction();
        $memebrAttendance = $this->memberAttendanceRepository->store($data);
        /**
         * Create History
         */
        $message = 'Happy to see you at ' . $memebrAttendance->branch->title . ' / CS: ' . $user->member->name;
        $memberLog = MemberLog::create([
            'member_id' => $memebrAttendance->member_id,
            'branch_id' => $memebrAttendance->branch_id,
            'member_attendance_id' => $memebrAttendance->id,
            'status' => 1, // 1 = check in
            'message' => $message,
        ]);

        $dataFb = [
            'slug' => 'notifications',
            'param' => NULL
        ];

        event(new MemberAttendanceClockInEvent($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

        DB::commit();

        $message    = __(
            'Check In at :branch. :time.',
            // ['branch' => $memebrAttendance->branch->service->title, 'time' => date('H.i', strtotime('+7 hour'))]
            ['branch' => $memebrAttendance->branch->service->title, 'time' => date('H.i')]
        );

        return $this->respondWithCustomData(['message' => $message, 'status' => true], Response::HTTP_CREATED, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param MemberAttendanceCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clockout(MemberAttendanceCreateRequest $request)
    {
        // Employee
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        // Check karyawan bukan
        $memberBranch = app(MemberBranchRepository::class)->checkBy([
            'member_id' => $user->member->id,
            'branch_id' => $data['branch_id'],
        ]);
        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'You are not own staff!'], Response::HTTP_FORBIDDEN, 'failed');
        }

        // Check last status 
        $lastStatus = $this->memberAttendanceRepository->checkBy([
            'member_id' => $data['member_id'],
            'branch_id' => $data['branch_id'],
        ]);
        if (isset($lastStatus) && $lastStatus->status == 0) {
            return $this->respondWithCustomData(['message' => 'Access Denied, Login first'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $data['status'] = 0;
        $data['time'] = date('Y-m-d H:i:s');
        $data['scanned_by'] = $user->id;

        DB::beginTransaction();
        $memebrAttendance = $this->memberAttendanceRepository->store($data);
        /**
         * Create History
         */
        $message = 'You’ve doing a great workout at ' . $memebrAttendance->branch->title . ', see you soon! / CS: ' . $user->member->name;
        $memberLog = MemberLog::create([
            'member_id' => $memebrAttendance->member_id,
            'branch_id' => $memebrAttendance->branch_id,
            'member_attendance_id' => $memebrAttendance->id,
            'status' => 2, // 2 = checkout
            'message' => $message,
        ]);

        $dataFb = [
            'slug' => 'notifications',
            'param' => NULL
        ];

        event(new MemberAttendanceClockOutEvent($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

        DB::commit();

        $message    = __(
            'Last Check Out from :branch. :time.',
            // ['branch' => $memebrAttendance->branch->service->title, 'time' => date('H.i', strtotime('+7 hour'))]
            ['branch' => $memebrAttendance->branch->service->title, 'time' => date('H.i')]
        );

        return $this->respondWithCustomData(['message' => $message, 'status' => false], Response::HTTP_CREATED, 'success');
    }
}
