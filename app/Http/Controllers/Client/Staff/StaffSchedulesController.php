<?php

namespace App\Http\Controllers\Client\Staff;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Client\Staff\Schedule;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffAttendance;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleRepository;
use App\Http\Resources\Client\Staff\Schedule\StaffScheduleResource;
use App\Http\Resources\Client\Staff\Schedule\StaffScheduleCollection;

class StaffSchedulesController extends Controller
{
    private $staffScheduleRepository;

    /**
     * @param StaffScheduleRepository $staffScheduleRepository
     */
    public function __construct(StaffScheduleRepository $staffScheduleRepository)
    {
        $this->staffScheduleRepository = $staffScheduleRepository;
        $this->resourceCollection = StaffScheduleCollection::class;
        $this->resourceItem = StaffScheduleResource::class;
        // $this->authorizeResource(MemberBranch::class);
    }

    /**
     * Display a lisitng of the resource
     *
     * @param $id $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($member_branch_id)
    {
        $user = auth()->user();
        app(MemberBranchRepository::class)->findOneBy(['id' => $member_branch_id, 'member_id' => $user->member->id]);
        $collection = $this->staffScheduleRepository->findByFilters([
            'member_branch_id' => $member_branch_id
        ]);

        return $this->respondWithCollection($collection);
    }

    public function store()
    {
        $user = auth()->user();
        return $user->member::location(1, -6.1596555, 106.8925028);

        return $user;
    }

    /**
     * Worker Schedule
     *
     * @param int $member_branch_id
     * @param int $month
     * @param int $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function workerSchedule($member_branch_id, $month, $year)
    {
        $user = auth()->user();

        $checkMemberBranch = MemberBranch::where([
            ['id', $member_branch_id],
            ['member_id', $user->member->id],
        ])->first();

        if (!$checkMemberBranch) {
            return $this->respondWithCustomData(['message' => 'access denied'], Response::HTTP_FORBIDDEN);
        }

        // check schedule
        // $schedules = StaffSchedule::where('member_branch_id', $member_branch_id)->where('is_published', 1)->first();
        $schedules = Schedule::where('month', $month)
            ->where('year', $year)
            ->where('status', 1)
            ->where('branch_id', $checkMemberBranch->branch_id)
            ->orderBy('month', 'ASC')
            ->first();

        if (!$schedules) {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'you dont have schedule'
            ], Response::HTTP_OK);
        }

        // check date avaibility
        $collections = StaffScheduleDetail::where([
            // ['staff_schedule_id', $schedules->id],
            ['schedule_id', $schedules->id],
            ['member_branch_id', $member_branch_id],
            ['is_published', 1],
        ])
            ->whereMonth('start_time', $month)
            ->whereYear('start_time', $year)
            // ->get();
            ->get()->groupBy(function ($q) {
                return Carbon::parse($q->start_time)->format('Y-m-d');
            });

        $tempCollection = [];
        foreach ($collections as $key => $collection) {

            if ($key >= date('Y-m-d')) {
                $tempItem = [];
                foreach ($collection as $item) {
                    $time = Carbon::parse($item->start_time)->format('H.i') . '-' . Carbon::parse($item->end_time)->format('H.i');
                    $tempItem[] = [
                        'title' => $item->shift->title,
                        'date' => $time,
                    ];
                }

                $today = $key == date('Y-m-d') ? 'today' : Carbon::parse($key)->format('D');

                $tempCollection[] = [
                    'day' => $today,
                    'date' => Carbon::parse($key)->format('d F Y'),
                    'shift' => $tempItem
                ];

                $tempItem = [];
            }
        }

        $status = count($tempCollection) == 0 ? 0 : 1;

        return $this->respondWithCustomData([
            'status' => $status,
            'collection' => $tempCollection
        ], Response::HTTP_OK);

        // return $scheduleDetails;
    }

    /**
     * Check today schedule
     *
     * @param int $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkSchedule($member_branch_id)
    {

        $user = auth()->user();
        $checkMemberBranch = MemberBranch::where([
            ['id', $member_branch_id],
            ['member_id', $user->member->id],
        ])->first();

        if (!$checkMemberBranch) {
            return $this->respondWithCustomData(['message' => 'access denied'], Response::HTTP_FORBIDDEN);
        }

        // check schedule
        // $schedules = StaffSchedule::where('member_branch_id', $member_branch_id)->where('is_published', 1)->first();
        // $schedules = Schedule::where('member_branch_id', $member_branch_id)->where('is_published', 1)->first();
        // if (!$schedules) {
        //     return $this->respondWithCustomData([
        //         'status' => 0,
        //         'message' => 'you dont have schedule'
        //     ], Response::HTTP_OK);
        // }

        // check date avaibility
        // $date = '2021-11-17';
        $date = date('Y-m-d H:i:s');
        $nowTimeStamp = ConvertToTimestamp($date);
        // return $nowTimeStamp;
        $scheduleDetail = StaffScheduleDetail::where([
            // ['staff_schedule_id', $schedules->id],
            ['member_branch_id', $member_branch_id],
            ['is_published', 1],
        ])
            // ->where('start_timestamp', '>=', $nowTimeStamp)
            ->where('end_timestamp', '>=', $nowTimeStamp)
            ->orderBy('start_time', 'ASC')
            ->first();

        // false
        if (!$scheduleDetail) {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'you don’t have any work schedule today'
            ], Response::HTTP_OK);
        }

        // true
        $name = $checkMemberBranch->member->name . ' ' . $checkMemberBranch->member->last_name;
        $startAt = Carbon::parse($scheduleDetail->shift->start_time)->format('H.i');
        $endAt = Carbon::parse($scheduleDetail->shift->end_time)->format('H.i');
        return $this->respondWithCustomData([
            'status' => 1,
            'message' => 'Hi ' . $name . ', your work schedule today begin at ' . $startAt . ' until ' . $endAt
        ], Response::HTTP_OK);
    }
}
