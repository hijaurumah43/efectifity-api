<?php

namespace App\Http\Controllers\Client\Staff;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Staff\StaffCollection;
use App\Http\Resources\Client\Staff\StaffResource;
use Symfony\Component\HttpFoundation\Response;

class StaffController extends Controller
{

    public function __construct()
    {
        $this->resourceCollection = StaffCollection::class;
        $this->resourceItem = StaffResource::class;
    }

    /**
     * Show a current logged user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {
        $user = auth()->user();
        return $this->respondWithItem($user->member, Response::HTTP_OK, 'success');
    }
}
