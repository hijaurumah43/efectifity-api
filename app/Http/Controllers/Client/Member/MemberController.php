<?php

namespace App\Http\Controllers\Client\Member;

use Image;
use App\Models\User;
use Illuminate\Http\Request;
use App\Contracts\UserRepository;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Service\Service;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Client\Member\MemberRepository;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Client\Member\MemeberResource;
use App\Http\Resources\Client\Member\MemeberCollection;
use App\Http\Requests\Client\Member\Settings\MemberAvatarRequest;
use App\Http\Requests\Client\Member\Settings\MemberUpdateRequest;
use App\Http\Requests\Client\Member\Settings\MemberUpdatePasswordRequest;

class MemberController extends Controller
{
    private $memberRepository;

    /**
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->resourceCollection = MemeberCollection::class;
        $this->resourceItem = MemeberResource::class;
    }

    /**
     * Show a current logged user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {
        $user = auth()->user();
        return $this->respondWithItem($user->member, Response::HTTP_OK, 'success');
    }

    /**
     * Update the current logged user.
     *
     * @param MemberUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMe(MemberUpdateRequest $request)
    {
        $user = auth()->user();
        $user = $user->member;
        return $this->update($request, $user);
    }

    /**
     * Update an user.
     *
     * @param MemberUpdateRequest $request
     * @param Member              $member
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MemberUpdateRequest $request, Member $member)
    {
        $user = auth()->user();
        $data = $request->validated();
        DB::beginTransaction();
        $response = $this->memberRepository->update($member, $data);
        if (isset($data['phone'])) {
            if ($response->phone == $data['phone']) {
                $data['phone'] = $this->phoneFormat($data['phone']);
            } else {
                $data['phone'] = $this->phoneFormat($data['phone']);
                $data['phone_verified'] = 0;
            }

            $checkPhoneNumber = User::where('phone', $data['phone'])->where('id', '!=', $user->id)->first();
            if ($checkPhoneNumber) {
                return $this->respondWithCustomData([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'phone' => [
                            'validation.unique'
                        ],
                    ],
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
        app(UserRepository::class)->update($member->user, $data);
        DB::commit();

        return $this->respondWithItem($response, Response::HTTP_OK, 'success');
    }

    /**
     * Create a new password
     *
     * @param MemberUpdatePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(MemberUpdatePasswordRequest $request)
    {
        $user = auth()->user();
        $data = $request->validated();

        app(UserRepository::class)->update($user, $data);

        return $this->respondWithCustomData(['message' => 'Password Successfully Created'], Response::HTTP_OK, 'success');
    }

    /**
     * Store avatar
     *
     * @param MemberAvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function avatar2(MemberAvatarRequest $request)
    {
        $user = auth()->user();
        $member = $user->member;

        $image = $request->file('avatar');
        $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
        DB::beginTransaction();

        // reduce quality
        $img = Image::make($request->file('avatar')->getRealPath());
        $img->save(public_path() . '/image/avatar/' . $imageFileName, 40);
        $file = public_path() . '/image/avatar/' . $imageFileName;

        Storage::disk('sftp')
            ->put(
                config('cdn.memberAvatar') . $imageFileName,
                // file_get_contents($request->file('avatar')->getRealPath())
                file_get_contents($file)
            );
        Storage::disk('sftp')->setVisibility(config('cdn.memberAvatar'), 'public');
        unlink($file);

        $data['avatar'] = $imageFileName;
        $this->memberRepository->update($member, $data);
        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'Upload avatar has been successfully',
            'links' => Member::avatarStorage($imageFileName)
        ], Response::HTTP_OK, 'success');
    }

    public function avatar(MemberAvatarRequest $request)
    {
        $user = auth()->user();
        $member = $user->member;

        $image = $request->file('avatar');
        $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
        DB::beginTransaction();

        Storage::disk('sftp')
            ->put(
                config('cdn.memberAvatar') . $imageFileName,
                file_get_contents($request->file('avatar')->getRealPath())
            );
        Storage::disk('sftp')->setVisibility(config('cdn.memberAvatar'), 'public');

        $data['avatar'] = $imageFileName;
        $this->memberRepository->update($member, $data);
        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'Upload avatar has been successfully',
            'links' => Member::avatarStorage($imageFileName)
        ], Response::HTTP_OK, 'success');
    }

    /**
     * Change phone to format +62
     *
     * @param string $phone
     * @return void
     */
    private function phoneFormat($phone)
    {
        if (substr($phone, 0, 1) == '0') {
            $from = '/' . preg_quote(substr($phone, 0, 1), '/') . '/';
            $phone = preg_replace($from, '62', $phone, 1);
        } else if (substr($phone, 0, 2) == '62') {
            $phone = $phone;
        } else {
            $phone = preg_replace('/[^0-9]/', '', $phone);
            if (substr($phone, 0, 2) == '62') {
                $phone = $phone;
            } else {
                $phone = '62' . $phone;
            }
        }
        return $phone;
    }

    public function access(Request $request)
    {
        $user = auth()->user();
        $canAccess = false;
        if ($request->has('url')) {

            // Get path request url
            $url = $request->input('url');
            $url = explode('//', $url);
            $url = explode('.', end($url));
            $service = $url[0];

            if(substr($service, 0, 7) == 'staging') {

                $url = explode('-', $service);
                $service = $url[1];
            }

            // Check service
            $service = Service::where('slug', strtolower($service))->first();
            if (!$service) {
                return $this->respondWithCustomData(['message' => 'service tidak terdafatar /  url tidak valid'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            if ($service->slug == 'fitbiz') {
                $partner = false;
                foreach ($user->roles as $data) {
                    if ($data->name == 'partner') {
                        $partner = true;
                        $canAccess = true;
                    }
                }
                return $this->respondWithCustomData(['canAccess' => $canAccess], Response::HTTP_OK, 'success');
            }

            // Check access
            $memberBranch = MemberBranch::where([
                ['is_published', 1],
                ['is_active', 1],
                ['member_id', $user->member->id],
            ])
                ->whereHas('branch', function ($q) use ($service) {
                    $q->where('service_id', $service->id);
                })
                ->first();

            if ($memberBranch) {
                $canAccess = true;
            }
        } else {
            $canAccess = true;
        }

        return $this->respondWithCustomData(['canAccess' => $canAccess], Response::HTTP_OK, 'success');
    }
}
