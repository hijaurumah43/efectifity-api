<?php

namespace App\Http\Controllers\Client\Member\Log;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;

class MemberLogController extends Controller
{
    /**
     * Display a listing member log branch
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function branchLog($branch_id)
    {
        $group = [];
        $user = auth()->user();
        $collections = MemberLog::where([
            'member_id' => $user->member->id,
            'branch_id' => $branch_id
        ])->orderBy('created_at', 'DESC')->get()->groupBy(function ($q) {
            return Carbon::parse($q->created_at)->format('Y-m-d H:i:s');
        });
        foreach ($collections as $key => $collection) {
            $attend = [];
            foreach ($collection as $item) {
                $attend[] = [
                    'attributes' => [
                        'time' => ConvertToTimestamp($item->created_at),
                        'message' => $item->message,
                        'label' => $this->statusLog($item->status),
                    ]
                ];
            }
            $group[] = [
                'date' => ConvertToTimestamp($key),
                'collections' => $attend,
            ];
        }

        return $this->respondWithCustomData($group, Response::HTTP_OK, 'success');
    }

    private function statusLog($status)
    {
        switch ($status) {
            case '1':
                $status = '[CHECK-IN]'; // Check in
                break;

            case '2':
                $status = '[CHECK-OUT]'; // Check out
                break;

            case '3':
                $status = '[CLASS][-1]'; // Booked class
                break;

            case '4':
                $status = '[CLASS]'; // Attend class
                break;

            case '5':
                $status = '[CLASS][+1]'; // Cancel by branch
                break;

            case '6':
                $status = '[CLASS][+1]'; // Cancel by self
                break;

            case '7':
                $status = '[CLASS]'; // Not Show
                break;

            default:
                $status = '[NOT FOUND]';
                break;
        }

        return $status;
    }
}
