<?php

namespace App\Http\Controllers\Client\Member\Attendances;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Services\MemberPackageService;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Member\Classes\ClassCancel;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;
use App\Events\Member\Classes\ClassBookedEvent;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Events\Member\Program\ReservationProgram;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Contracts\Common\Branch\BranchScheduleDetailRepository;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;
use App\Contracts\Client\Member\Attendances\ClassAttendanceRepository;
use App\Http\Resources\Client\Member\Attendances\ClassAttendanceResource;
use App\Http\Resources\Client\Member\Attendances\ClassAttendanceCollection;
use App\Http\Requests\Client\Member\Attendances\ClassAttendanceCreateRequest;
use App\Models\Client\Staff\PersonalTrainerAttendance;

class ClassAttendanceController extends Controller
{
    private $classAttendanceRepository;

    /**
     * @param ClassAttendanceRepository $classAttendanceRepository
     */
    public function __construct(ClassAttendanceRepository $classAttendanceRepository)
    {
        $this->classAttendanceRepository = $classAttendanceRepository;
        $this->resourceCollection = ClassAttendanceCollection::class;
        $this->resourceItem = ClassAttendanceResource::class;
    }

    public function index(Request $request)
    {
        $status = request()->get('status');
        $perPage = request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 20 ? $perPage : 20;

        $classes = ClassAttendance::where([
            ['member_id', auth()->user()->member->id],
        ])->status($status)->whereHas('schedule', function ($query) {
            return $query->orderBy('date', 'ASC');
        })->select('branch_schedule_id',)->groupBy('branch_schedule_id')->get();


        $collectDate = [];
        $tempClasses = [];
        foreach ($classes as $class) {
            $tempClasses[] = [
                'date' => $class->branch_schedule_id,
                'type' => '1',
            ];

            $collectDate[] = $class->schedule->date;
        }

        $ptClasses = PersonalTrainerAttendance::where([
            ['member_id', auth()->user()->member->id],
        ])->status($status)->orderBy('date', 'ASC')->select('date')->groupBy('date')->get();

        $tempPtClasses = [];
        foreach ($ptClasses as $ptClass) {
            $tempPtClasses[] = [
                'date' => $ptClass->date,
                'type' => '2',
            ];
            $collectDate[] = $ptClass->date;
        }

        $tempMerge = collect([$tempClasses, $tempPtClasses]);
        $tempMerge = $tempMerge->collapse();

        $collection = [];
        foreach ($tempMerge as $val) {

            if ($val['type'] == 1) {
                $itemClass = ClassAttendance::where([
                    ['member_id', auth()->user()->member->id],
                    ['branch_schedule_id', $val['date']]
                ])->status($status)->get();

                if (count($itemClass) > 0) {
                    foreach ($itemClass as $item) {
                        $schedule = $item->scheduleDetail;
                        $class = $item->scheduleDetail->class;
                        $collection[] = [
                            'date' => $item->schedule->date,
                            'id' => $item->id,
                            'type' => '1',
                        ];
                    }
                }
            }

            if ($val['type'] == 2) {
                $itemClass = PersonalTrainerAttendance::where([
                    ['member_id', auth()->user()->member->id],
                    ['date', $val['date']]
                ])->status($status)->get();

                if (count($itemClass) > 0) {
                    foreach ($itemClass as $item) {
                        $collection[] = [
                            'date' => $val['date'],
                            'id' => $item->id,
                            'type' => '2',
                        ];
                    }
                }
            }
        }

        $collection = collect($collection)->sortBy('date');
        $collections = $collection->values()->all();

        $tempFinal = [];
        if ($collections > 0) {
            $groupDates =  array_unique($collectDate);

            foreach ($groupDates as $groupDate) {
                $finalCollection = [];

                foreach ($collections as $collection) {

                    if ($groupDate == $collection['date']) {
                        if ($collection['type'] == 1) {
                            $item = ClassAttendance::where('id', $collection['id'])->first();

                            $schedule = $item->scheduleDetail;
                            $class = $item->scheduleDetail->class;
                            $finalCollection[] = [
                                'id' => $item->id,
                                'startTime' => $schedule->start_time,
                                'duration' => (strtotime($schedule->end_time) - strtotime($schedule->start_time)) / 60,
                                'title' => $class->title,
                                'image' => ENV('CDN') . '/' . config('cdn.classes') . $class->image,
                                'level' => $class->level->title,
                                'branch' => $item->branch->title,
                                'status' => attendanceStatus($item->class_attendance_status_id),
                                'coach' => ucwords($class->branchCoach->first()->member->name . ' ' . $class->branchCoach->first()->member->last_name),
                                'type' => [
                                    'isClass' => true,
                                    'isPt' => false,
                                ],
                            ];
                        }

                        if ($collection['type'] == 2) {
                            $item = PersonalTrainerAttendance::where('id', $collection['id'])->first();
                            $finalCollection[] = [
                                'id' => $item->id,
                                'startTime' => $item->start_time,
                                'duration' => (strtotime($item->end_time) - strtotime($item->start_time)) / 60,
                                'title' => 'Personal Training',
                                'image' => ENV('CDN') . '/' . config('cdn.classes') . '60.jpeg',
                                'level' => 'Intermediate',
                                'branch' => $item->branch->title,
                                'status' => ptAttendanceStatus($item->personal_trainer_attendance_status_id),
                                'coach' => ucwords($item->pt->member->name . ' ' . $item->pt->member->last_name),
                                'type' => [
                                    'isClass' => false,
                                    'isPt' => true,
                                ],
                            ];
                        }
                    }
                }

                //sort by time
                $finalCollection = collect($finalCollection)->sortBy('startTime');
                $finalCollection = $finalCollection->values()->all();

                $tempFinal[] = [
                    'createdAt' => Carbon::parse($groupDate)->format('F d, Y'),
                    'collection' => $finalCollection,
                ];

                $finalCollection = [];
            }
        }

        $tempFinal = collect($tempFinal)->sortBy('createdAt');
        $tempFinal = $tempFinal->values()->all();

        /* Collect */
        $temp           = collect([$tempFinal]);
        $temp           = $temp->collapse();
        /* Paginate */
        $data           = collect(json_decode($temp));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $data->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $data->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataSchedule   = $paginate->items();

        return response()->json([
            "data" => $dataSchedule,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
        ], Response::HTTP_OK);
    }

    /**
     * Display a listinng of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexOld(Request $request)
    {
        $status = request()->get('status');
        $perPage = request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 20 ? $perPage : 20;

        // Class Attendance
        $collections = ClassAttendance::where([
            ['member_id', auth()->user()->member->id],
        ])->status($status)->orderBy('created_at', 'ASC')->get()->groupBy(function ($q) {
            return Carbon::parse($q->schedule->date)->format('F d, Y');
        });
        $temp = [];
        foreach ($collections as $key => $items) {
            $collection = [];
            foreach ($items as $item) {
                $schedule = $item->scheduleDetail;
                $class = $item->scheduleDetail->class;
                $collection[] = [
                    'id' => $item->id,
                    'startTime' => $schedule->start_time,
                    'duration' => (strtotime($schedule->end_time) - strtotime($schedule->start_time)) / 60,
                    'title' => $class->title,
                    'image' => ENV('CDN') . '/' . config('cdn.classes') . $class->image,
                    'level' => $class->level->title,
                    'branch' => $item->branch->title,
                    'status' => attendanceStatus($item->class_attendance_status_id),
                    'coach' => ucwords($class->branchCoach->first()->member->name . ' ' . $class->branchCoach->first()->member->last_name),
                    'type' => [
                        'isClass' => true,
                        'isPt' => false,
                    ],
                ];
            }

            $temp[] = [
                'createdAt' => $key,
                'collection' => $collection,
            ];
        }

        // PT Attendance
        $pTcollections = PersonalTrainerAttendance::where([
            ['member_id', auth()->user()->member->id],
        ])->status($status)->orderBy('created_at', 'ASC')->get()->groupBy(function ($q) {
            return Carbon::parse($q->date)->format('F d, Y');
        });

        $pTtemp = [];
        foreach ($pTcollections as $key => $items) {
            $collection = [];
            foreach ($items as $item) {
                $schedule = $item->date;
                $collection[] = [
                    'id' => $item->id,
                    'startTime' => $item->start_time,
                    'duration' => (strtotime($item->end_time) - strtotime($item->start_time)) / 60,
                    'title' => 'Personal Training',
                    'image' => ENV('CDN') . '/' . config('cdn.classes') . '60.jpeg',
                    'level' => 'Intermediate',
                    'branch' => $item->branch->title,
                    'status' => ptAttendanceStatus($item->personal_trainer_attendance_status_id),
                    'coach' => ucwords($item->pt->member->name . ' ' . $item->pt->member->last_name),
                    'type' => [
                        'isClass' => false,
                        'isPt' => true,
                    ],
                ];
            }

            $pTtemp[] = [
                'createdAt' => $key,
                'collection' => $collection,
            ];
        }

        /* Collect */
        $temp           = collect([$temp, $pTtemp]);
        $temp           = $temp->collapse();
        /* Paginate */
        $data           = collect(json_decode($temp));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $data->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $data->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataSchedule   = $paginate->items();

        return response()->json([
            "data" => $dataSchedule,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
        ], Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = auth()->user();
        $classAttendance = ClassAttendance::where([
            ['id', $id],
            ['member_id', $user->member->id]
        ])->firstOrFail();

        // $classAttendance = $this->classAttendanceRepository->findOneById((string) $id);
        return $this->respondWithItem($classAttendance);
    }

    /**
     * Store a  newly resource
     *
     * @param ClassAttendanceCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClassAttendanceCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        $memberPackage = app(MemberPackageRepository::class)->findOneBy(['id' => $request->program, 'member_id' => $user->member->id,]);

        // Check expiry date
        if (date('Y-m-d H:i:s') > $memberPackage->expiry_date) {
            return $this->respondWithCustomData(['message' => 'Transaction has been expired'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // Package non unlimited
        if ($memberPackage->session > 0) {
            if ($memberPackage->session_remaining < 1) {
                return $this->respondWithCustomData(['message' => 'Session has been expired'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
        }

        // Package
        $branchScheduleDetail = app(BranchScheduleDetailRepository::class)->findOneBy(['id' => $request->branch_schedule_detail_id]);

        if ($branchScheduleDetail->schedule->branch_id != $memberPackage->branch_id) {
            return $this->respondWithCustomData(['message' => 'This package is not for a branch of this class, please check your package!'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // NOTE:: CHECK BLOCKING
        $isBlock = ClassAttendance::quotaCancelled($user, $memberPackage);

        $typeRule = 1;
        $class = $branchScheduleDetail->class;
        if ($class->is_online == 1) {
            $typeRule = 2;
        }

        $now = date('Y-m-d H:i:s');

        // GET RULES
        $branchRules = Branch::rules($memberPackage->branch_id, $typeRule);

        // NOTE:: SET TIME
        $reserveTimeMin = '';
        $reserveTimeMax = '';
        $cancellationQuota = 3; // default

        foreach ($branchRules as $rule) {
            if ($rule['id'] == 1 || $rule['id'] == 11) {
                $reserveTimeMin = $rule['duration'];
                $reserveTimeMinPeriod = strtolower(ruleNamePeriode($rule['period']));
            }

            if ($rule['id'] == 2 || $rule['id'] == 12) {
                $reserveTimeMax = $rule['duration'];
                $reserveTimeMaxPeriod = strtolower(ruleNamePeriode($rule['period']));
            }

            if ($rule['id'] == 6 || $rule['id'] == 6) {
                $cancellationQuota = $rule['duration'];
            }
        }

        // return $isBlock;
        if ($isBlock == 1) {
            return $this->respondWithCustomData(['message' => 'This happened because previously you had canceled Class Reservation for ' . $cancellationQuota . ' within 7 day(s)'], Response::HTTP_BAD_REQUEST);
        }

        $messageReserve = 'You could reserve the class from ' . $reserveTimeMin . ' ' . $reserveTimeMinPeriod . ' until ' . $reserveTimeMax . ' ' . $reserveTimeMaxPeriod . ' before the class started';

        $classTime = $branchScheduleDetail->start_time;
        $classTimeFormat = Carbon::parse((date($branchScheduleDetail->schedule->date . $classTime . '.00')))->format('Y-m-d H:i:s');

        // Convert to date format
        $reserveTimeMin = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $reserveTimeMin . ' ' . $reserveTimeMinPeriod));
        $reserveTimeMax = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $reserveTimeMax . ' ' . $reserveTimeMaxPeriod));

        // Min time booking class
        if ($now < $reserveTimeMin) {
            return $this->respondWithCustomData(['message' => $messageReserve], Response::HTTP_BAD_REQUEST);
        }

        // Max time Booking class
        if ($now > $reserveTimeMax) {
            return $this->respondWithCustomData(['message' => $messageReserve], Response::HTTP_BAD_REQUEST);
        }

        // Class has been booked
        $check = ClassAttendance::where([
            ['member_id', $user->member->id],
            ['member_package_id', $request->program],
            ['class_attendance_status_id', 1],
            ['branch_schedule_detail_id', $request->branch_schedule_detail_id],
        ])->count();

        if ($check > 0) {
            return $this->respondWithCustomData(['message' => 'Class has been booked'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $data['member_package_id'] = $request->program;
        $data['member_id'] = $user->member->id;
        $data['branch_id'] = $branchScheduleDetail->schedule->branch_id;
        $data['branch_schedule_id'] = $branchScheduleDetail->schedule->id;

        DB::beginTransaction();
        $classAttendance = $this->classAttendanceRepository->store($data);

        /**
         * Dcrement Session
         */
        if ($memberPackage->session != 0 && $memberPackage->session_remaining != 0) {
            $memberPackageService = app(MemberPackageService::class);
            $memberPackageService->decrementSession($classAttendance->memberPackage);
        }

        /**
         * Create History
         */
        $className = $classAttendance->scheduleDetail->class->title;
        $classBranch = $classAttendance->branch->title;
        $classCoach = $classAttendance->scheduleDetail->class->branchCoach->first()->member->name . ' ' . $classAttendance->scheduleDetail->class->branchCoach->first()->member->last_name;
        $classTime = Carbon::parse($classAttendance->scheduleDetail->schedule->date)->format('d F Y') . ' ' . $classAttendance->scheduleDetail->start_time;
        $message = "Successfull reservation for a " . $className . " class with coach " . $classCoach . " at " . $classBranch . " on " . $classTime;

        $memberLog = MemberLog::create([
            'member_id' => $classAttendance->member_id,
            'branch_id' => $classAttendance->branch_id,
            'class_attendance_id' => $classAttendance->id,
            'status' => 3, // 3 = booked
            'message' => $message,
        ]);

        $dataFb = [
            'slug' => 'upcoming',
            'param' => [
                'key' => 'status',
                'value' => 'upcoming',
                // 'value' => $memberLog->class_attendance_id,
            ]
        ];
        event(new ClassBookedEvent($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Class reservation successfully made!'], Response::HTTP_CREATED, 'success');
    }

    /**
     * Attendance Class
     *
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function attendanceClass($id)
    {
        $user = auth()->user();

        $classAttendance = $this->classAttendanceRepository->findOneById($id);
        if ($classAttendance->member_id == $user->member->id) {
            return $this->respondWithCustomData(['message' => "You can't scanned yourself"], Response::HTTP_BAD_REQUEST, 'failed');
        }

        $data['scan_time'] = Carbon::now()->format('Y-m-d H:i:s');
        $data['user_id'] = $user->id;
        $data['class_attendance_status_id'] = 2;
        $data['is_active'] = 0;

        DB::beginTransaction();
        $this->classAttendanceRepository->update($classAttendance, $data);

        $message    = __(
            "Your're attend the Class on :scan_time and scanned by :staff ",
            ['scan_time' => $data['scan_time'], 'staff' => $user->member->name]
        );

        DB::commit();

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
    }

    /**
     * Cancel Class
     *
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelledClass($id)
    {
        $user = auth()->user();

        $classAttendance = $this->classAttendanceRepository->findOneById($id);
        if ($classAttendance->member_id != $user->member->id) {
            return $this->respondWithCustomData(['message' => "You must scanned yourself"], Response::HTTP_BAD_REQUEST, 'failed');
        }

        if ($classAttendance->status == 4) {
            return $this->respondWithCustomData(['message' => "Class has been passed, because you'r not attend"], Response::HTTP_BAD_REQUEST, 'failed');
        }

        $typeRule = 1;
        $class = $classAttendance->scheduleDetail->class;
        if ($class->is_online == 1) {
            $typeRule = 2;
        }

        $now = date('Y-m-d H:i:s');

        // GET RULES
        $branchRules = Branch::rules($classAttendance->branch_id, $typeRule);

        // NOTE:: SET TIME
        $cancelTime = '';

        foreach ($branchRules as $rule) {
            if ($rule['id'] == 3 || $rule['id'] == 13) {
                $cancelTime = $rule['duration'];
                $cancelTimePeriod = strtolower(ruleNamePeriode($rule['period']));
            }
        }

        $cancelTimeMsg = 'The reservation only allowed to be canceled ' . $cancelTime . ' ' . $cancelTimePeriod . ' before class started';

        $classTime = $classAttendance->scheduleDetail->start_time;
        $classTimeFormat = Carbon::parse((date($classAttendance->scheduleDetail->schedule->date . $classTime . '.00')))->format('Y-m-d H:i:s');

        // Convert to date format
        $cancelTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $cancelTime . ' ' . $cancelTimePeriod));

        // Max cancel time
        if ($now > $cancelTime) {
            return $this->respondWithCustomData(['message' => $cancelTimeMsg], Response::HTTP_BAD_REQUEST);
        }

        $data['cancel_time'] = Carbon::now()->format('Y-m-d H:i:s');
        $data['user_id'] = $user->id;
        $data['class_attendance_status_id'] = 3;
        $data['is_active'] = 0;
        $data['is_cancellation'] = 0;

        DB::beginTransaction();
        $this->classAttendanceRepository->update($classAttendance, $data);

        // Set the first time count canncelled
        $setCancelled = false;
        if ($user->member->block_reservation_at == NULL) {
            $setCancelled = true;
        } else {
            $addDays = Carbon::parse($user->member->block_reservation_at)->addDays(7)->format('Y-m-d H:i:s');
            if (date('Y-m-d H:i:s') > $addDays) {
                $setCancelled = true;
            }
        }

        if ($setCancelled == true) {
            $member = Member::where('id', $user->member->id)->first();
            $member->block_reservation_at = date('Y-m-d H:i:s');
            $member->save();
        }

        /**
         * Increment Session
         */
        $memberPackageService = app(MemberPackageService::class);
        $memberPackageService->incrementSession($classAttendance->memberPackage);

        $message    = __(
            "This reservation has been cancelled by :person on :cancel_time",
            ['cancel_time' => $data['cancel_time'], 'person' => $user->member->name]
        );

        /**
         * Create History
         */
        $className = $classAttendance->scheduleDetail->class->title;
        $classBranch = $classAttendance->branch->title;
        $classCoach = $classAttendance->scheduleDetail->class->branchCoach->first()->member->name . ' ' . $classAttendance->scheduleDetail->class->branchCoach->first()->member->last_name;
        $classTime = Carbon::parse($classAttendance->scheduleDetail->schedule->date)->format('d F Y') . ' ' . $classAttendance->scheduleDetail->start_time;
        $message = "Canceled reservation for a " . $className . " class with coach " . $classCoach . " at " . $classBranch . " on " . $classTime;

        $memberLog = MemberLog::create([
            'member_id' => $classAttendance->member_id,
            'branch_id' => $classAttendance->branch_id,
            'class_attendance_id' => $classAttendance->id,
            'status' => 6, // 6 = cancelled
            'message' => $message,
        ]);

        $dataFb = [
            'slug' => 'upcoming',
            'param' => [
                'key' => 'status',
                'value' => 'expired', //$memberLog->class_attendance_id,
            ]
        ];

        // NOTE:: CHECK BLOCKING
        // $isBlock = ClassAttendance::quotaCancelled($user, $classAttendance->memberPackage);

        event(new ClassCancel($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

        DB::commit();

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
    }
}
