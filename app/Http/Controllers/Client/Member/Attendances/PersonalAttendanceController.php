<?php

namespace App\Http\Controllers\Client\Member\Attendances;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Staff\Rules\Rule;
use App\Models\Client\Staff\Rules\BranchRule;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Staff\PersonalTrainerAttendanceStatus;

class PersonalAttendanceController extends Controller
{
    /**
     * Display a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = auth()->user();
        $resource = PersonalTrainerAttendance::where([
            ['id', $id],
            ['member_id', $user->member->id]
        ])->firstOrFail();

        $canceledBy = null;
        $scannedBy = null;
        $message = null;

        // Cancel
        if ($resource->personal_trainer_attendance_status_id == 5) {
            $canceledBy = $resource->member->name;
            $message = 'This reservation has been cancelled by ' . $canceledBy . ' on ' . $resource->cancel_time;
        }

        // Attend
        if ($resource->personal_trainer_attendance_status_id == 2) {
            $scannedBy = $resource->user->member->name;
            $message = 'You’re attend the personal trainering class on ' . $resource->scan_time . ' and scanned by ' . $scannedBy;
        }

        // Passed
        if ($resource->personal_trainer_attendance_status_id == 3) {
            $message = 'You’re not attend the personal trainering class';
        }

        $branch_id = $resource->branch_id;
        $rules = BranchRule::where([
            ['branch_id', $branch_id],
            ['type', 3],
        ])
            ->orderBy('id', 'ASC')
            ->get();

        if (count($rules) < 1) {

            $rules = Rule::where('type', 3)->orderBy('id', 'ASC')->get();
        }

        $tempRule = [];
        foreach ($rules as $key => $value) {
            if ($key == 0) {
                $tempRule[] = 'Reservation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the PT session started';
            } else if ($key == 1) {
                $tempRule[] = 'Cancellation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the PT session started';
            } else if ($key == 2) {
                $tempRule[] = 'Present at the session at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' after the actual PT session schedule';
            }
        }
        
        $ptasStatus = null;
        $ptas = PersonalTrainerAttendanceStatus::where('id', $resource->personal_trainer_attendance_status_id)->first();
        if ($ptas) {
            $ptasStatus = $ptas->title;
        }

        $responnse = [
            'createdAt' => Carbon::parse($resource->created_at)->format('d F Y'),
            'collection' => [
                'id' => $resource->id,
                'date' => Carbon::parse($resource->date)->format('d F Y'),
                'startTime' => $resource->start_time,
                'duration' => (strtotime($resource->end_time) - strtotime($resource->start_time)) / 60,
                'title' => 'Personal Training',
                'image' => ENV('CDN') . '/' . config('cdn.classes') . '60.jpeg',
                'coach' => ucwords($resource->pt->member->name . ' ' . $resource->pt->member->last_name),
            ],
            'isPT' => true,
            'rules' => $tempRule,
            'attendance' => [
                'id' => $resource->id,
                // 'bookedAt' => Carbon::parse($resource->created_at)->toDateTimeString(),
                'bookedAt' => ConvertToTimestamp($resource->created_at),
                'scannedAt' => $resource->scan_time != '' ? ConvertToTimestamp($resource->scan_time) : null,
                'scannedBy' =>  $scannedBy,
                'canceledAt' => $resource->cancel_time != '' ? ConvertToTimestamp($resource->cancel_time) : null,
                'canceledBy' => $canceledBy,
                'note' => null,
                'message' => $message,
                'attendanceStatus' => ucwords($ptasStatus),
                'status' => $resource->is_active == 1 ? 'active' : 'past',
            ],

            'branch' => new BranchResource($resource->branch),
        ];

        return $this->respondWithCustomData($responnse, Response::HTTP_OK);
    }
}
