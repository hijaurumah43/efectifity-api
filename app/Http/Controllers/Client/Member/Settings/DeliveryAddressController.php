<?php

namespace App\Http\Controllers\Client\Member\Settings;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Settings\DeliveryAddress;
use App\Contracts\Client\Member\Settings\DeliveryAddressRepository;
use App\Http\Resources\Client\Member\Settings\DeliveryAddressResource;
use App\Http\Resources\Client\Member\Settings\DeliveryAddressCollection;
use App\Http\Requests\Client\Member\Settings\DeliveryAddressCreateRequest;
use App\Http\Requests\Client\Member\Settings\DeliveryAddressUpdateRequest;

class DeliveryAddressController extends Controller
{
    private $deliveryAddressRepository;

    /**
     * @param DeliveryAddressRepository $deliveryAddressRepository
     */
    public function __construct(DeliveryAddressRepository $deliveryAddressRepository)
    {
        $this->deliveryAddressRepository = $deliveryAddressRepository;
        $this->resourceCollection = DeliveryAddressCollection::class;
        $this->resourceItem = DeliveryAddressResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();
        $collection = $this->deliveryAddressRepository->findByFilters(['member_id' => $user->member->id, 'is_published' => 1]);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified of the resource
     *
     * @param DeliveryAddress $deliveryAddress
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(DeliveryAddress $deliveryAddress)
    {
        $user = auth()->user();
        $deliveryAddress = $this->deliveryAddressRepository->findOneBy([
            'member_id' => $user->member->id,
            'id' => $deliveryAddress->id
        ]);

        return $this->respondWithItem($deliveryAddress, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param DeliveryAddressCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DeliveryAddressCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        $data['member_id'] = $user->member->id;
        $deliveryAddress = $this->deliveryAddressRepository->store($data);

        return $this->respondWithItem($deliveryAddress, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param DeliveryAddressUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DeliveryAddressUpdateRequest $request, DeliveryAddress $deliveryAddress)
    {
        $user = auth()->user();

        $deliveryAddress = $this->deliveryAddressRepository->findOneBy([
            'id' => $deliveryAddress->id,
            'member_id' => $user->member->id,
        ]);

        $data = $request->only(array_keys($request->rules()));
        $data['member_id'] = $user->member->id;
        $deliveryAddress = $this->deliveryAddressRepository->update($deliveryAddress, $data);

        return $this->respondWithItem($deliveryAddress, Response::HTTP_OK, 'success');
    }

    /**
     * Undisplayed of the resouce
     *
     * @param Int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = auth()->user();

        $deliveryAddress = $this->deliveryAddressRepository->findOneBy([
            'id' => $id,
            'member_id' => $user->member->id,
            'is_published' => 1,
        ]);

        $data['is_published'] = 0;
        $deliveryAddress = $this->deliveryAddressRepository->update($deliveryAddress, $data);

        return $this->respondWithCustomData(['message' => 'Delivery Address Has Been Deleted!'], Response::HTTP_OK, 'success');
    }
}
