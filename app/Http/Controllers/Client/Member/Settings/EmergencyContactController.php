<?php

namespace App\Http\Controllers\Client\Member\Settings;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Settings\EmergencyContact;
use App\Contracts\Client\Member\Settings\EmergencyContactRepository;
use App\Http\Resources\Client\Member\Settings\EmergencyContactResource;
use App\Http\Resources\Client\Member\Settings\EmergencyContactCollection;
use App\Http\Requests\Client\Member\Settings\EmergencyContactCreateRequest;
use App\Http\Requests\Client\Member\Settings\EmergencyContactUpdateRequest;

class EmergencyContactController extends Controller
{
    private $emergencyContactRepository;

    /**
     * @param EmergencyContactRepository $emergencyContactRepository
     */
    public function __construct(EmergencyContactRepository $emergencyContactRepository)
    {
        $this->emergencyContactRepository = $emergencyContactRepository;
        $this->resourceCollection = EmergencyContactCollection::class;
        $this->resourceItem = EmergencyContactResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();

        $collection = $this->emergencyContactRepository->findByFilters(['member_id' => $user->member->id, 'is_published' => 1,]);
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified resource
     *
     * @param EmergencyContact $emergencyContact
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(EmergencyContact $emergencyContact)
    {
        $user = auth()->user();
        $emergencyContact = $this->emergencyContactRepository->findOneBy([
            'member_id' => $user->member->id,
            'id' => $emergencyContact->id
        ]);

        return $this->respondWithItem($emergencyContact, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param EmergencyContactCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EmergencyContactCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        $data['member_id'] = $user->member->id;
        $emergencyContact = $this->emergencyContactRepository->store($data);

        return $this->respondWithItem($emergencyContact, Response::HTTP_CREATED, 'success');
    }

    /**
     * Update resource
     *
     * @param EmergencyContactUpdateRequest $request
     * @param EmergencyContact $emergencyContact
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EmergencyContactUpdateRequest $request, EmergencyContact $emergencyContact)
    {
        $user = auth()->user();

        $emergencyContact = $this->emergencyContactRepository->findOneBy([
            'member_id' => $user->member->id,
            'id' => $emergencyContact->id
        ]);

        $data = $request->only(array_keys($request->rules()));
        $data['member_id'] = $user->member->id;
        $emergencyContact = $this->emergencyContactRepository->update($emergencyContact, $data);

        return $this->respondWithItem($emergencyContact, Response::HTTP_OK, 'success');
    }

    /**
     * Undisplayed of the resouce
     *
     * @param Int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = auth()->user();

        $emergencyContact = $this->emergencyContactRepository->findOneBy([
            'id' => $id,
            'member_id' => $user->member->id,
            'is_published' => 1,
        ]);

        $data['is_published'] = 0;
        $emergencyContact = $this->emergencyContactRepository->update($emergencyContact, $data);

        return $this->respondWithCustomData(['message' => 'Emergency Contact Has Been Deleted!'], Response::HTTP_OK, 'success');
    }
}
