<?php

namespace App\Http\Controllers\Client\Member\Transactions;

use Xendit\Xendit;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Berkayk\OneSignal\OneSignalFacade;
use App\Models\Internal\Voucher\Voucher;
use App\Models\Client\Staff\PersonalTrainer;
use Symfony\Component\HttpFoundation\Response;
use App\Events\Member\Program\ReservationProgram;
use App\Models\Client\Staff\PersonalTrainerMember;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Partner\Branch\BranchAgreement;
use App\Models\Client\Member\Transactions\Transaction;
use App\Contracts\Common\Payment\PaymentTypeRepository;
use App\Contracts\Common\Branch\BranchPackageRepository;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Staff\Transaction\TransactionDetail;
use App\Contracts\Client\Member\Transactions\TransactionRepository;
use App\Http\Resources\Client\Member\Transactions\TransactionResource;
use App\Http\Resources\Client\Member\Transactions\TransactionCollection;
use App\Http\Requests\Client\Member\Transactions\TransactionCreateRequest;

class TransactionController extends Controller
{
    private $transactionRepository;

    /**
     * @param TransactionRepository $transactionRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
        $this->resourceCollection = TransactionCollection::class;
        $this->resourceItem = TransactionResource::class;
        Xendit::setApiKey(ENV('XENDIT_SECRET'));
    }

    /**
     * Display a listing of the resouce
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();

        $collection = $this->transactionRepository->findByFilters(['member_id' => $user->member->id]);
        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified resource
     *
     * @param Transaction $transaction
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Transaction $transaction)
    {
        $user = auth()->user();
        $transaction = $this->transactionRepository->findOneBy([
            'id' => $transaction->id,
            'member_id' => $user->member->id
        ]);

        return $this->respondWithItem($transaction, Response::HTTP_OK, 'success');
    }


    /**
     * Store a newly resource
     *
     * @param TransactionCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransactionCreateRequest $request)
    {
        $user = auth()->user();
        $package = app(BranchPackageRepository::class)->findOneBy(['id' => $request->package]);
        $paymentType = app(PaymentTypeRepository::class)->findOneBy([
            'id' => $request->payment_type,
            'is_published' => 1,
        ]);

        $data = $request->only(array_keys($request->rules()));

        $data['member_id'] = $user->member->id;
        $data['branch_id'] = $package->branch->id;
        $data['branch_package_id'] = $package->id;
        $data['service_id'] = $package->branch->service_id;
        $data['transaction_status_id'] = 1;
        $data['payment_type_id'] = $request->payment_type;
        $data['personal_trainer_id'] = $request->pt_id;

        // if has voucher
        if ($request->voucher) {
            $voucher = Voucher::find($request->voucher);
            if (!$voucher) {
                return $this->respondWithCustomData(['message' => 'Voucher not found'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            if (date('Y-m-d H:i:s') > $voucher->expired) {
                return $this->respondWithCustomData(['message' => 'Voucher has been expired'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            if ($voucher->qty < 1) {
                return $this->respondWithCustomData(['message' => 'Quantity voucher has been empty'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            $discount = $voucher->discount;
            $totalPrice = ($package->price * (int) $discount) / 100;
            $totalPrice = $package->price - $totalPrice;
            $data['voucher_id'] = $voucher->id;
            $data['total_payment'] = $totalPrice;
        } else {
            $data['total_payment'] = $package->price;
        }

        if ($paymentType->channel_code == 'ID_OVO') {
            if (!$request->has('phone_number')) {
                return $this->respondWithCustomData(['message' => 'OVO required phone number'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
        }

        DB::beginTransaction();
        if ($request->has('branch_schedule_detail_id')) {
            if ($request->input('branch_schedule_detail_id') != NULL || strtoupper($request->input('branch_schedule_detail_id')) != "NULL") {
                $data['branch_schedule_detail_id'] = $request->input('branch_schedule_detail_id');
            }
        }

        $transaction = $this->transactionRepository->store($data);

        // TODO:: TIMEZONE +7
        // date_default_timezone_set('Asia/Jakarta');
        $now = date('Y-m-d H:i:s');
        $order_time_expire = date("Y-m-d H:i:s", strtotime($now . '+1 day'));

        switch ($paymentType->methode) {
            case '1':
                $payload = [
                    "external_id" => $transaction->id,
                    "bank_code" => $paymentType->channel_code,
                    "name" => $user->member->name,
                    "expected_amount" => $data['total_payment'],
                    "expiration_date" => $order_time_expire,
                ];

                $result = \Xendit\VirtualAccounts::create($payload);
                break;

            case '2':
                $payload = [
                    'external_id' => $transaction->id,
                    'retail_outlet_name' => $paymentType->channel_code,
                    'name' => $user->member->name,
                    'expected_amount' => $data['total_payment']
                ];

                $result = \Xendit\Retail::create($payload);
                break;

            case '3':
                $channelProperties = '';
                if ($paymentType->channel_code == 'ID_LINKAJA' || $paymentType->channel_code == 'ID_DANA' || $paymentType->channel_code == 'ID_SHOPEEPAY') {
                    $channelProperties = [
                        'success_redirect_url' => 'https://efectifity.com'
                    ];
                } else if ($paymentType->channel_code == 'ID_OVO' || $paymentType->channel_code == 'ID_DANA') {
                    $channelProperties = [
                        'mobile_number' => '+' . $this->phoneFormat($request->input('phone_number')),
                    ];
                }
                $payload = [
                    'reference_id' => $transaction->id,
                    'currency' => 'IDR',
                    'amount' => $data['total_payment'],
                    'checkout_method' => 'ONE_TIME_PAYMENT',
                    'ewallet_type' => $paymentType->title,
                    'channel_code' => $paymentType->channel_code,
                    'channel_properties' => $channelProperties
                ];

                $result = \Xendit\EWallets::createEWalletCharge($payload);
                break;
            case '4':
                $payload = [
                    'token_id' => $request->input('token'),
                    'external_id' => $transaction->id,
                    'authentication_id' => $request->input('authentication_id'),
                    'amount' => $data['total_payment'],
                    // 'card_cvn' => '123',
                    'capture' => false
                ];

                $result = \Xendit\Cards::create($payload);
                break;
            case '5':
                $payload = [
                    'external_id' => $transaction->id,
                    'type' => 'DYNAMIC',
                    'callback_url' => 'https://api.efectifity.com/v1/callback/xendit/qrcode',
                    'amount' => $data['total_payment'],
                ];

                $result = \Xendit\QRCode::create($payload);
                break;

            default:
                return $this->respondWithCustomData(['message' => 'Payment type not available'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
                break;
        }

        if (strtolower($result['status']) == 'pending' || strtolower($result['status']) == 'authorized') {

            $data['old_values'] =  $result;
            $data['channels'] = $paymentType->channel_code;

            // branch agreement
            $agreement = BranchAgreement::where([
                ['is_published', 1],
                ['branch_id', $data['branch_id']],
            ])->orderBy('created_at', 'DESC')->first();
            if ($agreement) {
                $data['branch_agreement_id'] = $agreement->id;
            } else {
                $data['branch_agreement_id'] = NULL;
            }

            switch ($paymentType->methode) {
                case '1':
                    $data['action_to'] = $result['account_number'];
                    break;
                case '3':
                    if ($paymentType->channel_code == 'ID_LINKAJA' || $paymentType->channel_code == 'ID_DANA') {
                        $data['action_to'] = $result['actions']['desktop_web_checkout_url'];
                    } else if ($paymentType->channel_code == 'ID_OVO') {
                        $data['action_to'] = $request->input('phone_number');
                    } else if ($paymentType->channel_code == 'ID_SHOPEEPAY') {
                        $data['action_to'] = $result['actions']['mobile_deeplink_checkout_url'];
                    }
                    break;

                case '4':
                    $data['action_to'] = $result['masked_card_number'];
                    break;

                default:
                    return $this->respondWithCustomData(['message' => 'Payment type not available'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
                    break;
            }

            // update notification
            $transaction = $this->transactionRepository->update($transaction, $data);

            // TODO buat endpoint baru, pindahkan kode berikut
            if ($paymentType->methode == 4) {
                $transaction = Transaction::where('id', $transaction->id)->firstOrFail();
                $transaction->callback_values = $result;
                $transaction->callback_at = date('Y-m-d H:i:s');
                $transaction->transaction_status_id = 2;
                $transaction->is_callback = 1;
                $transaction->save();

                // Store Income
                $income = new TransactionDetail();
                $income->transaction_id = $transaction->id;
                $income->order_number = $transaction->order_number;
                $income->member_id = $transaction->member_id;
                $income->branch_id = $transaction->branch_id;
                $income->service_id = $transaction->branch->service_id;
                $income->transaction_type_id = 1;
                $income->status = 0;
                $income->amount = $transaction->total_payment;
                $income->is_published = 1;
                $income->save();

                $duration = $transaction->branchPackage->duration;
                $periode = $transaction->branchPackage->periode;

                $expiryDate = date('Y-m-d H:i:s', strtotime('+' . $duration . ' ' . $periode . ''));
                // $expiryDate = date($expiryDate, strtotime('+7 hour'));
                $expiryDate = date($expiryDate);

                $branch_agreement_id = NULL;
                $ba = BranchAgreement::where([
                    ['is_published', 1],
                    ['branch_id', $transaction->branch_id],
                    ['is_draft', 0],
                ])->orderBy('created_at', 'DESC')->first();

                if ($ba) {
                    $branch_agreement_id = $ba->id;
                }

                $mp = MemberPackage::create([
                    'member_id' => $transaction->member_id,
                    'transaction_id' => $transaction->id,
                    'expiry_date' => $expiryDate,
                    'branch_id' => $transaction->branch_id,
                    'branch_agreement_id' => $branch_agreement_id,
                    'branch_package_id' => $transaction->branch_package_id,
                    'branch_agreement_id' => $transaction->branch_agreement_id,
                    'session' => $transaction->branchPackage->session,
                    'session_remaining' => $transaction->branchPackage->session,
                    'pt_session' => $transaction->branchPackage->pt_session,
                    'pt_session_remaining' => $transaction->branchPackage->pt_session,
                ]);

                if ($transaction->branch_schedule_detail_id != NULL) {

                    // Get Schedule ID
                    $bsd = BranchScheduleDetail::find($transaction->branch_schedule_detail_id);

                    // Store Class
                    $att = new ClassAttendance();
                    $att->member_package_id = $mp->id;
                    $att->member_id = $mp->member_id;
                    $att->branch_id = $mp->branch_id;
                    $att->branch_schedule_id = $bsd->branch_schedule_id;
                    $att->branch_schedule_detail_id = $transaction->branch_schedule_detail_id;
                    $att->save();

                    // Kurangin MemberPackage
                    $mpdecrement = MemberPackage::find($mp->id);
                    $mpdecrement->decrement('session_remaining', 1);
                    $mpdecrement->save();
                }

                $package = $transaction->branchPackage;
                if ($package->pt_session > 0) {
                    $ptMp = MemberPackage::find($mp->id);
                    PersonalTrainerMember::create([
                        'personal_trainer_id' => $transaction->personal_trainer_id,
                        'member_id' => $transaction->member_id,
                        'member_package_id' => $ptMp->id,
                    ]);
                }
            }

            $message = 'You have awaiting transaction for IDR ' . $transaction->total_payment . ' - tap to finish the payment';
            $dataFb = [
                'slug' => 'transaction',
                'param' => [
                    'key' => 'transantion-status',
                    'value' => $transaction->transactionStatus->title,
                ],
            ];
            event(new ReservationProgram($transaction));
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($transaction->member->user->fcm_token, $message, $dataFb);
        } else {
            if (strtolower($result['status']) == 'authorized') {
                return $this->respondWithCustomData(['message' => 'Credit Card Charge is failed'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
            return $this->respondWithCustomData(['message' => $result['status_message']], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        // check package has pt_session > 0
        if ($package->pt_session > 0) {
            // check PT_ID
            $pt = PersonalTrainer::where([
                ['id', $request->pt_id],
                ['branch_id', $package->branch_id],
            ])->first();

            if (!$pt) {
                return $this->respondWithCustomData(['message' => 'Personal Trainer not found or Personal Trainer has not our branch'], Response::HTTP_BAD_REQUEST);
            }
        }

        DB::commit();

        return $this->respondWithItem($transaction, Response::HTTP_CREATED, 'success');
    }

    /**
     * Change phone to format +62
     *
     * @param string $phone
     * @return void
     */
    private function phoneFormat($phone)
    {
        if (substr($phone, 0, 1) == '0') {
            $from = '/' . preg_quote(substr($phone, 0, 1), '/') . '/';
            $phone = preg_replace($from, '62', $phone, 1);
        } else if (substr($phone, 0, 2) == '62') {
            $phone = $phone;
        } else {
            $phone = preg_replace('/[^0-9]/', '', $phone);
            if (substr($phone, 0, 2) == '62') {
                $phone = $phone;
            } else {
                $phone = '62' . $phone;
            }
        }
        return $phone;
    }
}
