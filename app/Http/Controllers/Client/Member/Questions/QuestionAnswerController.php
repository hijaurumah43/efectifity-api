<?php

namespace App\Http\Controllers\Client\Member\Questions;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Questions\QuestionAnswer;
use App\Contracts\Internal\Question\QuestionCategoryRepository;
use App\Http\Resources\Internal\Question\QuestionCategoryResource;
use App\Contracts\Client\Member\Questions\QuestionAnswerRepository;
use App\Http\Resources\Internal\Question\QuestionCategoryCollection;
use App\Http\Requests\Client\Member\Questions\MemberQuestionCreateRequest;

class QuestionAnswerController extends Controller
{
    private $questionAnswerRepository;
    private $questionCategoryRepository;

    /**
     * @param QuestionAnswerRepository $questionAnswerRepository
     */
    public function __construct(QuestionAnswerRepository $questionAnswerRepository, QuestionCategoryRepository $questionCategoryRepository)
    {
        $this->questionAnswerRepository = $questionAnswerRepository;
        $this->questionCategoryRepository = $questionCategoryRepository;
        $this->resourceCollection = QuestionCategoryCollection::class;
        $this->resourceItem = QuestionCategoryResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->questionCategoryRepository->findByFilters([
            'type' => 1
        ]);
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Store newly resource
     *
     * @param MemberQuestionCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MemberQuestionCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        DB::beginTransaction();
            foreach ($data['answer'] as $key => $value) {

                QuestionAnswer::where([
                    ['member_id', $user->member->id],
                    ['question_id', $value['id']]
                ])->delete();

                $data['member_id'] = $user->member->id;
                $data['question_id'] = $value['id'];
                $data['response'] = $value['text'];
                $data['note'] = $value['note'];

                $this->questionAnswerRepository->store($data);
            }
        DB::commit();

        return $this->respondWithCustomData(['message' => 'Personal information has been saved'], Response::HTTP_CREATED, 'success');
    }
}
