<?php

namespace App\Http\Controllers\Client\Member\PT;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Member\PT\PersonalTrainerReviewCollection;
use App\Http\Resources\Client\Member\PT\PersonalTrainerReviewResource;
use App\Models\Client\Member\PT\PersonalTrainerReview;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PersonalTrainerReviewController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PersonalTrainerReviewCollection::class;
        $this->resourceItem = PersonalTrainerReviewResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param intt $pt_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $pt_id)
    {
        // Query Param
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;
        
        // Fetch Data
        $collection = PersonalTrainerReview::where([
            ['status', 1],
            ['personal_trainer_id', $pt_id],
        ])->paginate($perPage);

        return $this->respondWithCollection($collection, Response::class);
    }
}
