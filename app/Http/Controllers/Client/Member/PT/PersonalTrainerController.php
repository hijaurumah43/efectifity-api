<?php

namespace App\Http\Controllers\Client\Member\PT;

use App\Http\Controllers\Controller;
use App\Http\Resources\Client\Member\PT\PersonalTrainerCollection;
use App\Http\Resources\Client\Member\PT\PersonalTrainerResource;
use App\Models\Client\Staff\PersonalTrainer;
use App\Models\Common\Branch\BranchPackage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PersonalTrainerController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PersonalTrainerCollection::class;
        $this->resourceItem = PersonalTrainerResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $package_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $package_id)
    {
        // Query Param
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        // Get Package
        $package = BranchPackage::where('id', $package_id)->first();
        if (!$package) {
            return $this->respondWithCustomData(['message' => 'Package Not Found'], Response::HTTP_NOT_FOUND);
        }

        // Fetch PT by Branch
        $collection = PersonalTrainer::where([
            ['status', 1],
            ['branch_id', $package->branch_id]
        ])->paginate($perPage);

        // Resource
        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $pt_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($pt_id)
    {
        $resource = PersonalTrainer::where([
            ['status', 1],
            ['id', $pt_id],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource Not Found'], Response::HTTP_NOT_FOUND);
        }
        return $this->respondWithItem($resource);
    }
}
