<?php

namespace App\Http\Controllers\Client\Member\Notifications;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Notifications\NotificationLimit;
use App\Contracts\Client\Member\Notifications\NotificationLimitRepository;
use App\Http\Resources\Client\Member\Notifications\NotificationLimitResource;
use App\Http\Resources\Client\Member\Notifications\NotificationLimitCollection;

class NotificationLimitController extends Controller
{
    private $notificationLimitRepository;

    /**
     * @param NotificationLimitRepository $notificationLimitRepository
     */
    public function __construct(NotificationLimitRepository $notificationLimitRepository)
    {
        $this->notificationLimitRepository = $notificationLimitRepository;
        $this->resourceCollection = NotificationLimitCollection::class;
        $this->resourceItem = NotificationLimitResource::class;
    }

    /**
     * Notification limit of resouce
     *
     * @param int $args
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection($args)
    {
        $type = $this->typeNotification($args);

        if($type < 1) {
            return $this->respondWithCustomData(['message' => 'Type of notification not found'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $notificationLimit = NotificationLimit::whereDate('created_at', date('Y-m-d'))->where([
            ['user_id', auth()->user()->id],
            ['type', $type],
        ])->count();

        return $this->respondWithCustomData([
            'type' => $args,
            'limit' => 1,
            'used' => $notificationLimit,
        ], Response::HTTP_OK, 'success');
    }

    private function typeNotification($args)
    {
        switch ($args) {
            case 'otp':
                $response = 1;
                break;

            case 'email':
                $response = 2;
                break;

            default:
                $response =  0;
                break;
        }

        return $response;
    }
}
