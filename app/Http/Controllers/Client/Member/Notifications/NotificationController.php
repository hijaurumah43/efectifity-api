<?php

namespace App\Http\Controllers\Client\Member\Notifications;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\PermissionUser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use function GuzzleHttp\json_decode;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Member\Notifications\Notification;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Staff\PersonalTrainerAttendance;

class NotificationController extends Controller
{
    /**
     * Display a lisitng of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $group = [];
        $user = auth()->user();
        $collections = Notification::where('notifiable_id', $user->id)->orderBy('created_at', 'desc')->get()->groupBy(function ($q) {
            return Carbon::parse($q->created_at)->format('F d, Y');
        });

        foreach ($collections as $key => $collection) {
            $collectionNotif = [];
            foreach ($collection as $item) {
                $data = json_decode($item->data, true);
                
                // MANAGER INVITATION
                if (strtolower($data['type']) == 'invitation') {

                    $branchName = 'Not Found';
                    $branch = Branch::where('id', $data['data']['branch_id'])->first();
                    if ($branch) {
                        $branchName = $branch->title;
                    }
                    $message = 'You’ve invited as manager by ' . $branchName . ', click to confirm';
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            // 'time' => $item->created_at->format('H.i'),
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => $message,
                            'path' => 'staff-invitation',
                            'status' => $this->statusNotification($item->status),
                            'param' => [
                                'key' => 'notification-id',
                                'value' => $item->id,
                            ]
                        ]
                    ];
                }

                if (strtolower($data['type']) == 'remove-invitation') {

                    $branchName = 'Not Found';
                    $branch = Branch::where('id', $data['data']['branch_id'])->first();
                    if ($branch) {
                        $branchName = $branch->title;
                    }
                    $message = 'The owner of ' . $branchName . ' has removed the invitation as manager ';
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => $message,
                            'status' => $this->statusNotification($item->status),
                            'path' => 'staff-invitation',
                            'param' => [
                                'key' => 'notification-id',
                                'value' => $item->id,
                            ]
                        ]
                    ];
                }

                // STAFF INVITATION
                if (strtolower($data['type']) == 'staff-invitation') {

                    $branchName = 'Not Found';
                    $branch = Branch::where('id', $data['data']['branch_id'])->first();
                    if ($branch) {
                        $branchName = $branch->title;
                    }

                    $message = $data['data']['message'];
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => $message,
                            'status' => $data['data']['label'],
                            'path' => 'staff-invitation',
                            'param' => [
                                'key' => 'notification-id',
                                'value' => $item->id,
                            ]
                        ]
                    ];
                }

                // CLASS
                $typeClasses = [
                    "class-booked",
                    "class-attendance",
                    "class-cancel-branch",
                    "class-cancel",
                    "class-expired",
                    "program-pending",
                    "pt-session-deduct",
                    "pt-class-expired",
                    "pt-class-booked",
                    "pt-class-cancel",
                ];

                if (in_array(strtolower($data['type']), $typeClasses) == 1) {
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => isset($data['data']['message']) ? $data['data']['message'] : 'Nothing message',
                            'status' => isset($data['data']['label']) ? $data['data']['label'] : 'none',
                            'path' => $data['slug'],
                            'param' => [
                                'key' => isset($data['param']['key']) ? $data['param']['key'] : 'none',
                                'value' => isset($data['param']['value']) ? $data['param']['value'] : 'none',
                            ]
                        ]
                    ];
                }

                // MEMBER ATTENDANCE
                $typeClasses = [
                    "member-attendance-clockin",
                    "member-attendance-clockout",
                ];

                if (in_array(strtolower($data['type']), $typeClasses) == 1) {
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => $data['data']['message'],
                            'status' => $data['data']['label'],
                            'path' => $data['slug'],
                            'param' => NULL
                        ]
                    ];
                }
                
                // PT-RESERVATION
                if (strtolower($data['type']) == 'pt-reservation') {
                    $collectionNotif[] = [
                        'type' => strtolower($data['type']),
                        'attributes' => [
                            'notificationId' => $item->id,
                            'isRead' => $item->read_at != NULL ? true : false,
                            'time' => ConvertToTimestamp($item->created_at),
                            'message' => $data['data']['message'],
                            'status' => $data['data']['label'],
                            'path' => 'pt-reservation',
                            'param' => [
                                'key' => 'notification-id',
                                'value' => $item->id,
                            ]
                        ]
                    ];
                }
            }
            $group[] = [
                'date' => $key,
                'collections' => $collectionNotif,
            ];
        }

        return $this->respondWithCustomData($group, Response::HTTP_OK, 'success');
    }

    /**
     * Update read at notification
     *
     * @param int $notificationId
     * @return \Illuminate\Http\JsonResponse
     */
    public function readAt($notificationId)
    {
        $notification = Notification::findOrFail($notificationId);
        if ($notification->read_at == NULL) {
            $notification->read_at = date('Y-m-d H:i:s');
        }
        $notification->save();

        return $this->respondWithCustomData(200, Response::HTTP_OK);
    }


    /**
     * Display a specified resoruce
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($notification)
    {
        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        $branch = Branch::where('id', $data['data']['branch_id'])->first();
        $member = Member::where('id', $data['data']['member_id'])->first();

        $now = date('Y-m-d H:i:s');
        $expiry = Carbon::parse($notification->created_at)->addMinute(30)->format('Y-m-d H:i:s');
        $status = $notification->status;
        $duration = (strtotime($now) -  strtotime($expiry)) / 60;
        $duration = abs($duration);

        // readAt
        $notification->read_at = date('Y-m-d H:i:s');

        if ($now > $expiry) {
            $status = 5;
            // $duration = 0;
            $expiry = 0;

            if ($type == 'staff-invitation') {
                $memberBranch = MemberBranch::where('id', $data['data']['member_branch_id'])->first();
                if ($memberBranch) {
                    $memberBranch->status == 5;
                    $memberBranch->save();
                }
            }

            $notification->status = 5;
            $notification->save();
        }

        if ($status != 1) {
            // $duration = 0;
            $expiry = 0;
        }

        $notification->save();

        $type = $data['type'];
        if ($type == 'staff-invitation') {
            $message = $data['data']['message'];
        } else {
            $message = "You’ve been invited as Manager by ...";
        }

        $item = [
            'id' => $notification->id,
            'branch' => [
                'id' => $branch->id,
                'image' => ENV('CDN') . '/' . config('cdn.branchLogo') . $branch->logo,
                'title' => $branch->title,
                'message' => $message,
            ],
            'member' => [
                'id' => $member->id,
                'name' => $member->name,
                'avatar' => Member::avatarStorage($member->avatar),
            ],
            'status' => $this->statusNotification($status),
            'type' => strtolower($type),
            'expiry' => $expiry,
            'createdAt' => ConvertToTimestamp($notification->created_at)
            // 'createdAt' => Carbon::parse($notification->created_at)->format('d F Y, H.i')
        ];

        return $this->respondWithCustomData($item, Response::HTTP_OK, 'success');
    }

    /**
     * Invitation accepted
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function accepted($notification)
    {
        $user = auth()->user();

        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        if ($notification->status == 1) {

            DB::beginTransaction();

            $memberBranch = MemberBranch::where([
                'id' => $data['data']['member_branch_id'],
                'branch_id' => $data['data']['branch_id'],
                'member_id' => $data['data']['member_id'],
            ])->first();

            $memberBranch->is_published = 1;
            $memberBranch->is_active = 1;
            $memberBranch->status = 2;
            if ($type == 'invitation') {
                $memberBranch->type = 1;
                $user->attachRole(Role::MANAGER);
            }
            $memberBranch->save();


            $notification->status = 2;
            $notification->save();

            DB::commit();
        } else {
            return $this->respondWithCustomData(['message' => 'notification has been updated'], Response::HTTP_UNPROCESSABLE_ENTITY, 'success');
        }

        $message = 'Staff Accepted';
        if ($type == 'invitation') {
            $message = 'Congratulations & Welcome aboard At ' . $memberBranch->branch->title;
        }

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
    }

    /**
     * Invitation rejected
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejected($notification)
    {
        $user = auth()->user();
        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        if ($notification->status == 1) {

            DB::beginTransaction();

            $memberBranch = MemberBranch::where([
                'id' => $data['data']['member_branch_id'],
                'branch_id' => $data['data']['branch_id'],
                'member_id' => $data['data']['member_id'],
            ])->first();

            $memberBranch->is_active = 0;
            $memberBranch->is_published = 0;
            $memberBranch->status = 3;
            if ($type == 'staff-invitation') {
                /* RoleUser::where('user_id', $user->id)->delete();
                PermissionUser::where('user_id', $user->id)->delete(); */
                $user->attachRole(Role::MEMBER);
            }
            $memberBranch->save();

            $notification->status = 3;
            $notification->save();

            DB::commit();
        } else {
            return $this->respondWithCustomData(['message' => 'notification has been updated'], Response::HTTP_UNPROCESSABLE_ENTITY, 'success');
        }

        $message = 'Invitation Rejected Thank you for your response';

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
    }

    /**
     * Information...
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection()
    {
        $user = auth()->user();

        $bellNotification = false;
        $notif = Notification::where('notifiable_id', $user->id)->whereNull('read_at')->count();
        if ($notif > 0) {
            $bellNotification = true;
        }

        $program = MemberPackage::where([
            ['member_id', $user->member->id],
            ['status', 1],
            ['created_at', '<=', $user->member->id],
        ])->count();

        $class = ClassAttendance::where([
            ['member_id', $user->member->id],
            ['class_attendance_status_id', 1],
            ['is_active', 1],
        ])->count();

        $pt = PersonalTrainerAttendance::where([
            ['member_id', $user->member->id],
            ['personal_trainer_attendance_status_id', 1],
            ['is_active', 1],
        ])->count();

        $transaction = Transaction::where([
            ['member_id', $user->member->id],
            ['transaction_status_id', 1],
            ['is_published', 1],
            ['is_active', 1],
            ['is_callback', 0],
        ])->count();
        $totalUpcoming = $class + $pt;
        return $this->respondWithCustomData([
            'programs' => $program,
            'class' => $totalUpcoming,
            'transaction' => $transaction,
            'bellNotification' => $bellNotification,
        ], Response::HTTP_OK, 'success');
    }

    private function statusNotification($status)
    {
        switch ($status) {
            case '1':
                $status = 'pending';
                break;

            case '2':
                $status = 'accepted';
                break;

            case '3':
                $status = 'rejected';
                break;

            case '4':
                $status = 'cancelled';
                break;

            case '5':
                $status = 'expired';
                break;

            default:
                $status = 'not found';
                break;
        }

        return $status;
    }
}
