<?php

namespace App\Http\Controllers\Client\Member\Notifications;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\PermissionUser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use function GuzzleHttp\json_decode;

use Illuminate\Support\Facades\Auth;
use App\Models\Client\Member\Log\MemberLog;
use App\Events\Member\PT\PtClassBookedEvent;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Member\Notifications\Notification;
use App\Models\Client\Member\Attendances\ClassAttendance;

class NotificationPTController extends Controller
{
    /**
     * Display a specified resoruce
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($notification)
    {
        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        $branch = Branch::where('id', $data['data']['branch_id'])->first();
        $member = Member::where('id', $data['data']['member_id'])->first();

        $now = date('Y-m-d H:i:s');
        $expiry = Carbon::parse($notification->created_at)->addMinute(1)->format('Y-m-d H:i:s');
        $status = $notification->status;
        $duration = (strtotime($now) -  strtotime($expiry)) / 60;
        $duration = abs($duration);

        // readAt
        $notification->read_at = date('Y-m-d H:i:s');

        if ($now > $expiry) {
            $status = 9;
            // $duration = 0;
            $expiry = 0;

            if ($type == 'pt-reservation') {
                $resource = PersonalTrainerAttendance::where('id', $data['data']['personal_trainer_attendance_id'])->first();
                if ($resource) {
                    $resource->personal_trainer_attendance_status_id = 5;
                    $resource->save();
                }
            }

            $notification->status = 5;
            $notification->save();
        }

        if ($status != 1) {
            // $duration = 0;
            $expiry = 0;
        }

        $notification->save();

        $ptResource = [];
        $schedule = null;
        if ($type == 'pt-reservation') {
            $resource = PersonalTrainerAttendance::where('id', $data['data']['personal_trainer_attendance_id'])->first();
            if ($resource) {
                $ptResource = [
                    'firstName' => ucwords($resource->pt->member->name),
                    'lastName' => ucwords($resource->pt->member->last_name),
                    'avatar' => Member::avatarStorage($resource->pt->member->avatar),
                ];

                $schedule = Carbon::parse($resource->date . ' ' . $resource->start_time)->format('d F Y H:i');
            }
        }

        $item = [
            'id' => $notification->id,
            'branch' => [
                'id' => $branch->id,
                'title' => $branch->title,
                'image' => ENV('CDN') . '/' . config('cdn.branchLogo') . $branch->logo,
            ],
            'personalTrainer' => $ptResource,
            'status' => $this->statusNotification($status),
            'type' => strtolower($type),
            'expiry' => $expiry == 0 ? $expiry : ConvertToTimestamp($expiry),
            'schedule' => $schedule,
        ];

        return $this->respondWithCustomData($item, Response::HTTP_OK, 'success');
    }

    /**
     * Invitation accepted
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function accepted($notification)
    {
        $user = auth()->user();

        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        if ($notification->status == 1) {

            DB::beginTransaction();

            if ($type == 'pt-reservation') {
                $resource = PersonalTrainerAttendance::where('id', $data['data']['personal_trainer_attendance_id'])->first();
                if ($resource) {
                    $resource->personal_trainer_attendance_status_id = 1;
                    $resource->save();

                    $memberPackage = MemberPackage::where('id', $resource->member_package_id)->first();
                    $memberPackage->decrement('pt_session_remaining', 1);
                    $memberPackage->save();
                }
            }

            $notification->status = 2;
            $notification->save();

            /**
             * Create History
             */
            $ptName = $resource->pt->member->name . ' ' . $resource->pt->member->last_name;
            $ptBranch = $resource->branch->title;
            $ptTime = Carbon::parse($resource->date)->format('d F Y') . ' ' . $resource->start_time;
            $message = "Successfull reservation for a Personal Training session with " . $ptName . " at " . $ptBranch . " on " . $ptTime;

            $memberLog = MemberLog::create([
                'member_id' => $resource->member_id,
                'branch_id' => $resource->branch_id,
                'personal_trainer_attendance_id' => $resource->id,
                'status' => 3, // 3 = booked
                'message' => $message,
            ]);

            $dataFb = [
                'slug' => 'upcoming',
                'param' => [
                    'key' => 'status',
                    'value' => 'upcoming',
                ]
            ];
            event(new PtClassBookedEvent($memberLog));
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);


            DB::commit();
            $message = 'Personal Trainer schedule has been accepted';
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
        } else {
            return $this->respondWithCustomData(['message' => 'notification has been expired or inactive'], Response::HTTP_UNPROCESSABLE_ENTITY, 'success');
        }
    }

    /**
     * Invitation rejected
     *
     * @param string $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function rejected($notification)
    {
        $user = auth()->user();

        $notification = Notification::findOrFail($notification);
        $data = json_decode($notification->data, TRUE);
        $type = $data['type'];

        if ($notification->status == 1) {

            DB::beginTransaction();

            if ($type == 'pt-reservation') {
                $resource = PersonalTrainerAttendance::where('id', $data['data']['personal_trainer_attendance_id'])->first();
                if ($resource) {
                    $resource->personal_trainer_attendance_status_id = 8;
                    $resource->save();
                }
            }

            $notification->status = 3;
            $notification->save();

            DB::commit();
            $message = 'Personal Trainer schedule has been canceled';
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, 'success');
        } else {
            return $this->respondWithCustomData(['message' => 'notification has been expired or inactive'], Response::HTTP_UNPROCESSABLE_ENTITY, 'success');
        }
    }

    private function statusNotification($status)
    {
        switch ($status) {
            case '1':
                $status = 'pending';
                break;

            case '2':
                $status = 'accepted';
                break;

            case '3':
                $status = 'rejected';
                break;

            case '4':
                $status = 'cancelled';
                break;

            case '5':
                $status = 'expired';
                break;

            default:
                $status = 'not found';
                break;
        }

        return $status;
    }
}
