<?php

namespace App\Http\Controllers\Client\Member\Classes;

use App\Http\Controllers\Controller;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;
use App\Http\Resources\Client\Member\Classes\MemberBranchResource;
use App\Http\Resources\Client\Member\Classes\MemberBranchCollection;

class MemberBranchController extends Controller
{
    private $memberBranchRepository;

    /**
     * @param MemberBranchRepository $memberBranchRepository
     */
    public function __construct(MemberBranchRepository $memberBranchRepository)
    {
        $this->memberBranchRepository = $memberBranchRepository;
        $this->resourceCollection = MemberBranchCollection::class;
        $this->resourceItem = MemberBranchResource::class;
    }

    /**
     * Display a listing of the
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();
        $collection = $this->memberBranchRepository->findByFilters([
            'member_id' => $user->member->id,
            'status' => 2,
            'is_active' => 1,
            'is_published' => 1,
        ]);

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified resouce
     *
     * @param MemberBranch $memberBranch
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(MemberBranch $memberBranch)
    {
        $user = auth()->user();
        $collection = $this->memberBranchRepository->findOneBy([
            'id' => $memberBranch->id,
            'member_id' => $user->member->id,
            'status' => 2,
            'is_active' => 1,
            'is_published' => 1,
        ]);

        return $this->respondWithItem($collection);
    }
}
