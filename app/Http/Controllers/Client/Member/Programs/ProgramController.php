<?php

namespace App\Http\Controllers\Client\Member\Programs;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;
use App\Http\Resources\Client\Member\Programs\MemberPackageResource;
use App\Http\Resources\Client\Member\Programs\MemberPackageCollection;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    private $memberPackageRepository;

    /**
     * @param MemberPackageRepository $memberPackageRepository
     */
    public function __construct(MemberPackageRepository $memberPackageRepository)
    {
        $this->memberPackageRepository = $memberPackageRepository;
        $this->resourceCollection = MemberPackageCollection::class;
        $this->resourceItem = MemberPackageResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $collection = $this->memberPackageRepository->findByFilters();
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified resource
     *
     * @param int $transaction_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($transaction_id)
    {
        $user = auth()->user();
        $memberPackage = $this->memberPackageRepository->findOneBy([
            'id' => $transaction_id,
            'member_id' => $user->member->id
        ]);
        return $this->respondWithItem($memberPackage, Response::HTTP_OK, 'success');
    }
}
