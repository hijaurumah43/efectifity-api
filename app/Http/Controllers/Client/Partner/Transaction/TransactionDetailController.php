<?php

namespace App\Http\Controllers\Client\Partner\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Feature\Fitclub\Transaction\CreateWithdrawelRequest;
use App\Http\Resources\Feature\Fitclub\Transaction\TransactionDetailCollection;
use App\Http\Resources\Feature\Fitclub\Transaction\TransactionDetailResource;
use App\Models\Client\Partner\Bank\MemberBank;
use App\Models\Client\Staff\Transaction\TransactionDetail;
use App\Models\Common\Company\Company;
use App\Models\WithdrawalLog;
use Xendit\Xendit;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class TransactionDetailController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = TransactionDetailCollection::class;
        $this->resourceItem = TransactionDetailResource::class;
        Xendit::setApiKey(ENV('XENDIT_SECRET'));
    }

    /**
     * Get balance
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance()
    {
        $balance = TransactionDetail::balanceCompany();
        $balance = number_format($balance);

        return $this->respondWithCustomData(['balance' => $balance], Response::HTTP_OK);
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = TransactionDetail::whereDate('created_at', '>=', $request->get('start_date'))
            ->whereDate('created_at', '<=', $request->get('end_date'))
            ->orderBy('created_at', 'DESC');

        if ($request->has('type')) {
            $query->where('transaction_type_id', $request->get('type'));
        }

        if ($request->has('service')) {
            $query->where('service_id', $request->get('service'));
        }

        if ($request->has('branch')) {
            $query->where('branch_id', $request->get('branch'));
        }

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    public function summary(Request $request)
    {
        $amountIn = TransactionDetail::whereIn('transaction_type_id', [1])
            ->whereDate('created_at', '>=', $request->get('start_date'))
            ->whereDate('created_at', '<=', $request->get('end_date'))
            ->sum('amount');

        $countIn = TransactionDetail::whereIn('transaction_type_id', [1])
            ->whereDate('created_at', '>=', $request->get('start_date'))
            ->whereDate('created_at', '<=', $request->get('end_date'))
            ->count();

        $amountOut = TransactionDetail::where([
            ['is_failed', 0]
        ])
            ->whereIn('transaction_type_id', [2, 3, 4, 5])
            ->whereDate('created_at', '>=', $request->get('start_date'))
            ->whereDate('created_at', '<=', $request->get('end_date'))
            ->sum('amount');

        $countOut = TransactionDetail::where([
            ['is_failed', 0]
        ])
            ->whereIn('transaction_type_id', [2, 3, 4, 5])
            ->whereDate('created_at', '>=', $request->get('start_date'))
            ->whereDate('created_at', '<=', $request->get('end_date'))
            ->count();

        return $this->respondWithCustomData([
            'incoming' => [
                'amount' => number_format($amountIn),
                'transactionCount' => $countIn
            ],
            'outgoing' => [
                'amount' => number_format($amountOut),
                'transactionCount' => $countOut
            ]
        ], Response::HTTP_OK);
    }

    /**
     * Display a listing of banks
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function bank()
    {
        $user = auth()->user();

        $company = Company::where('user_id', $user->id)->first();
        if (!$company) {
            return $this->respondWithCustomData(['message' => 'company not found'], Response::HTTP_BAD_REQUEST);
        }

        $member_id = $company->user->member->id;
        $banks = MemberBank::where('member_id', $member_id)->get();

        $temp = [];
        if (count($banks) > 0) {
            foreach ($banks as $bank) {
                $temp[] = [
                    'id' => $bank->id,
                    'accountHolder' => $bank->account_holder,
                    'accountNumber' => $bank->account_number,
                    'bank' => isset($bank->bank) ? $bank->bank->title : '',
                ];
            }
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * WD
     *
     * @param CreateWithdrawelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateWithdrawelRequest $request)
    {
        $user  = auth()->user();

        $data = $request->only(array_keys($request->rules()));
        $wd = $data['amount'];
        $bank_id = $data['bank_id'];

        $memberBank = MemberBank::where('id', $bank_id)->first();
        if (!$memberBank) {
            return $this->respondWithCustomData(['message' => 'bank not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $bankCode = strtoupper($memberBank->bank->title);
        $account = $memberBank->account_holder;
        $noRek = $memberBank->account_number;

        $balance = TransactionDetail::balanceCompany();

        // Check Balance
        if ($wd > $balance) {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'Withdrawal amount exceeded the cash balance'
            ], Response::HTTP_BAD_REQUEST);
        }

        $externalID = 'VT' . rand('1111111', '9999999');

        //WD to Xendit
        $payload = [
            'external_id' => $externalID,
            'amount' => $wd,
            'bank_code' => $bankCode,
            'account_holder_name' => $account,
            'account_number' => $noRek,
            'description' => 'Disbursement from Example',
            'email_to' => [
                $user->email,
            ],
            /* 'email_cc' => [],
            'email_bcc' => [], */
        ];

        $result = \Xendit\Disbursements::create($payload);

        if (strtolower($result['status']) == 'pending') {

            DB::beginTransaction();

            $resource = new WithdrawalLog();
            $resource->order_number = $externalID;
            $resource->account_holder_name = $account;
            $resource->account_number = $noRek;
            $resource->bank_code = $bankCode;
            $resource->amount = $wd;
            // $resource->old_values = $result;
            $resource->is_callback = 0;
            $resource->is_published = 1;
            $resource->save();

            // Store income with status pending
            $income = new TransactionDetail();
            $income->branch_id = $resource->branch_id;
            $income->order_number = $resource->order_number;
            $income->transaction_type_id = 4;
            $income->status = 0;
            $income->amount = $resource->amount;
            $income->is_published = 1;
            $income->withdrawal_log_id = $resource->id;
            $income->save();

            DB::commit();

            return $this->respondWithCustomData([
                'status' => 1,
                'message' => 'Withdrawal has successfully submitted!'
            ], Response::HTTP_CREATED);
        } else {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'Something wrong'
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
