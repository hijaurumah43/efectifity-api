<?php

namespace App\Http\Controllers\Client\Partner\Bank;

use App\Contracts\Client\Partner\Bank\MemberBankRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Client\Partner\Bank\MemberBankCreateRequest;
use App\Http\Requests\Client\Partner\Bank\MemberBankUpdateRequest;
use App\Http\Resources\Client\Partner\Bank\MemberBankCollection;
use App\Http\Resources\Client\Partner\Bank\MemberBankResource;
use App\Models\Client\Partner\Bank\MemberBank;
use Symfony\Component\HttpFoundation\Response;

class MemberBankController extends Controller
{
    private $memberBankRepository;

    /**
     * @param MemberBankRepository $memberBankRepository
     */
    public function __construct(MemberBankRepository $memberBankRepository)
    {
        $this->memberBankRepository = $memberBankRepository;
        $this->resourceCollection = MemberBankCollection::class;
        $this->resourceItem = MemberBankResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();
        $collection = $this->memberBankRepository->findByFilters([
            'is_published' => 1,
            'member_id' => $user->member->id,
        ]);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified resource
     *
     * @param MemberBank $memberBank
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(MemberBank $memberBank)
    {
        $user = auth()->user();
        $memberBank = $this->memberBankRepository->findOneBy([
            'is_published' => 1,
            'member_id' => $user->member->id,
            'id' => $memberBank->id
        ]);
        return $this->respondWithItem($memberBank);
    }

    /**
     * Store a newly resource
     *
     * @param MemberBankCreateRequest $request
     * @return void
     */
    public function store(MemberBankCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        $data['member_id'] = $user->member->id;
        $this->memberBankRepository->store($data);

        return $this->respondWithCustomData(['message' =>  'Data successfully saved'], Response::HTTP_CREATED, 'success');
    }

    /**
     * Udpate a resource
     *
     * @param MemberBankUpdateRequest $request
     * @param MemberBank $memberBank
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MemberBankUpdateRequest $request, MemberBank $memberBank)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        $memberBank = $this->memberBankRepository->findOneBy([
            'is_published' => 1,
            'member_id' => $user->member->id,
            'id' => $memberBank->id
        ]);

        $data['member_id'] = $user->member->id;
        $this->memberBankRepository->update($memberBank, $data);
        
        return $this->respondWithCustomData(['message' =>  'Data successfully saved'], Response::HTTP_CREATED, 'success');
    }

    /**
     * Undisplayed of the resouce
     *
     * @param Int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = auth()->user();

        $memberBank = $this->memberBankRepository->findOneBy([
            'is_published' => 1,
            'member_id' => $user->member->id,
            'id' => $id
        ]);

        $data['is_published'] = 0;
        $memberBank = $this->memberBankRepository->update($memberBank, $data);

        return $this->respondWithCustomData(['message' => 'Bank Has Been Deleted!'], Response::HTTP_OK, 'success');
    }
}
