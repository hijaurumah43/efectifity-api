<?php

namespace App\Http\Controllers\Client\Partner;

use Illuminate\Http\Request;
use App\Contracts\UserRepository;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Client\Member\MemberRepository;
use App\Contracts\Common\Company\CompanyRepository;
use App\Http\Resources\Client\Partner\PartnerResource;
use App\Http\Resources\Client\Partner\PartnerCollection;
use App\Http\Requests\Client\Member\Settings\MemberAvatarRequest;
use App\Http\Requests\Client\Member\Settings\MemberUpdateRequest;
use App\Http\Requests\Client\Staff\Settings\PartnerUpdateRequest;
use App\Http\Requests\Client\Member\Settings\MemberUpdatePasswordRequest;

class PartnerController extends Controller
{
    private $memberRepository;

    /**
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
        $this->resourceCollection = PartnerCollection::class;
        $this->resourceItem = PartnerResource::class;
    }

    /**
     * Show a current logged user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {
        $user = auth()->user();   
        return $this->respondWithItem($user->member, Response::HTTP_OK, 'success');
    }

    /**
     * Update the current logged user.
     *
     * @param MemberUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMe(PartnerUpdateRequest $request)
    {
        $user = auth()->user();
        $user = $user->member;
        return $this->update($request, $user);
    }

    /**
     * Update an user.
     *
     * @param PartnerUpdateRequest $request
     * @param Member              $member
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PartnerUpdateRequest $request, Member $member)
    {
        $data = $request->validated();
        DB::beginTransaction();
            $response = $this->memberRepository->update($member, $data);
            if($response->phone == $data['phone']) {
                $data['phone'] = $this->phoneFormat($data['phone']);
            } else {
                $data['phone'] = $this->phoneFormat($data['phone']);
                $data['phone_verified'] = 0;
            }
            $user = app(UserRepository::class)->update($member->user, $data);
            $company = app(CompanyRepository::class)->findOneBy([
                'user_id' => $user->id
            ]);

            app(CompanyRepository::class)->update($company, $data);
        DB::commit();

        return $this->respondWithItem($response, Response::HTTP_OK, 'success');
    }

    /**
     * Create a new password
     *
     * @param MemberUpdatePasswordRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(MemberUpdatePasswordRequest $request)
    {
        $user = auth()->user();
        $data = $request->validated();
        
        app(UserRepository::class)->update($user, $data);

        return $this->respondWithCustomData(['message' => 'Password Successfully Created'], Response::HTTP_OK, 'success');
    }

    /**
     * Store avatar
     *
     * @param MemberAvatarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function avatar(MemberAvatarRequest $request)
    {
        $user = auth()->user();
        $member = $user->member;
        
        $image = $request->file('avatar');
        $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
       
        DB::beginTransaction();
            Storage::disk('sftp')
                ->put(
                    config('cdn.memberAvatar') . $imageFileName,
                    file_get_contents($request->file('avatar')->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.memberAvatar'), 'public');

            $data['avatar'] = $imageFileName;
            $this->memberRepository->update($member, $data);
        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'Upload avatar has been successfully',
            'links' => Member::avatarStorage($imageFileName)
        ], Response::HTTP_OK, 'success');
    }

    /**
     * Change phone to format +62
     *
     * @param string $phone
     * @return void
     */
    private function phoneFormat($phone)
    {
        return $phone;
    }
}
