<?php

namespace App\Http\Controllers\Client\Partner\Branch;

use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Client\Partner\Branch\BranchStatusLog;
use App\Http\Resources\Client\Partner\Branch\PlatformLogResource;
use App\Http\Resources\Client\Partner\Branch\PlatformLogCollection;

class PlatformLogController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PlatformLogCollection::class;
        $this->resourceItem = PlatformLogResource::class;
    }

    public function index($branch_id)
    {
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->firstOrFail();

        $collection = BranchStatusLog::where([
            ['branch_id', $branch->id],
            ['branch_status_id', '!=', 1]
        ])->orderBy('id', 'ASC')->paginate();

        return $this->respondWithCollection($collection);
    }
}
