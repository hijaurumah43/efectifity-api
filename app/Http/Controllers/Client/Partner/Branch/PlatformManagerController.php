<?php

namespace App\Http\Controllers\Client\Partner\Branch;

use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Events\Partner\RemoveManager;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Partner\InvitationManager;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Client\Partner\Branch\PlatformManagerResource;
use App\Http\Resources\Client\Partner\Branch\PlatformManagerCollection;
use App\Http\Requests\Client\Partner\Branch\PlatformManagerCreateRequest;

class PlatformManagerController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PlatformManagerCollection::class;
        $this->resourceItem = PlatformManagerResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($branch_id)
    {
        $user = auth()->user();
        $check = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->first();

        if (!$check) {
            return $this->respondWithCustomData(['message' => 'Access denied'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $memberBranch = MemberBranch::where([
            ['branch_id', $branch_id],
            ['type', 1],
            // ['is_active', 1],
            ['status', 2],
        ])->orderBy('created_at', 'DESC')->firstOrFail();

        return $this->respondWithItem($memberBranch, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param PlatformManagerCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PlatformManagerCreateRequest $request, $branch_id)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        $check = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->first();

        if (!$check) {
            return $this->respondWithCustomData(['message' => 'Access denied'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $member = Member::where('member_id', $data['member_id'])->firstOrFail();
        $memberBranch = MemberBranch::where([
            ['branch_id', $branch_id],
            ['type', 1],
            ['member_id', $member->id],
            // ['status', 2],
            // ['is_active', 1],
        ])->first();

        if (!$memberBranch) {

            $data['member_id'] = $member->id;
            $data['branch_id'] = $branch_id;
            $data['type'] = 1;
            $data['status'] = 1;
            $memberBranch = MemberBranch::create($data);
        } else {
            
            if ($memberBranch->status == 1) {
                return $this->respondWithCustomData(['message' => 'Manager has been invited'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
    
            if ($memberBranch->status == 2) {
                return $this->respondWithCustomData(['message' => 'Manager has been actived'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            $memberBranch->status = 1;
            $memberBranch->save();
        }

        event(new InvitationManager($memberBranch));
        $message = 'You’ve invited as manager by ' . $memberBranch->branch->title . ', click to confirm';

        $dataFb = [
            'account' => '',
            'param' => NULL
        ];
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($member->user->fcm_token, $message, $dataFb);

        return $this->respondWithCustomData(['message' => 'Invitation sent'], Response::HTTP_CREATED, 'success');
    }

    public function reInvite(PlatformManagerCreateRequest $request, $branch_id)
    {
        return $this->store($request, $branch_id);
    }

    /**
     * Update status resouce
     *
     * @param int $branch_id
     * @param int $member_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($branch_id, $member_id)
    {
        DB::beginTransaction();
        $member = Member::where('member_id', $member_id)->firstOrFail();
        $memberBranch = MemberBranch::where([
            ['branch_id', $branch_id],
            ['type', 1],
            ['status', 2],
            ['member_id', $member->id],
        ])->firstOrFail();

        $memberBranch->is_published = 0; // not active
        $memberBranch->is_active = 0; //remove
        $memberBranch->status = 6; //remove
        $memberBranch->save();

        // Remove Role
        $userId = $memberBranch->member->user->id;
        RoleUser::where('user_id', $userId)->delete();

        // Role Member
        $user = User::where('id', $userId)->first();
        if ($user) {
            $user->attachRole(Role::MEMBER);
        }

        event(new RemoveManager($memberBranch));
        $message = 'The owner of ' . $memberBranch->branch->title . ' has removed the invitation as manager';
        $dataFb = [
            'account' => '',
            'param' => NULL
        ];
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($member->user->fcm_token, $message, $dataFb);
        DB::commit();

        return $this->respondWithCustomData(['message' => 'Manager has been removed'], Response::HTTP_OK, 'success');
    }
}
