<?php

namespace App\Http\Controllers\Client\Partner\Branch;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Service\Service;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Partner\Branch\BranchStatusLog;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Http\Resources\Client\Partner\Branch\PlatformResource;
use App\Http\Requests\Client\Partner\Branch\PlatformUploadLogo;
use App\Http\Resources\Client\Partner\Branch\PlatformCollection;
use App\Http\Requests\Client\Partner\Branch\PlatformCreateRequest;
use App\Http\Requests\Client\Partner\Branch\PlatformUpdateRequest;
use App\Http\Requests\Client\Partner\Branch\PlatformUploadThumbnail;
use App\Models\Client\Member\Classes\MemberBranch;

class PlatformController extends Controller
{

    public function __construct()
    {
        $this->resourceCollection = PlatformCollection::class;
        $this->resourceItem = PlatformResource::class;
    }

    public function index()
    {
        $user = auth()->user();
        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $collection = Branch::where([
            ['company_id', $user->company->id],
        ])->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param PlatformCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PlatformCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        if ($request->has('front_side'))
            $data['front_side'] = $this->uploadBranchGallery($request->file('front_side'));

        if ($request->has('inside'))
            $data['inside'] = $this->uploadBranchGallery($request->file('inside'));

        $data['company_id'] = $user->company->id;
        $data['slug'] = Str::slug($data['title']);

        DB::beginTransaction();
        $branch = Branch::create($data);
        BranchStatusLog::create([
            'branch_id' => $branch->id,
            'branch_status_id' => 1,
            'user_id' => $user->id
        ]);

        $memberBranch = new MemberBranch();
        $memberBranch->member_id = $user->member->id;
        $memberBranch->branch_id = $branch->id;
        $memberBranch->status = 2;
        $memberBranch->type = 1;
        $memberBranch->is_published = 1;
        $memberBranch->is_active = 1;
        $memberBranch->save();

        DB::commit();
        return $this->respondWithItem($branch, Response::HTTP_CREATED, 'success');
    }

    /**
     * Update resource
     *
     * @param PlatformUpdateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PlatformUpdateRequest $request, $branch_id)
    {
        $user = auth()->user();
        $check = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->first();

        if (!$check) {
            return $this->respondWithCustomData(['message' => 'Branch not found'], Response::HTTP_NOT_FOUND, "failed");
        }

        $data = $request->only(array_keys($request->rules()));
        if ($request->has('front_side'))
            $data['front_side'] = $this->uploadBranchGallery($request->file('front_side'));

        if ($request->has('inside'))
            $data['inside'] = $this->uploadBranchGallery($request->file('inside'));

        $data['company_id'] = $user->company->id;
        $data['slug'] = Str::slug($data['title']);

        DB::beginTransaction();
        $branch = $check->update($data);
        DB::commit();
        return $this->respondWithItem($check, Response::HTTP_CREATED, 'success');
    }

    /**
     * Display a specified resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id)
    {
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->first();

        return $this->respondWithItem($branch, Response::HTTP_OK, 'success');
    }

    /**
     * Submit to review platforms
     *
     * @param PlatformCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(PlatformCreateRequest $request, $branch_id = null)
    {
        $user = auth()->user();
        $branch_id = (int) request()->get('branchId');
        if ($branch_id == NULL) {
            $this->store($request);
            $branch = Branch::where([
                ['is_published', 0],
                ['is_active', 0],
            ])->orderBy('created_at', 'DESC')->first();
        } else {
            $branch = Branch::where([
                ['company_id', $user->company->id],
                ['id', $branch_id]
            ])->first();

            if (!$branch) {
                return $this->respondWithCustomData(['message' => 'Branch not found'], Response::HTTP_NOT_FOUND, "failed");
            }
            $statusId = $branch->statusLog->last()->status->id;

            if ($statusId != 1 && $statusId != 4) {
                return $this->respondWithCustomData(['message' => 'Branch under review or has been actived'], Response::HTTP_BAD_REQUEST, "failed");
            }
        }
        DB::beginTransaction();
        BranchStatusLog::create([
            'branch_id' => $branch->id,
            'branch_status_id' => 2,
            'user_id' => $user->id
        ]);
        DB::commit();

        return $this->respondWithItem($branch, Response::HTTP_OK, 'success');
    }

    private function uploadBranchGallery($file)
    {
        $imageFileName = NULL;
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.branchGallery') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.branchGallery'), 'public');
        }

        return $imageFileName;
    }

    /**
     * @param string $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkLegalDocument($service)
    {
        $user = auth()->user();
        $service = Service::where('slug', $service)->first();
        if (!$service) {
            return $this->respondWithCustomData(['message' => 'Service not found'], Response::HTTP_NOT_FOUND, "failed");
        }

        $checkDocument = CompanyDocument::where([
            ['user_id', $user->id],
            ['service_id', $service->id],
        ])->firstOrFail();

        if ($checkDocument) {
            return $this->respondWithCustomData(['message' => 'ok'], Response::HTTP_OK, "success");
        }
    }

    /**
     * Upload Logo
     *
     * @param PlatformUploadLogo $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadLogo(PlatformUploadLogo $request, $branch_id)
    {
        $imageFileName = NULL;
        $link = NULL;
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->firstOrFail();


        $file = $request->file('logo');
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.branchLogo') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.branchLogo'), 'public');

            $branch->logo = $imageFileName;
            $branch->save();

            $link = ENV('CDN') . '/' . config('cdn.branchLogo') . $branch->logo;
        } else {
            $branch->logo = NULL;
            $branch->save();
        }

        return $this->respondWithCustomData(['link' => $link], Response::HTTP_OK, 'success');
    }

    /**
     * Upload uploadThumbnail
     *
     * @param PlatformUploadThumbnail $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadThumbnail(PlatformUploadThumbnail $request, $branch_id)
    {
        $imageFileName = NULL;
        $link = NULL;
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id', $branch_id]
        ])->firstOrFail();


        $file = $request->file('thumbnail');
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.branchThumbnail') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.branchThumbnail'), 'public');

            $branch->thumbnail = $imageFileName;
            $branch->save();

            $link = ENV('CDN') . '/' . config('cdn.branchThumbnail') . $branch->thumbnail;
        } else {
            $branch->thumbnail = NULL;
            $branch->save();
        }

        return $this->respondWithCustomData(['link' => $link], Response::HTTP_OK, 'success');
    }
}
