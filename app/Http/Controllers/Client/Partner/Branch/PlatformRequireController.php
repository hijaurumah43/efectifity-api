<?php

namespace App\Http\Controllers\Client\Partner\Branch;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchRequire;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Internal\Question\QuestionCategory;
use App\Http\Resources\Client\Partner\Branch\PlatformRequireResource;
use App\Http\Resources\Client\Partner\Branch\PlatformRequireCollection;
use App\Http\Requests\Client\Partner\Branch\PlatformRequireCreateRequest;

class PlatformRequireController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PlatformRequireCollection::class;
        $this->resourceItem = PlatformRequireResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = QuestionCategory::where([
            ['is_active', 1],
            ['is_published', 1],
        ])->paginate();

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param PlatformRequireCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PlatformRequireCreateRequest $request)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id',  $data['branch_id']]
        ])->firstOrFail();

        DB::beginTransaction();
            foreach ($request['requirements'] as $key => $value) {

                $checkQuestion = QuestionCategory::where('id', $value['id'])->first();
                if(!$checkQuestion) {
                    return $this->respondWithCustomData(['message' => 'requirements not found'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
                }

                BranchRequire::where([
                    ['branch_id', $branch->id],
                    ['question_category_id', $value['id']]
                ])->delete();

                $payLoad['branch_id'] = $branch->id;
                $payLoad['question_category_id'] = $value['id'];
                $payLoad['status'] = $value['status'];

                BranchRequire::create($payLoad);
            }
        DB::commit();

        return $this->respondWithCustomData(['message' => 'Successfully Saved'], Response::HTTP_CREATED, 'success');
    }
}
