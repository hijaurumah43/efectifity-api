<?php

namespace App\Http\Controllers\Client\Partner\Branch;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Partner\Branch\BranchAgreement;
use App\Http\Resources\Client\Partner\Branch\PlatformAgreementResource;
use App\Http\Resources\Client\Partner\Branch\PlatformAgreementCollection;
use App\Http\Requests\Client\Partner\Branch\PlatformAgreementCreateRequest;

class PlatformAgreementController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PlatformAgreementCollection::class;
        $this->resourceItem = PlatformAgreementResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($branch_id)
    {
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id',  $branch_id]
        ])->firstOrFail();
        $version = [];
        $draft = [];
        $collections = BranchAgreement::where('branch_id', $branch->id)->orderBy('created_at', 'DESC')->get();
        foreach ($collections as $collection) {
            if ($collection->is_draft == 1) {
                $draft = [
                    'id' => $collection->id,
                    'version' => $collection->version,
                    'content' => $collection->content,
                    'isDraft' => $collection->is_draft == 1 ? true : false,
                    'status' => $collection->is_published == 1 ? 'active' : 'inactive',
                ];
            } else {
                $version [] = [
                    'id' => $collection->id,
                    'version' => $collection->version,
                    'content' => $collection->content,
                    'isDraft' => $collection->is_draft == 1 ? true : false,
                    'status' => $collection->is_published == 1 ? 'active' : 'inactive',
                ];
            }
        }

        return $this->respondWithCustomData([
            'version' => $version,
            'draft' => count($draft) > 0 ? $draft : NULL,
        ], Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param PlatformAgreementCreateRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PlatformAgreementCreateRequest $request, $branch_id)
    {
        $user = auth()->user();
        $agreementId = (int) request()->get('agreementId');
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id',  $branch_id]
        ])->firstOrFail();

        $data = $request->only(array_keys($request->rules()));
        $data['branch_id'] = $branch->id;
        $data['version'] = date('Y-m-d H:i');

        switch (strtolower($data['type'])) {
            case 'draft':
                $type = 1;
                break;

            case 'publish':
                $type = 2;
                break;

            default:
                return $this->respondWithCustomData(['message' => 'Type not supported'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
                break;
        }

        if ($type == 1) {
            $branchAgreement = BranchAgreement::where('is_draft', 1)->first();
            if ($branchAgreement) {
                // return $this->respondWithCustomData(['message' => 'Draft is already exist'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
                $branchAgreement->delete();
            }
            $data['is_draft'] = 1;
            $data['is_published'] = 0;
            BranchAgreement::create($data);

            return $this->respondWithCustomData(['message' => 'Data saved'], Response::HTTP_CREATED, 'created');
        } else {

            $data['is_draft'] = 0;
            $data['is_published'] = 1;
            DB::beginTransaction();
            BranchAgreement::where('branch_id', $branch->id)->update([
                'is_published' => 0,
                'is_draft' => 0
            ]);

            if ($agreementId == NULL) {
                BranchAgreement::create($data);
            } else {
                $branchAgreement = BranchAgreement::findOrFail($agreementId);
                $branchAgreement->update($data);
            }

            DB::commit();

            return $this->respondWithCustomData(['message' => 'New membership agreement has published'], Response::HTTP_CREATED, 'created');
        }
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $agreementId
     * @return \Illuminate\http\JsonResponse
     */
    public function show($branch_id, $agreementId)
    {
        $user = auth()->user();
        $branch = Branch::where([
            ['company_id', $user->company->id],
            ['id',  $branch_id]
        ])->firstOrFail();

        $branchAgreement = BranchAgreement::where([
            ['branch_id', $branch->id],
            ['id', $agreementId],
        ])->firstOrFail();

        return $this->respondWithItem($branchAgreement, Response::HTTP_OK, 'success');
    }
}
