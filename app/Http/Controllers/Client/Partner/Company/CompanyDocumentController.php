<?php

namespace App\Http\Controllers\Client\Partner\Company;

use Image;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Service\ServiceRepository;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Models\Internal\Company\Document\DocumentStatusLog;
use App\Contracts\Client\Partner\Company\CompanyDocumentRepository;
use App\Http\Resources\Client\Partner\Company\CompanyDocumentResource;
use App\Http\Resources\Client\Partner\Company\CompanyDocumentCollection;
use App\Http\Requests\Client\Partner\Company\CompanyDocumentCreateRequest;

class CompanyDocumentController extends Controller
{
    private $companyDocumentRepository;

    /**
     * @param CompanyDocumentRepository $companyDocumentRepository
     */
    public function __construct(CompanyDocumentRepository $companyDocumentRepository)
    {
        $this->companyDocumentRepository = $companyDocumentRepository;
        $this->resourceCollection = CompanyDocumentCollection::class;
        $this->resourceItem = CompanyDocumentResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();

        $services = app(ServiceRepository::class)->findByFilters([
            'is_published' => 1,
            'is_active' => 1.
        ]);

        $temp = [];

        foreach ($services as $service) {
            $companyDocumentId = null;
            $brandName = null;
            $owner = null;
            $isComplete = null;

            $companyDocument = $this->companyDocumentRepository->checkBy([
                'user_id' => $user->id,
                'service_id' => $service->id,
            ]);

            if ($companyDocument) {

                $resource = DocumentStatusLog::where('company_document_id', $companyDocument->id)->orderBy('id', 'DESC')->first();

                $companyDocumentId = $companyDocument->id;
                $brandName = $companyDocument->company_name;
                $owner =  $companyDocument->owner_name;
                $isComplete = ucwords($resource->status->title);
            }

            $temp[] = [
                'service' => [
                    'serviceId' => $service->id,
                    'title' => $service->title,
                    'slug' => $service->slug,
                ],
                'company' => [
                    'companyDocumentId' => $companyDocumentId,
                    'brandName' => $brandName,
                    'owner' => $owner,
                    'isComplete' => $isComplete,
                ]
            ];
        }

        return $this->respondWithCustomData($temp);
    }

    /**
     * Store a newly resource
     *
     * @param CompanyDocumentCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CompanyDocumentCreateRequest $request)
    {
        $user = auth()->user();

        $check = CompanyDocument::where([
            ['user_id', $user->id],
            ['service_id', $request->service_id]
        ])->first();

        $data = $request->only(array_keys($request->rules()));

        if ($request->has('identity_card')) {
            $imgIc = Image::make($request->file('identity_card')->getRealPath());
            $sizeIc = $imgIc->filesize();

            // 2mb
            if ($sizeIc > 2048000) {
                return $this->respondWithCustomData(['message' => 'File Size To Big'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }

            $data['identity_card'] = $this->uploadIdentityCard($request->file('identity_card'));
        }

        if ($request->has('tax_number')) {
            $imgTn = Image::make($request->file('tax_number')->getRealPath());
            $sizeTn = $imgTn->filesize();

            // 2mb
            if ($sizeTn > 2048000) {
                return $this->respondWithCustomData(['message' => 'File Size To Big'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
            $data['tax_number'] = $this->uploadTaxNumber($request->file('tax_number'));
        }

        if ($request->has('company_registration')) {
            $imgCr = Image::make($request->file('company_registration')->getRealPath());
            $sizeCr = $imgCr->filesize();

            // 2mb
            if ($sizeCr > 2048000) {
                return $this->respondWithCustomData(['message' => 'File Size To Big'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
            }
            $data['company_registration'] = $this->uploadCompanyReg($request->file('company_registration'));
        }

        if ($check) {
            DB::beginTransaction();
            $doc = $this->companyDocumentRepository->update($check, $data);

            $statusLog = 1;
            if ($request->is_published == true || $request->is_published == 1) {
                $statusLog = 2;
            }

            DocumentStatusLog::create([
                'company_document_id' => $doc->id,
                'document_status_id' => $statusLog,
                'user_id' => $user->id,
            ]);
            DB::commit();
            return $this->respondWithCustomData(['message' => 'Data successfully saved!'], Response::HTTP_OK);
        }

        DB::beginTransaction();
        $data['user_id'] = $user->id;
        $data['company_id'] = $user->company->id;
        $doc = $this->companyDocumentRepository->store($data);

        $statusLog = 1;
        if ($request->is_published == true || $request->is_published == 1) {
            $statusLog = 2;
        }

        DocumentStatusLog::create([
            'company_document_id' => $doc->id,
            'document_status_id' => $statusLog,
            'user_id' => $user->id,
        ]);

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Data successfully saved'], Response::HTTP_CREATED);
    }

    /**
     * Display a specified resource
     *
     * @param CompanyDocument $companyDocument
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CompanyDocument $companyDocument)
    {
        return $this->respondWithItem($companyDocument, Response::HTTP_OK, 'success');
    }

    private function uploadIdentityCard($file)
    {
        $imageFileName = NULL;
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.companyDocIdentityCard') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.companyDocIdentityCard'), 'public');
        }

        return $imageFileName;
    }

    private function uploadTaxNumber($file)
    {
        $imageFileName = NULL;
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.companyDocTaxNumber') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.companyDocTaxNumber'), 'public');
        }

        return $imageFileName;
    }

    private function uploadCompanyReg($file)
    {
        $imageFileName = NULL;
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.companyDocReg') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.companyDocReg'), 'public');
        }

        return $imageFileName;
    }

    public function log($id)
    { }
}
