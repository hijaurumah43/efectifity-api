<?php

namespace App\Http\Controllers\Client\Partner\Company;

use App\Http\Controllers\Controller;
use App\Http\Resources\Internal\Branch\DocumentLogCollection;
use App\Http\Resources\Internal\Branch\DocumentLogResource;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Models\Internal\Company\Document\DocumentStatusLog;
use Illuminate\Http\Request;

class CompanyDocumentLogController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = DocumentLogCollection::class;
        $this->resourceItem = DocumentLogResource::class;
    }

    public function index($document_id)
    {
        $user = auth()->user();
        $resource = CompanyDocument::where([
            ['company_id', $user->company->id],
            ['id', $document_id]
        ])->firstOrFail();

        $collection = DocumentStatusLog::where([
            ['company_document_id', $resource->id],
            ['document_status_id', '!=', 1]
        ])->orderBy('id', 'ASC')->paginate();

        return $this->respondWithCollection($collection);
    }
}
