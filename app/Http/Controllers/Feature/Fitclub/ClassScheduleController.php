<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Berkayk\OneSignal\OneSignalClient;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Fitclub\Classes\ClassCancel;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Http\Requests\Feature\Fitclub\Classes\ClassCancelRequest;
use App\Http\Resources\Feature\Fitclub\Classes\ClassScheduleResource;
use App\Http\Resources\Feature\Fitclub\Classes\ClassScheduleCollection;

class ClassScheduleController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassScheduleCollection::class;
        $this->resourceItem = ClassScheduleResource::class;
        $this->middleware(['hasBranch']);
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @param int $schedule_detail_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id, $schedule_detail_id)
    {
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = ClassAttendance::where([
            'branch_id' => $branch_id,
            'branch_schedule_detail_id' => $schedule_detail_id
        ]);

        if ($request->has('name')) {
            $query->whereHas('member', function ($q) use ($request) {
                $name = $request->input('name');
                return $q->where('name', 'like', '%' . $name . '%');
            });
        }

        $collection = $query->paginate($perPage);
        return $this->respondWithCollection($collection);
    }

    /**
     * Cancel class by staff
     *
     * @param ClassCancelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(ClassCancelRequest $request, $branch_id, $schedule_detail_id)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();
        if ($request->input('manager') == true) {
            $classes = ClassAttendance::where([
                'branch_id' => $branch_id,
                'branch_schedule_detail_id' => $schedule_detail_id,
                'class_attendance_status_id' => 1
            ])->get();
        } else {
            $classes = ClassAttendance::whereIn('id', $data['class_attendance_id'])->where([
                'branch_id' => $branch_id,
                'branch_schedule_detail_id' => $schedule_detail_id,
                'class_attendance_status_id' => 1
            ])->get();
        }

        foreach ($classes as $class) {
            // CANCEL CLASS
            $class->note = $data['note'];
            $class->cancel_time = date('Y-m-d H:i:s');
            $class->user_id = $user->id;
            $class->class_attendance_status_id = 3;
            $class->is_cancellation = 1;
            $class->is_active = 0;
            $class->save();

            // UPDATE INFORMATION ON PROGRAM TAB HISTORY
            $classBranch = $class->branch->title;
            $className = $class->scheduleDetail->class->title;
            $classCoach = $class->scheduleDetail->class->branchCoach->first()->member->name . ' ' . $class->scheduleDetail->class->branchCoach->first()->member->last_name;
            $classTime = Carbon::parse($class->scheduleDetail->schedule->date)->format('d F Y') . ' ' . $class->scheduleDetail->start_time;
            $message = $classBranch . " has canceled " . $className . " class with coach " . $classCoach . " on " . $classTime;

            $memberLog = MemberLog::create([
                'member_id' => $class->member_id,
                'branch_id' => $branch_id,
                'class_attendance_id' => $class->id,
                'status' => 5, // Cancel By Compnay or Branch
                'message' => $message,
            ]);

            // UPDATE INFORMATION ON FITCLUB CLASS TAB
            /* if ($request->input('manager') == true) {
                $scheduleDetail = BranchScheduleDetail::find($schedule_detail_id);
                $scheduleDetail->is_cancelled = 1;
                $scheduleDetail->save();
            } */

            // UPDATE INFORMATION ON NOTIFICATION
            event(new ClassCancel($memberLog));
            $dataFb = [
                'slug' => 'upcoming',
                'param' => [
                    'key' => 'status',
                    'value' => 'expired', // class att id
                ]
            ];
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);

            // INCREMENT SESSION
            $memberPackage = MemberPackage::find($class->member_package_id);
            $memberPackage->increment('session_remaining', 1);
            $memberPackage->save();
        }

        // UPDATE INFORMATION ON FITCLUB CLASS TAB
        if ($request->input('manager') == true) {
            $scheduleDetail = BranchScheduleDetail::find($schedule_detail_id);
            $scheduleDetail->is_cancelled = 1;
            $scheduleDetail->save();
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Sucessfully Cancelled'], Response::HTTP_OK, 'success');
    }
}
