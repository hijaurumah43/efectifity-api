<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Feature\Fitclub\Member\MemberActivityCollection;
use App\Http\Resources\Feature\Fitclub\Member\MemberActivityResource;
use App\Models\Client\Member\Log\MemberLog;
use Symfony\Component\HttpFoundation\Response;

class CustomerActivityController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = MemberActivityCollection::class;
        $this->resourceItem = MemberActivityResource::class;
        $this->middleware(['hasBranch']);
    }
    public function history(Request $request, $branch_id, $member_id)
    {
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        if (!$request->has('rangeTime')) {
            $this->respondWithCustomData(['message' => 'Query param rangeTime is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $dateTime = $request->input('rangeTime');

        $dateTime = explode(',', $dateTime);
        $from = $dateTime[0];
        $to = $dateTime[1];

        $collection = MemberLog::where([
            ['branch_id', $branch_id],
            ['member_id', $member_id]
        ])
            ->whereBetween('created_at', [$from, $to])
            ->paginate($perPage);
            
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }
}
