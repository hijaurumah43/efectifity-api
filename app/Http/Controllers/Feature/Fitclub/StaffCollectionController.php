<?php

namespace App\Http\Controllers\Feature\Fitclub;

use App\Http\Controllers\Controller;
use App\Http\Resources\Feature\Fitclub\Staff\StaffCollection;
use App\Http\Resources\Feature\Fitclub\Staff\StaffResource;
use App\Models\Client\Member\Classes\MemberBranch;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class StaffCollectionController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = StaffCollection::class;
        $this->resourceItem = StaffResource::class;
        $this->middleware('hasBranch');
    }

    public function index(Request $request, $branch_id)
    {
        // Query Param
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = MemberBranch::where([
            ['branch_id', $branch_id],
        ]);

        if ($request->has('name')) {
            $name = $request->get('name');
            $query->whereHas('member', function ($q) use ($name) {

                return $q->where('name', 'like', '%' . $name . '%');
            });
        }

        if ($request->has('status')) {
            $status = $request->get('status');
            if ($status == 1) {

                $query->where('status', 2);
            } else {
                $query->where('status', '!=', 2);
            }
        }


        $collection = $query->paginate($perPage);
        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }
}
