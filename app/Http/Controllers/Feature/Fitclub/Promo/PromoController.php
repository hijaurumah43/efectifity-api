<?php

namespace App\Http\Controllers\Feature\Fitclub\Promo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Staff\Promo\Promo;
use App\Models\Common\Branch\BranchPackage;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Feature\Fitclub\Promo\PromoResource;
use App\Http\Resources\Feature\Fitclub\Promo\PromoCollection;
use App\Http\Requests\Feature\Fitclub\Promo\CreatePromoRequest;
use App\Http\Requests\Feature\Fitclub\Promo\UpdatePromoRequest;

class PromoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['hasBranch']);
        $this->resourceCollection = PromoCollection::class;
        $this->resourceItem = PromoResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        // Note: get collection promo club
        $collection = Promo::where([
            'branch_id' => $branch_id,
            'type' => 1,
        ])->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $promo_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $promo_id)
    {
        $resource = Promo::where([
            ['branch_id', $branch_id],
            ['id', $promo_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource);
    }

    /**
     * Store a new resource
     *
     * @param CreatePromoRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreatePromoRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $user = auth()->user();
        $data['member_id'] = $user->member->id;

        // Note: check promo periode
        $package = BranchPackage::where('id', $data['branch_package_id'])->first();
        if (!$package) {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'Package not found',
            ], Response::HTTP_OK);
        }

        $data['branch_id'] = $package->branch_id;

        // Go to Publish
        if ($data['status'] == 1) {

            $changed = false;
            if ($package->title != $data['title']) {
                $changed == true;
            }

            if ($package->gym_access != $data['gym_access']) {
                $changed == true;
            }

            if ($package->session != $data['session']) {
                $changed == true;
            }

            if ($package->pt_session != $data['pt_session']) {
                $changed == true;
            }

            if ($package->price != $data['price']) {
                $changed == true;
            }

            if ($package->duration . ' ' . $package->periode != $data['duration'] . ' ' . $data['periode']) {
                $changed == true;
            }

            if ($changed = false) {
                return $this->respondWithCustomData([
                    'status' => 0,
                    'message' => 'Promo value is not allowed to be the same as original'
                ], Response::HTTP_OK);
            }

            // Note: check promo value
            $samePeriod = false;

            $checkPromo = Promo::where('start_time', '>=', $data['start_time'])->where('end_time', '<=', $data['end_time'])->get();
            if (count($checkPromo) > 0) {
                $samePeriod = true;
            }

            if ($samePeriod == true) {
                return $this->respondWithCustomData([
                    'status' => 0,
                    'message' => 'We found conflict promo period, please recheck'
                ], Response::HTTP_OK);
            }
        }

        DB::beginTransaction();
        // Note:  store
        $promo = Promo::create($data);
        $branchPackage = new BranchPackage();
        $branchPackage->promo_id = $promo->id;
        $branchPackage->branch_id = $promo->branch_id;
        $branchPackage->title = $promo->title;
        $branchPackage->gym_access = $promo->gym_access;
        $branchPackage->duration = $promo->duration;
        $branchPackage->periode = $promo->periode;
        $branchPackage->pt_session = $promo->pt_session;
        $branchPackage->session = $promo->session;
        $branchPackage->price = $promo->price;
        $branchPackage->is_published = $promo->status;
        $branchPackage->is_unlimited = $promo->is_unlimited;
        $branchPackage->is_promo = 1;
        $branchPackage->save();

        DB::commit();

        return $this->respondWithCustomData([
            'status' => 1,
            'message' => 'New Membership Promo has successfully added!',
        ], Response::HTTP_CREATED);
    }

    public function update(UpdatePromoRequest $request, $branch_id, $promo_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $user = auth()->user();
        $data['member_id'] = $user->member->id;

        // Note: check promo periode
        $package = BranchPackage::where('id', $data['branch_package_id'])->first();
        if (!$package) {
            return $this->respondWithCustomData([
                'status' => 0,
                'message' => 'Package not found',
            ], Response::HTTP_OK);
        }

        $data['branch_id'] = $package->branch_id;

        $resource = Promo::where([
            ['branch_id', $branch_id],
            ['id', $promo_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        // Go to Publish
        if ($data['status'] == 1) {

            $changed = false;
            if ($package->title != $data['title']) {
                $changed == true;
            }

            if ($package->gym_access != $data['gym_access']) {
                $changed == true;
            }

            if ($package->session != $data['session']) {
                $changed == true;
            }

            if ($package->pt_session != $data['pt_session']) {
                $changed == true;
            }

            if ($package->price != $data['price']) {
                $changed == true;
            }

            if ($package->duration . ' ' . $package->periode != $data['duration'] . ' ' . $data['periode']) {
                $changed == true;
            }

            if ($changed = false) {
                return $this->respondWithCustomData([
                    'status' => 0,
                    'message' => 'Promo value is not allowed to be the same as original'
                ], Response::HTTP_OK);
            }

            // Note: check promo value
            $samePeriod = false;

            $checkPromo = Promo::where('id', '!=', $resource->id)->where('start_time', '>=', $data['start_time'])->where('end_time', '<=', $data['end_time'])->get();

            if (count($checkPromo) > 0) {
                $samePeriod = true;
            }

            if ($samePeriod == true) {
                return $this->respondWithCustomData([
                    'status' => 0,
                    'message' => 'We found conflict promo period, please recheck'
                ], Response::HTTP_OK);
            }
        }

        DB::beginTransaction();

        // Note:  Update
        $resource->type = $data['type'];
        $resource->branch_package_id = $data['branch_package_id'];
        $resource->member_type = $data['member_type'];
        $resource->title = $data['title'];
        $resource->duration = $data['duration'];
        $resource->periode = $data['periode'];
        $resource->gym_access = $data['gym_access'];
        $resource->session = $data['session'];
        $resource->pt_session = $data['pt_session'];
        $resource->price = $data['price'];
        $resource->start_time = $data['start_time'];
        $resource->end_time = $data['end_time'];
        $resource->status = $data['status'];
        $resource->member_id = $data['member_id'];
        $resource->is_unlimited = $data['is_unlimited'];
        $resource->save();

        $branchPackage = BranchPackage::where('promo_id', $resource->id)->first();
        if ($branchPackage) {
            $branchPackage->branch_id = $resource->branch_id;
            $branchPackage->title = $resource->title;
            $branchPackage->gym_access = $resource->gym_access;
            $branchPackage->duration = $resource->duration;
            $branchPackage->periode = $resource->periode;
            $branchPackage->pt_session = $resource->pt_session;
            $branchPackage->session = $resource->session;
            $branchPackage->price = $resource->price;
            $branchPackage->is_published = $resource->status;
            $branchPackage->is_unlimited = $resource->is_unlimited;
            $branchPackage->is_promo = 1;
            $branchPackage->save();
        }

        DB::commit();

        return $this->respondWithCustomData([
            'status' => 1,
            'message' => 'Update resource!',
        ], Response::HTTP_OK);
    }
}
