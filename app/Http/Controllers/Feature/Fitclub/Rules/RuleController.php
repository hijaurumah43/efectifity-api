<?php

namespace App\Http\Controllers\Feature\Fitclub\Rules;

use App\Http\Controllers\Controller;
use App\Http\Requests\Feature\Fitclub\Settings\Rules\CreateRuleRequest;
use App\Models\Client\Staff\Rules\BranchRule;
use App\Models\Client\Staff\Rules\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class RuleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['hasBranch']);
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $collections = BranchRule::where('branch_id', $branch_id)->get();

        if (count($collections) < 1) {
            $collections = Rule::all();
        }

        $offlineClass = [];
        $onlineClass = [];
        $ptClass = [];
        foreach ($collections as $collection) {

            $data = [
                'id' => isset($collection->rule) ? $collection->rule_id : $collection->id,
                'sequence' => isset($collection->rule) ? $collection->rule->sequence : $collection->sequence,
                'title' => isset($collection->title) ? $collection->title : $collection->rule->title,
                'duration' => $collection->duration,
                'period' => [
                    'id' => $collection->period,
                    'title' => $this->ruleNamePeriode($collection->period)
                ],
                'description' => isset($collection->description) ? $collection->description : $collection->rule->description,
                'isActive' => $collection->is_active == 1 ? true : false,
                'isShow' => $collection->is_show == 1 ? true : false,
            ];

            // offline class
            if ($collection->type == 1) {
                $offlineClass[] = $data;
            }

            // online class
            if ($collection->type == 2) {
                $onlineClass[] = $data;
            }

            // personal trainer
            if ($collection->type == 3) {
                $ptClass[] = $data;
            }
        }

        // SORT BY SEQUENCE
        $offlineClass = collect($offlineClass)->sortBy('sequence');
        $offlineClass = $offlineClass->values()->all();

        $onlineClass = collect($onlineClass)->sortBy('sequence');
        $onlineClass = $onlineClass->values()->all();

        $ptClass = collect($ptClass)->sortBy('sequence');
        $ptClass = $ptClass->values()->all();

        $a = [
            'typeID' => 1,
            'title' => 'Offline Class Reservation',
            'collections' => $offlineClass
        ];

        $b = [
            'typeID' => 2,
            'title' => 'Online Class Reservation',
            'collections' => $onlineClass
        ];

        $c = [
            'typeID' => 3,
            'title' => 'Personal Trainer Reservation',
            'collections' => $ptClass
        ];

        $items = collect([$a, $b, $c]);

        return $this->respondWithCustomData($items, Response::HTTP_OK);
    }

    /**
     * convert
     *
     * @param int $id
     * @return string
     */
    private function ruleNamePeriode($id)
    {
        $name = NULL;
        switch ($id) {
            case '1':
                $name = 'Minute';
                break;

            case '2':
                $name = 'Hour';
                break;

            case '3':
                $name = 'Day';
                break;

            case '4':
                $name = 'Month';
                break;

            case '5':
                $name = 'Year';
                break;

            default:
                $name = 'Times';
                break;
        }

        return $name;
    }

    /**
     * Store a newly resource
     *
     * @param CreateRuleRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRuleRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();
        BranchRule::where('branch_id', $branch_id)->delete();

        // NOTE: STORE DATA
        $isShow = 0;
        foreach ($data['rules'] as $value) {

            if ($value['isShow'] == true) {
                $isShow = 1;
            } else {
                $isShow = 0;
            }

            BranchRule::create([
                'branch_id' => $branch_id,
                'rule_id' => $value['id'],
                'duration' => $value['duration'],
                'period' => $value['period'],
                'type' => $value['type'],
                'is_active' => $value['is_active'],
                'is_show' => $isShow,
            ]);
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Rules is successfully updated!'], Response::HTTP_OK);
    }

    /**
     * Reset rule
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset($branch_id)
    {
        BranchRule::where('branch_id', $branch_id)->delete();

        return $this->respondWithCustomData(['message' => 'Rules hees been reset'], Response::HTTP_OK);
    }
}
