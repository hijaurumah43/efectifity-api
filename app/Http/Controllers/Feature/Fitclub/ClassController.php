<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\BranchSchedule;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Http\Resources\Feature\Fitclub\Classes\ClassResource;
use App\Http\Resources\Feature\Fitclub\Classes\ClassCollection;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\User;

class ClassController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassCollection::class;
        $this->resourceItem = ClassResource::class;
        $this->middleware(['hasBranch']);
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        if (!$request->has('rangeTime')) {
            $this->respondWithCustomData(['message' => 'Query param rangeTime is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $dateTime = $request->input('rangeTime');

        $dateTime = explode(',', $dateTime);
        $from = $dateTime[0];
        $to = $dateTime[1];

        $scheduleId = BranchSchedule::where([
            'branch_id' => $branch_id,
            'is_published' => 1,
        ])
            ->whereBetween('date', [$from, $to])
            ->select('id')
            ->get();

        $collection = BranchScheduleDetail::whereIn('branch_schedule_id', $scheduleId)->where('is_published', 1)->paginate($perPage);
        // return $collection;
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display specified of resource
     *
     * @param int $branch_id
     * @param int $schedule_detail_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $schedule_detail_id)
    {
        $scheduelDetail = BranchScheduleDetail::where('id', $schedule_detail_id)->whereHas('schedule', function ($q) use ($branch_id) {
            $q->where('branch_id', $branch_id);
        })->first();
        if (!$scheduelDetail) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'success');
        }

        // check schedule is your branch
        if (!$scheduelDetail->schedule->where('branch_id', $branch_id)->first()) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'success');
        }

        return $this->respondWithItem($scheduelDetail, Response::HTTP_OK, 'success');
    }

    /**
     * Get Manager
     *
     * @param int $branch_id
     * @param int $schedule_detail_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getManager($branch_id)
    {
        $manager = MemberBranch::where([
            ['branch_id', $branch_id],
            ['type', 1],
            ['is_published', 1],
            ['is_active', 1],
        ])->first();

        if (!$manager) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'failed');
        }

        return $this->respondWithCustomData([
            'managerId' => $manager->id,
            'avatar' => $manager->member->avatar,
            'firstName' => $manager->member->name,
            'lastName' => $manager->member->last_name,
        ], Response::HTTP_OK, 'success');
    }

    /**
     * Check PIN Manager
     *
     * @param int $branch_id
     * @param int $memberBranchId
     * @param int $pin
     * @return \Illuminate\Http\JsonResponse
     */
    public function pinManager($branch_id, $memberBranchId, $pin)
    {
        $manager = MemberBranch::where([
            ['id', $memberBranchId],
            ['branch_id', $branch_id],
            ['type', 1],
            ['is_published', 1],
            ['is_active', 1],
        ])->first();

        if (!$manager) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'failed');
        }

        $user = User::where([
            'id' => $manager->member->user->id,
            'pin' => $pin,
        ])->first();

        if (!$user) {
            $message = __('Invalid pin');
            return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
        }

        return $this->respondWithCustomData([
            'message' => 'success',
        ], Response::HTTP_OK, 'success');
    }
}
