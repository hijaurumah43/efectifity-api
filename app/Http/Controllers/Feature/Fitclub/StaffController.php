<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Client\Staff\Schedule;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffAttendance;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Requests\Feature\Fitclub\Staff\CreateScheduleNoteRequest;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $required_fields = ['from', 'to'];
        $error_fields = '';
        $error = false;
        $parameter = array();
        foreach ($required_fields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $error_fields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => 'query param ' . $error_fields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $startDate = $request->get('from');
        $endDate = $request->get('to');

        $from = Carbon::parse($startDate);
        $to = Carbon::parse($endDate);

        $totalDate = $to->diffInDays($from);
        $totalAttendance = 0;
        $totalAtt = 0;

        // Get schedule by date and branch
        // $query = StaffSchedule::where([
        $query = Schedule::where([
            ['status', 1],
            ['branch_id', $branch_id]
        ])
            ->whereHas('detail', function ($q) use ($startDate, $endDate) {
                $q->whereDate('start_time', '>=', $startDate)->whereDate('end_time', '<=', $endDate);
            });

        if ($request->has('id')) {
            $memberBranchId = $request->input('id');
            $query->whereHas('detail', function ($q) use ($memberBranchId) {
                $q->where('member_branch_id', $memberBranchId);
            });
        }

        $staffSchedules = $query->get();

        //Define
        $duration = 0;
        $collection = [];

        // looping schedule detail
        foreach ($staffSchedules as $staffSchedule) {
            // detail schedule
            $staffScheduleDetail = $staffSchedule->detail()->whereDate('start_time', '>=', $startDate)->whereDate('end_time', '<=', $endDate)->orderBy('start_time', 'DESC') //;
                ->whereHas('memberBranch', function ($queryMemberBranch) {
                    return $queryMemberBranch->where('status', 2);
                });

            if ($request->has('id')) {
                $staffScheduleDetail->where('member_branch_id', $request->get('id'));
            }

            if ($request->has('name')) {
                $queryName = $request->get('name');
                $staffScheduleDetail->whereHas('memberBranch', function ($q) use ($queryName) {
                    $q->whereHas('member', function ($member) use ($queryName) {
                        $member->where('name', 'LIKE', '%' . $queryName . '%');
                    });
                });
            }

            $staffScheduleDetail = $staffScheduleDetail->get();
            $totalAtt  = count($staffScheduleDetail);

            $perPage = $request->get('limit');

            // return $staffScheduleDetail;
            foreach ($staffScheduleDetail as $item) {

                // Duration
                $from = Carbon::createFromFormat('Y-m-d H:i:s', $item->start_time);
                $to = Carbon::createFromFormat('Y-m-d H:i:s', $item->end_time);
                $duration = $to->diff($from)->format('%HH %iM');

                // Position
                $position = [];
                if ($item->memberBranch->member->user) {
                    $user = $item->memberBranch->member->user;
                    foreach ($user->roles as $data) {
                        $position[] = $data->name;
                    }
                }

                $clokcInFormat = '-';
                $clokcOutFormat = '-';
                $durationFormat = '-';
                if ($item->note != strtolower('not-checked-in')) {

                    // Get clock in
                    $getDateClockIn = Carbon::parse($item->start_time)->format('Y-m-d');
                    $setStartTime = Carbon::parse($item->start_time)->subHour(2)->format('Y-m-d H:i:s');
                    $clokcIn = StaffAttendance::where([
                        ['time', '>=', $setStartTime],
                        ['time', '<=', $item->end_time],
                        ['member_branch_id', $item->member_branch_id],
                        ['status', 1],
                        ['is_active', 1],
                    ])
                        // ->whereDate('time', $getDateClockIn)
                        ->orderBy('time', 'ASC')->first();
                    // return $clokcIn;

                    if ($clokcIn) {
                        /* $timeDisplay = Carbon::parse($item->start_time)->subHour(2)->format('Y-m-d H:i:s');
                        if ($clokcIn->time > $timeDisplay) {
                            $clokcInFormat = Carbon::parse($clokcIn->time)->format('H.i');
                        } */
                        // NOTE:: CONVERT_DT
                        // $clokcInFormat = Carbon::parse($clokcIn->time)->format('H.i');
                        $clokcInFormat = ConvertToTimestamp($clokcIn->time);
                        $totalAttendance++;
                    }

                    // Get clock out
                    $setStartTimeClockOut = Carbon::parse($item->start_time)->addMinutes(31)->format('Y-m-d H:i:s');
                    $setEndTime = Carbon::parse($item->end_time)->addMinutes(31)->format('Y-m-d H:i:s');
                    $clokcOut = StaffAttendance::where([
                        ['time', '>=', $setStartTimeClockOut],
                        ['time', '<=', $setEndTime],
                        ['member_branch_id', $item->member_branch_id],
                        ['status', 0],
                        ['is_active', 1],
                    ])->orderBy('time', 'DESC')->first();
                    // return $item->start_time;
                    if ($clokcOut) {
                        // NOTE:: CONVERT_DT
                        // $clokcOutFormat = Carbon::parse($clokcOut->time)->format('H.i');
                        $clokcOutFormat = ConvertToTimestamp($clokcOut->time);
                    }

                    if ($clokcIn && $clokcOut) {
                        $clockInFormat = Carbon::createFromFormat('Y-m-d H:i:s', $clokcIn->time);
                        $clockOutFormat = Carbon::createFromFormat('Y-m-d H:i:s', $clokcOut->time);
                        $durationFormat = $clockOutFormat->diff($clockInFormat)->format('%HH %iM');
                    }
                }

                // return $clokcOut->time;

                $collection[] = [
                    'scheduleDetailId' => $item->id,
                    // NOTE:: CONVERT_DT
                    // 'datetime' => Carbon::parse($item->start_time)->format('Y-m-d'),
                    'datetime' => ConvertToTimestamp($item->start_time),
                    'shift' => [
                        'title' => $item->title,
                        'startTime' => Carbon::parse($item->shift->start_time)->format('H.i'),
                        'endTime' => Carbon::parse($item->shift->end_time)->format('H.i'),
                        'duration' => $duration,
                    ],
                    'employee' => [
                        'id' => $item->memberBranch->id,
                        'memberId' => $item->memberBranch->member->member_id,
                        'firstName' => $item->memberBranch->member->name,
                        'lastName' => $item->memberBranch->member->last_name,
                        'avatar' => Member::avatarStorage($item->memberBranch->member->avatar),
                    ],
                    'position' => $position,
                    'clockin' => $clokcInFormat,
                    'clockout' => $clokcOutFormat,
                    'duration' => $durationFormat,
                    'attributes' => [
                        'note' => $item->note,
                        'createdBy' => isset($item->user) ? $item->user->member->name : null
                    ]
                ];
            }


            // $collection = collect($collection)->sortByDesc('date');
        }
        $collection     = collect($collection);
        /* Paginate */
        $collection     = collect(json_decode($collection));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $collection->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataSchedule   = $paginate->items();

        return response()->json([
            'attributes' => [
                // 'totalDate' => $totalDate,
                'totalDate' => $paginate->total(),
                'totalAttendance' => $totalAttendance,
            ],
            "data" => $dataSchedule,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
        ], Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $member_branch_id)
    {
        $temp = NULL;
        $memberBranch = MemberBranch::where('id', $member_branch_id)->first();
        if (!$memberBranch) {
            return $this->respondWithCustomData($temp, Response::HTTP_OK, 'success');
        }

        // Member
        $member = $memberBranch->member;
        // $isActive = $memberBranch->is_published == 1 && $memberBranch->is_active == 1 ? true : false;
        $isActive = $memberBranch->status == 2 ? true : false;

        // Position
        $position = [];
        if ($memberBranch->member->user) {
            $user = $memberBranch->member->user;
            foreach ($user->roles as $data) {
                $position[] = $data->name;
            }
        }

        $temp = [
            'employee' => [
                'id' => $memberBranch->id,
                'memberId' => $member->member_id,
                'firstName' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
                'isActive' => $isActive,
                'status' => statusStaff($memberBranch->status),
            ],
            'position' => $position,
            'startingDate' => Carbon::parse($memberBranch->created_at)->format('d F Y'),
            'contact' => [
                'mobilePhone' => $member->user->phone,
                'email' => $member->user->email,
            ],

        ];

        return $this->respondWithCustomData($temp, Response::HTTP_OK, 'success');
    }

    /**
     * Display resource by schedule id
     *
     * @param int $branch_id
     * @param int $schedule_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByScheduelId($branch_id, $schedule_id)
    {
        $temp = NULL;
        $item = StaffScheduleDetail::where('id', $schedule_id)->first();
        if (!$item) {
            return $this->respondWithCustomData($temp, Response::HTTP_OK, 'success');
        }

        // Duration
        $from = Carbon::createFromFormat('Y-m-d H:i:s', $item->start_time);
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $item->end_time);
        $duration = $to->diff($from)->format('%HH %iM');

        // Member
        $member = $item->memberBranch->member;

        $temp = [
            'scheduleDetailId' => $item->id,
            'employee' => [
                'id' => $item->memberBranch->id,
                'memberId' => $member->member_id,
                'firstName' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
            ],
            'date' => Carbon::parse($item->start_time)->format("Y-m-d"),
            'shift' => [
                'title' => $item->title,
                'startTime' => Carbon::parse($item->start_time)->format('H.i'),
                'endTime' => Carbon::parse($item->end_time)->format('H.i'),
                'duration' => $duration,
            ],
            'attributes' => [
                'note' => $item->note,
                'createdBy' => isset($item->user) ? $item->user->member->name : null
            ]
        ];

        return $this->respondWithCustomData($temp, Response::HTTP_OK, 'success');
    }

    /**
     * Add note in schedule
     *
     * @param CreateScheduleNoteRequest $request
     * @param int $branch_id
     * @param int $schedule_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function note(CreateScheduleNoteRequest $request, $branch_id, $schedule_id)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));

        $staffScheduleDetail = StaffScheduleDetail::where('id', $schedule_id)->first();

        if (!$staffScheduleDetail) {
            return $this->respondWithCustomData(['message' => 'Schedule note found'], Response::HTTP_OK, 'failed');
        }

        $staffScheduleDetail->note = $data['note'];
        $staffScheduleDetail->created_by = $user->id;
        $staffScheduleDetail->save();

        return $this->respondWithCustomData(['message' => 'Data Successfully Saved'], Response::HTTP_CREATED, 'success');
    }
}
