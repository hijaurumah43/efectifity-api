<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Feature\Fitclub\Member\MemberCollection;
use App\Http\Resources\Feature\Fitclub\Member\MemberResource;
use App\Models\Client\Member\Member;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Staff\Attendance\MemberAttendance;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = MemberCollection::class;
        $this->resourceItem = MemberResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        if (!$request->has('attributes')) {
            return $this->respondWithCustomData(['message' => 'query param attributes is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        if ($request->get('attributes') != $branch_id) {
            return $this->respondWithCustomData(['message' => 'access denied'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $name = $request->input('name');
        $collection = Member::whereHas('memberPackage', function ($query) use ($branch_id) {
            $query->where([
                ['branch_id', $branch_id],
            ]);
        })->where('name', 'like', '%' . $name . '%')->paginate($perPage);

        return $this->respondWithCollection($collection, $branch_id);
    }

    /**
     * Display a specified of resource
     *
     * @param int $branch_id
     * @param int $customer_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $branch_id, $customer_id)
    {
        if (!$request->has('attributes')) {
            return $this->respondWithCustomData(['message' => 'query param attributes is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        if ($request->get('attributes') != $branch_id) {
            return $this->respondWithCustomData(['message' => 'access denied'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $member = Member::whereHas('memberPackage', function ($query) use ($branch_id) {
            $query->where([
                ['branch_id', $branch_id],
            ]);
        })->where('id', $customer_id)->first();

        if (!$member) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'success');
        }

        return $this->respondWithItem($member, Response::HTTP_OK, 200);
    }
    public function attendance(Request $request, $branch_id)
    {
        // NOTE:: QUERY PARAMETER REQUIRED
        $required_fields = ['from', 'to', 'status'];
        $error_fields = '';
        $error = false;
        $parameter = array();
        foreach ($required_fields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $error_fields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => 'query param ' . $error_fields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // NOTE:: RANGE DATE
        if ($request->has('from') && $request->has('to')) {
            $from = $request->input('from');
            $to = $request->input('to');
        }

        // NOTE:: PAGINATION
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $type = strtolower($request->input('status'));

        switch ($type) {
            case 'attheclub':
                // NOTE:: AT THE CLUB
                $collections = MemberAttendance::where([
                    ['branch_id', $branch_id],
                    ['is_active', 1],
                ])
                    ->whereDate('time', '>=', $from)
                    ->whereDate('time', '<=', $to)
                    ->distinct()
                    ->select('member_id')
                    ->get();

                $data = [];
                foreach ($collections as $collection) {
                    // NOTE:: CHECK LAST STATUS
                    $lastStatus = MemberAttendance::where('member_id', $collection->member_id)->orderBy('time', 'DESC')->first();

                    if ($lastStatus->status == 1) {

                        // NOTE:: CONVERT_DT
                        /* $date = $lastStatus->time->format('Y-m-d');
                        $checkIn = $lastStatus->time->format('H.i.s'); */

                        $date = ConvertToTimestamp($lastStatus->time);
                        $checkIn = ConvertToTimestamp($lastStatus->time);

                        // NOTE:: DURATION
                        $from = Carbon::createFromFormat('Y-m-d H:i:s', $lastStatus->time);
                        $to = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
                        $duration = $to->diff($from)->format('%HH %iM');

                        $data[] = [
                            'attID' => $collection->id,
                            'id' => $collection->member->id,
                            'name' => $collection->member->name,
                            'fitId' => $collection->member->member_id,
                            'avatar' => Member::avatarStorage($collection->member->avatar),
                            'date' => $date,
                            'checkIn' => $checkIn,
                            'checkOut' => NULL,
                            'duration' => $duration,
                        ];
                    }
                }

                break;

            case 'checkin':
                // NOTE:: CHECKIN
                $collections = MemberAttendance::where([
                    ['is_active', 1],
                    ['branch_id', $branch_id],
                    ['status', 1]
                ])
                    ->whereDate('time', '>=', $from)
                    ->whereDate('time', '<=', $to)
                    ->orderBy('time', 'ASC')
                    ->get();
                
                $data = [];
                foreach ($collections as $collection) {
                    // NOTE:: GET CHECKOUT MEMBER
                    $items = MemberAttendance::where([
                        ['status', 0],
                        ['member_id', $collection->member_id],
                    ])
                        ->whereDate('time', $collection->time)
                        ->get();

                    $memberCheckOut = NULL;
                    $duration = NULL;
                    $counter = 0;
                    if (count($items) > 0) {
                        foreach ($items as $item) {

                            // NOTE:: Counter increment if require has heen completed
                            if ($counter == 0) {
                                if ($item->time > $collection->time) {

                                    // NOTE:: Counter increment
                                    $counter = 1;
                                    // NOTE:: CONVERT_DT
                                    // $memberCheckOut = $item->time->format('H.i.s');
                                    $memberCheckOut = ConvertToTimestamp($item->time);

                                    // NOTE:: DURATION
                                    $from = Carbon::createFromFormat('Y-m-d H:i:s', $collection->time);
                                    $to = Carbon::createFromFormat('Y-m-d H:i:s', $item->time);
                                    $duration = $to->diff($from)->format('%HH %iM');
                                }
                            }
                        }
                    }

                    // NOTE:: CONVERT_DT
                    /* $date = $collection->time->format('Y-m-d');
                    $checkIn = $collection->time->format('H.i.s'); */

                    $date = ConvertToTimestamp($collection->time);
                    $checkIn = ConvertToTimestamp($collection->time);

                    $data[] = [
                        'attID' => $collection->id,
                        'id' => $collection->member->id,
                        'name' => $collection->member->name,
                        'fitId' => $collection->member->member_id,
                        'avatar' => Member::avatarStorage($collection->member->avatar),
                        'date' => $date,
                        'checkIn' => $checkIn,
                        'checkOut' => $memberCheckOut,
                        'duration' => $duration,
                    ];
                }

                break;

            case 'checkout':
                // NOTE:: CHECKIN
                $collections = MemberAttendance::where([
                    ['is_active', 1],
                    ['branch_id', $branch_id],
                    ['status', 0]
                ])
                    ->whereDate('time', '>=', $from)
                    ->whereDate('time', '<=', $to)
                    ->orderBy('time', 'ASC')
                    ->get();

                $data = [];
                foreach ($collections as $collection) {
                    // NOTE:: GET CHECKOUT MEMBER
                    $items = MemberAttendance::where([
                        ['status', 1],
                        ['member_id', $collection->member_id],
                    ])
                        ->whereDate('time', $collection->time)
                        ->get();

                    $memberCheckIn = NULL;
                    $duration = NULL;
                    $counter = 0;
                    if (count($items) > 0) {
                        foreach ($items as $item) {

                            // NOTE:: Counter increment if require has heen completed

                            // TODO:: member check in belum bener
                            if ($counter == 0) {
                                if ($collection->time > $item->time) {

                                    // NOTE:: Counter increment
                                    $counter = 1;
                                    // NOTE:: CONVERT_DT
                                    // $memberCheckIn = $item->time->format('H.i.s');
                                    $memberCheckIn = ConvertToTimestamp($item->time);

                                    // NOTE:: DURATION
                                    $from = Carbon::createFromFormat('Y-m-d H:i:s', $item->time);
                                    $to = Carbon::createFromFormat('Y-m-d H:i:s', $collection->time);
                                    $duration = $to->diff($from)->format('%HH %iM');
                                }
                            }
                        }
                    }

                    // NOTE:: CONVERT_DT
                    /* $date = $collection->time->format('Y-m-d');
                    $checkOut = $collection->time->format('H.i.s'); */

                    $date = ConvertToTimestamp($collection->time);
                    $checkOut = ConvertToTimestamp($collection->time);

                    $data[] = [
                        'attID' => $collection->id,
                        'id' => $collection->member->id,
                        'name' => $collection->member->name,
                        'fitId' => $collection->member->member_id,
                        'avatar' => Member::avatarStorage($collection->member->avatar),
                        'date' => $date,
                        'checkIn' => $memberCheckIn,
                        'checkOut' => $checkOut,
                        'duration' => $duration,
                    ];
                }

                break;

            default:
                return $this->respondWithCustomData(['message' => 'status not define'], Response::HTTP_BAD_REQUESAT);
                break;
        }

        $collection     = collect($data)->sortBy('date')->reverse();

        /* Paginate */
        $customer       = collect(json_decode($collection));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $customer->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $customer->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataCustomers  = $paginate->items();

        $metaCheckIn = MemberAttendance::where([
            ['is_active', 1],
            ['branch_id', $branch_id],
            ['status', 1]
        ])
            ->whereDate('time', '>=', $request->input('from'))
            ->whereDate('time', '<=', $request->input('to'))
            ->orderBy('time', 'ASC')
            ->count();

        $metaCheckOut = MemberAttendance::where([
            ['is_active', 1],
            ['branch_id', $branch_id],
            ['status', 0]
        ])
            ->whereDate('time', '>=', $request->input('from'))
            ->whereDate('time', '<=', $request->input('to'))
            ->orderBy('time', 'ASC')
            ->count();

        $metaAtTheClub = $metaCheckIn - $metaCheckOut;

        return response()->json([
            "data" => $dataCustomers,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
            "meta" => [
                "atTheClub" => $metaAtTheClub,
                "checkIn" => $metaCheckIn,
                "checkOut" => $metaCheckOut,
            ],
        ], Response::HTTP_OK);
    }

    public function asdasd(Request $request, $branch_id)
    {
        if ($request->has('from') && $request->has('to')) {
            $from = $request->input('from');
            $to = $request->input('to');
        }

        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $status = 1;
        $atTheClub = false;
        $atCheckInCheckOut = false;
        $atCheckIn = false;
        $atCheckOut = false;

        $collections = MemberAttendance::where('is_active', 1)
            ->where('branch_id', $branch_id)
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->groupBy('member_id', 'date')
            ->orderBy('date', 'DESC')
            ->select('member_id', DB::raw('date(created_at) as date'))
            ->get();

        $data = [];
        $checkIn = NULL;
        $checkOut = NULL;
        foreach ($collections as $collection) {
            if ($request->has('status')) {
                $status = strtolower($request->input('status'));
                if ($status == 'checkin') {
                    $status = 1;
                } else if ($status == 'checkout') {
                    $status = 0;
                } else {
                    $status = 1;
                }
            }

            $lastMember = MemberAttendance::orderBy('created_at', 'DESC')
                ->whereDate('created_at', $collection->date)
                ->where('member_id', $collection->member_id)
                ->where('status', $status)
                ->first();

            // NOTE:: Chceck least data
            if ($lastMember) {

                if ($lastMember->status == 1) {

                    // NOTE:: CHECKIN
                    // NOTE:: has been checkout?
                    $checkOutTime = MemberAttendance::orderBy('created_at', 'DESC')
                        ->whereDate('created_at', $collection->date)
                        ->where('member_id', $collection->member_id)
                        ->where('status', 0)
                        ->first();

                    $checkIn = $lastMember->created_at->format('H.i');
                    if ($checkOutTime) {

                        // TODO :: Checkin - checkout and then checkin, and not checkout

                        // NOTE:: CHECKIN AND CHECKOUT
                        $queryParam = strtolower($request->input('status'));
                        if ($queryParam != 'checkin') {
                            $atTheClub = true;
                        }
                        $atCheckInCheckOut = true;
                        $checkOut = $checkOutTime->created_at->format('H.i');
                        $to = $checkOutTime->created_at;
                    } else {

                        // NOTE:: CHECKIN AND AT THE CLUB
                        $atTheClub = true;
                        $checkOut = NULL;
                        $to = date('Y-m-d H:i:s');
                    }

                    // NOTE:: SET TIME FOR DIFF TIME
                    $from = Carbon::createFromFormat('Y-m-d H:i:s', $lastMember->created_at);
                    $to = Carbon::createFromFormat('Y-m-d H:i:s', $to);
                } else {

                    // NOTE:: CHECKOUT
                    $atCheckInCheckOut = true;

                    // NOTE:: GET LAST TIME CHECKIN ON THE SAME DATE
                    $checkInTime = MemberAttendance::orderBy('created_at', 'DESC')
                        ->whereDate('created_at', $collection->date)
                        ->where('member_id', $collection->member_id)
                        ->where('status', 1)
                        ->first();

                    $checkIn = NULL;
                    $from = NULL;
                    if ($checkInTime) {
                        $checkIn = $checkInTime->created_at->format('H.i');
                        $from = Carbon::createFromFormat('Y-m-d H:i:s', $checkInTime->created_at);
                    }

                    $checkOut = $lastMember->created_at->format('H.i');
                    $to = Carbon::createFromFormat('Y-m-d H:i:s', $lastMember->created_at);
                }
            } else {

                if ($status == 0) {
                    $checkInTime = MemberAttendance::orderBy('created_at', 'DESC')
                        ->whereDate('created_at', $collection->date)
                        ->where('member_id', $collection->member_id)
                        ->where('status', 1)
                        ->first();

                    $checkIn = $checkInTime->created_at->format('H.i');
                    $checkOut = NULL;
                    $to = date('Y-m-d H:i:s');
                    $from = Carbon::createFromFormat('Y-m-d H:i:s', $checkInTime->created_at);
                    $to = Carbon::createFromFormat('Y-m-d H:i:s', $to);
                }
            }

            $duration = $to->diff($from)->format('%HH %iM');

            if ($atTheClub == true || $atCheckInCheckOut == true || $atCheckIn == true || $atCheckOut == true) {

                if ($atTheClub == true && $atCheckInCheckOut == true) {
                    // $data[] = [];
                } else {
                    $data[] = [
                        'id' => $collection->member->id,
                        'name' => $collection->member->name,
                        'fitId' => $collection->member->member_id,
                        'avatar' => Member::avatarStorage($collection->member->avatar),
                        'date' => $collection->date,
                        'checkIn' => $checkIn,
                        'checkOut' => $checkOut,
                        'duration' => $duration,
                    ];
                }
            }
        }

        $collection     = collect($data);

        /* Paginate */
        $customer       = collect(json_decode($collection));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $customer->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $customer->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataCustomers  = $paginate->items();

        $metaCheckIn = MemberAttendance::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where([
            'is_active' => 1,
            'branch_id' => $branch_id,
            'status' => 1,
        ])
            // ->groupBy('date', 'member_id')
            ->select('member_id', DB::raw('date(created_at) as date'))
            ->get();

        // return $metaCheckIn;

        $metaCheckOut = MemberAttendance::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->where([
            'is_active' => 1,
            'branch_id' => $branch_id,
            'status' => 0,
        ])
            // ->groupBy('member_id', 'date')
            ->select('member_id', DB::raw('date(created_at) as date'))
            ->get();

        $metaAtTheClub = count($metaCheckIn) - count($metaCheckOut);

        return response()->json([
            "data" => $dataCustomers,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
            "meta" => [
                "atTheClub" => $metaAtTheClub,
                "checkIn" => count($metaCheckIn),
                "checkOut" => count($metaCheckOut),
            ],
        ], Response::HTTP_OK);
    }
}
