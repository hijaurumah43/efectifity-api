<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Feature\Fitclub\Settings\ClassScheduleRequest;
use App\Http\Resources\Feature\Fitclub\Settings\BranchCoachRessource;
use App\Http\Resources\Feature\Fitclub\Settings\ClassScheduleResource;
use App\Http\Resources\Feature\Fitclub\Settings\ClassScheduleCollection;
use App\Http\Resources\Feature\Fitclub\Settings\CoachRessource;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Staff\ClassSchedule;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchClass;
use App\Models\Common\Branch\BranchClassCoach;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Common\Coach\Coach;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class ClassScheduleController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassScheduleCollection::class;
        $this->resourceItem = ClassScheduleResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $schedules = ClassSchedule::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($schedules);
    }

    public function dateMarked(Request $request, $branch_id, $month, $year)
    {

        $collections = BranchSchedule::where('branch_id', $branch_id)->whereMonth('date', $month)->whereYear('date', $year)->select('date')->orderBy('date', 'ASC')->get();
        if (!$collections) {
            return $this->respondWithCustomData(['message' => 'Data not found']);
        }

        $temp = [];
        foreach ($collections as $value) {
            $temp[] = $value->date;
        }

        return $this->respondWithCustomData(['date' => $temp]);
    }

    public function store(ClassScheduleRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();

        $data['branch_id'] = $branch_id;

        $check = BranchSchedule::where([
            ['branch_id', $branch_id],
            ['date', $data['date']]
        ])->first();

        $data['timestamp'] = $data['end_timestamp'] . '000';

        if (!$check) {
            // Create Branch Schedule
            $branchSchedule = BranchSchedule::create($data);
            $data['branch_schedule_id'] = $branchSchedule->id;
        } else {
            $data['branch_schedule_id'] = $check->id;
        }

        // Create Branch Schedule Detail
        $data['max_quota'] = (int) $data['max_quota'];
        $data['start_timestamp'] = $data['start_timestamp'] . '000';
        $data['end_timestamp'] = $data['end_timestamp'] . '000';
        BranchScheduleDetail::create($data);

        // Destory
        BranchClassCoach::where('branch_class_id', $data['branch_class_id'])->delete();

        // Store Coach
        foreach ($data['coaches'] as $coachId) {
            $storeCoach = new BranchClassCoach();
            $storeCoach->branch_class_id = $data['branch_class_id'];
            // $storeCoach->coach_id = $coachId;
            $storeCoach->member_id = $coachId;
            $storeCoach->save();
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => 'New Class Schedule has Successfully Added!']);
    }

    public function checkAvailableClass($branch_id, $date)
    {
        $checkDate = BranchSchedule::whereDate('date', $date)->where('branch_id', $branch_id)->first();

        // if (!$checkDate) {
        //     return $this->respondWithCustomData(['message' => 'Schedule not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        // }

        // check class available
        $temp = [];
        $classes = BranchClass::where('branch_id', $branch_id)->where('is_published', 1)->get();

        if ($checkDate) {
            foreach ($classes as $class) {

                // $checkClass = BranchScheduleDetail::where([
                //     ['branch_schedule_id', $checkDate->id],
                //     ['branch_class_id', $class->id],
                // ])->first();

                // if (!$checkClass) {

                $tempCoach = NULL;
                if (isset($class->branchCoach)) {
                    $tempCoach = $class->branchCoach->map(function ($m) {
                        return [
                            'id' => isset($m->member) ? $m->member->id : NULL,
                            'name' => isset($m->member) ? ucwords($m->member->name) : NULL,
                        ];
                    });
                }

                $temp[] = [
                    'id' => $class->id,
                    'name' => ucwords($class->title),
                    'isOnline' => $class->is_online == 1 ? true : false,
                    'coach' => $tempCoach,
                ];
                // }
            }
        } else {
            foreach ($classes as $class) {
                $tempCoach = NULL;
                if (isset($class->branchCoach)) {
                    $tempCoach = $class->branchCoach->map(function ($m) {
                        return [
                            'id' => isset($m->member) ? $m->member->id : NULL,
                            'name' => isset($m->member) ? ucwords($m->member->name) : NULL,
                        ];
                    });
                }

                $temp[] = [
                    'id' => $class->id,
                    'name' => ucwords($class->title),
                    'isOnline' => $class->is_online == 1 ? true : false,
                    'coach' => $tempCoach,
                ];
            }
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    public function coaches($branch_id)
    {
        // GET STAFF
        $staffs = MemberBranch::where([
            ['branch_id', $branch_id],
            ['status', 2],
            ['is_active', 1],
            ['is_published', 1]
        ])->get();

        // COACHES
        $roleCoach = Role::where('name', 'coach')->first();
        if (!$roleCoach) {
            return $this->respondWithCustomData(['message' => 'Role coaches not found'], Response::HTTP_NOT_FOUND);
        }

        $roleCoachID = $roleCoach->id;

        // CHECK ROLE
        $temp = [];
        foreach ($staffs as $staff) {
            $userID = $staff->member->user->id;
            $isCoach = RoleUser::where([
                ['user_id', $userID],
                ['role_id', $roleCoachID]
            ])->first();

            if ($isCoach) {
                $temp[] = [
                    'memberID' => $staff->member->id,
                    'name' => ucwords($staff->member->name . ' ' . $staff->member->last_name),
                ];
            }
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);

        /* $collections = Coach::where([
            ['branch_id', $branch_id],
            ['is_active', 1],
        ])->get(); */

        // return $this->respondWithCustomData(CoachRessource::collection($collections));
    }

    public function classScheduled($branch_id, $date)
    {
        $checkDate = BranchSchedule::whereDate('date', $date)->where('branch_id', $branch_id)->first();

        if (!$checkDate) {
            return $this->respondWithCustomData(['message' => 'Schedule not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $collections = BranchScheduleDetail::where([
            ['branch_schedule_id', $checkDate->id],
        ])->orderBy('start_time', 'ASC')->get();

        $temp = [];
        foreach ($collections as $collection) {

            if (isset($collection->class)) {
                $class = $collection->class;

                $tempCoach = [];
                if (isset($class->branchCoach)) {
                    $tempCoach = $class->branchCoach->map(function ($m) {
                        return [
                            'id' => isset($m->member) ? $m->member->id : NULL,
                            'name' => isset($m->member) ? ucwords($m->member->name) : NULL,
                        ];
                    });
                }

                $temp[] = [
                    'scheduleClassId' => $collection->id,
                    'class' => [
                        'id' => $class->id,
                        'name' => ucwords($class->title),
                        'thumbnail' => ENV('CDN') . '/' . config('cdn.classes') . $class->image,
                        'isOnline' => $class->is_online == 1 ? true : false,
                        'coach' => $tempCoach,
                        'maxQuota' => $collection->max_quota,
                        'startTime' => $collection->start_time,
                        'endTime' => $collection->end_time,
                        'urlClass' => $collection->url_class,
                        'passwordClass' => $collection->password_class,
                        'status' => $collection->is_published == 1 ? true : false,
                        'duration' => (strtotime($collection->end_time) - strtotime($collection->start_time)) / 60,
                    ],
                ];
            }
        }

        return $this->respondWithCustomData($temp);
    }

    public function show($branch_id, $date, $scheduleId)
    {
        $checkDate = BranchSchedule::whereDate('date', $date)->where('branch_id', $branch_id)->first();

        if (!$checkDate) {
            return $this->respondWithCustomData(['message' => 'Schedule not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $resource = BranchScheduleDetail::where([
            ['branch_schedule_id', $checkDate->id],
            ['id', $scheduleId],
        ])->first();

        if (!$resource) {
            $data = null;
        }

        $class = $resource->class;
        $tempCoach = [];
        if (isset($class->branchCoach)) {
            $tempCoach = $class->branchCoach->map(function ($m) {
                return [
                    'id' => isset($m->member) ? $m->member->id : NULL,
                    'name' => isset($m->member) ? ucwords($m->member->name) : NULL,
                ];
            });
        }

        $data = [
            'scheduleClassId' => $resource->id,
            'class' => [
                'id' => $class->id,
                'name' => ucwords($class->title),
                'thumbnail' => ENV('CDN') . '/' . config('cdn.classes') . $class->image,
                'isOnline' => $class->is_online == 1 ? true : false,
                'coach' => $tempCoach,
                'maxQuota' => $resource->max_quota,
                'startTime' => $resource->start_time,
                'endTime' => $resource->end_time,
                'urlClass' => $resource->url_class,
                'passwordClass' => $resource->password_class,
                'status' => $resource->is_published == 1 ? true : false,
                'duration' => (strtotime($resource->end_time) - strtotime($resource->start_time)) / 60,
            ],
        ];

        return $this->respondWithCustomData($data);
    }

    public function update(ClassScheduleRequest $request, $branch_id, $date, $scheduleID)
    {
        $data = $request->only(array_keys($request->rules()));

        $checkDate = BranchSchedule::whereDate('date', $date)->where('branch_id', $branch_id)->first();

        if (!$checkDate) {
            return $this->respondWithCustomData(['message' => 'Schedule not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        DB::beginTransaction();

        // Update Branch Schedule Detail
        $resource = BranchScheduleDetail::where('id', $scheduleID)->where('branch_schedule_id', $checkDate->id)->first();
        $resource->branch_class_id = $data['branch_class_id'];
        $resource->start_time = $data['start_time'];
        $resource->end_time = $data['end_time'];
        $resource->start_timestamp = $data['start_timestamp'] . '000';
        $resource->end_timestamp = $data['end_timestamp'] . '000';
        // $resource->max_quota = $data['max_quota'];
        $resource->max_quota = (int) $data['max_quota'];
        $resource->url_class = $data['url_class'];
        $resource->password_class = $data['password_class'];

        // in active
        if ($data['is_published'] == 0) {
            // check reserverd class
            $check = ClassAttendance::where('branch_schedule_detail_id', $scheduleID)->first();
            if ($check) {
                return $this->respondWithCustomData('This Class Scedule cannot be changed, one fo your customers has reserved it', Response::HTTP_BAD_REQUEST);
            }
        }
        $resource->is_published = $data['is_published'];
        $resource->save();

        // Destory
        BranchClassCoach::where('branch_class_id', $data['branch_class_id'])->delete();

        // Store Coach
        foreach ($data['coaches'] as $coachId) {
            $storeCoach = new BranchClassCoach();
            $storeCoach->branch_class_id = $data['branch_class_id'];
            // $storeCoach->coach_id = $coachId;
            $storeCoach->member_id = $coachId;
            $storeCoach->save();
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => 'Class Schedule has successfully changed!']);
    }

    public function storeCollection(Request $request, $branch_id)
    {
        $user = auth()->user();
        $periode = $request->input('periode');
        $title = $request->input('title');
        $status = $request->input('status');
        $collections = $request->input('payload');
        DB::beginTransaction();

        $periode = explode('-', $periode);

        $schedule = ClassSchedule::where([
            ['branch_id', $branch_id],
            ['year', $periode[0]],
            ['month', $periode[1]],
        ])->first();

        // Jika Tidak ada, create schedule
        $statusSchedule = 0;
        if (!$schedule) {

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }

            $newSchedule = new ClassSchedule();
            $newSchedule->title = $title;
            $newSchedule->branch_id = $branch_id;
            $newSchedule->year = $periode[0];
            $newSchedule->month = $periode[1];
            $newSchedule->created_by = $user->id;
            $newSchedule->status = $statusSchedule;
            $newSchedule->save();

            foreach ($collections as $collection) {

                $branchSchedule = BranchSchedule::where([
                    ['branch_id', $branch_id],
                    ['date', $collection['date']],
                ])->first();

                if ($branchSchedule) {
                    $branchSchedule->class_schedule_id = $newSchedule->id;
                    $branchSchedule->save();
                }

                /* $branchSchedule = new BranchSchedule();
                $branchSchedule->class_schedule_id = $newSchedule->id;
                $branchSchedule->branch_id = $branch_id;
                $branchSchedule->is_published = 1;
                $branchSchedule->date = $collection['date'];
                $branchSchedule->save(); */
            }
        } else {

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }
            $schedule->status = $statusSchedule;
            $schedule->save();
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Store data schedule succussfully"], Response::HTTP_CREATED);
    }
}
