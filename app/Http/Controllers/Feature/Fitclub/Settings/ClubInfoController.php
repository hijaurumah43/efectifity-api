<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Internal\Amenity\Amenity;
use App\Models\Common\Branch\BranchAmenity;
use App\Models\Common\Branch\BranchCategory;
use App\Models\Common\Service\ServiceCategory;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Common\Branch\BranchAdditionalInformation;
use App\Models\Internal\Additional\AdditionalInformation;
use App\Http\Requests\Feature\Fitclub\Information\ClubAmenityRequest;
use App\Http\Requests\Feature\Fitclub\Information\MainServiceRequest;
use App\Http\Requests\Feature\Fitclub\Information\ClubDescriptionRequest;
use App\Http\Requests\Feature\Fitclub\Information\ClubSafetyCleanRequest;
use App\Http\Requests\Feature\Fitclub\Information\ClubSocialMediaRequest;

class ClubInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['hasBranch']);
    }

    /**
     * Display a listing of resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(Request $request, $branch_id)
    {
        $resources = ServiceCategory::where('is_published', 1)->get();
        $temp = [];
        foreach ($resources as $resource) {
            $status = false;

            $result = BranchCategory::where([
                ['branch_id', $branch_id],
                ['service_category_id', $resource->id],
            ])->first();

            if ($result) {

                $status = true;
            }

            $temp[] = [
                'id' => $resource->id,
                'title' => $resource->title,
                'status' => $status,
                'icon' => $resource->thumbnail != NULL ? ENV('CDN') . '/' . config('cdn.clubSettingService') . $resource->thumbnail : null,
            ];
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * Store or update resource
     *
     * @param MainServiceRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function categories(MainServiceRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();

        // NOTE: REMOVE DATA
        BranchCategory::where('branch_id', $branch_id)->delete();

        // NOTE: STORE DATA
        foreach ($data['services'] as $value) {
            if ($value['status'] == true) {
                BranchCategory::create([
                    'branch_id' => $branch_id,
                    'service_category_id' => $value['id'],
                ]);
            };
        }

        DB::commit();

        $message = strtolower('Main Service is successfully updated!');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_CREATED);
    }

    /**
     * Display a listing of resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAmenity(Request $request, $branch_id)
    {
        $resources = Amenity::where('is_published', 1)->get();
        $temp = [];
        foreach ($resources as $resource) {
            $status = false;

            $result = BranchAmenity::where([
                ['branch_id', $branch_id],
                ['amenity_id', $resource->id],
            ])->first();

            if ($result) {

                $status = true;
            }

            $temp[] = [
                'id' => $resource->id,
                'title' => $resource->title,
                'status' => $status,
                'icon' => $resource->thumbnail != NULL ? ENV('CDN') . '/' . config('cdn.clubSettingAmenity') . $resource->thumbnail : null,
            ];
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * Store or update resource
     *
     * @param ClubAmenityRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function amenity(ClubAmenityRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();

        // NOTE: REMOVE DATA
        BranchAmenity::where('branch_id', $branch_id)->delete();

        // NOTE: STORE DATA
        foreach ($data['amenities'] as $value) {
            if ($value['status'] == true) {
                BranchAmenity::create([
                    'branch_id' => $branch_id,
                    'amenity_id' => $value['id'],
                ]);
            }
        }

        DB::commit();

        $message = strtolower('Amenities is successfully updated!');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_CREATED);
    }

    /**
     * Display a listing of resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSafetyClean(Request $request, $branch_id)
    {
        $resources = AdditionalInformation::where('is_published', 1)->get();
        $temp = [];
        foreach ($resources as $resource) {
            $status = false;

            $result = BranchAdditionalInformation::where([
                ['branch_id', $branch_id],
                ['additional_information_id', $resource->id],
            ])->first();

            if ($result) {

                $status = true;
            }

            $temp[] = [
                'id' => $resource->id,
                'title' => $resource->title,
                'image' => $resource->image != NULL ? ENV('CDN') . '/' . config('cdn.clubSettingInformation') . $resource->image : null,
                'description' => $resource->description,
                'status' => $status,
            ];
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * Store or update resource
     *
     * @param ClubSafetyCleanRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function safetyClean(ClubSafetyCleanRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        DB::beginTransaction();

        // NOTE: REMOVE DATA
        BranchAdditionalInformation::where('branch_id', $branch_id)->delete();

        // NOTE: STORE DATA
        foreach ($data['additional_informations'] as $value) {
            if ($value['status'] == true) {
                BranchAdditionalInformation::create([
                    'branch_id' => $branch_id,
                    'additional_information_id' => $value['id'],
                ]);
            }
        }

        DB::commit();

        $message = strtolower('Main Service is successfully updated!');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_CREATED);
    }

    /**
     * Update description of branch
     *
     * @param ClubDescriptionRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function clubDescription(ClubDescriptionRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = Branch::where('id', $branch_id)->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        $resource->description = $data['description'];
        $resource->save();

        $message = strtolower('About is successfully updated!');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK);
    }

    /**
     * Update social media of branch
     *
     * @param ClubSocialMediaRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function clubSocialMedia(ClubSocialMediaRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = Branch::where('id', $branch_id)->first();

        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        $resource->phone_number = isset($data['phone']) ? $data['phone'] : NULL;
        $resource->website = isset($data['website']) ? $data['website'] : NULL;
        $resource->whatsapp = isset($data['whatsapp']) ? $data['whatsapp'] : NULL;
        $resource->instagram = isset($data['instagram']) ? $data['instagram'] : NULL;
        $resource->facebook = isset($data['facebook']) ? $data['facebook'] : NULL;
        $resource->youtube = isset($data['youtube']) ? $data['youtube'] : NULL;
        $resource->tiktok = isset($data['tiktok']) ? $data['tiktok'] : NULL;
        $resource->save();

        $message = strtolower('Social Network information is successfully updated!');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK);
    }
}
