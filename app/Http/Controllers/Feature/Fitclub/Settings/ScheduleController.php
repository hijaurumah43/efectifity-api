<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Models\Client\Staff\Schedule;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Feature\Fitclub\Settings\ScheduleResource;
use App\Http\Resources\Feature\Fitclub\Settings\ScheduleCollection;
use App\Models\Client\Staff\Shift;
use Symfony\Component\HttpFoundation\Response;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ScheduleCollection::class;
        $this->resourceItem = ScheduleResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $schedules = Schedule::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($schedules);
    }

    /**
     * Check available month
     *
     * @param int $branch_id
     * @param int $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function availableMonth($branch_id, $year)
    {
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $schedules = Schedule::where('branch_id', $branch_id)->where('year', $year)->select('month')->get();

        $temp = [];
        foreach ($schedules as $schedule) {
            $temp[] = $schedule->month;
        }
        $collection = collect([$months, $temp]);
        $collection = $collection->collapse();
        $result = array_unique($collection->toArray());

        return $result;
    }

    public function dateMarked(Request $request, $branch_id, $month, $year)
    {

        $collections = StaffScheduleDetail::where('branch_id', $branch_id)->whereMonth('start_time', $month)->whereYear('start_time', $year)->select('start_time')->orderBy('start_time', 'ASC')->get()->groupBy(function ($q) {
            return Carbon::parse($q->start_time)->format('Y-m-d');
        });
        if (!$collections) {
            return $this->respondWithCustomData(['message' => 'Data not found']);
        }

        $temp = [];
        foreach ($collections as $key => $value) {
            $temp[] = $key;
        }

        return $this->respondWithCustomData(['date' => $temp]);
    }

    public function staffs(Request $request, $branch_id)
    {
        $temp = [];
        $tempShift = [];

        $memberBranchs = MemberBranch::where([
            ['branch_id', $branch_id],
            ['status', 2],
            ['is_active', 1],
            ['is_published', 1],
        ])->whereNull('type')->get();

        // Display specified
        if ($request->has('date')) {
            $date = $request->get('date');

            // Check Schedule
            $date = explode('-', $date);
            $year = $date[0];
            $month = $date[1];
            $schedule = Schedule::where([
                ['month', $month],
                ['year', $year],
                // ['status', 1],
            ])->first();

            if (!$schedule) {
                return $this->respondWithCustomData([
                    'status' => 0,
                    'message' => 'Data not found'
                ], Response::HTTP_NOT_FOUND);
            }

            foreach ($memberBranchs as $memberBranch) {
                $data = StaffScheduleDetail::where([
                    ['schedule_id', $schedule->id],
                    ['member_branch_id', $memberBranch->id],
                ])->whereDate('start_time', $request->get('date'))->get();

                if (count($data) > 0) {

                    foreach ($data as $item) {
                        $tempShift[] = [
                            'id' => $item->id,
                            'shiftID' => $item->shift->id,
                            'title' => isset($item->shift) ? ucwords($item->shift->title) : 'No Name',
                            'clockIn' => Carbon::parse($item->shift->start_time)->format('H.i'),
                            'clockOut' => Carbon::parse($item->shift->end_time)->format('H.i')
                        ];
                    }
                }

                $member = $memberBranch->member;
                // Get Position
                $position = [];
                if ($member->user) {
                    $user = $member->user;
                    foreach ($user->roles as $data) {
                        $position[] = $data->display_name;
                    }
                }
                $temp[] = [
                    'id' => $memberBranch->id,
                    'name' => $member->name . ' ' . $member->last_name,
                    'avatar' => Member::avatarStorage($member->avatar),
                    'position' => $position,
                    'shift' => $tempShift,
                ];

                $tempShift = [];
            }
        } else {
            foreach ($memberBranchs as $memberBranch) {
                $member = $memberBranch->member;
                $member = $memberBranch->member;
                // Get Position
                $position = [];
                if ($member->user) {
                    $user = $member->user;
                    foreach ($user->roles as $data) {
                        $position[] = $data->display_name;
                    }
                }
                $temp[] = [
                    'id' => $memberBranch->id,
                    'name' => $member->name . ' ' . $member->last_name,
                    'avatar' => Member::avatarStorage($member->avatar),
                    'position' => $position,
                    'shift' => $tempShift,
                ];
            }
        }

        return $this->respondWithCustomData($temp);
    }

    public function store(Request $request, $branch_id)
    {
        $user = auth()->user();
        $periode = $request->input('periode');
        $title = $request->input('title');
        $status = $request->input('status');
        $collections = $request->input('payload');

        foreach ($collections as $collection) {
            foreach ($collection['attributes'] as $item) {
                $shift = Shift::where('id', $item['shift_id'])->first();

                if (!$shift) {
                    return $this->respondWithCustomData(["message" => "Shift " . $item['shift_id'] . " not found"], Response::HTTP_BAD_REQUEST);
                }
            }
        }

        DB::beginTransaction();

        $periode = explode('-', $periode);
        $schedule = Schedule::where([
            ['branch_id', $branch_id],
            ['year', $periode[0]],
            ['month', $periode[1]],
        ])->first();

        // Jika Tidak ada, create schedule
        $statusSchedule = 0;
        if (!$schedule) {

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }

            $newSchedule = new Schedule();
            $newSchedule->title = $title;
            $newSchedule->branch_id = $branch_id;
            $newSchedule->year = $periode[0];
            $newSchedule->month = $periode[1];
            $newSchedule->created_by = $user->id;
            $newSchedule->status = $statusSchedule;
            $newSchedule->save();

            // create staff schedule
            foreach ($collections as $collection) {

                foreach ($collection['attributes'] as $item) {
                    $start_time = "00:00:00";
                    $end_time = "00:00:00";

                    $shift = Shift::where('id', $item['shift_id'])->first();
                    if ($shift) {
                        $start_time = $collection['date'] . ' ' . $shift->start_time . ":00";
                        $end_time = $collection['date'] . ' ' . $shift->end_time . ":00";

                        // delete
                        StaffScheduleDetail::where([
                            ['branch_id', $branch_id],
                            ['member_branch_id', $item['member_branch_id']],
                            ['schedule_id', $newSchedule->id],
                            ['shift_id', $item['shift_id']],
                            ['start_time', $start_time],
                            ['end_time', $end_time],
                        ])->delete();
                        /* if ($check) {
                            // delete
                            $check->delete();
                        } */

                        $s = new StaffScheduleDetail();
                        $s->branch_id           = $branch_id;
                        $s->member_branch_id    = $item['member_branch_id'];
                        $s->schedule_id         = $newSchedule->id;
                        $s->shift_id            = $item['shift_id'];
                        $s->start_time          = $start_time;
                        $s->end_time            = $end_time;
                        $s->start_timestamp     = $item['start_timestamp'] . '000';
                        $s->end_timestamp       = $item['end_timestamp'] . '000';
                        $s->is_published        = 1;
                        $s->save();
                    }
                }
            }
        } else {

            $scheduleID = $schedule->id;

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }
            $schedule->status = $statusSchedule;
            $schedule->save();

            foreach ($collections as $collection) {

                // delete tanggal itu
                $s = StaffScheduleDetail::where('schedule_id', $scheduleID)->whereDate('start_time', $collection['date'])->delete();

                foreach ($collection['attributes'] as $item) {
                    $start_time = "00:00:00";
                    $end_time = "00:00:00";

                    $shift = Shift::where('id', $item['shift_id'])->first();

                    if ($shift) {

                        $start_time = $collection['date'] . ' ' . $shift->start_time . ":00";
                        $end_time = $collection['date'] . ' ' . $shift->end_time . ":00";

                        // delete
                        StaffScheduleDetail::where([
                            ['branch_id', $branch_id],
                            ['member_branch_id', $item['member_branch_id']],
                            ['schedule_id', $schedule->id],
                            ['shift_id', $item['shift_id']],
                            ['start_time', $start_time],
                            ['end_time', $end_time],
                        ])->delete();

                        $s = new StaffScheduleDetail();
                        $s->branch_id           = $branch_id;
                        $s->member_branch_id    = $item['member_branch_id'];
                        $s->schedule_id         = $scheduleID;
                        $s->shift_id            = $item['shift_id'];
                        $s->start_time          = $start_time;
                        $s->end_time            = $end_time;
                        $s->start_timestamp     = $item['start_timestamp'] . '000';
                        $s->end_timestamp       = $item['end_timestamp'] . '000';
                        $s->is_published        = 1;
                        $s->save();
                    }
                }
            }
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Store data schedule succussfully"], Response::HTTP_CREATED);
    }
}
