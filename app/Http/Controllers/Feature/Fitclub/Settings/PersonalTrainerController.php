<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Client\Staff\PersonalTrainer;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Feature\Fitclub\Settings\SettingsPersonalTrainerResource;
use App\Http\Resources\Feature\Fitclub\Settings\SettingsPersonalTrainerCollection;
use App\Http\Requests\Feature\Fitclub\Settings\PersonalTrainer\PersonalTrainerRequest;
use App\Http\Requests\Feature\Fitclub\Settings\PersonalTrainer\UpdatePersonalTrainerRequest;

class PersonalTrainerController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = SettingsPersonalTrainerCollection::class;
        $this->resourceItem = SettingsPersonalTrainerResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage <= 100 ? $perPage : 20;

        // Get Role PT
        $rolePT = Role::where('name', 'personal_trainer')->first();
        if (!$rolePT) {
            return $this->respondWithCustomData(["message" => "Your branch doesn't hanve personal trainers position"], Response::HTTP_BAD_REQUEST);
        }

        // Get User Role
        $users = RoleUser::where('role_id', $rolePT->id)->get();

        $memberID = [];
        foreach ($users as $user) {
            if (isset($user->user)) {
                $memberID[] = $user->user->member->id;
            }
        }

        // Get Staff Branche has Personal Trainer Role
        $collections = MemberBranch::where('branch_id', $branch_id)->whereIn('member_id', $memberID)->paginate($perPage);

        return $this->respondWithCollection($collections);
    }

    /**
     * Store a newly sesource
     *
     * @param PersonalTrainerRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PersonalTrainerRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $image = $request->file('image');
        $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
        DB::beginTransaction();

        Storage::disk('sftp')
            ->put(
                config('cdn.pt') . $imageFileName,
                file_get_contents($request->file('image')->getRealPath())
            );
        Storage::disk('sftp')->setVisibility(config('cdn.pt'), 'public');

        $data['image'] = $imageFileName;
        $data['branch_id'] = $branch_id;

        PersonalTrainer::create($data);

        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'PT Information has updated'
        ], Response::HTTP_CREATED);
    }

    /**
     * Update sesource
     *
     * @param UpdatePersonalTrainerRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePersonalTrainerRequest $request, $branch_id, $ptID)
    {
        $data = $request->only(array_keys($request->rules()));

        $pt = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['id', $ptID],
        ])->first();

        if (!$pt) {
            return $this->respondWithCustomData([
                'message' => 'PT not available'
            ], Response::HTTP_NOT_FOUND);
        }

        if ($request->has('image')) {
            $image = $request->file('image');
            $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
            DB::beginTransaction();

            Storage::disk('sftp')
                ->put(
                    config('cdn.pt') . $imageFileName,
                    file_get_contents($request->file('image')->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.pt'), 'public');

            $pt->image = $imageFileName;
        }

        $data['branch_id'] = $branch_id;

        $pt->quota = $data['quota'];
        $pt->duration = $data['duration'];
        $pt->qualifications = $data['qualifications'];
        $pt->specialities = $data['specialities'];
        $pt->status = $data['status'];
        $pt->save();

        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'PT Information has updated'
        ], Response::HTTP_OK);
    }
}
