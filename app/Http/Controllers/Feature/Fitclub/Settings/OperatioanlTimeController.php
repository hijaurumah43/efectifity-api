<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\OpertionalTime;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Common\Branch\BranchOperationalTime;
use App\Http\Resources\Feature\Fitclub\Settings\OperationalTimeResource;
use App\Http\Resources\Feature\Fitclub\Settings\OperationalTimeCollection;

class OperatioanlTimeController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = OperationalTimeCollection::class;
        $this->resourceItem = OperationalTimeResource::class;
        $this->middleware('hasBranch');
    }

    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $schedules = OpertionalTime::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($schedules);
    }

    /**
     * Check available month
     *
     * @param int $branch_id
     * @param int $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function availableMonth($branch_id, $year)
    {
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $schedules = OpertionalTime::where('branch_id', $branch_id)->where('year', $year)->select('month')->get();

        $temp = [];
        foreach ($schedules as $schedule) {
            $temp[] = $schedule->month;
        }
        $collection = collect([$months, $temp]);
        $collection = $collection->collapse();
        $result = array_unique($collection->toArray());

        return $result;
    }

    public function dateMarked(Request $request, $branch_id, $month, $year)
    {

        $collections = BranchOperationalTime::where('branch_id', $branch_id)->whereMonth('date', $month)->whereYear('date', $year)->select('date')->orderBy('date', 'ASC')->get()->groupBy(function ($q) {
            return Carbon::parse($q->date)->format('Y-m-d');
        });

        if (!$collections) {
            return $this->respondWithCustomData(['message' => 'Data not found']);
        }

        $temp = [];
        foreach ($collections as $key => $value) {
            $temp[] = $key;
        }

        return $this->respondWithCustomData(['date' => $temp]);
    }

    public function update(Request $request, $branch_id)
    {
        $periode = $request->input('periode');
        $status = $request->input('status');

        $periode = explode('-', $periode);
        $schedule = OpertionalTime::where([
            ['branch_id', $branch_id],
            ['year', $periode[0]],
            ['month', $periode[1]],
        ])->first();

        if (!$schedule) {
            return $this->respondWithCustomData(["message" => "Business hour of this period is empty! please select the date"], Response::HTTP_BAD_REQUEST);
        }

        /**
         * Set cannot publish on the current month
         */
        /* if (strtolower($status) == 'publish' && $periode[0] == date('Y') && $periode[1] == date('m')) {
            return $this->respondWithCustomData(["message" => "Cannot be drafted on active month"], Response::HTTP_BAD_REQUEST);
        } */

        $statusSchedule = 0;
        $message = '';
        if (strtolower($status) == 'publish') {
            $statusSchedule = 1;
            $message = 'Period schedule September 2020 is successfully published!';
        } else if (strtolower($status) == 'draft') {
            $statusSchedule = 0;
            $message = 'Period schedule September 2020 is successfully drafted!';
        } else {
            return $this->respondWithCustomData(["message" => "The status is not define"], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $schedule->status = $statusSchedule;
        $schedule->save();

        return $this->respondWithCustomData(["message" => $message], Response::HTTP_OK);
    }

    public function store(Request $request, $branch_id)
    {
        $user = auth()->user();
        $periode = $request->input('periode');
        $title = $request->input('title');
        $status = $request->input('status');
        $collections = $request->input('payload');

        DB::beginTransaction();

        $periode = explode('-', $periode);
        $schedule = OpertionalTime::where([
            ['branch_id', $branch_id],
            ['year', $periode[0]],
            ['month', $periode[1]],
        ])->first();

        // Jika Tidak ada, create schedule
        $statusSchedule = 0;
        if (!$schedule) {

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }

            $newSchedule = new OpertionalTime();
            $newSchedule->title = $title;
            $newSchedule->branch_id = $branch_id;
            $newSchedule->year = $periode[0];
            $newSchedule->month = $periode[1];
            $newSchedule->created_by = $user->id;
            $newSchedule->status = $statusSchedule;
            $newSchedule->save();

            // create staff schedule
            foreach ($collections as $collection) {

                foreach ($collection['attributes'] as $item) {
                    $s = new BranchOperationalTime();
                    $s->branch_id = $branch_id;
                    $s->operational_time_id = $newSchedule->id;
                    $s->title = $newSchedule->title;
                    $s->date = $collection['date'];
                    $s->start_time = $item['start_time'];
                    $s->end_time = $item['end_time'];
                    $s->status = $item['status'];
                    $s->save();
                }
            }
        } else {

            $scheduleID = $schedule->id;

            if (strtolower($status) == 'publish') {
                $statusSchedule = 1;
            }
            $schedule->status = $statusSchedule;
            $schedule->save();

            foreach ($collections as $collection) {

                // delete tanggal itu
                $s = BranchOperationalTime::where('operational_time_id', $scheduleID)->where('date', $collection['date'])->delete();

                foreach ($collection['attributes'] as $item) {
                    $s = new BranchOperationalTime();
                    $s->branch_id = $branch_id;
                    $s->operational_time_id = $schedule->id;
                    $s->title = $schedule->title;
                    $s->date = $collection['date'];
                    $s->start_time = $item['start_time'];
                    $s->end_time = $item['end_time'];
                    $s->status = $item['status'];
                    $s->save();
                }
            }
        }

        DB::commit();

        return $this->respondWithCustomData(["message" => "Store data business hour succussfully"], Response::HTTP_CREATED);
    }

    public function getTime(Request $request, $branch_id)
    {
        // Display specified
        if ($request->has('date')) {
            $date = $request->get('date');
        } else {
            return $this->respondWithCustomData(["message" => 'query param date is required'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $resource = BranchOperationalTime::where([
            ['branch_id', $branch_id],
            ['date', $date],
        ])->first();

        if(!$resource) {
            return $this->respondWithCustomData(["message" => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        $temp = [
            'id' => $resource->id,
            'date' => $resource->date,
            'startTime' => $resource->start_time,
            'endTime' => $resource->end_time,
            'status' => $resource->status,
        ];
        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }
}
