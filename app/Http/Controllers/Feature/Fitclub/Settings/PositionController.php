<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use App\Models\Role;
use App\Models\RoleUser;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\PermissionUser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use Berkayk\OneSignal\OneSignalFacade;
use App\Events\Fitclub\Staff\InvitationStaff;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Feature\Fitclub\Settings\StaffResource;
use App\Http\Resources\Feature\Fitclub\Settings\StaffCollection;
use App\Http\Requests\Feature\Fitclub\Settings\Position\PositionCreateRequest;
use App\Models\Common\Branch\Branch;
use App\Models\PermissionRole;

class PositionController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = StaffCollection::class;
        $this->resourceItem = StaffResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing role of branch
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function role()
    {
        $name = [
            'superadministrator',
            'administrator',
            'odin',
            'member',
            'owner',
            'partner',
            'manager',
        ];
        $roles = Role::whereNotIn('name', $name)->get();
        $temp = [];
        foreach ($roles as $role) {

            $permissions = PermissionRole::where('role_id', $role->id)->get();
            $tempPermission = [];
            foreach ($permissions as $item) {
                $tempPermission[] = [
                    'id' => $item->permission->id,
                    'name' => $item->permission->name,
                    'displayName' => $item->permission->display_name,
                ];
            }

            $temp[] = [
                'id' => $role->id,
                'name' => $role->name,
                'permissions' => $tempPermission
            ];

            $tempPermission = [];
        }

        return $this->respondWithCustomData($temp);
    }

    /**
     * Display a listing permission of branch
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function permission()
    {
        $name = [
            'superadministrator',
            'administrator',
            'odin',
            'member',
            'owner',
            'partner',
            'manager',
        ];

        $permissions = Permission::get();
        $temp = [];
        foreach ($permissions as $permission) {
            $temp[] = [
                'id' => $permission->id,
                'name' => $permission->name,
                'displayName' => $permission->display_name,
            ];
        }

        return $this->respondWithCustomData($temp);
    }

    public function positionMe($branch_id, $member_id)
    {
        // Role
    }

    /**
     * Dispay a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        if (!$request->has('memberId')) {
            return $this->respondWithCustomData(['message' => 'member id is required']);
        }

        // NOTE:: Get Member
        $memberId = $request->get('memberId');
        $member = Member::where('member_id', $memberId)->first();
        if (!$member) {
            return $this->respondWithCustomData(['message' => 'Member not found'], Response::HTTP_BAD_REQUEST);
        }

        // NOTE:: Check Branch
        $branch = Branch::where('id', $branch_id)->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => 'Branch is not found'], Response::HTTP_BAD_REQUEST);
        }

        // NOTE:: Member is employee in the same branch
        $memberBranch = MemberBranch::where([
            ['member_id', $member->id],
            ['branch_id', $branch->id],
            ['status', 2], // Active on the same branch
        ])->first();

        if ($memberBranch) {
            return $this->respondWithCustomData([
                'memberId' => $member->member_id,
                'firstName' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
            ]);
        }

        // NOTE:: Check Member
        $checkMembers = MemberBranch::where([
            ['member_id', $member->id],
            ['status', 2], // Active
        ])->get();

        // NOTE:: Check company
        $cannotInvite = false;
        if (count($checkMembers) > 0) {

            foreach ($checkMembers as $checkMember) {

                // NOTE:: the member is employee of other branch
                if ($checkMember->branch->company_id != $branch->company_id) {
                    $cannotInvite = true;
                }
            }
        }

        if ($cannotInvite == true) {
            return $this->respondWithCustomData(['message' => 'Cannot add this efectifity account'], Response::HTTP_BAD_REQUEST);
        }

        return $this->respondWithCustomData([
            'memberId' => $member->member_id,
            'firstName' => $member->name,
            'lastName' => $member->last_name,
            'avatar' => Member::avatarStorage($member->avatar),
        ]);
    }

    /**
     * Display a specified of resource
     *
     * @param int $branch_id
     * @param int $member_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $member_id)
    {
        // Select User
        $member = Member::where('member_id', $member_id)->first();
        if (!$member) {
            return $this->respondWithCustomData(['message' => 'Member not found'], Response::HTTP_OK);
        }

        // Check User your staff or not
        $memberBranch = MemberBranch::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id],
            // ['status', '!=', 2],
            // ['is_active', 1],
        ])->first();

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Not your staff'], Response::HTTP_OK);
        }

        $isActive = $memberBranch->is_published == 1 && $memberBranch->is_active == 1 ? true : false;

        $user = $member->user;
        $roles = RoleUser::where('user_id', $user->id)->get();
        $permissions = PermissionUser::where('user_id', $user->id)->get();

        $tempRole = [];
        if (count($roles) > 0) {
            foreach ($roles as $role) {
                $tempRole[] = [
                    'id' => $role->role->id,
                    'name' => $role->role->name,
                ];
            }
        }

        // $tempPermission = [];
        // if (count($permissions) > 0) {
        //     foreach ($permissions as $permission) {
        //         $tempPermission[] = [
        //             'id' => $permission->permission->id,
        //             'name' => $permission->permission->name,
        //         ];
        //     }
        // }

        return $this->respondWithCustomData([
            'employee' => [
                'memberId' => $member->member_id,
                'firstName' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
                'isActive' => $isActive,
            ],
            'role' => $tempRole,
            // 'permission' => $tempPermission,
        ]);
    }

    /**
     * Re-invite member with last position
     *
     * @param int $branch_id
     * @param int $member_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function reInvite($branch_id, $member_id)
    {
        // Get Member
        $member = Member::where('member_id', $member_id)->first();
        if (!$member) {
            return $this->respondWithCustomData(['message' => 'Member not found'], Response::HTTP_OK);
        }

        DB::beginTransaction();

        // Member on branch
        $memberBranch = MemberBranch::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id],
        ])->first();

        // Check Status
        if ($memberBranch->status == 1 || $memberBranch->status == 2) {
            // Staff has been inveted
            return $this->respondWithCustomData(['message' => 'status member is invited or is actived']);
        } else {
            // Update status staff to invite 
            $memberBranch->status = 1;
            $memberBranch->save();
        }

        $position = RoleUser::where([
            ['user_id', $memberBranch->member->user->id],
            ['branch_id', $branch_id]
        ])->get();

        // Get last position on the branch
        $positionName = '';
        foreach ($position as $item) {
            if (count($position) > 1) {
                $positionName .= $item->role->name . ', ';
            } else {
                $positionName .= $item->role->name;
            }
        }

        $message = "you've invited as " . $positionName . " by " . $memberBranch->branch->title . ", click to confirm";

        // create notification
        event(new InvitationStaff($memberBranch, $message));

        // push notification one signal
        $dataFb = [
            'slug' => 'notifications',
            'param' => NULL
        ];
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberBranch->member->user->fcm_token, $message, $dataFb);
        $messageRes = 'Invitation Has Sent';

        DB::commit();

        return $this->respondWithCustomData(['message' => $messageRes]);
    }

    /**
     * Invite staff
     *
     * @param PositionCreateRequest $request
     * @param int $branch_id
     * @param int $member_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function invite(PositionCreateRequest $request, $branch_id, $member_id)
    {
        $invite = true;
        return $this->store($request, $branch_id, $member_id, $invite);
    }

    /**
     * Store or update staff
     *
     * @param PositionCreateRequest $request
     * @param int $branch_id
     * @param int $member_id
     * @param boolean $invite
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PositionCreateRequest $request, $branch_id, $member_id, $invite = false)
    {
        $data = $request->only(array_keys($request->rules()));

        // Select User
        $member = Member::where('member_id', $member_id)->first();
        if (!$member) {
            return $this->respondWithCustomData(['message' => 'Member not found'], Response::HTTP_OK);
        }

        DB::beginTransaction();

        // Check User your staff or not
        if ($invite == false) {
            $memberBranch = MemberBranch::where([
                ['member_id', $member->id],
                ['branch_id', $branch_id],
                ['status', 2], // active staff
            ])->first();
            if (!$memberBranch) {
                return $this->respondWithCustomData(['message' => 'Not your staff'], Response::HTTP_OK);
            }
        } else {
            $memberBranch = MemberBranch::where([
                ['member_id', $member->id],
                ['branch_id', $branch_id],
            ])->first();

            if ($memberBranch) {
                if ($memberBranch->status == 1) {
                    // Staff has been inveted
                    return $this->respondWithCustomData(['message' => 'member is has been invited']);
                } else {
                    // Update status staff to invite 
                    $memberBranch->status = 1;
                    $memberBranch->save();
                }
            } else {
                // Create a new staff 
                $memberBranch = MemberBranch::create([
                    'member_id' => $member->id,
                    'branch_id' => $branch_id,
                    'status' => 1 // waiting
                ]);
            }
        }

        // Delete role and permission
        $user = $member->user;
        RoleUser::where('user_id', $user->id)->delete();
        PermissionUser::where('user_id', $user->id)->delete();

        // Store role and permission
        // Store Role
        $position = [];
        foreach ($data['role'] as $role) {
            $getRole = RoleUser::create([
                'role_id' => $role,
                'user_id' => $user->id,
                'user_type' => 'App\Models\User'
            ]);
            // ada branch

            // store permission
            /* $permissions = PermissionRole::where('role_id', $role)->get();
            foreach ($permissions as $permission) {
                PermissionUser::create([
                    'permission_id' => $permission->permission_id,
                    'user_id' => $user->id,
                    'user_type' => 'App\Models\User'
                ]);
            } */

            $position[] = $getRole->role->name;
        }

        // Store Permission
        // Get from branch
        /* foreach ($data['permission'] as $permission) {
            PermissionUser::create([
                'permission_id' => $permission,
                'user_id' => $user->id,
                'user_type' => 'App\Models\User'
            ]);
        } */

        $message = 'Data Successfully Saved';
        if ($invite == true) {
            $message = 'Invitation Has Sent';

            $positionName = '';
            foreach ($position as $item) {
                if (count($position) > 1) {
                    $positionName .= $item . ', ';
                } else {
                    $positionName .= $item;
                }
            }

            $messageNotification = "you've invited as " . $positionName . " by " . $memberBranch->branch->title . ", click to confirm";

            // create notification
            event(new InvitationStaff($memberBranch, $messageNotification));

            // push notification one signal
            $dataFb = [
                'slug' => 'notifications',
                'param' => NULL
            ];
            //NOTE:: NOTIF FIREBASE
            sendNotificationFirebase($memberBranch->member->user->fcm_token, $messageNotification, $dataFb);
        }

        DB::commit();

        return $this->respondWithCustomData(['message' => $message]);
    }

    /**
     * Search
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request, $branch_id)
    {
        // return 'a';
        // get user by role
        if (!$request->has('positions') || $request->input('positions') == '') {
            return $this->respondWithCustomData(['message' => 'Positions is required']);
        }

        $search = $request->input('query');
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $memberId = Member::where('name', 'like', '%' . $search . '%')
            ->whereHas('user', function ($q) use ($request, $branch_id) {
                $q->whereHas('roleUser', function ($query) use ($request, $branch_id) {
                    $position = $request->input('positions');
                    $position = explode(',', $position);
                    // $query->where('branch_id', $branch_id)->whereIn('role_id', $position);
                    $query->whereIn('role_id', $position);
                });
            })->select('id')->get();

        $collection = MemberBranch::where('branch_id', $branch_id)->whereIn('member_id', $memberId)->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    public function cancelled($branch_id, $member_id)
    {
        // Select User
        $member = Member::where('member_id', $member_id)->first();
        if (!$member) {
            return $this->respondWithCustomData(['message' => 'Member not found'], Response::HTTP_OK);
        }

        // Check User your staff or not
        $memberBranch = MemberBranch::where([
            ['member_id', $member->id],
            ['branch_id', $branch_id],
            ['status', '=', 1],
        ])->first();

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Staff not found'], Response::HTTP_OK);
        }

        $memberBranch->is_active = 0;
        $memberBranch->is_published = 0;
        $memberBranch->status = 6;
        $memberBranch->save();


        return $this->respondWithCustomData(['message' => 'Invitation has been cancelled']);
    }
}
