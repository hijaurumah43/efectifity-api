<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Feature\Fitclub\Settings\Gallery\CreateGalleryUploadRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\Common\Branch\BranchGallery;
use App\Models\Client\Staff\Settings\Section;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\Settings\BranchGallerySection;
use Image;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['hasBranch']);
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $sections = Section::where('status', 1)->get();
        $temp = [];

        foreach ($sections as $section) {
            # code...
            $count = BranchGallery::where([
                ['branch_id', $branch_id],
                ['section_id', $section->id],
            ])->count();

            $temp[] = [
                'sectionID' => $section->id,
                'title' => ucwords($section->title),
                'total' => $count,
                'isActive' => true,
            ];
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $section_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $section_id)
    {
        $items = BranchGallery::where([
            ['branch_id', $branch_id],
            ['section_id', $section_id],
        ])->get();

        $temp = [];
        if (count($items) > 0) {
            foreach ($items as $item) {
                $temp[] = [
                    'id' => $item->id,
                    'name' => $item->name,
                ];
            }
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    /**
     * Store a newly resouce
     *
     * @param CreateGalleryUploadRequest $request
     * @param int $branch_id
     * @param int $section_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateGalleryUploadRequest $request, $branch_id, $section_id)
    {
        $data = $request->only(array_keys($request->rules()));

        $checkSection = Section::where('id', $section_id)->first();
        if (!$checkSection) {
            return $this->respondWithCustomData(['message' => 'Section not found'], Response::HTTP_NOT_FOUND);
        }

        $data['branch_id'] = $branch_id;
        $data['section_id'] = $section_id;

        $items = BranchGallery::where([
            ['branch_id', $branch_id],
            ['section_id', $section_id],
        ])->count();

        if ($items >= 15) {
            return $this->respondWithCustomData(['message' => 'Your quantity image is maximum'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($request->has('image')) {
            $imgCr = Image::make($request->file('image')->getRealPath());
            $sizeCr = $imgCr->filesize();

            // 2mb
            if ($sizeCr > 2048000) {
                return $this->respondWithCustomData(['message' => 'File to large'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $data['title'] = $this->uploadImage($request->file('image'));
            $data['name'] = ENV('CDN') . '/' . config('cdn.branchGallery') . $data['title'];

            BranchGallery::create($data);

            return $this->respondWithCustomData(['message' => 'Photo uploaded successfully!'], Response::HTTP_CREATED);
        } else {
            return $this->respondWithCustomData(['message' => 'image is required'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    private function uploadImage($file)
    {
        $imageFileName = NULL;
        if ($file) {
            $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
            Storage::disk('sftp')
                ->put(
                    config('cdn.branchGallery') . $imageFileName,
                    file_get_contents($file->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.branchGallery'), 'public');
        }

        return $imageFileName;
    }

    public function destroy($branch_id, $section_id, $id)
    {
        $item = BranchGallery::where([
            ['branch_id', $branch_id],
            ['section_id', $section_id],
            ['id', $id],
        ])->first();

        if (!$item) {
            return $this->respondWithCustomData(['message' => 'resource not found!'], Response::HTTP_NOT_FOUND);
        }

        $item->delete();

        return $this->respondWithCustomData(['message' => 'Delete reousce has been successfully!'], Response::HTTP_OK);
    }
}
