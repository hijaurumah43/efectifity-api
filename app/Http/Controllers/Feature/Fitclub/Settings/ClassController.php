<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Common\Branch\BranchClass;
use App\Http\Resources\Feature\Fitclub\Settings\ClassResource;
use App\Http\Resources\Feature\Fitclub\Settings\ClassCollection;
use App\Http\Requests\Feature\Fitclub\Settings\CreateClassRequest;
use App\Http\Requests\Feature\Fitclub\Settings\UpdateClassRequest;
use App\Models\Common\Branch\BranchClassCategory;
use App\Models\Common\Branch\BranchClassLevel;
use Symfony\Component\HttpFoundation\Response;

class ClassController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassCollection::class;
        $this->resourceItem = ClassResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage <= 100 ? $perPage : 20;

        $collection = BranchClass::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified of the resource
     *
     * @param int $id
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $id)
    {
        $resource = BranchClass::where([
            ['id', $id],
            ['branch_id', $branch_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData([
                "status" => 0,
                "message" => "Class is not found"
            ]);
        }

        return $this->respondWithItem($resource);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClassRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClassRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));

        $image = $request->file('image');
        $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();
        DB::beginTransaction();

        Storage::disk('sftp')
            ->put(
                config('cdn.classes') . $imageFileName,
                file_get_contents($request->file('image')->getRealPath())
            );
        Storage::disk('sftp')->setVisibility(config('cdn.classes'), 'public');

        $data['image'] = $imageFileName;
        $data['branch_id'] = $branch_id;

        BranchClass::create($data);

        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'New Class has been created'
        ], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClassRequest $request
     * @param int $branch_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClassRequest $request, $branch_id, $id)
    {
        $data = $request->only(array_keys($request->rules()));

        $resource = BranchClass::where([
            ['id', $id],
            ['branch_id', $branch_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData([
                "status" => 0,
                "message" => "Class is not found"
            ]);
        }

        DB::beginTransaction();

        if ($request->has('image')) {

            $image = $request->file('image');
            $imageFileName  = microtime(true) . '.' . $image->getClientOriginalExtension();

            Storage::disk('sftp')
                ->put(
                    config('cdn.classes') . $imageFileName,
                    file_get_contents($request->file('image')->getRealPath())
                );
            Storage::disk('sftp')->setVisibility(config('cdn.classes'), 'public');

            $resource->image = $imageFileName;
        }

        $resource->branch_id = $branch_id;
        $resource->title = $data['title'];
        $resource->branch_class_category_id = $data['branch_class_category_id'];
        $resource->branch_class_level_id = $data['branch_class_level_id'];
        $resource->description = isset($data['description']) ? $data['description'] : NULL;
        $resource->is_online = $data['is_online'];
        $resource->is_published = $data['is_published'];
        $resource->save();

        DB::commit();

        return $this->respondWithCustomData([
            'message' => 'Class has succussfully updated'
        ], Response::HTTP_OK);
    }

    public function level()
    {
        $collections = BranchClassLevel::where('is_active', 1)->get();

        $temp = [];
        foreach ($collections as $collection) {
            $temp[] = [
                'id' => $collection->id,
                'title' => $collection->title,
                'description' => $collection->description,
            ];
        }

        return $this->respondWithCustomData($temp);
    }

    public function categories()
    {
        $collections = BranchClassCategory::where('is_published', 1)->get();

        $temp = [];
        foreach ($collections as $collection) {
            $temp[] = [
                'id' => $collection->id,
                'title' => $collection->title,
            ];
        }

        return $this->respondWithCustomData($temp);
    }
}
