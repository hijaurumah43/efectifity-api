<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Feature\Fitclub\Settings\StaffResource;
use App\Http\Resources\Feature\Fitclub\Settings\StaffCollection;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;

class StaffScheduleController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = StaffCollection::class;
        $this->resourceItem = StaffResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $user = auth()->user();
        $perPage = $request->get('limit');
        $perPage = $perPage <= 100 ? $perPage : 20;

        // if partner/owner
        $branch = Branch::where('id', $branch_id)->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        $company = Company::where([
            ['id', $branch->company_id],
            ['user_id', $user->id]
        ])->first();

        if ($company) {
            $collection = MemberBranch::where([
                ['branch_id', $branch_id],
                // ['status', 2],
                ['is_active', 1],
                ['is_published', 1],
                ['member_id', '!=', $user->member->id]
            ])->paginate($perPage);
        } else {
            $collection = MemberBranch::where([
                ['branch_id', $branch_id],
                // ['status', 2],
                ['is_active', 1],
                ['is_published', 1],
                ['member_id', '!=', $user->member->id]
            ])->whereNull('type')->paginate($perPage);
        }

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified of resource
     *
     * @param int $branch_id
     * @param int $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $member_branch_id)
    {
        $memberBranch = MemberBranch::where('id', $member_branch_id)->first();

        return $this->respondWithItem($memberBranch);
    }

    /**
     * Deactive staff
     *
     * @param int $branch_id
     * @param int $member_branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($branch_id, $member_branch_id)
    {
        DB::beginTransaction();
        $memberBranch = MemberBranch::where('id', $member_branch_id)->first();
        if (!$memberBranch) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'success');
        }
        $memberBranch->is_published = 0;
        $memberBranch->is_active = 0;
        $memberBranch->status = 6; // inactive
        $memberBranch->save();
        $message = 'Staff Successfully Removed';

        // Remove Role
        $userId = $memberBranch->member->user->id;
        RoleUser::where('user_id', $userId)->delete();

        // Role Member
        $user = User::where('id', $userId)->first();
        if ($user) {
            $user->attachRole(Role::MEMBER);
        }

        DB::commit();
        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK);
    }
}
