<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Feature\Fitclub\Settings\Shift\CreateShiftRequest;
use App\Http\Resources\Feature\Fitclub\Settings\ShiftCollection;
use App\Http\Resources\Feature\Fitclub\Settings\ShiftResource;
use App\Models\Client\Staff\Shift;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ShiftController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ShiftCollection::class;
        $this->resourceItem = ShiftResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $status = $request->get('status');
        $query = Shift::where('branch_id', $branch_id);
        if ($request->has('status')) {
            $query->status($status);
        }

        $query = $query->orderBy('start_time', 'ASC')->paginate($perPage);
        $collection = $query;

        return $this->respondWithCollection($collection);
    }

    /**
     * Store a newly resource
     *
     * @param CreateShiftRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateShiftRequest $request, $branch_id)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        $data['branch_id'] = $branch_id;
        $data['created_by'] = $user->id;

        Shift::create($data);
        return $this->respondWithCustomData([
            'message' => 'New Shift Succesfully Added'
        ], Response::HTTP_CREATED);
    }

    /**
     * Update the resource
     *
     * @param CreateShiftRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreateShiftRequest $request, $branch_id, $id)
    {
        $user = auth()->user();
        $data = $request->only(array_keys($request->rules()));
        $data['branch_id'] = $branch_id;
        $data['created_by'] = $user->id;

        $shift = Shift::where([
            ['id', $id],
            ['branch_id', $branch_id]
        ])->first();

        if (!$shift) {
            return $this->respondWithCustomData([
                "message" => 'The resource not found'
            ], 200);
        }

        $shift->title = $request->title;
        $shift->start_time = $request->start_time;
        $shift->end_time = $request->end_time;
        $shift->status = $request->status;
        $shift->save();

        return $this->respondWithCustomData([
            'message' => 'Update shift successfully'
        ], Response::HTTP_OK);
    }
}
