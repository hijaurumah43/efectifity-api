<?php

namespace App\Http\Controllers\Feature\Fitclub\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchPackage;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Feature\Fitclub\Settings\PackageResource;
use App\Http\Resources\Feature\Fitclub\Settings\PackageCollection;
use App\Http\Requests\Feature\Fitclub\Settings\Package\CreatePackageRequest;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PackageCollection::class;
        $this->resourceItem = PackageResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage <= 100 ? $perPage : 20;

        $collection = BranchPackage::where([
            ['branch_id', $branch_id],
            ['is_promo', '!=', 1]
        ])->orderBy('created_at', 'DESC')->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $id)
    {
        $resource = BranchPackage::where([
            ['id', $id],
            ['branch_id', $branch_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData([
                "status" => 0,
                "message" => "Package is not found"
            ]);
        }

        return $this->respondWithItem($resource);
    }

    /**
     * Store a newly resource
     *
     * @param CreatePackageRequest $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreatePackageRequest $request, $branch_id)
    {
        $data = $request->only(array_keys($request->rules()));
        $data['branch_id'] = $branch_id;

        BranchPackage::create($data);

        return $this->respondWithCustomData([
            'message' => 'New Membership has successfully added!'
        ], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param CreatePackageRequest $request
     * @param int $branch_id
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreatePackageRequest $request, $branch_id, $id)
    {
        $resource = BranchPackage::where([
            ['id', $id],
            ['branch_id', $branch_id]
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData([
                "status" => 0,
                "message" => "Package is not found"
            ]);
        }

        $resource->branch_id = $request->branch_id;
        $resource->title = $request->title;
        $resource->gym_access = $request->gym_access;
        $resource->duration = $request->duration;
        $resource->periode = $request->periode;
        $resource->pt_session = $request->pt_session;
        $resource->session = $request->session;
        $resource->description = $request->description;
        $resource->about = $request->about;
        $resource->benefit = $request->benefit;
        $resource->policy = $request->policy;
        $resource->price = $request->price;
        $resource->is_published = $request->is_published;
        $resource->is_unlimited = $request->is_unlimited;
        $resource->save();

        return $this->respondWithCustomData([
            'message' => 'Membership has successfully updated!'
        ], Response::HTTP_OK);
    }
}
