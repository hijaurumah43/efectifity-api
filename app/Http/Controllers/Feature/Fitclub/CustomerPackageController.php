<?php

namespace App\Http\Controllers\Feature\Fitclub;

use App\Http\Controllers\Controller;
use App\Http\Resources\Feature\Fitclub\Member\MemberPackageCollection;
use App\Http\Resources\Feature\Fitclub\Member\MemberPackageResource;
use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerPackageController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = MemberPackageCollection::class;
        $this->resourceItem = MemberPackageResource::class;
        $this->middleware(['hasBranch']);
    }

    public function history(Request $request, $branch_id, $member_id)
    {
        $perPage = $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        if (!$request->has('rangeTime')) {
            $this->respondWithCustomData(['message' => 'Query param rangeTime is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $dateTime = $request->input('rangeTime');

        $dateTime = explode(',', $dateTime);
        $from = $dateTime[0];
        $to = $dateTime[1];

        $collection = MemberPackage::where([
            ['branch_id', $branch_id],
            ['member_id', $member_id]
        ])
            ->whereBetween('created_at', [$from, $to])
            ->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }
}
