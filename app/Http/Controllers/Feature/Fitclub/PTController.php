<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Events\Member\PT\PtClassCancel;
use App\Models\Client\Member\Log\MemberLog;
use App\Models\Client\Staff\PersonalTrainer;
use App\Events\Fitclub\PT\PTReservationEvent;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Staff\PersonalTrainerMember;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Staff\PersonalTrainerAttendance;
use App\Models\Client\Staff\PersonalTrainerAttendanceStatus;
use App\Http\Resources\Feature\Fitclub\PersonalTrainer\PersonalTrainerResource;
use App\Http\Requests\Feature\Fitclub\PersonalTrainer\CreatePTAttendanceRequest;
use App\Http\Resources\Feature\Fitclub\PersonalTrainer\PersonalTrainerCollection;

class PTController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PersonalTrainerCollection::class;
        $this->resourceItem = PersonalTrainerResource::class;
        $this->middleware('hasBranch');
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, $branch_id)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage <= 100 ? $perPage : 20;

        $collection = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['status', 1]
        ])->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified resource
     *
     * @param int $branch_id
     * @param int $ptID
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branch_id, $ptID)
    {
        $resource = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['id', $ptID],
        ])->first();

        if (!$resource) {
            return $this->respondWithCustomData(["message" => "The resource not found"], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource);
    }

    /**
     * Create schedule
     *
     * @param int $branch_id
     * @param int $ptID
     * @param string $memberPackageID
     * @return \Illuminate\Http\JsonResponse
     */
    public function schedule($branch_id, $ptID, $memberPackageID)
    {
        // check PT
        $pt = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['id', $ptID],
        ])->first();

        if (!$pt) {
            return $this->respondWithCustomData(["message" => "The resource not found"], Response::HTTP_NOT_FOUND);
        }

        /** SCHEDULE */
        // check member package
        $memberPackage = MemberPackage::where([
            ['id', $memberPackageID],
            ['branch_id', $branch_id]
        ])->first();

        if (!$memberPackage) {
            return $this->respondWithCustomData(["message" => "Package is not your ownership"], Response::HTTP_NOT_FOUND);
        }

        $schedules = PersonalTrainerMember::where([
            ['member_package_id', $memberPackageID],
            ['member_id', $memberPackage->member_id]
        ])->get();

        $member = $memberPackage->member;
        $package = $memberPackage->package;
        $validity = Carbon::parse($memberPackage->created_at)->format('d F Y') . ' until ' . Carbon::parse($memberPackage->expiry_date)->format('d F Y');
        $account = [
            'member' => [
                'memberID' => $member->id,
                'avatar' => Member::avatarStorage($member->avatar),
                'firstName' => ucwords($member->name),
                'lastName' => ucwords($member->last_name),
                'email' => ucwords($member->user->email),
                'telp' => ucwords($member->user->phone),
            ],
            'personalTrainer' => [
                'ptID' => $pt->id,
                'image' => $pt->image != NULL ? ENV('CDN') . '/' . config('cdn.pt') . $pt->image : NULL,
                'firstName' => ucwords($pt->member->name),
                'lastName' => ucwords($pt->member->last_name),
                'email' => ucwords($pt->member->user->email),
                'telp' => ucwords($pt->member->user->phone),

            ],
            'package' => [
                'packageID' => $memberPackage->id,
                'name' => $package->title,
                'validity' => $validity,
                'endDate' => Carbon::parse($memberPackage->expiry_date)->format('Y-m-d'),
                'usage' => $memberPackage->pt_session - $memberPackage->pt_session_remaining,
                'total' => $memberPackage->pt_session,
                'remaining' => $memberPackage->pt_session_remaining,
                'isActive' => $memberPackage->status == 1 ? true : false
            ]
        ];

        $collection = [];
        $attendances = PersonalTrainerAttendance::where([
            ['member_id', $member->id],
            ['member_package_id', $memberPackage->id],
        ])->orderBy('date', 'DESC')->get();

        foreach ($attendances as $item) {

            $collection[] = [
                'id' => $item->id,
                'ptName' => $item->pt->member->name,
                'scheduleDate' => $item->date,
                'startTime' => $item->start_time,
                'endTime' => $item->end_time,
                // NOTE:: CONVERT_DT
                // 'scannedTime' => $item->scan_time != NULL ? $item->scan_time : null,
                'scannedTime' =>  $item->scan_time != NULL ? ConvertToTimestamp($item->scan_time) : null,
                'status' => ptAttendanceStatus($item->personal_trainer_attendance_status_id),
            ];
        }

        return $this->respondWithCustomData([
            'attributes' => $account,
            'schedule' =>  $collection,
        ], Response::HTTP_OK);
    }

    /**
     * Show schedule resource
     *
     * @param int $branch_id
     * @param string $memberPackageID
     * @param string $ptAttendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function showSchedule($branch_id, $memberPackageID, $ptAttendanceID)
    {
        $item = PersonalTrainerAttendance::where([
            ['member_package_id', $memberPackageID],
            ['id', $ptAttendanceID],
            ['branch_id', $branch_id],
        ])->first();

        if (!$item) {
            return $this->respondWithCustomData(['message' =>  'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        $data = [
            'id' => $item->id,
            'scheduleDate' => $item->date,
            'startTime' => $item->start_time,
            'endTime' => $item->end_time,
            'status' => ptAttendanceStatus($item->personal_trainer_attendance_status_id),
        ];

        return $this->respondWithCustomData($data, Response::HTTP_OK);
    }

    /**
     * Update resource
     *
     * @param CreatePTAttendanceRequest $request
     * @param int $branch_id
     * @param int $ptID
     * @param string $memberPackageID
     * @param string $ptAttendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CreatePTAttendanceRequest $request, $branch_id, $ptID, $memberPackageID, $ptAttendanceID)
    {
        $ptAttendance = PersonalTrainerAttendance::where([
            ['id', $ptAttendanceID],
            ['member_package_id', $memberPackageID],
            ['branch_id', $branch_id],
            ['personal_trainer_id', $ptID],
            ['personal_trainer_attendance_status_id', 1],
        ])->first();

        if (!$ptAttendance) {
            return $this->respondWithCustomData(['message' =>  'Your schedule not found'], Response::HTTP_BAD_REQUEST);
        }

        $ptAttendanceID = $ptAttendance->id;

        // $ptAttendance->personal_trainer_attendance_status_id = 4;
        // $ptAttendance->save();
        return $this->store($request, $branch_id, $ptID, $memberPackageID, true, $ptAttendanceID);

        // return $this->respondWithCustomData(['message' =>  'The schedule has updated successfully'], Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreatePTAttendanceRequest $request
     * @param int $branch_id
     * @param int $ptID
     * @param int $memberPackageID
     * @param boolean $reschedule
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreatePTAttendanceRequest $request, $branch_id, $ptID, $memberPackageID, $reschedule = false, $ptAttendanceID = NULL)
    {
        $data = $request->only(array_keys($request->rules()));

        // Check Package bener atau Tidak
        $memberPackage = MemberPackage::where([
            ['branch_id', $branch_id],
            ['id', $memberPackageID]
        ])->first();

        if (!$memberPackage) {
            return $this->respondWithCustomData(['message' => 'Package not found'], Response::HTTP_BAD_REQUEST);
        }

        // session empty or expiry date
        if ($memberPackage->pt_session_remaining < 1 || $memberPackage->expiry_date < date('Y-m-d H:i:s')) {
            return $this->respondWithCustomData(['message' => 'Quota PT session has been empty or package is expired'], Response::HTTP_BAD_REQUEST);
        }

        // Check PT ada Tidak
        $pt = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['id', $ptID],
            ['status', 1] // active
        ])->first();

        if (!$pt) {
            return $this->respondWithCustomData(['message' => 'Personal Trainer Not Found'], Response::HTTP_BAD_REQUEST);
        }

        // Check Jadwal PT
        $memberBranch = MemberBranch::where([
            ['member_id', $pt->member_id],
            ['branch_id', $branch_id],
            ['status', 2] // accepted
        ])->first();

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Personal Trainer is not Our staff or inactivated'], Response::HTTP_BAD_REQUEST);
        }

        $shift = StaffScheduleDetail::where([
            ['branch_id', $branch_id],
            ['member_branch_id', $memberBranch->id],
            ['is_published', 1]
        ])->whereDate('start_time', $data['date'])->first();

        if (!$shift) {
            return $this->respondWithCustomData(['message' => 'Personal Trainer day off'], Response::HTTP_BAD_REQUEST);
        }

        $startTime = Carbon::parse($shift->shift->start_time)->format("H:i");
        $endTime = Carbon::parse($shift->shift->end_time)->format("H:i");

        // check slot time
        if ($data['start_time'] < $startTime || $data['end_time'] > $endTime) {
            return $this->respondWithCustomData(['message' => 'This Time Slot is not available, please select another Time Slot'], Response::HTTP_BAD_REQUEST);
        }

        // can't booked with same package on the same day
        $checkBooked = PersonalTrainerAttendance::where([
            ['personal_trainer_id', $ptID],
            ['branch_id', $branch_id],
            ['member_package_id', $memberPackageID],
            ['personal_trainer_attendance_status_id', 1],
        ])
            ->whereDate('date', $data['date'])
            ->orWhere([
                ['personal_trainer_id', $ptID],
                ['branch_id', $branch_id],
                ['member_package_id', $memberPackageID],
                ['personal_trainer_attendance_status_id', 7],
            ])
            ->whereDate('date', $data['date'])->count();
        // return $checkBooked;
        if ($checkBooked) {
            if ($reschedule == false) {
                return $this->respondWithCustomData(['message' => 'Schedule unsaved, conflicted with existing time schedule'], Response::HTTP_BAD_REQUEST);
            }
        }

        // not time available
        $checkBooked = PersonalTrainerAttendance::where([
            ['personal_trainer_id', $ptID],
            ['branch_id', $branch_id],
            // ['member_package_id', $memberPackageID],
            ['personal_trainer_attendance_status_id', 1],
        ])->whereDate('date', $data['date'])
            /* ->where('start_time', '>=', $data['start_time'])
            ->where('end_time', '<=', $data['end_time']) */
            ->get();

        if (count($checkBooked) > 0) {

            // check slot time
            $slotAvailable = true;
            foreach ($checkBooked as $item) {

                if ($data['start_time'] > $item->start_time && $data['end_time'] < $item->end_time) {
                    // return 'a';
                    // 15.30 > 15.00 && 16.30 < 16.00 x
                    // 15.30 > 15.00 && 15.45 < 16.00 v
                    // 15.50 > 15.30 && 16.50 < 16.30 x
                    $slotAvailable = false;
                }

                // if ($data['start_time'] > $item->end_time && $item->end_time < $data['end_time']) {
                // return 'b';
                // 15.30 > 15.00 && 15.45 < 16.00 x
                // 15.30 > 16.30 && 16.00 < 16.30 x
                // 15.50 > 16.30 && 16.30 < 16.50 x
                // 17.00 > 16.30 && 16.30 < 18.00 v
                // $slotAvailable = false;
                // }

                // if ($data['start_time'] < $item->start_time && $data['end_time'] < $item->end_time) {
                if ($data['start_time'] < $item->start_time && $data['end_time'] > $item->start_time) {
                    // return 'c';
                    // 14.30 < 15.00 && 20.00 < 16.00 x
                    // 14.30 < 15.00 && 15.30 < 15.00 v
                    // 15.50 > 15.30 && 16.50 < 16.30 x

                    $slotAvailable = false;
                }

                if ($data['start_time'] < $item->start_time && $data['end_time'] > $item->end_time) {
                    // return 'd';
                    // 14.30 < 15.00 && 20.00 < 16.00 x
                    // 14.30 < 15.00 && 17.30 < 16.00 v
                    // 15.50 > 15.30 && 16.50 < 16.30 x

                    $slotAvailable = false;
                }

                if ($data['start_time'] < $item->end_time && $item->end_time < $data['end_time']) {
                    // return 'e';
                    // 15.50 > 16.30 && 16.30 < 16.50 v
                    // 17.00 > 16.30 && 16.30 < 18.00 v
                    $slotAvailable = false;
                }

                // 17.00 > 16.30 && 16.30 < 18.00 v

                /* $bookedTime = Carbon::parse($data['date'] . ' ' . $data['start_time'] . ':00');

                $avaialbleTIme = $data['date'] . ' ' . $item->start_time . ':00';
                $startTimeAvailable = date($avaialbleTIme, strtotime('+60 minutes')); */

                // if ($startTimeAvailable > $bookedTime) {
                if ($slotAvailable == false) {
                    return $this->respondWithCustomData(['message' => 'This Time Slot is not available, please select another Time Slot'], Response::HTTP_BAD_REQUEST);
                }
            }
        }

        // return 'stop';

        DB::beginTransaction();
        if ($reschedule == false) {

            // Check the sime time can't be store
            $ptAttendances = PersonalTrainerAttendance::where([
                ['personal_trainer_id', $ptID],
                ['branch_id', $branch_id],
                ['personal_trainer_attendance_status_id', 1],
            ])
                ->whereDate('date', $data['date'])
                ->where('start_time', '>=', $data['start_time'])
                ->where('end_time', '<=', $data['end_time'])
                ->get();

            if (count($ptAttendances) > 0) {
                return $this->respondWithCustomData(['message' => 'This Time Slot is used, please select another Time Slot'], Response::HTTP_BAD_REQUEST);
            }
        } else if ($reschedule == true) {

            $ptAttendance = PersonalTrainerAttendance::where([
                ['id', $ptAttendanceID],
                ['member_package_id', $memberPackageID],
                ['branch_id', $branch_id],
                ['personal_trainer_id', $ptID],
                ['personal_trainer_attendance_status_id', 1],
            ])->first();

            $ptAttendance->personal_trainer_attendance_status_id = 4;
            $ptAttendance->save();

            $memberPackage->increment('pt_session_remaining', 1);
        }

        $data['branch_id'] = $branch_id;
        $data['personal_trainer_id'] = $ptID;
        $data['member_package_id'] = $memberPackageID;
        $data['member_id'] = $memberPackage->member_id;
        $data['personal_trainer_attendance_status_id'] = 7;
        $data['start_timestamp'] = $data['start_timestamp'] . '000';
        $data['end_timestamp'] = $data['end_timestamp'] . '000';

        $pt = PersonalTrainerAttendance::create($data);
        // $memberPackage->decrement('pt_session_remaining', 1);

        $ptName = ucwords($pt->pt->member->name . ' ' . $pt->pt->member->last_name);
        $ptClubName = ucwords($pt->branch->title);
        $ptReservation = Carbon::parse($pt->date . ' ' . $pt->start_time)->format('d F Y H:i');

        $message = 'Confirm or Reject your reservation for a Personal Training session with ' . $ptName . ' at ' . $ptClubName . ' on ' . $ptReservation;
        $memberLog = MemberLog::create([
            'member_id' => $pt->member_id,
            'branch_id' => $pt->branch_id,
            'personal_trainer_attendance_id' => $pt->id,
            'status' => 7, // pending
            'message' => $message,
        ]);
        $dataFb = [
            'slug' => 'notifications',
            'param' => [
                'key' => 'notification-id',
                'value' => $pt->id,
            ]
        ];

        event(new PTReservationEvent($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberPackage->member->user->fcm_token, $message, $dataFb);


        DB::commit();

        return $this->respondWithCustomData(['message' => 'New Time Slot has added successfuly'], Response::HTTP_CREATED);
    }

    /**
     * Existing list by date
     *
     * @param int $branch_id
     * @param int $ptID
     * @param string $memberPackageID
     * @param string $date
     * @return \Illuminate\Http\JsonResponse
     */
    public function bookedList($branch_id, $ptID, $memberPackageID, $date)
    {
        // check PT
        $pt = PersonalTrainer::where([
            ['branch_id', $branch_id],
            ['id', $ptID],
        ])->first();

        if (!$pt) {
            return $this->respondWithCustomData(["message" => "The resource not found"], Response::HTTP_NOT_FOUND);
        }

        // Check Jadwal PT
        $memberBranch = MemberBranch::where([
            ['member_id', $pt->member_id],
            ['branch_id', $branch_id],
            ['status', 2] // accepted
        ])->first();

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Personal Trainer is Not our staff or inactivated'], Response::HTTP_BAD_REQUEST);
        }

        // PT schedule
        $shift = StaffScheduleDetail::where([
            ['branch_id', $branch_id],
            ['member_branch_id', $memberBranch->id],
            ['is_published', 1]
        ])->whereDate('start_time', $date)->first();

        if (!$shift) {
            return $this->respondWithCustomData(['message' => 'Personal Trainer day off'], Response::HTTP_BAD_REQUEST);
        }

        // Check your self
        $tempMe = null;
        $me = PersonalTrainerAttendance::where([
            ['branch_id', $branch_id],
            ['personal_trainer_id', $ptID],
            ['member_package_id', $memberPackageID]
        ])
            ->whereDate('date', $date)
            ->orderBy('created_at', 'DESC')
            ->first();

        if ($me) {
            $tempMe = [
                'id' => $me->id,
                'date' => $me->date,
                'startTime' => $me->start_time,
                'endTime' => $me->end_time,
                'status' => ptAttendanceStatus($me->personal_trainer_attendance_status_id),
            ];
        }

        // Collection
        $collections = PersonalTrainerAttendance::where([
            ['branch_id', $branch_id],
            ['personal_trainer_id', $ptID],
            ['personal_trainer_attendance_status_id', 1],
        ])
            ->whereDate('date', $date)
            ->orderBy('start_time', 'ASC');

        // include
        /* if ($me) {
            $collections->where('id', '!=', $me->id);
        } */

        $collections = $collections->get();

        $temp = [];
        foreach ($collections as $collection) {

            if (isset($collection->member)) {
                $member = $collection->member;
                $temp[] = [
                    'id' => $collection->id,
                    'startTime' => $collection->start_time,
                    'endTime' => $collection->end_time,
                    'status' => ptAttendanceStatus($collection->personal_trainer_attendance_status_id),
                    'member' => [
                        'memberID' => $member->id,
                        'avatar' => Member::avatarStorage($member->avatar),
                        'firstName' => ucwords($member->name),
                        'lastName' => ucwords($member->last_name),
                        'email' => ucwords($member->user->email),
                        'telp' => ucwords($member->user->phone),
                    ]
                ];
            }
        }

        return $this->respondWithCustomData([
            'ptSchedule' => [
                'startTime' => Carbon::parse($shift->shift->start_time)->format('H.i'),
                'endTime' => Carbon::parse($shift->shift->end_time)->format('H.i'),
                'duration' => $pt->duration,
            ],
            'schedule' => $tempMe,
            'collection' => $temp,
        ], Response::HTTP_OK);
    }

    /**
     * Change status resource to cancelled
     *
     * @param int $branch_id
     * @param int $ptID
     * @param string $memberPackageID
     * @param string $ptAttendanceID
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelled($branch_id, $memberPackageID, $ptAttendanceID)
    {
        $user = auth()->user();

        // Check Package bener atau Tidak
        $memberPackage = MemberPackage::where([
            ['branch_id', $branch_id],
            ['id', $memberPackageID]
        ])->first();

        if (!$memberPackage) {
            return $this->respondWithCustomData(['message' => 'Package not found'], Response::HTTP_BAD_REQUEST);
        }

        $ptAttendance = PersonalTrainerAttendance::where([
            ['id', $ptAttendanceID],
            ['member_package_id', $memberPackageID],
            ['branch_id', $branch_id],
            ['personal_trainer_attendance_status_id', 1],
        ])->first();

        if (!$ptAttendance) {
            return $this->respondWithCustomData(['message' =>  'Your schedule not found'], Response::HTTP_BAD_REQUEST);
        }

        DB::beginTransaction();
        $ptAttendance->personal_trainer_attendance_status_id = 5;
        $ptAttendance->cancel_time = date('Y-m-d H:i:s');
        $ptAttendance->save();

        $memberPackage->increment('pt_session_remaining', 1);
        $memberPackage->save();

        /**
         * Create History
         */
        $ptStaff = $user->member->name . '' . $user->member->last_name;
        $ptName = $ptAttendance->pt->member->name . '' . $ptAttendance->pt->member->last_name;
        $ptTime = Carbon::parse($ptAttendance->date)->format('d F Y') . ' ' . $ptAttendance->start_time;
        $ptCancel = Carbon::parse($ptAttendance->cancel_time)->format('d F Y H:i:s');
        $message = "PT Session at " . $ptTime . "with " . ucwords($ptName) . " has been cancelled by " . ucwords($ptStaff) . " on " . $ptCancel;

        $memberLog = MemberLog::create([
            'member_id' => $ptAttendance->member_id,
            'branch_id' => $ptAttendance->branch_id,
            'personal_trainer_attendance_id' => $ptAttendance->id,
            'status' => 5, // 5 = cancel
            'message' => $message,
        ]);

        $dataFb = [
            'slug' => 'upcoming',
            'param' => [
                'key' => 'status',
                'value' => 'expired',
            ]
        ];
        event(new PtClassCancel($memberLog));
        //NOTE:: NOTIF FIREBASE
        sendNotificationFirebase($memberLog->member->user->fcm_token, $message, $dataFb);
        DB::commit();

        return $this->respondWithCustomData(['message' =>  'The schedule has updated successfully'], Response::HTTP_OK);
    }

    public function dateMarked($branch_id, $ptID, $memberPackageID)
    {

        $collections = PersonalTrainerAttendance::where('branch_id', $branch_id)
            ->where([
                ['personal_trainer_id', $ptID],
                ['personal_trainer_attendance_status_id', 1],
                ['member_package_id', $memberPackageID]
            ])
            ->select('date')
            ->orderBy('date', 'ASC')
            ->get()->groupBy(function ($q) {
                return Carbon::parse($q->date)->format('Y-m-d');
            });
        if (!$collections) {
            return $this->respondWithCustomData(['message' => 'Data not found']);
        }

        $temp = [];
        foreach ($collections as $key => $value) {
            $temp[] = $key;
        }

        return $this->respondWithCustomData(['date' => $temp]);
    }
}
