<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Http\Resources\Feature\Fitclub\Branch\BranchResource;
use App\Http\Resources\Feature\Fitclub\Branch\BranchCollection;

class BranchController extends Controller
{

    public function __construct()
    {
        $this->resourceCollection = BranchCollection::class;
        $this->resourceItem = BranchResource::class;
    }

    /**
     * Display a listing of resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $sort = 'asc';
        $user = auth()->user();
        if (strtolower(request()->get('sort')) == 'desc') {
            $sort = 'desc';
        }

        $query = Branch::where([
            ['is_published', 1],
            ['is_active', 1]
        ])->orderBy('title', $sort);

        $company = Company::where([
            'user_id' => $user->id
        ])->first();

        if ($company) {
            $query->where('company_id', $company->id);
        } else {
            $data = MemberBranch::where([
                'member_id' => $user->member->id,
                'is_published' => 1,
                'is_active' => 1,
            ])->select('branch_id')->get();
            foreach($data  as $data) {
                $id[] = $data->branch_id;
            }
            $query->whereIn('id', $id);
        }
        $collection = $query->paginate();
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }
}
