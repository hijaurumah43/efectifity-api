<?php

namespace App\Http\Controllers\Feature\Fitclub;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Models\Client\Staff\StaffAttendance;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchSchedule;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasBranch');
    }

    public function branch($branch_id)
    {
        $resource = Branch::where('id', $branch_id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithCustomData([
            'id' => $resource->id,
            'title' => $resource->title,
            'slug' => $resource->slug,
            'thumbnail' => $resource->logo != null ? ENV('CDN') . '/' . config('cdn.branchThumbnail') . $resource->thumbnail : null,
            'logo' => $resource->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $resource->logo : null,
            'address' => $resource->region,
        ], Response::HTTP_OK);
    }

    /**
     * Get attendance staff
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function clockIn($branch_id)
    {
        // $time = '00.00';
        $user = auth()->user();

        $date = date('Y-m-d');
        $memberBranch = MemberBranch::where([
            'branch_id' => $branch_id,
            'member_id' => $user->member->id,
            'status' => 2,
            // 'is_published' => 1,
            // 'is_active' => 1,
        ])
            // ->whereDate('time', $date)
            ->first();

        if (!$memberBranch) {
            return $this->respondWithCustomData(['message' => 'Access denied'], Response::HTTP_FORBIDDEN, 'failed');
        }

        $clockIn = StaffAttendance::where([
            'member_id' => $user->member->id,
            'member_branch_id' => $memberBranch->id, //
            'status' => 1,
            'is_active' => 1,
        ])
            ->whereDate('time', $date)
            ->first();

        if (!$clockIn) {
            return $this->respondWithCustomData([
                'message' => 'Please Clock In yourself on App',
                'attended' => false,
            ], Response::HTTP_OK, 'failed');
        }

        $time = Carbon::parse($clockIn->time)->format('H.i');

        return $this->respondWithCustomData([
            'message' => 'Clock In: ' . $time,
            'attended' => true,
        ], Response::HTTP_OK, 'success');
    }
    /**
     * Display total current visitor
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function visitor($branch_id)
    {
        $date = date('Y-m-d');
        // $date = date('2021-11-16');
        $checkIn = MemberAttendance::whereDate('time', $date)->where([
            'is_active' => 1,
            'branch_id' => $branch_id,
            'status' => 1,
        ])->count();

        $checkOut = MemberAttendance::whereDate('time', $date)->where([
            'is_active' => 1,
            'branch_id' => $branch_id,
            'status' => 0,
        ])->count();

        $atTheClub = $checkIn - $checkOut;

        return $this->respondWithCustomData([
            'checkIn' => $checkIn,
            'checkOut' => $checkOut,
            'atTheClub' => $atTheClub,
        ], Response::HTTP_OK, 'success');
    }

    /**
     * Display upcoming classes
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function classes($branch_id)
    {
        $schedule = BranchSchedule::where([
            'date' => date('Y-m-d'),
            'branch_id' => $branch_id,
            'is_published' => 1,
        ])->firstOrFail();

        $class = [];

        foreach ($schedule->detail as $item) {
            $coach = [];
            if ($item->class->branchCoach) {
                foreach ($item->class->branchCoach as $value) {
                    $coach[] = $value->member->name;
                }
            }
            $class[] = [
                'id' => $item->id,
                'title' => $item->class->title,
                'image' => ENV('CDN') . '/' . config('cdn.classes') . $item->class->image,
                'startTime' => Carbon::parse($item->start_time)->format('H.i'),
                'endTime' => Carbon::parse($item->end_time)->format('H.i'),
                'coach' => $coach,
            ];
        }

        $result = collect($class)->sortBy('startTime');
        $result = $result->values()->all();

        return $this->respondWithCustomData($result, Response::HTTP_OK, 'success');
    }
}
