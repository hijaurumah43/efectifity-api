<?php

namespace App\Http\Controllers\Common\Search;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchSchedule;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Http\Resources\Common\Company\CompanyResource;
use App\Http\Resources\Common\Service\ServiceResource;
use App\Models\Client\Member\Attendances\ClassAttendance;

class SearchController extends Controller
{
    public function collection(Request $request, $category)
    {
        $required_fields = ['sch', 'latlong', 'date'];
        $error_fields = '';
        $error = false;
        $parameter = array();
        foreach ($required_fields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $error_fields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => $error_fields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $latlong = $request->get('latlong');
        $query = $request->get('sch');
        $lat = '';
        $long = '';
        $checkLatLong =  preg_match('/^[-]?((([0-8]?[0-9])(\.(\d+))?)|(90(\.0+)?)),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))(\.(\d+))?)|180(\.0+)?)$/', $latlong);

        if ($checkLatLong == 1) {
            $latlong = explode(',', $latlong);
            $lat = $latlong[0];
            $long = $latlong[1];
        }


        $fixCollect = [];
        switch ($category) {
            case 'club':
                $branches = Branch::where([
                    ['is_active', 1],
                    ['is_published', 1]
                ])
                    ->where('title', 'LIKE', '%' . $query . '%')->get();
                $collection = [];
                foreach ($branches as $branch) {

                    $distance   = 0;
                    if ($checkLatLong == 1) {
                        $distance = $branch->setDistance($lat, $long, $branch->id);
                        $distance = explode('.', $distance);
                        $distance = $distance[0] . '.' . substr($distance[1], 0, 1);
                    }
                    $collection[] = [
                        'id' => $branch->id,
                        'title' => $branch->title,
                        'logo' => $branch->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $branch->logo : null,
                        'address' => $branch->region,
                        'distance' => (float) $distance . ' km',
                    ];
                }
                $fixCollect = $collection;
                break;

            case 'class':

                $date = date('Y-m-d');
                $isCancelled = false;
                // GET BRANCH BY SCHEDULE / DATE
                $branchScheduleId = BranchSchedule::where('date', $date)->select('id')->get();

                // GET CLASS
                $queryModel = BranchScheduleDetail::whereIn('branch_schedule_id', $branchScheduleId);

                if ($request->get('sch')) {
                    // return $request->get('sch');
                    $queryModel->whereHas('class', function ($q) use ($request) {
                        $query = $request->get('sch');
                        $q->where('title', 'like', '%' . $query . '%');
                    });
                }
                $classes =  $queryModel->get();

                $collection = [];
                foreach ($classes as $item) {
                    $distance   = 0;
                    if ($checkLatLong == 1) {
                        $distance = $item->schedule->branch->setDistance($lat, $long, $item->schedule->branch->id);
                        $distance = explode('.', $distance);
                        $distance = $distance[0] . '.' . substr($distance[1], 0, 1);
                    }

                    $collection[] = [
                        'id' => $item->id,
                        // 'id' => $item->branch_class_id,
                        'title' => $item->class->title,
                        'logo' => ENV('CDN') . '/' . config('cdn.classes') . $item->class->image,
                        'address' => $item->class->branch->title,
                        'distance' => $distance . ' km',
                    ];
                    $coaches = [];
                }

                $collectionData = collect($collection);
                $fixCollect = [];
                foreach ($collectionData as $ac) {
                    $fixCollect[] = $ac;
                }
                $fixCollect = collect($fixCollect);
                break;

            default:
                $fixCollect = [];
                break;
        }

        /* Paginate */
        $item           = collect($fixCollect);
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = 15;
        $currentResults = $item->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $item->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $data           = $paginate->items();

        return response()->json([
            "data" => $data,
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
        ], Response::HTTP_OK);
    }
}
