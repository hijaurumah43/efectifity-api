<?php

namespace App\Http\Controllers\Common\Voucher;

use App\Http\Controllers\Controller;
use App\Http\Resources\Common\Voucher\VoucherCollection;
use App\Http\Resources\Common\Voucher\VoucherResource;
use App\Models\Internal\Voucher\Voucher;
use Symfony\Component\HttpFoundation\Response;

class VoucherController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = VoucherCollection::class;
        $this->resourceItem = VoucherResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($branch_id)
    {
        $merchant = Voucher::where('is_published', 1)->where('branch_id', $branch_id)->where('expired', '>', date('Y-m-d H:i:s'))->get();
        $internal = Voucher::where('is_published', 1)->whereNull('branch_id')->where('expired', '>', date('Y-m-d H:i:s'))->get();

        $collect = collect([$merchant, $internal]);
        $collect = $collect->collapse()->sortByDesc('created_at');

        return $this->respondWithCustomData(VoucherResource::collection($collect), Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified resource
     *
     * @param Voucher $voucher
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($voucher)
    {
        $voucher = Voucher::findOrFail($voucher);
        return $this->respondWithItem($voucher);
    }
}
