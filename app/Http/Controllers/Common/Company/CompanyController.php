<?php

namespace App\Http\Controllers\Common\Company;

use App\Http\Controllers\Controller;
use App\Contracts\Common\Company\CompanyRepository;
use App\Http\Resources\Common\Company\CompanyResource;
use App\Http\Resources\Common\Company\CompanyCollection;
use App\Models\Common\Company\Company;

class CompanyController extends Controller
{
    private $companyRepository;

    /**
     * @param CompanyRepository $companyRepository
     */
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
        $this->resourceCollection = CompanyCollection::class;
        $this->resourceItem = CompanyResource::class;        
    }

    /**
     * Dispaly a lisitng of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->companyRepository->findByFilters();
        
        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified of the resource
     *
     * @param Company $company
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Company $company)
    {
        return $this->respondWithItem($company);
    }
}
