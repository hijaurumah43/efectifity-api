<?php

namespace App\Http\Controllers\Common\Payment;

use Xendit\Xendit;
use App\Http\Controllers\Controller;
use App\Contracts\Common\Payment\PaymentMethodRepository;
use App\Http\Resources\Common\Payment\PaymentMethodResource;
use App\Http\Resources\Common\Payment\PaymentMethodCollection;

class PaymentMethodController extends Controller
{
    private $paymentMethodRepository;

    /**
     * @param PaymentMethodRepository $paymentMethodRepository
     */
    public function __construct(PaymentMethodRepository $paymentMethodRepository)
    {
        $this->paymentMethodRepository = $paymentMethodRepository;
        $this->resourceCollection = PaymentMethodCollection::class;
        $this->resourceItem = PaymentMethodResource::class;
        Xendit::setApiKey(ENV('XENDIT_SECRET'));
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->paymentMethodRepository->findByFilters([
            'is_active' => 1,
        ]);
        return $this->respondWithCollection($collection);
    }
}
