<?php

namespace App\Http\Controllers\Common\Payment;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Payment\PaymentTypeRepository;
use App\Http\Resources\Common\Payment\PaymentTypeResource;
use App\Http\Resources\Common\Payment\PaymentTypeCollection;

class PaymentTypeController extends Controller
{
    private $paymentTypeRepository;

    /**
     * @param PaymentTypeRepository $paymentTypeRepository
     */
    public function __construct(PaymentTypeRepository $paymentTypeRepository)
    {
        $this->paymentTypeRepository = $paymentTypeRepository;
        $this->resourceCollection = PaymentTypeCollection::class;
        $this->resourceItem = PaymentTypeResource::class;        
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->paymentTypeRepository->findByFilters(['is_published' => 1]);

        $va = [];
        $ew = [];
        foreach ($collection as $key => $value) {
            switch ($value->methode) {
                case '1':
                    $methode = 'virtual account';
                    
                    $va[] = [
                        'id' => $value->id,
                        'title' => $value->title,
                        'image' => $value->image,
                        'methode' => $methode,
                    ];

                    break;
    
                case '2':
                    $methode = 'e-wallets';

                    $ew[] = [
                        'id' => $value->id,
                        'title' => $value->title,
                        'image' => $value->image,
                        'methode' => $methode,
                    ];
                    break;
                
                default:
                    $methode = 'payment methode not registered';
                    break;
            }
        }
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }
}
