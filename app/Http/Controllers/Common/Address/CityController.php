<?php

namespace App\Http\Controllers\Common\Address;

use App\Models\Common\Address\City;
use App\Http\Controllers\Controller;
use App\Contracts\Common\Address\CityRepository;
use App\Http\Resources\Common\Address\CityResource;
use App\Http\Resources\Common\Address\CityCollection;

class CityController extends Controller
{
    private $cityRepository;

    /**
     * @param CityRepository $cityRepository
     */
    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->resourceCollection = CityCollection::class;
        $this->resourceItem = CityResource::class;
    }

    /**
     * Display a listing of the resouce
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = City::paginate(200); 
        return $this->respondWithCollection($collection);
    }
}
