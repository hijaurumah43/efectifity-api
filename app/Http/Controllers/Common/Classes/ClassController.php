<?php

namespace App\Http\Controllers\Common\Classes;

use Carbon\Carbon;
use Sujip\Ipstack\Ipstack;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use App\Models\Client\Staff\Rules\Rule;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Client\Staff\Rules\BranchRule;
use App\Models\Common\Branch\BranchClassLevel;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Common\Branch\BranchClassCategory;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Attendances\ClassAttendance;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $required_fields = ['latlong', 'date'];
        $error_fields = '';
        $error = false;
        $parameter = array();
        foreach ($required_fields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $error_fields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => $error_fields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $latlong = $request->input('latlong');
        $latlong = explode(',', $latlong);
        $lat = $latlong[0];
        $long = $latlong[1];

        $collection = [];
        $date = $request->input('date');
        $isCancelled = false;
        $perPage = 15;
        $distance = 0;

        // GET BRANCH BY SCHEDULE / DATE
        $branchScheduleId = BranchSchedule::where('date', $date)
            ->whereHas('branch', function ($q) use ($lat, $long) {
                if ($lat != 'null' || $long != 'null') {
                    $q->location($lat, $long);
                }
            })
            ->select('id')->get();

        // GET CLASS
        $query = BranchScheduleDetail::whereIn('branch_schedule_id', $branchScheduleId);
        if ($request->has('category')) {
            $query->whereHas('class', function ($q) use ($request) {
                $category = explode(',', $request->input('category'));
                $q->whereIn('branch_class_category_id', $category);
            });
        }

        if ($request->has('level')) {
            $query->whereHas('class', function ($q) use ($request) {
                $level = explode(',', $request->input('level'));
                $q->whereIn('branch_class_level_id', $level);
            });
        }

        $classes =  $query->get();
        $coaches = [];
        foreach ($classes as $item) {

            // Coaches
            if ($item->class->branchCoach) {
                foreach ($item->class->branchCoach as $data) {
                    if ($data->member) {
                        $coaches[] = $data->member->name;
                    }
                }
            }

            // Quota
            $quota = ClassAttendance::where([
                ['class_attendance_status_id', 1],
                ['branch_schedule_detail_id', $item->id]
            ])->count();

            if ($item->is_cancelled == 1) {
                $isCancelled = true;
            }

            if ($lat != 'null' || $long != 'null') {
                $distance = $item->schedule->branch->setDistance($lat, $long, $item->schedule->branch->id);
                $distance = explode('.', $distance);
                $distance = $distance[0] . '.' . substr($distance[1], 0, 1);
            } else {
                $distance = 0;
            }


            // just offline
            if ($item->class->is_online == 0) {
                $collection[] = [
                    'id' => $item->id,
                    'startTime' => $item->start_time,
                    'endTime' => $item->end_time,
                    'duration' => (strtotime($item->end_time) - strtotime($item->start_time)) / 60,
                    'quota' => $quota . '/' . $item->max_quota,
                    'isCancelled' => $isCancelled,
                    'class' => [
                        'id' => $item->branch_class_id,
                        'title' => $item->class->title,
                        'category' => ucwords($item->class->category->title),
                        'image' => ENV('CDN') . '/' . config('cdn.classes') . $item->class->image,
                        'isOnline' => $item->class->is_online == 1 ? true : false,
                        'maxBookTime' => $item->class->max_book_time,
                        'maxCancelTime' => $item->class->max_cancel_time,
                        'maxDelayTime' => $item->class->delay_time,
                        'level' => [
                            'label' => $item->class->level->title,
                            'description' => $item->class->description,
                        ],
                        'promo' => $item->schedule->branch->promo,
                    ],
                    'distance' => $distance . ' km',
                    'coaches' => $coaches,
                ];

                $coaches = [];
            }
        }

        $collectionData = collect($collection);
        if ($request->has('sort')) {
            // return strtolower($request->input('sort'));
            if (strtolower($request->input('sort')) == 'nearby') {
                $collectionData = $collectionData->sortBy('distance', SORT_NATURAL);
            } elseif (strtolower($request->input('sort')) == 'aztime') {
                $collectionData = $collectionData->sortBy('startTime', SORT_NATURAL);
            } elseif (strtolower($request->input('sort')) == 'zatime') {
                $collectionData = $collectionData->sortByDesc('startTime', SORT_NATURAL);
            }
        } else {
            $collectionData = $collectionData->sortBy('startTime', SORT_NATURAL);
        }

        // $collectionData = $collectionData->sortBy('distance', SORT_NATURAL);
        $fixCollect = [];
        foreach ($collectionData as $ac) {
            $fixCollect[] = $ac;
        }
        $fixCollect = collect($fixCollect);

        /* Paginate */
        $customer       = collect(json_decode($fixCollect));
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $perPage        = $perPage;
        $currentResults = $customer->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $paginate       =  new LengthAwarePaginator($currentResults, $customer->count(), $perPage);
        $paginate       = $paginate->setPath($request->url());
        $dataSchedule   = $paginate->items();

        return response()->json([
            "data" => [
                'date' => Carbon::parse($date)->format('d F Y'),
                'schedules' => $dataSchedule,
            ],
            'pagination' => [
                'total' => $paginate->total(),
                'perPage' => $paginate->perPage(),
                'currentPage' => $paginate->currentPage(),
                'nextPageUrl' => $paginate->nextPageUrl(),
                'previousPageUrl' => $paginate->previousPageUrl(),
                'lastPage' => $paginate->lastPage(),
                'from' => $paginate->firstItem(),
                'to' => $paginate->lastItem(),
            ],
        ], Response::HTTP_OK);
    }

    /**
     * Display a specified resource
     *
     * @param int $schedule_detail_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($schedule_detail_id)
    {
        $branchScheduleDetail = BranchScheduleDetail::where('id', $schedule_detail_id)->first();
        if (!$branchScheduleDetail) {
            return $this->respondWithCustomData(NULL, Response::HTTP_OK, 'success');
        }

        $quota      = 0;
        $coaches    = [];
        $isCancelled = false;

        // Today Coaches
        if ($branchScheduleDetail->class->branchCoach) {
            foreach ($branchScheduleDetail->class->branchCoach as $data) {
                if ($data->member) {
                    $coaches[] = $data->member->name;
                }
            }
        }

        // Quota
        $quota = ClassAttendance::where([
            ['class_attendance_status_id', 1],
            ['branch_schedule_detail_id', $branchScheduleDetail->id]
        ])->count();

        if ($branchScheduleDetail->is_cancelled == 1) {
            $isCancelled = true;
        }

        $branchDestruct = $branchScheduleDetail->schedule->branch;

        $ruleType = 1;
        $isPT = false;

        // CHECK CLASS
        if ($branchScheduleDetail->class->is_online == 1) {
            // ONLINE
            $ruleType = 2;
        } else {
            // OFFLINE
            $ruleType = 1;
        }

        $rules = BranchRule::where([
            ['branch_id', $branchDestruct->id],
            ['type', $ruleType],
        ])
            ->orderBy('id', 'ASC')
            ->get();

        if (count($rules) < 1) {
            $rules = Rule::where('type', $ruleType)->orderBy('id', 'ASC')->get();
        }

        $tempRule = [];
        foreach ($rules as $key => $value) {
            if ($key == 0) {
                $tempRule[] = 'Reservation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the Class started';
            } else if ($key == 2) {
                $tempRule[] = 'Cancellation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the Class started';
            } else if ($key == 3) {
                $tempRule[] = 'Present at the class at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' after the actual Class schedule';
            } else if ($key == 5) {
                if (isset($rules[6])) {
                    $tempRule[] = 'If you reach ' . $value->duration . ' times of Class cancellation within 7 days, you will not be able to make Class reservation in ' . $rules[6]->duration . ' ' . ruleNamePeriode($rules[6]->period) . ' ahead ';
                }
            }
        }

        // just offline
        if ($branchScheduleDetail->class->is_online == 0) {
            $temp = [
                'id' => $branchScheduleDetail->id,
                'startTime' => $branchScheduleDetail->start_time,
                'endTime' => $branchScheduleDetail->end_time,
                'duration' => (strtotime($branchScheduleDetail->end_time) - strtotime($branchScheduleDetail->start_time)) / 60,
                'quota' => $quota . '/' . $branchScheduleDetail->max_quota,
                'isCancelled' => $isCancelled,
                'date' => $branchScheduleDetail->schedule->date,
                'branch' => [
                    'id' => $branchDestruct->id,
                    'title' => $branchDestruct->title,
                    'logo' => ENV('CDN') . '/' . config('cdn.branchLogo') . $branchDestruct->logo,
                    'address' => $branchDestruct->address,
                    'latitude' => $branchDestruct->latitude,
                    'longitude' => $branchDestruct->longitude,
                ],
                'rules' => $tempRule,
                'class' => [
                    'id' => $branchScheduleDetail->branch_class_id,
                    'title' => $branchScheduleDetail->class->title,
                    'description' => $branchScheduleDetail->class->description,
                    'category' => ucwords($branchScheduleDetail->class->category->title),
                    'image' => ENV('CDN') . '/' . config('cdn.classes') . $branchScheduleDetail->class->image,
                    'isOnline' => $branchScheduleDetail->class->is_online == 1 ? true : false,
                    'maxBookTime' => $branchScheduleDetail->class->max_book_time,
                    'maxCancelTime' => $branchScheduleDetail->class->max_cancel_time,
                    'maxDelayTime' => $branchScheduleDetail->class->delay_time,
                    'level' => [
                        'label' => $branchScheduleDetail->class->level->title,
                        'description' => $branchScheduleDetail->class->description,
                    ],
                ],
                'coaches' => $coaches,
            ];
        }

        return $this->respondWithCustomData($temp, Response::HTTP_OK, 'success');
    }

    public function level()
    {
        $temp = [];
        $collections = BranchClassLevel::where('is_active', 1)->get();
        foreach ($collections as $collection) {
            $temp[] = [
                'id' => $collection->id,
                'title' => $collection->title,
            ];
        }
        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }

    public function category()
    {
        $temp = [];
        $collections = BranchClassCategory::where('is_published', 1)->get();
        foreach ($collections as $collection) {
            $temp[] = [
                'id' => $collection->id,
                'title' => $collection->title,
            ];
        }
        return $this->respondWithCustomData($temp, Response::HTTP_OK);
    }
}
