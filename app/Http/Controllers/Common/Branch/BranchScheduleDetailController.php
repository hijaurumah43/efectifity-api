<?php

namespace App\Http\Controllers\Common\Branch;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Branch\BranchScheduleRepository;
use App\Contracts\Common\Branch\BranchScheduleDetailRepository;
use App\Http\Resources\Common\Branch\BranchScheduleDetailResource;
use App\Http\Resources\Common\Branch\BranchScheduleDetailCollection;

class BranchScheduleDetailController extends Controller
{
    private $branchScheduleDetailRepository;

    /**
     * @param BranchScheduleDetailRepository $branchScheduleDetailRepository
     */
    public function __construct(BranchScheduleDetailRepository $branchScheduleDetailRepository)
    {
        $this->branchScheduleDetailRepository = $branchScheduleDetailRepository;
        $this->resourceCollection = BranchScheduleDetailCollection::class;
        $this->resourceItem = BranchScheduleDetailResource::class;
    }

    /**
     * Display a specified resource
     *
     * @param int $id
     * @param int $class
     * @param int $date
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id, $class, $date)
    {
        $schedule = app(BranchScheduleRepository::class)->findOneBy(['branch_id' => $id, 'date' => $date]);
        $scheduleDetail = $this->branchScheduleDetailRepository->findOneBy(['branch_schedule_id' => $schedule->id, 'branch_class_id' => $class]);

        return $this->respondWithItem($scheduleDetail, Response::HTTP_OK, 'success');
    }
}
