<?php

namespace App\Http\Controllers\Common\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\BranchGallery;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Common\Branch\BranchGalleryResource;
use App\Http\Resources\Common\Branch\BranchGalleryCollection;
use App\Models\Client\Staff\Settings\Section;

class BranchGalleryController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = BranchGalleryCollection::class;
        $this->resourceItem = BranchGalleryResource::class;
    }

    public function index(Request $request, $branch_id)
    {
        $perPage = (int) $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = BranchGallery::where([
            ['is_active', 1],
            ['branch_id', $branch_id],
        ])
            ->select('section_id')
            ->groupBy('section_id');

        /* if ($request->has('type')) {
            $query->where('type', $request->input('type'));
        } */

        $collections = $query->get();

        // return $collections;
        $data = [];
        foreach ($collections as $collection) {
            // return $collection;
            $items = BranchGallery::where([
                ['is_active', 1],
                ['branch_id', $branch_id],
                ['section_id', $collection->section_id]
            ])->get();

            $img = [];
            foreach ($items as $item) {
                $img[] = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'url' => $item->name
                ];
            }

            $nameSection = 'none';
            $section =  Section::where('id', $collection->section_id)->first();
            if ($section) {
                $nameSection = ucwords($section->title);
            }

            $data[] = [
                'title' => $nameSection,
                'total' => count($items),
                'images' => $img,
            ];

            $img = [];
        }

        return $this->respondWithCustomData($data, Response::HTTP_OK);
    }

    public function collection(Request $request, $branch_id)
    {
        $perPage = (int) $request->input('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = BranchGallery::where([
            ['is_active', 1],
            ['branch_id', $branch_id],
        ]);
        if ($request->has('type')) {
            $query->where('section_id', $request->get('type'));
        }

        $collections = $query->paginate($perPage);
        return $this->respondWithCollection($collections);
    }
}
