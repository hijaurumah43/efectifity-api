<?php

namespace App\Http\Controllers\Common\Branch;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Common\Branch\BranchPackage;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Branch\BranchRepository;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Http\Resources\Common\Branch\BranchCollection;
use App\Models\Client\Partner\Branch\BranchAgreement;

class BranchController extends Controller
{
    private $branchRepository;

    /**
     * @param BranchRepository $branchRepository
     */
    public function __construct(BranchRepository $branchRepository)
    {
        $this->branchRepository = $branchRepository;
        $this->resourceCollection = BranchCollection::class;
        $this->resourceItem = BranchResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requiredFields = ['location'];
        $errorFields = '';
        $error = false;
        $parameter = array();
        foreach ($requiredFields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $errorFields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => $errorFields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        $latlong = request()->get('location');
        $latlong = explode(',', $latlong);
        $latitude = $latlong[0];
        $longitude = $latlong[1];

        if ($latitude == 'null' || $longitude == 'null') {
            $query = Branch::published();
        } else {
            $query = Branch::published()->location($latitude, $longitude, 50);
            $countBranch = $query->get();

            if (count($countBranch) < 3) {

                $query = Branch::published()->location($latitude, $longitude, 100000);
            }
        }

        if ($request->has('services')) {
            $query->whereHas('service', function ($q) use ($request) {
                $service = strtolower($request->input('services'));
                return $q->where('slug', $service);
            });
        }

        if ($request->has('club')) {
            $query->whereHas('branchCategories', function ($q) use ($request) {
                $club = explode(',', $request->input('club'));
                return $q->whereIn('service_category_id', $club);
            });
        }

        if ($request->has('offer')) {
            if ($request->input('offer') == 'true') {
                $query->whereHas('vouchers');
            } else {
                $query;
            }
        }
        if ($request->has('price') || $request->has('budget')) {
            if ($request->has('price')) {
                if (strtolower($request->input('price')) == 'heighest') {
                    $sort = 'desc';
                } else {
                    $sort = 'asc';
                }

                $query->addSelect([
                    'price' => BranchPackage::select(DB::raw('min(price)'))
                        ->whereColumn('branch_id', 'branches.id')->latest()->take(1)
                ])->orderBy('price', $sort);
            }

            if ($request->has('budget')) {
                $budget = request()->get('budget');
                $budget = explode(',', $budget);
                $from = $budget[0];
                $to = $budget[1];
                $query->addSelect([
                    'budget' => BranchPackage::select(DB::raw('min(price)'))
                        ->whereColumn('branch_id', 'branches.id')->take(1)->whereBetween('price', [$from, $to])
                ]);
            }
        } else {
            if ($latitude != 'null' || $longitude != 'null') {
                $query->orderBy('distance', 'ASC');
            }
        }

        if ($request->has('status')) {
            $query->whereHas('operationalTimes', function ($q) use ($request) {
                $status = explode(',', $request->input('status'));
                return $q->where('date', date('Y-m-d'));
            });
        }

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified of the resource
     *
     * @param Branch $branches
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Branch $branch)
    {
        return $this->respondWithItem($branch, Response::HTTP_OK, 'success');
    }

    /**
     * Display other resource
     *
     * @param int $branch
     * @return void
     */
    public function location($branch)
    {
        $branch = $this->branchRepository->findOneBy(['id' => $branch]);

        /* $collection = $this->branchRepository->findByFilters([
            'service_id' => $branch->service_id,
            'company_id' => $branch->company_id,
        ]); */

        if ($branch) {

            $collection = Branch::where([
                ['service_id', $branch->service_id],
                ['company_id', $branch->company_id],
                ['id', '!=', $branch->id]
            ])->paginate();
        }

        return $this->respondWithCollection($collection);
    }

    public function agreements($branch_id)
    {
        $data = BranchAgreement::where([
            ['is_published', 1],
            ['branch_id', $branch_id],
            ['is_draft', 0],
        ])->orderBy('created_at', 'DESC')->first();

        if (!$data) {
            return $this->respondWithCustomData(['message' => 'Agreement not found'], Response::HTTP_OK, 'success');
        }

        return $this->respondWithCustomData([
            'id' => $data->id,
            'version' => ConvertToTimestamp($data->version),
            'content' => $data->content,
            /* 'signedOn' => $data->created_at->format('Y-m-d H.i'),
            'createdBy' => 'Faisal Murkadi',
            'memberId' => '202010002112' */
        ], Response::HTTP_OK, 'success');
    }
}
