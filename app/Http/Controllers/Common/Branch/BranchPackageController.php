<?php

namespace App\Http\Controllers\Common\Branch;

use App\Http\Controllers\Controller;
use App\Models\Common\Branch\BranchPackage;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Branch\BranchPackageRepository;
use App\Http\Resources\Common\Branch\BranchPackageResource;
use App\Http\Resources\Common\Branch\BranchPackageCollection;

class BranchPackageController extends Controller
{
    private $branchPackageRepository;

    /**
     * @param BranchPackageRepository $branchPackageRepository
     */
    public function __construct(BranchPackageRepository $branchPackageRepository)
    {
        $this->branchPackageRepository = $branchPackageRepository;
        $this->resourceCollection = BranchPackageCollection::class;
        $this->resourceItem = BranchPackageResource::class;
    }

    /**
     * Display a listing of the resource by branch
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection($branch_id)
    {
        // $collection = $this->branchPackageRepository->findByFilters(['branch_id' => $branch_id, 'is_published' => 1]);

        $perPage = (int) request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        $collection = BranchPackage::where([
            ['branch_id', $branch_id],
            ['is_published', 1],
        ])
            ->orderBy('is_promo', 'DESC')
            ->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display a specified of the resource
     *
     * @param BranchPackage $branchPackage
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BranchPackage $branchPackage)
    {
        return $this->respondWithItem($branchPackage, Response::HTTP_OK, 'success');
    }
}
