<?php

namespace App\Http\Controllers\Common\Branch;

use App\Http\Controllers\Controller;
use App\Contracts\Common\Branch\BranchScheduleRepository;
use App\Http\Resources\Common\Branch\BranchScheduleResource;
use App\Http\Resources\Common\Branch\BranchScheduleCollection;

class BranchScheduleController extends Controller
{
    private $branchScheduleRepository;

    /**
     * @param BranchScheduleRepository $branchScheduleRepository
     */
    public function __construct(BranchScheduleRepository $branchScheduleRepository)
    {
        $this->branchScheduleRepository = $branchScheduleRepository;
        $this->resourceCollection = BranchScheduleCollection::class;
        $this->resourceItem = BranchScheduleResource::class;        
    }

    /**
     * Display a listing of the resource by branch
     *
     * @param int $branch_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function collection($branch_id)
    {
        $collection = $this->branchScheduleRepository->findByFilters(['branch_id' => $branch_id]);
        return $this->respondWithCollection($collection);
    }
}
