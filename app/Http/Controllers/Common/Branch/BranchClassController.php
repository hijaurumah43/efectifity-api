<?php

namespace App\Http\Controllers\Common\Branch;

use App\Http\Controllers\Controller;
use App\Contracts\Common\Branch\BranchClassRepository;
use App\Http\Resources\Common\Branch\BranchClassResource;
use App\Http\Resources\Common\Branch\BranchClassCollection;

class BranchClassController extends Controller
{
    private $branchClassRepository;

    /**
     * @param BranchClassRepository $branchClassRepository
     */
    public function __construct(BranchClassRepository $branchClassRepository)
    {
        $this->branchClassRepository = $branchClassRepository;
        $this->resourceCollection = BranchClassCollection::class;
        $this->resourceItem = BranchClassResource::class;
    }
}
