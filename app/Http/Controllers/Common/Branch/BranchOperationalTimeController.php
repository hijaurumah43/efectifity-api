<?php

namespace App\Http\Controllers\Common\Branch;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Common\Branch\BranchOperationalTimeRepository;
use App\Http\Resources\Common\Branch\BranchOperationalTimeResource;
use App\Http\Resources\Common\Branch\BranchOperationalTimeCollection;
use App\Models\Common\Branch\BranchOperationalTime;
use App\Models\Common\Branch\OpertionalTime;

class BranchOperationalTimeController extends Controller
{
  private $branchOperationalTimeRepository;

  /**
   * @param BranchOperationalTimeRepository $branchOperationalTimeRepository
   */
  public function __construct(BranchOperationalTimeRepository $branchOperationalTimeRepository)
  {
    $this->branchOperationalTimeRepository = $branchOperationalTimeRepository;
    $this->resourceCollection = BranchOperationalTimeCollection::class;
    $this->resourceItem = BranchOperationalTimeResource::class;
  }

  /**
   * Display listing of the resource by branch id
   *
   * @param int $branch_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function collection($branch_id)
  {
    $check = OpertionalTime::where([
      ['status', 1],
      ['branch_id', $branch_id]
    ])
      ->orderBy('id', 'ASC')
      ->select("id")
      ->get()
      ->toArray();

    if (count($check) < 1) {
      return $this->respondWithCustomData([], Response::HTTP_OK);
    }

    /* $collection = $this->branchOperationalTimeRepository->findByFilters([
      'branch_id' => $branch_id,
      'operational_time_id' => $check->id,
    ]); */

    $date = date('Y-m-d');

    $collection = BranchOperationalTime::where([
      ['branch_id', $branch_id],
      ['date', '>=', $date],
    ])
      ->whereIn('operational_time_id', $check)
      ->orderBy('date', 'ASC')
      ->paginate(40);

    return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
  }

  /**
   * Display listing of the resource by branch id
   *
   * @param int $branch_id
   * @return \Illuminate\Http\JsonResponse
   */
  public function showByDate($branch_id, $date)
  {

    $data = $this->branchOperationalTimeRepository->findByFilters(['branch_id' => $branch_id, 'date' => $date]);
    if (@count($data) > 0) {
      $data = $data[0];
      return $this->respondWithItem($data, Response::HTTP_OK, 'success');
    } else {
      $data = null;
      return $this->respondWithCustomData([], Response::HTTP_OK, 'success');
    }
  }
}
