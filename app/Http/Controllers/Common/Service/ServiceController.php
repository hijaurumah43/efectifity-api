<?php

namespace App\Http\Controllers\Common\Service;

use App\Http\Controllers\Controller;
use App\Models\Common\Service\Service;
use App\Contracts\Common\Service\ServiceRepository;
use App\Http\Resources\Common\Service\ServiceResource;
use App\Http\Resources\Common\Service\ServiceCollection;

class ServiceController extends Controller
{
    private $serviceRepository;

    /**
     * @param ServiceRepositry $serviceRepository
     */
    public function __construct(ServiceRepository $serviceRepository)
    {
        $this->serviceRepository = $serviceRepository;
        $this->resourceCollection = ServiceCollection::class;
        $this->resourceItem = ServiceResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->serviceRepository->findByFilters(['is_published' => 1]);
        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified of the resource
     *
     * @param Service $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($service)
    {
        $service = $this->serviceRepository->findOneBy(['slug' => $service]);
        return $this->respondWithItem($service);
    }
}
