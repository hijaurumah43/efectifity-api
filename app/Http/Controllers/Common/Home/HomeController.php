<?php

namespace App\Http\Controllers\Common\Home;

use App\Http\Controllers\Controller;
use App\Http\Resources\Common\Home\FitClassResource;
use App\Http\Resources\Common\Home\FitClubResource;
use App\Http\Resources\Common\Home\FitKitchenResource;
use App\Http\Resources\Common\Home\HomeResource;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchScheduleDetail;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends Controller
{
    public function __construct()
    { }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($type)
    {
        $collections = [];
        $type = strtolower($type);
        switch ($type) {
            case 'fitclub':
                $service_id = [1];
                break;

            case 'fitclass':
                $service_id = [1, 2];
                break;

            case 'fitkitchen':
                $service_id = [2];
                break;

            default:
                return abort(404);
                break;
        }

        $required_fields = ['latlong'];
        $error_fields = '';
        $error = false;
        $parameter = array();
        foreach ($required_fields as $field) {
            if (request()->get($field) == '') {
                $error = true;
                $error_fields .= $field . ', ';
            } else {
                $parameter[$field] = request()->get($field);
            }
        }

        if ($error) {
            return $this->respondWithCustomData(['message' => $error_fields . 'is required'], Response::HTTP_UNPROCESSABLE_ENTITY, 'failed');
        }

        $latlong = request()->get('latlong');
        $latlong = explode(',', $latlong);

        $lat = $latlong[0];
        $long = $latlong[1];
        $data = [];

        if ($lat == 'null' || $long == 'null') {
            $branches = Branch::whereIn('service_id', $service_id)->where('is_published', 1)->orderBy('ID', 'ASC')->limit(20)->get();
        } else {

            $branches = Branch::whereIn('service_id', $service_id)->where('is_published', 1)->location($lat, $long, 50)->orderBy('distance', 'ASC')->get();
            $countBranch = count($branches);
            if ($countBranch < 3) {
                $branches = Branch::whereIn('service_id', $service_id)->where('is_published', 1)->location($lat, $long, 100000)->orderBy('distance', 'ASC')->get();
            }
        }
        if ($type == 'fitclass') {
            foreach ($branches as $branchs) {

                $now = date('Y-m-d');
                $nowTimeStamp = ConvertToTimestamp($now);
                $today =  $branchs->schdules->where('timestamp', '>=', $nowTimeStamp)->first();
                if ($today) {
                    $collections[] = BranchScheduleDetail::where('branch_schedule_id', $today->id)->get();
                }
            }

            $collections = collect($collections);
            $collections = $collections->collapse();
            $collections = $collections->all();

            $data =  FitClassResource::collection($collections);
        } else if ($type == 'fitclub') {
            $collections = $branches;
            $data =  FitClubResource::collection($collections);
        } else if ($type == 'fitkitchen') {
            $collections = $branches;
            $data =  FitKitchenResource::collection($collections);
        }

        return $this->respondWithCustomData($data, Response::HTTP_OK, 'success');
    }
}
