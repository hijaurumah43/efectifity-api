<?php

namespace App\Http\Controllers\Internal\Question;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Internal\Question\QuestionTypeRepository;
use App\Http\Resources\Internal\Question\QuestionTypeResource;
use App\Http\Resources\Internal\Question\QuestionTypeCollection;
use App\Http\Requests\Internal\Question\QuestionTypeCreateRequest;

class QuestionTypeController extends Controller
{
    private $questionTypeRepository;

    /**
     * @param QuestionTypeRepository $questionTypeRepository
     */
    public function __construct(QuestionTypeRepository $questionTypeRepository)
    {
        $this->questionTypeRepository = $questionTypeRepository;
        $this->resourceCollection = QuestionTypeCollection::class;
        $this->resourceItem = QuestionTypeResource::class;        
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->questionTypeRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    /**
     * Store a newly resource
     *
     * @param QuestionTypeCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionTypeCreateRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        $questionType =  $this->questionTypeRepository->store($data);

        return $this->respondWithItem($questionType, Response::HTTP_CREATED, 'success');
    }
}
