<?php

namespace App\Http\Controllers\Internal\Question;

use App\Http\Controllers\Controller;
use App\Contracts\Internal\Question\QuestionCategoryRepository;
use App\Http\Resources\Internal\Question\QuestionCategoryResource;
use App\Http\Resources\Internal\Question\QuestionCategoryCollection;
use App\Http\Requests\Internal\Question\QuestionCategoryCreateRequest;

class QuestionCategoryController extends Controller
{
    private $questionCategoryRepository;

    /**
     * @param QuestionCategoryRepository $questionCategoryRepository
     */
    public function __construct(QuestionCategoryRepository $questionCategoryRepository)
    {
        $this->questionCategoryRepository = $questionCategoryRepository;
        $this->resourceCollection = QuestionCategoryCollection::class;
        $this->resourceItem = QuestionCategoryResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection =  $this->questionCategoryRepository->findByFilters();
        return $this->respondWithCollection($collection);
    }

    /**
     * Store a newly resource
     *
     * @param QuestionCategoryCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionCategoryCreateRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        $questionCategory = $this->questionCategoryRepository->store($data); 

        return $this->respondWithItem($questionCategory);
    }
}
