<?php

namespace App\Http\Controllers\Internal\Question;

use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Internal\Question\QuestionRepository;
use App\Http\Resources\Internal\Question\QuestionResource;
use App\Http\Resources\Internal\Question\QuestionCollection;
use App\Http\Requests\Internal\Question\QuestionCreateRequest;

class QuestionController extends Controller
{
    private $questionRepository;

    /**
     * @param QuestionRepository $questionRepository
     */
    public function __construct(QuestionRepository $questionRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->resourceCollection = QuestionCollection::class;
        $this->resourceItem = QuestionResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = $this->questionRepository->findByFilters();
        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Store a newly resource
     *
     * @param QuestionCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionCreateRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        $question = $this->questionRepository->store($data);

        return $this->respondWithItem($question, Response::HTTP_CREATED, 'success');
    }
}
