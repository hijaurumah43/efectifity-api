<?php

namespace App\Http\Controllers\Internal\Question;

use App\Http\Controllers\Controller;
use App\Contracts\Internal\Question\QuestionDetailRepository;
use App\Http\Resources\Internal\Question\QuestionDetailResource;
use App\Http\Resources\Internal\Question\QuestionDetailCollection;
use App\Http\Requests\Internal\Question\QuestionDetailCreateRequest;

class QuestionDetailController extends Controller
{
    private $questionDetailRepository;

    /**
     * @param QuestionDetailRepository $questionDetailRepository
     */
    public function __construct(QuestionDetailRepository $questionDetailRepository)
    {
        $this->questionDetailRepository = $questionDetailRepository;
        $this->resourceCollection = QuestionDetailCollection::class;
        $this->resourceItem = QuestionDetailResource::class;
    }
    
    /**
     * Store a newly resource
     *
     * @param QuestionDetailCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionDetailCreateRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        $questionDetail = $this->questionDetailRepository->store($data);

        return $this->respondWithItem($questionDetail);
    }
}
