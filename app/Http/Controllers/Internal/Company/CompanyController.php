<?php

namespace App\Http\Controllers\Internal\Company;

use App\Http\Controllers\Controller;
use App\Http\Resources\Internal\Company\CompanyCollection;
use App\Http\Resources\Internal\Company\CompanyResource;
use App\Models\Common\Company\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = CompanyCollection::class;
        $this->resourceItem = CompanyResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collection = Company::orderBy('created_at', 'DESC')->paginate();
        return $this->respondWithCollection($collection);
    }

    /**
     * Display a specified resource
     *
     * @param Company $company
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Company $company)
    {
        return $this->respondWithItem($company);
    }
}
