<?php

namespace App\Http\Controllers\Internal\Company;

use App\Http\Controllers\Controller;
use App\Http\Resources\Internal\Company\CompanyTempCollection;
use App\Http\Resources\Internal\Company\CompanyTempResource;
use App\Models\Client\Partner\Temporary\CompanyTemp;
use Illuminate\Http\Request;

class CompanyTempController extends Controller
{
    public function __construct()
    {
       $this->resourceCollection = CompanyTempCollection::class;
       $this->resourceItem = CompanyTempResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $collecton = CompanyTemp::where('is_activated', 0)->orderBy('created_at', 'DESC')->paginate(15);
        return $this->respondWithCollection($collecton);
    }

    /**
     * Display a specified of resource
     *
     * @param CompanyTemp $companyTemp
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CompanyTemp $companyTemp)
    {
        return $this->respondWithItem($companyTemp);
    }
}
