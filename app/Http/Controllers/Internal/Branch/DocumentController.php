<?php

namespace App\Http\Controllers\Internal\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Internal\Company\Document\CreateDocumentReviewRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Http\Resources\Internal\Branch\DocumentResource;
use App\Models\Internal\Company\Document\DocumentStatus;
use App\Http\Resources\Internal\Branch\DocumentCollection;
use App\Models\Internal\Company\Document\DocumentStatusLog;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = DocumentCollection::class;
        $this->resourceItem = DocumentResource::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = (int) $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;
        
        if ($request->has('section')) {
            $section = $request->get('section');
            if ($section == 'onprocess') {
                $query = CompanyDocument::orderBy('created_at', 'DESC');
                /* ->where([
                    ['is_published', 0],
                    ['is_active', 0],
                ]); */

                $statusSection = [1, 2, 3, 4];

                $query->whereHas('statusLog', function ($q) use ($statusSection) {
                    return $q->whereIn('document_status_id', $statusSection);
                });
            } else if ($section == 'onboard') {
                $query = CompanyDocument::orderBy('created_at', 'DESC')->where([
                    ['is_published', 1],
                ]);
                
                $statusSection = [6, 7];

                $query->whereHas('statusLog', function ($q) use ($statusSection) {
                    return $q->whereIn('document_status_id', $statusSection);
                });
            }
        }

        // NOTE:: QUERY PARAM
        // type
        if ($request->has('type')) {
            $query->where('service_id', $request->get('type'));
        }

        // status
        if ($request->has('status')) {
            $status = $request->get('status');
            $query->whereHas('statusLog', function ($q) use ($status) {
                return $q->where('document_status_id', $status);
            });
        }
        // name
        if ($request->has('name')) {
            $name = $request->get('name');
            $query->whereHas('company', function ($q) use ($name) {
                return $q->where('title', 'like', '%' . $name . '%');
            });
        }

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resource = CompanyDocument::findOrFail($id);
        return $this->respondWithItem($resource);
    }

    public function review(CreateDocumentReviewRequest $request)
    {
        $user = auth()->user();

        $data = $request->only(array_keys($request->rules()));
        $data['user_id'] = $user->id;

        // Check Document
        $document = CompanyDocument::where('id', $data['company_document_id'])->first();
        if (!$document) {
            return $this->respondWithCustomData(['message' => 'Document not found'], Response::HTTP_NOT_FOUND);
        }

        // Check Status
        $documentStatus = DocumentStatus::where('id', $data['document_status_id'])->first();

        if (!$documentStatus) {
            return $this->respondWithCustomData(['message' => 'Status log not found'], Response::HTTP_NOT_FOUND);
        }

        DocumentStatusLog::create($data);

        if ($documentStatus->id == 5 || $documentStatus->id == 6) {
            $document->status = 1;
            $document->is_published = 1;
            $document->is_active = 1;
            $document->save();
        }

        return $this->respondWithCustomData('Review has been successfully', Response::HTTP_CREATED);
    }

    public function reviewerStatus()
    {
        $status = [3, 4, 6];
        $collection  = DocumentStatus::whereIn('id', $status)->get();

        foreach ($collection as $item) {
            $resource[] = [
                'id' => $item->id,
                'title' => ucwords($item->title),
            ];
        }

        return $this->respondWithCustomData($resource, Response::HTTP_OK);
    }

    public function status(Request $request, $param)
    {
        $param = strtolower($param);

        if ($param == 'onprocess') {
            $status = [1, 2, 3, 4];
        } else if ($param == 'onboard') {
            $status = [6, 7];
        } else {
            $status = [0];
        }

        $collection  = DocumentStatus::whereIn('id', $status)->get();
        $resource = [];
        if (count($collection) < 1) {
            return $this->respondWithCustomData($resource, Response::HTTP_NOT_FOUND);
        }

        foreach ($collection as $item) {
            $resource[] = [
                'id' => $item->id,
                'title' => ucwords($item->title),
            ];
        }

        return $this->respondWithCustomData($resource, Response::HTTP_OK);
    }
}
