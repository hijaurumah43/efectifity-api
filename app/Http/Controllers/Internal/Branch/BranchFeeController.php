<?php

namespace App\Http\Controllers\Internal\Branch;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Internal\Branch\BranchFee;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Internal\Branch\BranchFeeResource;
use App\Http\Resources\Internal\Branch\BranchFeeCollection;
use App\Http\Requests\Internal\Platforms\CreateBranchFeeRequest;
use App\Http\Requests\Internal\Platforms\UpdateBranchFeeRequest;

class BranchFeeController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = BranchFeeCollection::class;
        $this->resourceItem = BranchFeeResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = BranchFee::orderBy('created_at', 'DESC');

        if ($request->has('branch_id')) {
            $branchID = $request->get('branch_id');
            $query->where('branch_id', $branchID);
        }

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $branchFeeID
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($branchFeeID)
    {
        $resource = BranchFee::where('id', $branchFeeID)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateBranchFeeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateBranchFeeRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));

        $check = BranchFee::where('branch_id', $data['branch_id'])->whereDate('start_date', '>=', $data['start_date'])->whereDate('end_date', '<=', $data['end_date'])->count();

        if ($check > 0) {
            return $this->respondWithCustomData(['message' => 'Data not saved, please change the Period'], Response::HTTP_BAD_REQUEST);
        }

        if ($data['is_active'] == true) {
            /* $collections = BranchFee::where('branch_id', $data['branch_id'])->get();
            if (count($collections) > 0) {
                foreach ($collections as $collection) {
                    $collection->is_active = 0;
                    $collection->save();
                }
            } */

            $data['is_active'] = 1;
        } else {
            $data['is_active'] = 0;
        }

        BranchFee::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateBranchFeeRequest $request
     * @param int $branchFeeID
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBranchFeeRequest $request, $branchFeeID)
    {
        $data = $request->only(array_keys($request->rules()));

        $resource = BranchFee::where('id', $branchFeeID)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }
        $isActive = 0;
        if ($data['is_active'] == true) {

            /* $collections = BranchFee::where('branch_id', $data['branch_id'])->get();
            if (count($collections) > 0) {
                foreach ($collections as $collection) {
                    $collection->is_active = 0;
                    $collection->save();
                }
            } */

            $isActive = 1;
        }

        $resource->branch_id = $data['branch_id'];
        $resource->fee = $data['fee'];
        $resource->start_date = $data['start_date'];
        $resource->end_date = $data['end_date'];
        $resource->is_active = $isActive;
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_OK);
    }
}
