<?php

namespace App\Http\Controllers\Internal\Branch;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\Internal\Platforms\CreatePlatformReviewRequest;
use App\Models\Common\Branch\Branch;
use App\Http\Resources\Internal\Branch\PlatformResource;
use App\Http\Resources\Internal\Branch\PlatformCollection;
use App\Models\Client\Partner\Branch\BranchStatusLog;
use App\Models\Internal\Branch\BranchStatus;

class PlatformController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = PlatformCollection::class;
        $this->resourceItem = PlatformResource::class;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = (int) $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        if ($request->has('section')) {
            $section = $request->get('section');
            if ($section == 'onprocess') {
                $query = Branch::orderBy('created_at', 'DESC')->where([
                    ['is_published', 0],
                    ['is_active', 0],
                ]);

                $statusSection = [1, 2, 3, 4];

                $query->whereHas('statusLog', function ($q) use ($statusSection) {
                    return $q->whereIn('branch_status_id', $statusSection);
                });
            } else if ($section == 'onboard') {
                $query = Branch::orderBy('created_at', 'DESC')->where([
                    ['is_published', 1],
                ]);
                $statusSection = [6, 7];

                $query->whereHas('statusLog', function ($q) use ($statusSection) {
                    return $q->whereIn('branch_status_id', $statusSection);
                });
            }
        }

        // NOTE:: QUERY PARAM
        // type
        if ($request->has('type')) {
            $query->where('service_id', $request->get('type'));
        }

        // status
        if ($request->has('status')) {
            $status = $request->get('status');
            $query->whereHas('statusLog', function ($q) use ($status) {
                return $q->where('branch_status_id', $status);
            });
        }
        // name
        if ($request->has('name')) {
            $name = $request->get('name');
            $query->whereHas('company', function ($q) use ($name) {
                return $q->where('title', 'like', '%' . $name . '%');
            });
        }

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK, 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resource = Branch::findOrFail($id);
        return $this->respondWithItem($resource);
    }

    public function review(CreatePlatformReviewRequest $request)
    {
        $user = auth()->user();

        $data = $request->only(array_keys($request->rules()));
        $data['user_id'] = $user->id;

        // Check branch
        $branch = Branch::where('id', $data['branch_id'])->first();
        if (!$branch) {
            return $this->respondWithCustomData(['message' => 'Branch not found'], Response::HTTP_NOT_FOUND);
        }

        // Check Status
        $branchStatus = BranchStatus::where('id', $data['branch_status_id'])->first();
        
        if (!$branchStatus) {
            return $this->respondWithCustomData(['message' => 'Status log not found'], Response::HTTP_NOT_FOUND);
        }

        BranchStatusLog::create($data);

        if ($branchStatus->id == 5 || $branchStatus->id == 6) {
            $branch->is_published = 1;
            $branch->is_active = 1;
            $branch->save();
        }

        return $this->respondWithCustomData('Review has been successfully', Response::HTTP_CREATED);
    }

    public function reviewerStatus()
    {
        $status = [3, 4, 6];
        $branchStatus  = BranchStatus::whereIn('id', $status)->get();

        foreach ($branchStatus as $item) {
            $resource[] = [
                'id' => $item->id,
                'title' => ucwords($item->title),
            ];
        }

        return $this->respondWithCustomData($resource, Response::HTTP_OK);
    }

    public function status(Request $request, $param)
    {
        $param = strtolower($param);

        if ($param == 'onprocess') {
            $status = [1, 2, 3, 4];
        } else if ($param == 'onboard') {
            $status = [6, 7];
        } else {
            $status = [0];
        }

        $branchStatus  = BranchStatus::whereIn('id', $status)->get();
        $resource = [];
        if (count($branchStatus) < 1) {
            return $this->respondWithCustomData($resource, Response::HTTP_NOT_FOUND);
        }

        foreach ($branchStatus as $item) {
            $resource[] = [
                'id' => $item->id,
                'title' => ucwords($item->title),
            ];
        }

        return $this->respondWithCustomData($resource, Response::HTTP_OK);
    }
}
