<?php

namespace App\Http\Controllers\Internal\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Internal\Admin\AdminResource;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->resourceItem = AdminResource::class;
    }

    /**
     * Show a current logged user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(Request $request)
    {
        $user = auth()->user();   
        return $this->respondWithItem($user->member, Response::HTTP_OK, 'success');
    }
}
