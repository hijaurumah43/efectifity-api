<?php

namespace App\Http\Controllers\Internal\Bank;

use App\Http\Controllers\Controller;
use App\Http\Resources\Internal\Bank\BankCollection;
use App\Http\Resources\Internal\Bank\BankResource;
use App\Models\Internal\Bank\Bank;

class BankController extends Controller
{

    public function __construct()
    {
        $this->resourceCollection = BankCollection::class;
        $this->resourceItem = BankResource::class;    
    }

    public function index()
    {
        $collection = Bank::paginate(1000);

        return $this->respondWithCollection($collection);
    }
}
