<?php

namespace App\Http\Controllers\Internal\Settings\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Internal\Settings\Club\CreateClubSectionRequest;
use App\Http\Requests\Internal\Settings\Club\UpdateClubSectionRequest;
use App\Models\Client\Staff\Settings\Section;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Internal\Settings\Club\ClubSectionResource;
use App\Http\Resources\Internal\Settings\Club\ClubSectionCollection;

class ClubSectionController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClubSectionCollection::class;
        $this->resourceItem = ClubSectionResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = Section::orderBy('created_at', 'DESC');

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resource = Section::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClubSectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClubSectionRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        Section::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClubSectionRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClubSectionRequest $request, $id)
    {
        $data = $request->only(array_keys($request->rules()));

        $resource = Section::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }
        $resource->title = $data['title'];
        $resource->status = $data['status'];
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully updated'], Response::HTTP_OK);
    }
}
