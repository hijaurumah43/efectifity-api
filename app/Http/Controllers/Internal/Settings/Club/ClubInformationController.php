<?php

namespace App\Http\Controllers\Internal\Settings\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Internal\Additional\AdditionalInformation;
use App\Http\Resources\Internal\Settings\Club\ClubInformationResource;
use App\Http\Resources\Internal\Settings\Club\ClubInformationCollection;
use App\Http\Requests\Internal\Settings\Club\CreateClubInformationRequest;
use App\Http\Requests\Internal\Settings\Club\UpdateClubInformationRequest;

class ClubInformationController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClubInformationCollection::class;
        $this->resourceItem = ClubInformationResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = AdditionalInformation::orderBy('created_at', 'DESC');

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resource = AdditionalInformation::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClubInformationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClubInformationRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        if ($request->has('image')) {
            $path = 'cdn.clubSettingInformation';
            $data['image'] = uploadAssets($request->file('image'), $path);
        }
        AdditionalInformation::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClubInformationRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClubInformationRequest $request, $id)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = AdditionalInformation::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        if ($request->has('image') || $request->input('image') != NULL) {
            $path = 'cdn.clubSettingInformation';
            $data['image'] = uploadAssets($request->file('image'), $path);
            $resource->image = $data['image'];
        }

        $resource->title = $data['title'];
        $resource->description = $data['description'];
        $resource->is_published = $data['is_published'];
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully updated'], Response::HTTP_OK);
    }
}
