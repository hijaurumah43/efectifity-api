<?php

namespace App\Http\Controllers\Internal\Settings\Club;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Internal\Amenity\Amenity;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Internal\Settings\Club\ClubAmenityResource;
use App\Http\Resources\Internal\Settings\Club\ClubAmenityCollection;
use App\Http\Requests\Internal\Settings\Club\CreateClubAmenityRequest;
use App\Http\Requests\Internal\Settings\Club\UpdateClubAmenityRequest;

class ClubAmenityController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClubAmenityCollection::class;
        $this->resourceItem = ClubAmenityResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = Amenity::orderBy('created_at', 'DESC');

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resource = Amenity::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClubAmenityRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClubAmenityRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        if ($request->has('thumbnail')) {
            $path = 'cdn.clubSettingAmenity';
            $data['thumbnail'] = uploadAssets($request->file('thumbnail'), $path);
        }
        $data['service_id'] = 1;
        Amenity::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClubAmenityRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClubAmenityRequest $request, $id)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = Amenity::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        if ($request->has('thumbnail') || $request->input('thumbnail') != NULL) {
            $path = 'cdn.clubSettingAmenity';
            $data['thumbnail'] = uploadAssets($request->file('thumbnail'), $path);
            $resource->thumbnail = $data['thumbnail'];
        }

        $resource->title = $data['title'];
        $resource->is_published = $data['is_published'];
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully updated'], Response::HTTP_OK);
    }
}
