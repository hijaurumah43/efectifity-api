<?php

namespace App\Http\Controllers\Internal\Settings\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Common\Branch\BranchClassCategory;
use App\Http\Resources\Internal\Settings\Classes\ClassCategoryResource;
use App\Http\Resources\Internal\Settings\Classes\ClassCategoryCollection;
use App\Http\Requests\Internal\Settings\Classes\CreateClassCategoryRequest;
use App\Http\Requests\Internal\Settings\Classes\UpdateClassCategoryRequest;

class ClassCategoryController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassCategoryCollection::class;
        $this->resourceItem = ClassCategoryResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = BranchClassCategory::orderBy('created_at', 'DESC');

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resource = BranchClassCategory::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClassCategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClassCategoryRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        if ($request->has('thumbnail')) {
            $path = 'cdn.classSettingCategory';
            $data['thumbnail'] = uploadAssets($request->file('thumbnail'), $path);
        }
        BranchClassCategory::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClassCategoryRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClassCategoryRequest $request, $id)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = BranchClassCategory::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }
    
        if ($request->has('thumbnail') || $request->input('thumbnail') != NULL) {
            $path = 'cdn.classSettingCategory';
            $data['thumbnail'] = uploadAssets($request->file('thumbnail'), $path);
            $resource->thumbnail = $data['thumbnail'];
        }

        $resource->title = $data['title'];
        $resource->is_published = $data['is_published'];
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully updated'], Response::HTTP_OK);
    }
}
