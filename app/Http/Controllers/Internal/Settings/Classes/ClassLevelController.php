<?php

namespace App\Http\Controllers\Internal\Settings\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Internal\Settings\Classes\CreateClassLevelRequest;
use App\Http\Requests\Internal\Settings\Classes\UpdateClassLevelRequest;
use App\Models\Common\Branch\BranchClassLevel;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Internal\Settings\Classes\ClassLevelResource;
use App\Http\Resources\Internal\Settings\Classes\ClassLevelCollection;

class ClassLevelController extends Controller
{
    public function __construct()
    {
        $this->resourceCollection = ClassLevelCollection::class;
        $this->resourceItem = ClassLevelResource::class;
    }

    /**
     * Display a listing of the resource
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $perPage = $request->get('limit');
        $perPage = $perPage >= 1 && $perPage <= 100 ? $perPage : 20;

        $query = BranchClassLevel::orderBy('created_at', 'DESC');

        $collection = $query->paginate($perPage);

        return $this->respondWithCollection($collection, Response::HTTP_OK);
    }

    /**
     * Displat a specified resource
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $resource = BranchClassLevel::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->respondWithItem($resource, Response::HTTP_OK);
    }

    /**
     * Store a newly resource
     *
     * @param CreateClassLevelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateClassLevelRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        BranchClassLevel::create($data);

        return $this->respondWithCustomData(['message' => 'Data successfully submitted'], Response::HTTP_CREATED);
    }

    /**
     * Update a resource
     *
     * @param UpdateClassLevelRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateClassLevelRequest $request, $id)
    {
        $data = $request->only(array_keys($request->rules()));

        $resource = BranchClassLevel::where('id', $id)->first();
        if (!$resource) {
            return $this->respondWithCustomData(['message' => 'Resource not found'], Response::HTTP_NOT_FOUND);
        }
        $resource->title = $data['title'];
        $resource->description = $data['description'];
        $resource->is_active = $data['is_active'];
        $resource->save();

        return $this->respondWithCustomData(['message' => 'Data successfully updated'], Response::HTTP_OK);
    }
}
