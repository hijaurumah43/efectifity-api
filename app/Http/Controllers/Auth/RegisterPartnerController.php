<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Events\Partner\RegisterPartner;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Client\Partner\Temporary\CompanyTemp;

class RegisterPartnerController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $companyTemp = $this->create($request->all());
        event(new RegisterPartner($companyTemp));

        if ($response = $this->registered($request, $companyTemp)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param CompanyTemp    $comapanyTemp
     * @return mixed
     */
    protected function registered(Request $request, $companyTemp)
    {
        $message    = __(
            'We sent a confirmation email to :email. Please follow the instructions to complete your registration.',
            ['email' => $companyTemp->email]
        );

        return $this->respondWithCustomData([
            'message'     => $message,
        ], Response::HTTP_CREATED, "success");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'title' => [
                'required',
                'string',
                'max:255',
            ],
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'phone' => [
                'required',
                'numeric',
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users,email',
            ],
            'agent' => [
                'max:255',
            ],
            'city_id' => [
                'required',
                'numeric',
                'min:1',
            ],
        ]);

        if ($validator->fails()) {
            $form = new FormRequest();
            $form->failedValidation($validator);
        }

        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        CompanyTemp::where('email', $data['email'])->delete();
        $companyTemp = CompanyTemp::create($data);
        DB::commit();
        return $companyTemp;
    }
}
