<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Sujip\Ipstack\Ipstack;
use App\Http\ResponseTrait;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Contracts\UserRepository;
use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\DB;
use App\Exceptions\LockedException;
use App\Http\Controllers\Controller;
use App\Services\LoginHistoryService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Contracts\Client\Member\MemberRepository;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param User    $user
     * @return mixed
     */
    protected function registered(Request $request, User $user)
    {
        try {
            $data = $this->getDeviceInfo($request);
            $data['user_id'] = $user->id;
        } catch (LockedException $exception) {
            return $this->respondWithCustomData([
                'message'     => $exception->getMessage(),
            ], Response::HTTP_LOCKED);
        }

        $this->createNewLoginHistory($user, $data);

        $token      = (string)$this->guard()->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');
        $message    = __(
            'We sent a confirmation email to :email. Please follow the instructions to complete your registration.',
            ['email' => $user->email]
        );

        return $this->respondWithCustomData([
            'token'     => $token,
            'tokenType' => 'Bearer',
        ], Response::HTTP_CREATED, "success");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'name'     => [
                'required',
                'string',
                'max:255',
            ],
            'last_name'     => [
                'required',
                'string',
                'max:255',
            ],
            'email'    => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users,email',
            ],
            'password' => [
                'required',
                'string',
                'min:8',
            ]
        ]);

        if ($validator->fails()) {
            $form = new FormRequest();
            $form->failedValidation($validator);
        }

        return $validator;

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        $fcmToken = isset($data['fcm_token']) ? $data['fcm_token'] : '';
        $userRepository = app(UserRepository::class);
        
        $user =  $userRepository->store([
            'email'                       => $data['email'],
            'password'                    => bcrypt($data['password']),
            'email_token_confirmation'    => Uuid::uuid4()->toString(),
            'email_token_disable_account' => Uuid::uuid4()->toString(),
            'is_active'                   => 0,
            'email_verified_at'           => null,
            'fcm_token'                   => $fcmToken,
        ]);

        $user->attachRole(Role::MEMBER);

        $memberRepository = app(MemberRepository::class);
        $memberRepository->store([
            'user_id'   => $user->id,
            'name'      => $data['name'],
            'last_name' => $data['last_name'],
        ]);
        
        DB::commit();

        return $user;

    }

    private function getDeviceInfo(Request $request)
    {
        $agent = new Agent();
        $agent->setUserAgent($request->userAgent());
        $agent->setHttpHeaders($request->headers);

        $ipstack = new Ipstack($request->ip(), ENV("IPSTACK_KEY"));

        return [
            'id'               => Uuid::uuid4(),
            'user_id'          => auth()->id(),
            'ip'               => $request->ip(),
            'device'           => $agent->device(),
            'platform'         => $agent->platform(),
            'platform_version' => $agent->version($agent->platform()),
            'browser'          => $agent->browser(),
            'browser_version'  => $agent->version($agent->browser()),
            'city'             => $ipstack->city(),
            'region_code'      => $ipstack->regionCode(),
            'region_name'      => $ipstack->region(),
            'country_code'     => $ipstack->countryCode(),
            'country_name'     => $ipstack->country(),
            'continent_code'   => $ipstack->continentCode(),
            'continent_name'   => $ipstack->continent(),
            'latitude'         => $ipstack->latitude(),
            'longitude'        => $ipstack->longitude(),
            'zipcode'          => $ipstack->zip(),
        ];
    }

    private function createNewLoginHistory(User $user, array $data)
    {
        $loginHistoryService = app(LoginHistoryService::class);
        $loginHistoryService->store($user, $data);
    }
}
