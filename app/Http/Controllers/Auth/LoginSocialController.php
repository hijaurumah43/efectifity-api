<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Sujip\Ipstack\Ipstack;
use Tymon\JWTAuth\JWTAuth;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exceptions\LockedException;
use App\Http\Controllers\Controller;
use App\Models\Client\Member\Member;
use App\Services\LoginHistoryService;
use Symfony\Component\HttpFoundation\Response;

class LoginSocialController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    public function verify(Request $request, $provider)
    {
        $token  = $request->token;
        $fcm_token  = $request->fcm_token;

        switch ($provider) {
            case 'google':
                $url    = "https://oauth2.googleapis.com/tokeninfo?id_token=" . $token;
                break;

            case 'facebook':
                $url    = "https://graph.facebook.com/me?fields=id,name,email,picture&access_token=" . $token;
                break;

            default:
                return $this->respondWithCustomData(['message' => 'Providers not allowed'], 422, "failed");
                break;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($data, true);

        if (isset($data['error'])) {
            return $this->respondWithCustomData(['message' => $data['error']], 422, "failed");
        }

        DB::beginTransaction();

        $user = $this->findOrCreate($data, $provider, $fcm_token);

        try {
            $data = $this->getDeviceInfo($request);
            $data['user_id'] = $user->id;
        } catch (LockedException $exception) {
            return $this->respondWithCustomData([
                'message'     => $exception->getMessage(),
            ], Response::HTTP_LOCKED);
        }

        $this->createNewLoginHistory($user, $data);

        $token = (string) $this->auth->fromUser($user);

        DB::commit();

        return $this->respondWithCustomData([
            'token'     => $token,
            'tokenType' => 'Bearer',
        ], Response::HTTP_OK, "success");
    }

    private function findOrCreate($data, $provider, $fcm_token = NULL)
    {
        $data['name'] = isset($data['name']) ? $data['name'] : '';
        $data['picture'] = isset($data['picture']) ? $data['picture'] : '';
        // $data['fcm_token'] = isset($data['fcm_token']) ? $data['fcm_token'] : '';
        if ($provider == 'facebook') {
            $data['picture'] = isset($data['picture']) ? $data['picture']['data']['url'] : '';
        }
        $user = User::where('email', $data['email'])->first();
        if (!$user) {

            $user = new User();
            $user->email = $data['email'];
            $user->password = bcrypt('123456789');
            $user->provider = strtolower($provider);
            $user->email_verified_at = Carbon::now();
            $user->fcm_token = $fcm_token;
            $user->save();

            $user->attachRole(Role::MEMBER);

            $member = new Member();
            $member->user_id = $user->id;
            $member->name = $data['name'];
            $member->avatar = $data['picture'];
            $member->save();

            return $user;
        } else {
            $user->fcm_token = $fcm_token;
            $user->save();
        }

        return $user;
    }

    public function verifyApple(Request $request, $provider = 'apple')
    {
        DB::beginTransaction();
        if (!isset($request->email)) {
            return $this->respondWithCustomData(['message' => 'email is required'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = $this->findOrCreateApple($request, $provider);

        try {
            $data = $this->getDeviceInfo($request);
            $data['user_id'] = $user->id;
        } catch (LockedException $exception) {
            return $this->respondWithCustomData([
                'message'     => $exception->getMessage(),
            ], Response::HTTP_LOCKED);
        }
        $this->createNewLoginHistory($user, $data);
        $token = (string) $this->auth->fromUser($user);

        DB::commit();

        return $this->respondWithCustomData([
            'token'     => $token,
            'tokenType' => 'Bearer',
        ], Response::HTTP_OK, "success");
    }

    private function findOrCreateApple($data, $provider)
    {
        $user = User::where('email', $data->email)->first();
        $fcmToken = isset($data->fcm_token) ? $data->fcm_token : '';
        if (!$user) {

            $user = new User();
            $user->email = $data->email;
            $user->password = bcrypt('123456789');
            $user->provider = strtolower($provider);
            $user->email_verified_at = Carbon::now();
            $user->fcm_token = $fcmToken;
            $user->save();

            $user->attachRole(Role::MEMBER);

            $member = new Member();
            $member->user_id = $user->id;
            $member->name = $data->first_name;
            $member->last_name = $data->last_name;
            $member->save();

            return $user;
        } else {
            $user->fcm_token = $fcmToken;
            $user->save();
        }

        return $user;
    }

    private function getDeviceInfo(Request $request)
    {
        $agent = new Agent();
        $agent->setUserAgent($request->userAgent());
        $agent->setHttpHeaders($request->headers);

        $ipstack = new Ipstack($request->ip(), ENV("IPSTACK_KEY"));

        return [
            'id'               => Uuid::uuid4(),
            'user_id'          => auth()->id(),
            'ip'               => $request->ip(),
            'device'           => $agent->device(),
            'platform'         => $agent->platform(),
            'platform_version' => $agent->version($agent->platform()),
            'browser'          => $agent->browser(),
            'browser_version'  => $agent->version($agent->browser()),
            'city'             => $ipstack->city(),
            'region_code'      => $ipstack->regionCode(),
            'region_name'      => $ipstack->region(),
            'country_code'     => $ipstack->countryCode(),
            'country_name'     => $ipstack->country(),
            'continent_code'   => $ipstack->continentCode(),
            'continent_name'   => $ipstack->continent(),
            'latitude'         => $ipstack->latitude(),
            'longitude'        => $ipstack->longitude(),
            'zipcode'          => $ipstack->zip(),
        ];
    }

    private function createNewLoginHistory(User $user, array $data)
    {
        $loginHistoryService = app(LoginHistoryService::class);
        $loginHistoryService->store($user, $data);
    }
}
