<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Contracts\UserRepository;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use App\Contracts\Client\Member\MemberRepository;
use App\Contracts\Common\Company\CompanyRepository;
use App\Models\Client\Partner\Temporary\CompanyTemp;

class ResetPasswordPartnerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token'    => 'required',
            'email'    => [
                'required',
                'email',
            ],
            'password' => [
                'required',
                'string',
                'confirmed',
                'min:8',
            ],
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        $check = DB::table('password_resets')->where([
            ['email', $request->email],
        ])->first();

        $user = User::where('email', $request->email)->first();

        if (!$user) {

            $companyTemp = CompanyTemp::where('email', $request->email)->first();

            if (!$check || !$companyTemp) {
                return $this->respondWithCustomData(['message' => 'The resource not found'], Response::HTTP_NOT_FOUND, "failed");
            }

            $tokenCheck = Hash::check($request->token, $check->token);
            if ($tokenCheck != true) {
                return $this->respondWithCustomData(['message' => 'Token is invalid'], Response::HTTP_UNPROCESSABLE_ENTITY, "failed");
            }

            DB::beginTransaction();

            $userRepository = app(UserRepository::class);

            $user =  $userRepository->store([
                'email'                       => $companyTemp->email,
                'password'                    => bcrypt($request->password),
                'email_token_confirmation'    => Uuid::uuid4()->toString(),
                'email_token_disable_account' => Uuid::uuid4()->toString(),
                'is_active'                   => 1,
                'email_verified_at'           => date('Y-m-d H:i:s'),
            ]);

            $user->attachRole(Role::PARTNER);

            $memberRepository = app(MemberRepository::class);
            $memberRepository->store([
                'user_id'   => $user->id,
                'name'      => $companyTemp->name,
            ]);

            $companyRepository = app(CompanyRepository::class);
            $companyRepository->store([
                'user_id'   => $user->id,
                'title'     => $companyTemp->title,
                'phone'     => $companyTemp->phone,
                'city_id'   => $companyTemp->city_id,
                'agent'     => $companyTemp->agent,
            ]);

            DB::table('password_resets')->where('email', $request->email)->delete();
            CompanyTemp::where('email', $request->email)->delete();

            DB::commit();
        } else {
            DB::beginTransaction();
            $user->password = bcrypt($request->password);
            $user->save();

            DB::table('password_resets')->where('email', $request->email)->delete();

            DB::commit();
        }

        return $this->respondWithCustomData(['message' => 'Password has been created'], Response::HTTP_OK, "success");
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }
}
