<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Events\Partner\RegisterPartner;
use Illuminate\Support\Facades\Validator;
use App\Events\Partner\ForgotPasswordPartner;
use App\Models\Client\Partner\Temporary\CompanyTemp;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        $message = __('Email sent, please check your email to continue');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, "sucess");
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $response
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        $message = __('We could not complete your request, are your sure that this email is correct?');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_BAD_REQUEST, "failed");
    }

    public function checkToken()
    {
        $token = request()->get('token');
        $email = request()->get('email');

        if($token == '' || $email == '') {
            return $this->respondWithCustomData(['message' => 'Token is required'], Response::HTTP_UNPROCESSABLE_ENTITY, "failed");
        }

        $check = DB::table('password_resets')->where([
            ['email', $email]
        ])->first();

        if(!$check) {
            return $this->respondWithCustomData(['message' => 'Email not found'], Response::HTTP_NOT_FOUND, "failed");
        }

        $tokenCheck = Hash::check($token, $check->token);
        if($tokenCheck == true) {
            if(Carbon::parse($check->created_at)->addHour(1) >= date('Y-m-d H:i:s')){

                return $this->respondWithCustomData(['message' => 'Ok'], Response::HTTP_OK, "success");
            } else {
                DB::table('password_resets')->where('email', $email)->delete();

                return $this->respondWithCustomData(['message' => 'Token not found or expired'], Response::HTTP_NOT_FOUND, "failed");
            }
        } else {
            return $this->respondWithCustomData(['message' => 'Token not found'], Response::HTTP_NOT_FOUND, "failed");
        }
    }

    public function sendLink(Request $request)
    {
        $this->validator($request->all())->validate();

        $companyTemp = CompanyTemp::where('email', $request->email)->first();
        if(!$companyTemp) {
            return $this->respondWithCustomData(['message' => 'The resource not found'], Response::HTTP_NOT_FOUND, "failed");
        }
        
        event(new RegisterPartner($companyTemp));

        if ($response = $this->registered($request, $companyTemp)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect($this->redirectPath());
    }

    public function sendLinkForgotPassword(Request $request)
    {
        $this->validatorForgotPassword($request->all())->validate();

        $user = User::where('email', $request->email)->first();
        if(!$user) {
            return $this->respondWithCustomData(['message' => 'The resource not found'], Response::HTTP_NOT_FOUND, "failed");
        }

        event(new ForgotPasswordPartner($user));
        $message = __('Email sent, please check your email to continue');

        return $this->respondWithCustomData(['message' => $message], Response::HTTP_OK, "sucess");
    }

    protected function validatorForgotPassword(array $data)
    {
        $validator = Validator::make($data, [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
            ],
        ]);

        if ($validator->fails()) {
            $form = new FormRequest();
            $form->failedValidation($validator);
        }

        return $validator;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users,email',
            ],
        ]);

        if ($validator->fails()) {
            $form = new FormRequest();
            $form->failedValidation($validator);
        }

        return $validator;
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param CompanyTemp    $comapanyTemp
     * @return mixed
     */
    protected function registered(Request $request, $companyTemp)
    {
        $message    = __(
            'We sent a confirmation email to :email. Please follow the instructions to complete your registration.',
            ['email' => $companyTemp->email]
        );

        return $this->respondWithCustomData([
            'message'     => $message,
        ], Response::HTTP_CREATED, "success");
    }
}
