<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\LockedException;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\LoginHistoryService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Jenssegers\Agent\Agent;
use Ramsey\Uuid\Uuid;
use Sujip\Ipstack\Ipstack;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    use ValidatesRequests;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * {@inheritdoc}
     */
    protected function attemptLogin(Request $request)
    {
        $token = $this->guard()->attempt($this->credentials($request));

        if ($token) {
            $this->guard()->setToken($token);
            return true;
        }

        return false;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $user = auth()->user();
        if($request->url() == route('internal.auth.login')) {
            if(!$user->hasRole('administrator')) {
                return $this->respondWithCustomData(['message' => 'Access Denied'], Response::HTTP_FORBIDDEN, 'failed');
            }
        }

        try {
            $data = $this->getDeviceInfo($request);
            $data['user_id'] = $user->id;
        } catch (LockedException $exception) {
            return $this->respondWithCustomData([
                'message'     => $exception->getMessage(),
            ], Response::HTTP_LOCKED);
        }

        $this->createNewLoginHistory($user, $data);

        $updateTokenFB = User::where('id', $user->id)->first();
        if($updateTokenFB) {
            $updateTokenFB->fcm_token = $request->fcm_token;
            $updateTokenFB->save();
        }

        $token = (string)$this->guard()->getToken();

        return $this->respondWithCustomData([
            'token'     => $token,
            'tokenType' => 'Bearer',
        ], Response::HTTP_OK, "success");
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        $id = $this->guard()->id();

        $this->guard()->logout(true);

        return $this->respondWithCustomData(['message' => 'Accounts has been logouted'], Response::HTTP_OK, 'success');
    }

    private function getDeviceInfo(Request $request)
    {
        $agent = new Agent();
        $agent->setUserAgent($request->userAgent());
        $agent->setHttpHeaders($request->headers);

        $ipstack = new Ipstack($request->ip(), ENV("IPSTACK_KEY"));

        return [
            'id'               => Uuid::uuid4(),
            'user_id'          => auth()->id(),
            'ip'               => $request->ip(),
            'device'           => $agent->device(),
            'platform'         => $agent->platform(),
            'platform_version' => $agent->version($agent->platform()),
            'browser'          => $agent->browser(),
            'browser_version'  => $agent->version($agent->browser()),
            'city'             => $ipstack->city(),
            'region_code'      => $ipstack->regionCode(),
            'region_name'      => $ipstack->region(),
            'country_code'     => $ipstack->countryCode(),
            'country_name'     => $ipstack->country(),
            'continent_code'   => $ipstack->continentCode(),
            'continent_name'   => $ipstack->continent(),
            'latitude'         => $ipstack->latitude(),
            'longitude'        => $ipstack->longitude(),
            'zipcode'          => $ipstack->zip(),
        ];
    }

    private function createNewLoginHistory(User $user, array $data)
    {
        $loginHistoryService = app(LoginHistoryService::class);
        $loginHistoryService->store($user, $data);
    }
}
