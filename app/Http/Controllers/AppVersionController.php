<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAppVersionRequest;
use App\Models\AppVersion;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AppVersionController extends Controller
{
    /**
     * Display a specified resource
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $resource = AppVersion::orderBy('created_at', 'DESC')->firstOrFail();

        return $this->respondWithCustomData([
            'version' => $resource->version,
            'forceUpdate' => $resource->force_update == 1 ? true : false,
            'androidUrl' => $resource->android_url,
            'iosUrl' => $resource->ios_url,
        ], Response::HTTP_OK);
    }

    public function update(CreateAppVersionRequest $request)
    {
        $data = $request->only(array_keys($request->rules()));
        $resource = AppVersion::orderBy('created_at', 'DESC')->firstOrFail();
        $resource->version = $data->version;
        $resource->force_update = $data->force_update;
        $resource->android_url = $data->android_url;
        $resource->ios_url = $data->ios_url;
        $resource->save();

        return $this->respondWithCustomData([
            'version' => $resource->version,
            'forceUpdate' => $resource->force_update == 1 ? true : false,
            'androidUrl' => $resource->android_url,
            'iosUrl' => $resource->ios_url,
        ], Response::HTTP_OK);
    }

    public function time($date_time)
    {
        return $now = date('Y-m-d H:i:s') . '##' . ConvertToTimeServer($date_time);
    }
}
