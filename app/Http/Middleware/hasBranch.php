<?php

namespace App\Http\Middleware;

use App\Http\ResponseTrait;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Common\Branch\Branch;
use Closure;
use Illuminate\Http\Request;

class hasBranch
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = auth()->user();
        $branchId = $request->route()->parameter('branch_id');

        if ($branchId) {
            $branch = Branch::find($branchId);
            if (!$branch) {
                return abort(403);
            }

            $memberBranch = MemberBranch::where([
                'branch_id' => $branchId,
                'member_id' => $user->member->id,
                'status' => 2,
            ])->first();

            if (!$memberBranch) {
                return abort(403);
            }
        }
        return $next($request);
    }
}
