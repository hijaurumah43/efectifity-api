<?php

namespace App\Http;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

trait ResponseTrait
{
    /**
     * The current class of resource to respond
     *
     * @var string
     */
    protected $resourceItem;

    /**
     * The current class of collection resource to respond
     *
     * @var string
     */
    protected $resourceCollection;

    protected function respondWithCustomData($data, $code = 200, $status = null): JsonResponse
    {
        return new JsonResponse([
            'data' => $data,
            'meta' => [
                // 'code' => $code,
                // 'status' => $status,
                'timestamp' => $this->getTimestampInMilliseconds(),
            ],
        ], $code);
    }

    protected function getTimestampInMilliseconds(): int
    {
        return intdiv((int)now()->format('Uu'), 1000);
    }

    /**
     * Return no content for delete requests
     *
     * @return JsonResponse
     */
    protected function respondWithNoContent(): JsonResponse
    {
        return new JsonResponse([
            'data' => null,
            'meta' => ['timestamp' => $this->getTimestampInMilliseconds()],
        ], Response::HTTP_NO_CONTENT);
    }

    /**
     * Return collection response from the application
     *
     * @param LengthAwarePaginator $collection
     * @return mixed
     */
    protected function respondWithCollection(LengthAwarePaginator $collection, $addtional = null)
    {
        $resource = $collection->items();
        if (is_array($resource)) {
            $resource = new Collection($resource);
        }
        return (new $this->resourceCollection($resource, $addtional))->additional(
            [
                'pagination' => [
                    'total' => $collection->total(),
                    'perPage' => $collection->perPage(),
                    'currentPage' => $collection->currentPage(),
                    'nextPageUrl' => $collection->nextPageUrl(),
                    'previousPageUrl' => $collection->previousPageUrl(),
                    'lastPage' => $collection->lastPage(),
                    'from' => $collection->firstItem(),
                    'to' => $collection->lastItem(),
                ],
                'meta' => [
                    // 'code' => $code,
                    // 'status' => $status,
                    'timestamp' => $this->getTimestampInMilliseconds()
                ]
            ]
        );
    }

    /**
     * Return single item response from the application
     *
     * @param Model $item
     * @return mixed
     */
    protected function respondWithItem(Model $item, $addtional = NULL)
    {
        return (new $this->resourceItem($item, $addtional))->additional(
            ['meta' => [
                // 'code' => $code,
                // 'status' => $status,
                'timestamp' => $this->getTimestampInMilliseconds(),
            ]]
        );
    }
}
