<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UserPinCreateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'pin' => [
                'required',
                'numeric',
                'digits_between:6,6',
                'confirmed'
            ]
        ];
    }
}
