<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UserPinUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'pin' => [
                'required',
                'numeric',
                'digits_between:6,6',
                'confirmed'
            ],
            'current_pin' => [
                'required',
                'numeric',
                'digits_between:6,6',
            ]
        ];
    }
}