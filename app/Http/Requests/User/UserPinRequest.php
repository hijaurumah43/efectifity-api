<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UserPinRequest extends FormRequest
{
    public function rules()
    {
        return [
            'pin_token_confirmation' => [
                'required',
                'numeric',
                'digits_between:6,6',
            ]
        ];
    }
}
