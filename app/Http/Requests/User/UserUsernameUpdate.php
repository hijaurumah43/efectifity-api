<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class UserUsernameUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => [
                'string',
                'max:20',
                // 'unique:users,username,'.auth()->user()->id
            ],
        ];
    }
}
