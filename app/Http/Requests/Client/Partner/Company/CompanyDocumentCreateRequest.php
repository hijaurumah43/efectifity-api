<?php

namespace App\Http\Requests\Client\Partner\Company;

use App\Http\Requests\FormRequest;

class CompanyDocumentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'company_name' => [
                'required',
                'string',
                'min:3',
            ],
            'company_format' => [
                'required',
                'numeric',
                'min:1',
            ],
            'owner_name' => [
                'required',
                'string',
                'min:3',
            ],
            'email' => [
                'required',
                'email',
            ],
            'phone_number' => [
                'required',
                'numeric',
                'digits_between:10,15'
            ],
            'identity_card' => [
                // 'required',
                'file',
                // 'max:2048',
                'mimes:jpg,png,jpeg,pdf'
            ],
            'tax_number' => [
                'file',
                // 'max:2048',
                'mimes:jpg,png,jpeg,pdf'
            ],
            'company_registration' => [
                'file',
                // 'max:2048',
                'mimes:jpg,png,jpeg,pdf'
            ],
            'website_url' => [
                'string'
            ],
            'instagram_url' => [
                'string'
            ],
            'facebook_url' => [
                'string'
            ],
            'youtube_url' => [
                'string'
            ],
            'tiktok_url' => [
                'string'
            ],
            'is_published' => [
                'required',
            ],
        ];
    }
}
