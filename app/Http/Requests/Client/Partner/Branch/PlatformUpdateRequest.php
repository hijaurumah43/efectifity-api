<?php

namespace App\Http\Requests\Client\Partner\Branch;

use App\Http\Requests\FormRequest;

class PlatformUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'title' => [
                'required',
                'string',
            ],
            'phone_number' => [
                'required',
                'string'
            ],
            'address' => [
                'required',
                'string'
            ],
            'front_side' => [
                'file',
                'max:2048',
                'mimes:jpg,png,jpeg,pdf'
            ],
            'inside' => [
                'file',
                'max:2048',
                'mimes:jpg,png,jpeg,pdf'
            ],
            'website' => [
                'string'
            ],
            'whatsapp' => [
                'string'
            ],
            'instagram' => [
                'string'
            ],
            'facebook' => [
                'string'
            ],
            'youtube' => [
                'string'
            ],
            'tiktok' => [
                'string'
            ],
            'latitude' => [
                'required',
                'numeric',
            ],
            'longitude' => [
                'required',
                'numeric',
            ],
            'description' => [
                'string'
            ],
            'postal_code' => [
                'numeric'
            ],
            'region' => [
                'required',
                'string',
            ],
        ];
    }
}
