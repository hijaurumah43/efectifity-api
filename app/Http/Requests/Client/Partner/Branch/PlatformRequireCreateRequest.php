<?php

namespace App\Http\Requests\Client\Partner\Branch;

use App\Http\Requests\FormRequest;

class PlatformRequireCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'requirements' => [
                'required',
                'array'
            ],
            'branch_id' => [
                'required',
                'numeric',
                'min:1'
            ]
        ];
    }
}
