<?php

namespace App\Http\Requests\Client\Staff\Attendance;

use App\Http\Requests\FormRequest;

class StaffAttendanceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude' => [
                'required',
                'numeric',
            ],
            'longitude' => [
                'required',
                'numeric',
            ],
            'member_branch_id' => [
                'required',
                'numeric',
                'min:1'
            ],
        ];
    }
}
