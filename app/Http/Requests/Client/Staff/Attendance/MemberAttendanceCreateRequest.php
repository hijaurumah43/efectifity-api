<?php

namespace App\Http\Requests\Client\Staff\Attendance;

use App\Http\Requests\FormRequest;

class MemberAttendanceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'member_id' => [
                'required',
                'string',
            ],
            'branch_id' => [
                'required',
                'numeric',
                'min:1'
            ],
        ];
    }
}
