<?php

namespace App\Http\Requests\Client\Staff\Settings;

use App\Http\Requests\FormRequest;

class PartnerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'numeric',
                // TODO make unique phone number
                // Rule::unique('users')->ignore(request()->route('api.me.update')),
            ],
            'name' => [
                'required',
                'string',
                'min:3',
                'max:20',
            ],
            'last_name' => [
                'required',
                'string',
                'min:3',
                'max:20',
            ],
            'address' => [
                'required',
                'string',
            ],
            'city_id' => [
                'required',
                'numeric',
                'min:1'
            ],
            'postal_code' => [
                'required',
                'numeric',
            ],
            'title' => [
                'required',
                'string'
            ]
        ];
    }
}
