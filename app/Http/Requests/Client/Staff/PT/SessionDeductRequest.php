<?php

namespace App\Http\Requests\Client\Staff\PT;

use App\Http\Requests\FormRequest;

class SessionDeductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /* 'timestamp' => [
                'string'
            ] */
            /* 'pin' => [
                'required',
                'numeric',
                'digits_between:6,6',
            ] */
        ];
    }
}
