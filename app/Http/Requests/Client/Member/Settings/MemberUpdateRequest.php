<?php

namespace App\Http\Requests\Client\Member\Settings;

use Illuminate\Validation\Rule;
use App\Http\Requests\FormRequest;

class MemberUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone' => [
                'numeric',
                'unique:users,phone,'.auth()->user()->id
            ],
            'name' => [
                'required',
                'string',
                'min:3',
                'max:20',
            ],
            'last_name' => [
                'required',
                'string',
                'min:3',
                'max:20',
            ]
        ];
    }
}