<?php

namespace App\Http\Requests\Client\Member\Settings;

use App\Http\Requests\FormRequest;

class MemberAvatarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'avatar' => [
                'required',
                'image',
                'file',
                'max:4048',
                'mimes:jpg,png'
            ]
        ];
    }
}
