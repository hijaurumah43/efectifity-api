<?php

namespace App\Http\Requests\Client\Member\Settings;

use App\Http\Requests\FormRequest;

class DeliveryAddressUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => [
                'required',
                'string',
                'min:3'
            ],
            'receiver' => [
                'required',
                'string',
            ],
            'phone_number' => [
                'required',
                'numeric',
                'digits_between:10,15'
            ],
            'address' => [
                'required',
                'string',
            ],
            'zip_code' => [
                'numeric'
            ],
            'latitude' => [
                'numeric'
            ],
            'longitude' => [
                'numeric'
            ],
        ];
    }
}
