<?php

namespace App\Http\Requests\Client\Member\Settings;

use App\Http\Requests\FormRequest;

class EmergencyContactUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'label' => [
                'required',
                'string',
            ],
            'name' => [
                'required',
                'string',
            ],
            'phone_number' => [
                'required',
                'numeric',
                // 'digits_between:10,15'
            ],
            'relation' => [
                'required',
                'string',
            ],
        ];
    }
}
