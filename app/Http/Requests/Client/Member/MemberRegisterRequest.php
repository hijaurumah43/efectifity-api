<?php

namespace App\Http\Requests\Client\Member;

use App\Http\Requests\FormRequest;

class MemberRegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name'     => [
                'required',
                'string',
                'max:255',
            ],
            'email'    => [
                'required',
                'string',
                'email',
                'max:255',
                'unique:users,email',
            ],
            'password' => [
                'required',
                'string',
                'min:8',
            ]
        ];
    }
}
