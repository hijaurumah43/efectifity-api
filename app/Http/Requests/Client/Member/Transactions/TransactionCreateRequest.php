<?php

namespace App\Http\Requests\Client\Member\Transactions;

use Illuminate\Foundation\Http\FormRequest;

class TransactionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_type' => [
                'required',
                'numeric',
            ],
            'package' => [
                'required',
                'numeric',
            ],
            'voucher' => [
                'numeric',
            ],
            'phone_number' => [
                'numeric',
            ],
            'branch_schedule_detail_id' => [
                'nullable',
                'numeric'
            ],
            'pt_id' => [
                'nullable',
                'numeric',
                'min:1'
            ]
        ];
    }
}
