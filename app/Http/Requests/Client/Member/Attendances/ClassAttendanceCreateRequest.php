<?php

namespace App\Http\Requests\Client\Member\Attendances;

use App\Http\Requests\FormRequest;
class ClassAttendanceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'program' => [
                'required',
                'string',
            ],
            'branch_schedule_detail_id' => [
                'required',
                'numeric',
                'min:1',
            ]
        ];
    }
}
