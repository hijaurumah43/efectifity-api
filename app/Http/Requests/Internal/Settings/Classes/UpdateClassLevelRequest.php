<?php

namespace App\Http\Requests\Internal\Settings\Classes;

use App\Http\Requests\FormRequest;

class UpdateClassLevelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
                'max:50',
            ],
            'description' => [
                'string',
                'nullable'
            ],
            'is_active' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
        ];
    }
}
