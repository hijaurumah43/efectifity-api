<?php

namespace App\Http\Requests\Internal\Settings\Club;

use App\Http\Requests\FormRequest;

class CreateClubSectionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'string',
                'required',
                'max:50',
            ],
            'status' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
        ];
    }
}
