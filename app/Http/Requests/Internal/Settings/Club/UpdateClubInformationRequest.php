<?php

namespace App\Http\Requests\Internal\Settings\Club;

use App\Http\Requests\FormRequest;

class UpdateClubInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:50',
            ],
            'description' => [
                'required',
                'string',
                'max:100',
            ],
            /* 'image' => [
                'image',
                'max:512',
                'mimes:svg'
            ], */
            'is_published' => [
                'required',
            ],
        ];
    }
}
