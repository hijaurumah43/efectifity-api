<?php

namespace App\Http\Requests\Internal\Settings\Club;

use App\Http\Requests\FormRequest;

class UpdateClubAmenityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:50',
            ],
            /* 'thumbnail' => [
                'image',
                'max:512',
                'mimes:svg'
            ], */
            'is_published' => [
                'required',
            ],
        ];
    }
}
