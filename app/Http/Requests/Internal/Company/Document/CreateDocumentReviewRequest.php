<?php

namespace App\Http\Requests\Internal\Company\Document;

use App\Http\Requests\FormRequest;

class CreateDocumentReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_document_id' => [
                'required',
                'string',
            ],
            'document_status_id' => [
                'required',
                'numeric',
                'min:3',
                'max:6'
            ],
            'note' => [
                'string',
            ],
        ];
    }
}
