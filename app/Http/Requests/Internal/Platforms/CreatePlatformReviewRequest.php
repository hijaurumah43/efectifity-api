<?php

namespace App\Http\Requests\Internal\Platforms;

use App\Http\Requests\FormRequest;

class CreatePlatformReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'branch_status_id' => [
                'required',
                'numeric',
                'min:3',
                'max:6'
            ],
            'note' => [
                'string',
            ],
        ];
    }
}
