<?php

namespace App\Http\Requests\Internal\Platforms;

use App\Http\Requests\FormRequest;

class UpdateBranchFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'fee' => [
                'required',
                'numeric',
                'min:0',
            ],
            'start_date' => [
                'required',
                'string',
            ],
            'end_date' => [
                'required',
                'string',
            ],
            'is_active' => [
                'required',
                'boolean',
            ],
        ];
    }
}
