<?php

namespace App\Http\Requests;

use App\Http\Requests\FormRequest;

class CreateAppVersionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'version' => [
                'required',
                'string',
            ],
            'force_update' => [
                'required',
                'numeric',
                'max:1',
                'min:0',
            ],
            'android_url' => [
                'string',
            ],
            'ios_url' => [
                'string',
            ],
        ];
    }
}
