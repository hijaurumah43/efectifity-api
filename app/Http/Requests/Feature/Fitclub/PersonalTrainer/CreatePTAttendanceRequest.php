<?php

namespace App\Http\Requests\Feature\Fitclub\PersonalTrainer;

use App\Http\Requests\FormRequest;

class CreatePTAttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => [
                'required',
                'string',
            ],
            'start_time' => [
                'required',
                'string',
            ],
            'end_time' => [
                'required',
                'string',
            ],
            'start_timestamp' => [
                'required',
                'string',
            ],
            'end_timestamp' => [
                'required',
                'string',
            ],
        ];
    }
}
