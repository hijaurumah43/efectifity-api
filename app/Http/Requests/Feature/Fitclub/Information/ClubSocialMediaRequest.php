<?php

namespace App\Http\Requests\Feature\Fitclub\Information;

use App\Http\Requests\FormRequest;

class ClubSocialMediaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'numeric',
                'nullable',
            ],
            'website' => [
                'string',
                'nullable',
                'max:255'
            ],
            'whatsapp' => [
                'string',
                'nullable',
                'max:255'
            ],
            'instagram' => [
                'string',
                'nullable',
                'max:255'
            ],
            'facebook' => [
                'string',
                'nullable',
                'max:255'
            ],
            'youtube' => [
                'string',
                'nullable',
                'max:255'
            ],
            'tiktok' => [
                'string',
                'nullable',
                'max:255'
            ],
        ];
    }
}
