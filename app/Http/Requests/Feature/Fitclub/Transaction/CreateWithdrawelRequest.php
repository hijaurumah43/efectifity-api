<?php

namespace App\Http\Requests\Feature\Fitclub\Transaction;

use App\Http\Requests\FormRequest;

class CreateWithdrawelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:10000',
                'max:10000000',
            ],
            'bank_id' => [
                'required',
                'numeric',
                'min:1',
            ]
        ];
    }
}
