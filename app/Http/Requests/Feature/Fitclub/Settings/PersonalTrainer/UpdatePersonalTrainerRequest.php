<?php

namespace App\Http\Requests\Feature\Fitclub\Settings\PersonalTrainer;

use App\Http\Requests\FormRequest;

class UpdatePersonalTrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'quota' => [
                'required',
                'numeric',
                'min:0',
            ],
            'duration' => [
                'numeric',
                'min:0',
            ],
            'qualifications' => [
                'string',
                'nullable'
            ],
            'specialities' => [
                'string',
                'nullable'
            ],
            'image' => [
                'image',
                'file',
                'max:4048',
                'mimes:jpg,png',
                'nullable',
            ],
            'status' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
        ];
    }
}
