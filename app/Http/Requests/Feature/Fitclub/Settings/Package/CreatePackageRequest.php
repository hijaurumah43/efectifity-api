<?php

namespace App\Http\Requests\Feature\Fitclub\Settings\Package;

use App\Http\Requests\FormRequest;

class CreatePackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required'
            ],
            'gym_access' => [
                'numeric',
                'min:0',
                'max:1'
            ],
            'duration' => [
                'required',
                'numeric',
                'min:0',
            ],
            'periode' => [
                'required',
                'string'
            ],
            'session' => [
                'required',
                'numeric',
                'min:0',
            ],
            'pt_session' => [
                'required',
                'numeric',
                'min:0',
            ],
            'description' => [
                'string',
            ],
            'about' => [
                'string',
            ],
            'benefit' => [
                'string',
            ],
            'policy' => [
                'string',
            ],
            'price' => [
                'required',
                'numeric',
                'min:10000',
            ],
            'is_published' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
            'is_unlimited' => [
                'required',
                /* 'numeric',
                'min:0',
                'max:1', */
            ]
        ];
    }
}
