<?php

namespace App\Http\Requests\Feature\Fitclub\Settings\Shift;

use App\Http\Requests\FormRequest;

class CreateShiftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'max:20',
                'string',
            ],
            'start_time' => [
                'required',
                'string',
            ],
            'end_time' => [
                'required',
                'string',
            ],
            'status' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
        ];
    }
}
