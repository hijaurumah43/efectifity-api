<?php

namespace App\Http\Requests\Feature\Fitclub\Settings;

use App\Http\Requests\FormRequest;

class UpdateClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:50'
            ],
            'image' => [
                'image',
                'file',
                'max:4048',
                'mimes:jpg,png'
            ],
            'branch_class_category_id' => [
                'required',
                'numeric',
                'min:1'
            ],
            'branch_class_level_id' => [
                'required',
                'numeric',
                'min:1'
            ],
            'is_online' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
            'description' => [
                'string',
                'nullable',
            ],
            'is_published' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
        ];
    }
}
