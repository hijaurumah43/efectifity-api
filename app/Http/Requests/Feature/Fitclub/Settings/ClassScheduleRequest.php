<?php

namespace App\Http\Requests\Feature\Fitclub\Settings;

use App\Http\Requests\FormRequest;

class ClassScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => [
                'required',
                'string',
            ],
            'branch_class_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'start_time' => [
                'required',
                'string',
            ],
            'end_time' => [
                'required',
                'string',
            ],
            'start_timestamp' => [
                'required',
                'string',
            ],
            'end_timestamp' => [
                'required',
                'string',
            ],
            /* 'quota' => [
                
            ], */
            'max_quota' => [
                'numeric',
                'min:0'
            ],
            'is_quota' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
            'is_published' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
            'coaches' => [
                'array',
            ],
            'url_class' => [
                // 'string',
            ],
            'password_class' => [
                // 'string',
            ]
        ];
    }
}
