<?php

namespace App\Http\Requests\Feature\Fitclub\Classes;

use App\Http\Requests\FormRequest;

class ClassCancelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_attendance_id' => [
                // 'required',
                'array'
            ],
            'note' => [
                // 'required',
                'string',
                'min:5'
            ],
            'manager' => [
                'required',
                'boolean'
            ]
        ];
    }
}
