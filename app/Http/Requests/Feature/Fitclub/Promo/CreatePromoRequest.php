<?php

namespace App\Http\Requests\Feature\Fitclub\Promo;

use App\Http\Requests\FormRequest;

class CreatePromoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                'numeric',
                'min:1',
            ],
            'branch_package_id' => [
                'required',
                'numeric',
                'min:1',
            ],
            'member_type' => [
                'required',
                'numeric',
                'min:1',
                'max:3',
            ],
            'title' => [
                'required',
                'string',
                'max:50',
            ],
            'duration' => [
                'required',
                'numeric',
            ],
            'periode' => [
                'required',
                'string',
            ],
            'gym_access' => [
                'required',
                'numeric',
                'min:0',
                'max:1',
            ],
            'is_unlimited' => [
                'numeric',
                'min:0',
                'max:1'
            ],
            'session' => [
                'numeric'
            ],
            'pt_session' => [
                'required',
                'numeric',
            ],
            'price' => [
                'required',
                'numeric',
            ],
            'start_time' => [
                'required',
                'string'
            ],
            'end_time' => [
                'required',
                'string'
            ],
            'status' => [
                'required',
                'numeric',
                'min:0',
                'max:2',
            ],
        ];
    }
}
