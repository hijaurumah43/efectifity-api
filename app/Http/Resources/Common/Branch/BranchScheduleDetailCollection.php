<?php

namespace App\Http\Resources\Common\Branch;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BranchScheduleDetailCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => BranchScheduleDetailResource::collection($this->collection)
        ];
    }
}
