<?php

namespace App\Http\Resources\Common\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchOperationalTimeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->start_time . ' - ' . $this->end_time,
            'startTime' => $this->start_time,
            'endTime' => $this->end_time,
            'day'   => Carbon::parse($this->date)->format('l'),
            'date'  => Carbon::parse($this->date)->format('d F Y'),
            'createdAt'  => Carbon::parse($this->date)->format('Y-m-d'),
            'note'  => $this->status == 1 ? 'Open' : 'Close',
        ];
    }
}
