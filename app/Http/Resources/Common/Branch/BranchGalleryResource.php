<?php

namespace App\Http\Resources\Common\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchGalleryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'url' => $this->name,
            'type' => $this->type,
            'sectionName' => isset($this->section) ? ucwords($this->section->title) : NULL,
            'sectionID' => isset($this->section) ? $this->section->id : NULL
        ];
    }
}
