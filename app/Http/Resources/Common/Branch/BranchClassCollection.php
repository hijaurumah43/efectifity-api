<?php

namespace App\Http\Resources\Common\Branch;

use App\Http\Resources\Common\Branch\BranchResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BranchClassCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => BranchResource::collection($this->collection)
        ];
    }
}
