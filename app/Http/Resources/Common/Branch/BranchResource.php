<?php

namespace App\Http\Resources\Common\Branch;

use App\Models\Common\Service\ServiceCategory;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Company\CompanyResource;
use App\Http\Resources\Common\Service\ServiceResource;
use App\Models\Common\Branch\BranchOperationalTime;
use Carbon\Carbon;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status     = 'Close';
        $category   = [];
        $today      = [];
        $coaches    = [];
        $distance   = 0;

        // Branch Status
        $checkstatus = $this->operationalTimes->where('date', date('Y-m-d'))->first();
        if ($checkstatus) {
            $status = 'Open';
        }

        // Branch Category
        if ($this->branchCategories) {
            foreach ($this->branchCategories as $branchCategory) {
                $serviceCategory = ServiceCategory::where('id', $branchCategory->service_category_id)->where('is_published', 1)->first();
                if ($serviceCategory) {
                    $category[] = $serviceCategory->title;
                }
            }
        }

        // Today Schedule
        $schedules = $this->schdules->where('date', date('Y-m-d'))->first();
        if ($schedules) {
            foreach ($schedules->detail as $todays) {

                // Today Coaches
                if ($todays->class->branchCoach) {
                    foreach ($todays->class->branchCoach as $data) {
                        if ($data->member) {
                            $coaches[] = $data->member->name;
                        }
                    }
                }

                // just offline
                if ($todays->class->is_online == 0) {

                    $today[] = [
                        'startTime'  => $todays->start_time,
                        'endTime' => $todays->end_time,
                        'duration' => (strtotime($todays->end_time) - strtotime($todays->start_time)) / 60,
                        'class' => [
                            'title' => $todays->class->title,
                            'category' => ucwords($todays->class->category->title),
                            'image' => ENV('CDN') . '/' . config('cdn.classes') . $todays->class->image,
                            'isOnline' => $todays->class->is_online == 1 ? true : false,
                        ],
                        'coaches'     => $coaches,
                    ];
                }

                $coaches = [];
            }
        }

        if ($this->distance) {
            $distance = explode('.', $this->distance);
            $distance = $distance[0] . '.' . substr($distance[1], 0, 1);
        }

        $branchAmenity = [];
        if ($this->branchAmenities) {
            foreach ($this->branchAmenities as $item) {
                $branchAmenity[] = $item->amenity->title;
            }
        }

        $additionalInformation = [];
        if ($this->barnchAdditionalInformation) {
            foreach ($this->barnchAdditionalInformation as $item) {
                $additionalInformation[] = [
                    'title' => $item->additionalInformation->title,
                    'image' => $item->additionalInformation->image != NULL ? ENV('CDN') . '/' . config('cdn.clubSettingInformation') . $item->additionalInformation->image : null,
                    'description' => $item->additionalInformation->description,
                ];
            }
        }


        $fixPrice = 0;

        if (isset($this->budget)) {
            if ($this->budget != null) {
                $fixPrice = $this->budget;
            } else {
                if (isset($this->budget)) {
                    if ($this->price != 0) {
                        $fixPrice = $this->this->price;
                    } else {
                        $fixPrice = $this->packages->min('price');
                    }
                } else {
                    $fixPrice = $this->packages->min('price');
                }
            }
        } else {
            $fixPrice = $this->packages->min('price');
        }

        $createdAtBH = date('Y-m-d');
        $dateBH = Carbon::parse(date('Y-m-d'))->format('d F Y');
        $noteBH = 'Close';
        $startTimeBH = '';
        $endTimeBH = '';
        $bussinesHour = BranchOperationalTime::where([
            ['date', date('Y-m-d')],
            ['branch_id', $this->id]
        ])->first();
        if ($bussinesHour) {
            $noteBH = $bussinesHour->status == 1 ? 'Open' : 'Close';
            $startTimeBH = $bussinesHour->start_time;
            $endTimeBH = $bussinesHour->end_time;
        }

        return [
            'id' => $this->id,
            'company' => new CompanyResource($this->company),
            'service' => new ServiceResource($this->service),
            'title' => $this->title,
            'slug' => $this->slug,
            'thumbnail' => $this->logo != null ? ENV('CDN') . '/' . config('cdn.branchThumbnail') . $this->thumbnail : null,
            'logo' => $this->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $this->logo : null,
            'startingFrom' => 'IDR ' . number_format($fixPrice),
            'promo' => $this->promo,
            'address' => $this->region,
            'date' => date('Y-m-d'),
            'status' => $status,
            'categories' => $category,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'distance' => (float) $distance . 'km',
            'required' => [
                'healthQuestionnaire' => $this->required->where('question_category_id', 1)->where('status', 1)->count() > 0 ? true : false,
                'bodyMassIndex' => $this->required->where('question_category_id', 2)->where('status', 1)->count() > 0 ? true : false,
                'foodAllergies' => $this->required->where('question_category_id', 3)->where('status', 1)->count() > 0 ? true : false,
            ],
            'description' => $this->description,
            "bussinessHour" => [
                "note" => $noteBH,
                "date" => $dateBH,
                "createdAt" => $createdAtBH,
                "startTime" => $startTimeBH,
                "endTime" => $endTimeBH,
            ],
            'attributes' => [
                'contacts' => [
                    'website' => $this->website,
                    'phone' => $this->phone_number,
                    'whatsapp' => $this->whatsapp,
                    'tiktok' => $this->tiktok,
                    'youtube' => $this->youtube,
                    'facebook' => $this->facebook,
                    'instagram' => $this->instagram,
                ],
                'galleries' => $this->branchGalleries->map(function ($m) {
                    return [
                        'id' => $m->id,
                        'title' => $m->title,
                        'url' => $m->name,
                    ];
                }),
                'amenities' => $branchAmenity,
                'additionalInformation' => $additionalInformation,
                'schdules' => $today,
            ],
        ];
    }
}
