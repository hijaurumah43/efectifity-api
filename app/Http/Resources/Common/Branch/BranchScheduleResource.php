<?php

namespace App\Http\Resources\Common\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Attendances\ClassAttendance;

class BranchScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $today      = [];
        $quota      = 0;
        $coaches    = [];

        if ($this->detail) {
            foreach ($this->detail as $todays) {
                $isCancelled = false;

                // Today Coaches
                if ($todays->class->branchCoach) {
                    foreach ($todays->class->branchCoach as $data) {
                        if ($data->member) {
                            $coaches[] = $data->member->name;
                        }
                    }
                }

                // Quota
                $quota = ClassAttendance::where([
                    ['class_attendance_status_id', 1],
                    ['branch_schedule_detail_id', $todays->id]
                ])->count();

                if ($todays->is_cancelled == 1) {
                    $isCancelled = true;
                }

                // just offline
                if ($todays->class->is_online == 0) {
                    $today[] = [
                        'id' => $todays->id,
                        'startTime' => $todays->start_time,
                        'endTime' => $todays->end_time,
                        'duration' => (strtotime($todays->end_time) - strtotime($todays->start_time)) / 60,
                        'quota' => $quota . '/' . $todays->max_quota,
                        'isCancelled' => $isCancelled,
                        'class' => [
                            'id' => $todays->branch_class_id,
                            'title' => $todays->class->title,
                            'category' => ucwords($todays->class->category->title),
                            'image' => ENV('CDN') . '/' . config('cdn.classes') . $todays->class->image,
                            'isOnline' => $todays->class->is_online == 1 ? true : false,
                            'maxBookTime' => $todays->class->max_book_time,
                            'maxCancelTime' => $todays->class->max_cancel_time,
                            'maxDelayTime' => $todays->class->delay_time,
                            'level' => [
                                'label' => $todays->class->level->title,
                                'description' => $todays->class->description,
                            ]
                        ],
                        'coaches' => $coaches,
                    ];
                    $coaches = [];
                }
            }
        }

        $finalCollection = collect($today)->sortBy('startTime');
        $finalCollection = $finalCollection->values()->all();

        return [
            'id' => $this->id,
            'date' => Carbon::parse($this->date)->format('d F Y'),
            'schedules' => $finalCollection,
        ];
    }
}
