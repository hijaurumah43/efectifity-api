<?php

namespace App\Http\Resources\Common\Branch;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Models\Client\Partner\Branch\BranchRefundPolicy;

class BranchPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $text = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.";
        $textPolicy = NULL;
        $refundPolicy = BranchRefundPolicy::where([
            ['branch_id', $this->branch_id],
            ['is_published', 1],
            ['is_draft', 0],
        ])
            ->orderBy('id', 'DESC')
            ->first();

        if ($refundPolicy) {
            $textPolicy = $refundPolicy->content;
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'duration' => $this->duration,
            'periode' => $this->periode,
            'session' => $this->session,
            'isUnlimited' => $this->is_unlimited == 1 ? true : false,
            'gymAccess' => $this->gym_access == 1 ? true : false,
            'ptSession' => $this->pt_session != NULL ? $this->pt_session : 0,
            'isPromo' => $this->is_promo == 1 ? true : false,
            'price' => 'IDR ' . $this->price,
            'description' => $this->description,
            'about' => $this->about,
            'benefit' => $this->benefit,
            'policy' => $this->policy,
            'refundPolicy' => $textPolicy,
            'membershipUpgradePolicy' => $text,
            'branch' => $this->when(
                $request->url() == route('api.branch-packages.show', [$this->id]),
                new BranchResource($this->branch)
            )
        ];
    }
}
