<?php

namespace App\Http\Resources\Common\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Attendances\ClassAttendance;

class BranchScheduleDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $today      = [];
        $quota      = 0;
        $coaches    = [];


        // Today Coaches
        if ($this->class->branchCoach) {
            foreach ($this->class->branchCoach as $data) {
                if ($data->member) {
                    $coaches[] = $data->member->name;
                }
            }
        }

        // Quota
        $quota = ClassAttendance::where([
            ['class_attendance_status_id', 1],
            ['branch_schedule_detail_id', $this->id]
        ])->count();

        // just offline
        if ($this->class->is_online == 0) {
            $today[] = [
                'startTime' => $this->start_time,
                'endTime' => $this->end_time,
                'duration' => (strtotime($this->end_time) - strtotime($this->start_time)) / 60,
                'quota' => $quota . '/' . $this->max_quota,
                'class' => [
                    'id' => $this->branch_class_id,
                    'title' => $this->class->title,
                    'description' => $this->class->description,
                    'category' => ucwords($this->class->category->title),
                    'image' => ENV('CDN') . '/' . config('cdn.classes') . $this->class->image,
                    'isOnline' => $this->class->is_online == 1 ? true : false,
                    'url' => $this->class->is_online == 1 ? $this->url_class : '',
                    'password' => $this->class->is_online == 1 ? $this->password_class : '',
                    'maxBookTime' => $this->class->max_book_time,
                    'maxCancelTime' => $this->class->max_cancel_time,
                    'maxDelayTime' => $this->class->delay_time,
                    'level' => [
                        'label' => $this->class->level->title,
                        'description' => $this->class->description,
                    ],
                ],
                'coaches' => $coaches,
            ];
            $coaches = [];
        }


        return [
            'id' => $this->id,
            'date' => Carbon::parse($this->schedule->date)->format('d F Y'),
            'schedules' => $today,
        ];
    }
}
