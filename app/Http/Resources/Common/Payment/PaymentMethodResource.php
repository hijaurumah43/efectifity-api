<?php

namespace App\Http\Resources\Common\Payment;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentMethodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'methode' => ucwords($this->title),
            'collection' => PaymentTypeResource::collection($this->paymentType->where('is_published', 1))
        ];
    }
}
