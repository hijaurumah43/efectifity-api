<?php

namespace App\Http\Resources\Common\Home;

use Illuminate\Http\Resources\Json\JsonResource;

class FitClubResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'thumbnail' => ENV('CDN') . '/' . config('cdn.branchThumbnail') . $this->thumbnail,
            'address' => $this->region,
            'startingFrom' => 'IDR ' . number_format($this->packages->min('price')),
        ];
    }
}
