<?php

namespace App\Http\Resources\Common\Home;

use Illuminate\Http\Resources\Json\JsonResource;

class FitClassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $duration = (strtotime($this->end_time) - strtotime($this->start_time)) / 60;
        $time = $this->start_time . '-' . $this->end_time;
        return [
            'class' => [
                'id' => $this->class->id,
                'title' => $this->class->title,
                'category' => ucwords($this->class->category->title),
                'thumbnail' => ENV('CDN') . '/' . config('cdn.classes') . $this->class->image,
            ],
            'scheduleDetail' => [
                'id' => $this->id,
                'time' => $time,
                'duration' => (string) $duration . ' Min',
            ],
        ];
    }
}
