<?php

namespace App\Http\Resources\Common\Voucher;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class VoucherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $expired = '';
        $isExpired = false;
        if (Carbon::parse($this->expired)->format('Y-m-d') ==  date('Y-m-d')) {
            $expired = (strtotime(date('Y-m-d 23:59:59')) - strtotime($this->expired)) / 60 / 60;
            $expired = 'expiring in ' . round($expired) . ' hours';
            $isExpired = true;
        } else {
            $expired = 'valid until ' . Carbon::parse($this->expired)->format('F d, Y');
        }
        
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $this->image,
            'qty' => $this->qty,
            'type' => 'merchant',
            'discount' => $this->discount,
            'isExpired' => $isExpired,
            'expired' => $expired,
            'description' => $this->description,
        ];
    }
}
