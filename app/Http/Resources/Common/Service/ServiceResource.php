<?php

namespace App\Http\Resources\Common\Service;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'image' => $this->image,
            'isPublished' => $this->is_published == 1 ? true : false,
            'categories' => $this->categories->where('is_published', 1)->map(function($m) {
                return $m->title;
            }),
        ];
    }
}
