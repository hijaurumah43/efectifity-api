<?php

namespace App\Http\Resources\User\Account;

use Illuminate\Http\Resources\Json\JsonResource;

class AccountAccessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $service = 'Not Found';

        if ($this->type == 1) {
            $service = 'FITBIZ';
        } else {
            $service = $this->branch->service->title;
        }

        return [
            'id' => $this->id,
            'title' => strtoupper($this->branch->title),
            'logo' => $this->branch->logo,
            'type' => strtoupper($service),
        ];
    }
}
