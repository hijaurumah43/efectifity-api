<?php

namespace App\Http\Resources\User\Account;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AccountAccessCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => AccountAccessResource::collection($this->collection)
        ];
    }
}
