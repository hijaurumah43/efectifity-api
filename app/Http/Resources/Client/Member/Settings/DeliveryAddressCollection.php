<?php

namespace App\Http\Resources\Client\Member\Settings;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DeliveryAddressCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => DeliveryAddressResource::collection($this->collection)
        ];
    }
}
