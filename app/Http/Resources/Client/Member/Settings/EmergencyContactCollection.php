<?php

namespace App\Http\Resources\Client\Member\Settings;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Client\Member\Settings\EmergencyContactResource;

class EmergencyContactCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => EmergencyContactResource::collection($this->collection)
        ];
    }
}
