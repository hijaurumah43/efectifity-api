<?php

namespace App\Http\Resources\Client\Member\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $city = 'Location not found';
        
        // Geo Location
        $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$this->latitude.','.$this->longitude.'&sensor=false&key=AIzaSyDgpPVX_dVXx0jDpa9Z-eI7bAwkd6sPkFY');
        if($geocode) {

            $output  = json_decode($geocode);
            if($output->results) {
                $city = $output->results[0]->formatted_address;
                $city = explode(',', $city);
                if(count($city) >= 2) {
                    $city = $city[count($city)-2].','. end($city);
                } else{
                    $city = end($city);
                }
            }
        }

        return [
            'id' => $this->id,
            'label' => strtoupper($this->label),
            'receiver' => strtoupper($this->receiver),
            'phoneNumber' => $this->phone_number,
            'city' => $city,
            'address' => $this->address,
            'zipCode' => $this->zip_code,
            'marked' => $this->latitude == 0 || $this->longitude == 0 ? false : true,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    }
}
