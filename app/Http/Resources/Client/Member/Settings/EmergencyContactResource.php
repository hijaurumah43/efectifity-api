<?php

namespace App\Http\Resources\Client\Member\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class EmergencyContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => ucwords($this->label),
            'name' => ucwords($this->name),
            'phoneNumber' => $this->phone_number,
            'relation' => ucwords($this->relation),
            'isPublished' => $this->is_published == 1 ? true : false,
        ];
    }
}
