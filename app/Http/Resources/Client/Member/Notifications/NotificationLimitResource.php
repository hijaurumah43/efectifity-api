<?php

namespace App\Http\Resources\Client\Member\Notifications;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationLimitResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
