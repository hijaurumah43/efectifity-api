<?php

namespace App\Http\Resources\Client\Member\PT;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PersonalTrainerReviewCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PersonalTrainerReviewResource::collection($this->collection)
        ];
    }
}
