<?php

namespace App\Http\Resources\Client\Member\PT;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonalTrainerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => $this->image != NULL ? ENV('CDN') . '/' . config('cdn.pt') . $this->image : NULL,
            "firstName" => $this->member->name,
            "lastName" => $this->member->last_name,
            "qualifications" => $this->qualifications,
            "specialities" => $this->specialities,
            "totalReviews" => $this->reviews->count(),
            "totalRatings" => 4,
        ];
    }
}
