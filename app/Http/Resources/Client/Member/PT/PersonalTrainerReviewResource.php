<?php

namespace App\Http\Resources\Client\Member\PT;

use App\Models\Client\Member\Member;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PersonalTrainerReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => Member::avatarStorage($this->member->avatar),
            'firstName' => ucwords($this->member->name),
            'lastName' => ucwords($this->member->last_name),
            'start' => $this->star,
            'content' => $this->review,
            'createdAt' => Carbon::parse($this->created_at)->format('F d, Y'),
        ];
    }
}
