<?php

namespace App\Http\Resources\Client\Member;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MemeberCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MemeberResource::collection($this->collection)
        ];
    }
}
