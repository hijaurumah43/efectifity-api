<?php

namespace App\Http\Resources\Client\Member\Programs;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MemberPackageCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MemberPackageResource::collection($this->collection)
        ];
    }
}
