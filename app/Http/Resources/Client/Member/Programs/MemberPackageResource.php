<?php

namespace App\Http\Resources\Client\Member\Programs;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Http\Resources\Common\Branch\BranchPackageResource;
use App\Models\Client\Partner\Branch\BranchRefundPolicy;

class MemberPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // Class Date
        $classDate = request()->get('date');
        $classDate = isset($classDate) ? $classDate : Carbon::now()->format('Y-m-d');

        // Remaining Days
        $date = Carbon::parse($this->expiry_date);
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
        $refundPolicy = BranchRefundPolicy::where([
            ['branch_id', $this->branch_id],
            ['is_draft', 0],
            ['is_published', 1]
        ])
            ->orderBy('id', 'DESC')
            ->first();

        $text = NULL;
        if ($refundPolicy) {
            $text = $refundPolicy->content;
        }

        $sessionRemainingText = (string) $this->session_remaining;
        if ($this->package->is_unlimited == 1) {
            $sessionRemainingText = 'unlimited';
        }

        $sessionText = (string) $this->session;
        if ($this->package->pt_session > 0) {
            $sessionText = (string) 0;
        } else {
            if ($this->session == 0) {
                $sessionText = "unlimited";
            } else {
                $sessionText = (string) $this->session;
            }
        }

        return [
            'program' => [
                'id' => $this->id,
                /* 'expiryDate' => Carbon::parse($this->expiry_date)->format('d F Y'),
                'startDate' => Carbon::parse($this->created_at)->format('d F Y'), */
                'expiryDate' => ConvertToTimestamp($this->expiry_date),
                'startDate' => ConvertToTimestamp($this->created_at),
                'remainingDays' => $diff > 0 ? $diff . ' Days left' : 0 . ' Days left',
                'sessionRemaining' => $sessionRemainingText,
                'classCredit' => $sessionText,
                'ptSession' => $this->pt_session != NULL ? $this->pt_session : 0,
                'ptSessionRemaining' => $this->pt_session_remaining != NULL ? $this->pt_session_remaining : 0,
                'gymAccess' => $this->package->gym_access == 1 ? true : false,
                'package' => new BranchPackageResource($this->transaction->branchPackage),
                'refundPolicy' => $text,
                'isPromo' => $this->is_promo == 1 ? true : false,
                'agreement' => [
                    'content' => isset($this->agreement) ? $this->agreement->content : NULL,
                    'agreementId' => $this->branch_agreement_id,
                    // 'signedOn' => $this->created_at->format('Y-m-d H:i'),
                    'signedOn' => ConvertToTimestamp($this->created_at),
                ]
            ],
            'branch' => new BranchResource($this->transaction->branch),
        ];
    }
}
