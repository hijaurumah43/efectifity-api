<?php

namespace App\Http\Resources\Client\Member\Attendances;

use Carbon\Carbon;
use App\Models\Client\Staff\Rules\Rule;
use App\Models\Client\Staff\Rules\BranchRule;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Http\Resources\Common\Branch\BranchScheduleDetailResource;

class ClassAttendanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $canceledBy = null;
        $scannedBy = null;
        $message = null;

        // Cancel
        if ($this->class_attendance_status_id == 3) {
            if ($this->note != '') {
                $canceledBy = $this->scheduleDetail->class->branch->title;
                $message = 'This reservation has been cancelled by ' . $canceledBy . ' on ' . $this->cancel_time . ' with reason : ' . $this->note;
            } else {
                $canceledBy = $this->member->name;
                $message = 'This reservation has been cancelled by ' . $canceledBy . ' on ' . $this->cancel_time;
            }
        }

        // Attend
        if ($this->class_attendance_status_id == 2) {
            $scannedBy = $this->user->member->name;
            $message = 'You’re attend the Class on ' . $this->scan_time . ' and scanned by ' . $scannedBy;
        }

        // Passed
        if ($this->class_attendance_status_id == 4) {
            $message = 'You’re not attend the class';
        }

        $ruleType = 1;
        $isPT = false;

        // CHECK CLASS
        if ($this->scheduleDetail->class->is_online == 1) {
            // ONLINE
            $ruleType = 2;
        } else if ($this->memberPackage_pt_session != NULL || $this->memberPackage->pt_session > 0) {
            // PT
            $ruleType = 3;
            $isPT = true;
        } else {
            // OFFLINE
            $ruleType = 1;
        }

        $branch_id = $this->scheduleDetail->class->branch_id;
        $rules = BranchRule::where([
            ['branch_id', $branch_id],
            ['type', $ruleType],
        ])
            ->orderBy('id', 'ASC')
            ->get();

        if (count($rules) < 1) {
            $rules = Rule::where('type', $ruleType)->orderBy('id', 'ASC')->get();
        }

        $tempRule = [];
        foreach ($rules as $key => $value) {
            if ($key == 0) {
                $tempRule[] = 'Reservation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the Class started';
            } else if ($key == 2) {
                $tempRule[] = 'Cancellation must be made at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' before the Class started';
            } else if ($key == 3) {
                $tempRule[] = 'Present at the class at least ' . $value->duration . ' ' . ruleNamePeriode($value->period) . ' after the actual Class schedule';
            } else if ($key == 5) {
                if (isset($rules[6])) {
                    $tempRule[] = 'If you reach ' . $value->duration . ' times of Class cancellation within 7 days, you will not be able to make Class reservation in ' . $rules[6]->duration . ' ' . ruleNamePeriode($rules[6]->period) . ' ahead ';
                }
            }
        }

        return [
            'createdAt' => Carbon::parse($this->created_at)->format('d F Y'),
            'collection' => new BranchScheduleDetailResource($this->scheduleDetail),
            'isPT' => $isPT,
            'rules' => $tempRule,
            'attendance' => [
                'id' => $this->id,
                'bookedAt' => Carbon::parse($this->created_at)->toDateTimeString(),
                'scannedAt' => $this->scan_time != '' ? $this->scan_time : null,
                'scannedBy' =>  $scannedBy,
                'canceledAt' => $this->cancel_time != '' ? $this->cancel_time : null,
                'canceledBy' => $canceledBy,
                'note' => null,
                'message' => $message,
                'attendanceStatus' => $this->attendanceStatus->title,
                'status' => $this->is_active == 1 ? 'active' : 'past',
                'sessionRemaining' => $this->memberPackage->session == 0 ? 'unlimited' : (string) $this->memberPackage->session_remaining,
            ],
            'branch' => new BranchResource($this->scheduleDetail->schedule->branch),
        ];
    }
}
