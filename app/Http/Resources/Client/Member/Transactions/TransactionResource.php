<?php

namespace App\Http\Resources\Client\Member\Transactions;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Branch\BranchResource;
use App\Http\Resources\Common\Branch\BranchPackageResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $actionTo = NULL;
        $phoneNumber = NULL;
        $ccNumber = NULL;

        $paymentMethodId = $this->paymentType->paymentMethod->id;
        $paymentTypeId = $this->paymentType->id;
        if ($paymentMethodId == 4) {
            $ccNumber = $this->action_to;
        } else if ($paymentTypeId == 6) {
            $phoneNumber = $this->action_to;
        } else {
            $actionTo = $this->action_to;
        }

        $expireDate = Carbon::parse($this->created_at)->addDay(1);

        return [
            'id' => $this->id,
            'orderNumber' => $this->order_number,
            'status' => $this->transactionStatus->title,
            'paymentType' => $this->channels,
            'paymentMethod' => $this->paymentType->paymentMethod->title,
            'image' => $this->paymentType->image,
            'actionTo' => $actionTo,
            'phoneNumber' => $phoneNumber,
            'ccNumber' => $ccNumber,
            'price' => 'IDR ' . number_format($this->total_payment),
            'subTotal' =>  $this->voucher != NULL ? (string) number_format($this->branchPackage->price) : NULL,
            'totalDiscount' => $this->voucher != NULL ? number_format($this->branchPackage->price - $this->total_payment) : NULL,
            'voucher' => $this->voucher != NULL ? $this->voucher->title : NULL,
            'expireTime' => [
                // "expiryDate" => Carbon::parse($this->created_at)->addDay(1)->format('l, d F Y H:i'),
                "expiryDate" => ConvertToTimestamp($expireDate),
                "expiryDuration" => 1,
                "unit" => "day"
            ],
            'attributes' => $this->when($request->url() == route('api.member.transactions.index') || $request->url() == route('api.member.transactions.show', $this->id), [
                'branch' => new BranchResource($this->branch),
                'package' => new BranchPackageResource($this->branchPackage),
            ]),
        ];
    }
}
