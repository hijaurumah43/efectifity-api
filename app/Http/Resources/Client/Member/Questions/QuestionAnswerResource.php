<?php

namespace App\Http\Resources\Client\Member\Questions;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Questions\QuestionAnswer;
use App\Http\Resources\Internal\Question\QuestionDetailResource;

class QuestionAnswerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = auth()->user();
        $question = QuestionAnswer::where([
            ['member_id', $user->member->id],
            ['question_id', $this->id]
        ])->first();

        return [
            'id' => $this->id,
            'title' => strtolower($this->title),
            'withNote' => $this->with_note == 1 ? true : false,
            'questionNote' => $this->question_note != '' ? $this->question_note : null,
            'questionType' => $this->type->title,
            'detail' => QuestionDetailResource::collection($this->details),
            'response' => [
                'id'   => $question ? $question->id : null,
                'text' => $question ? $question->response : null,
                'note' => $question ? $question->note : null,
            ],
        ];
    }
}
