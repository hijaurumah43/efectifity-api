<?php

namespace App\Http\Resources\Client\Member\Questions;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Client\Member\Questions\QuestionAnswerResource;

class QuestionAnswerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => QuestionAnswerResource::collection($this->collection)
        ];
    }
}
