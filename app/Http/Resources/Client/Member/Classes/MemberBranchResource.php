<?php

namespace App\Http\Resources\Client\Member\Classes;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Common\Branch\BranchResource;

class MemberBranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'branch' => new BranchResource($this->branch)
        ];
    }
}
