<?php

namespace App\Http\Resources\Client\Member\Classes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MemberBranchCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MemberBranchResource::collection($this->collection)
        ];
    }
}
