<?php

namespace App\Http\Resources\Client\Partner\Company;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyDocumentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => CompanyDocumentResource::collection($this->collection)
        ];
    }
}
