<?php

namespace App\Http\Resources\Client\Partner\Company;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Internal\Company\Document\DocumentStatusLog;

class CompanyDocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = 'ERROR';
        $resource = DocumentStatusLog::where('company_document_id', $this->id)->orderBy('id', 'DESC')->first();
        if ($resource) {
            $status = ucwords($resource->status->title);
        }

        return [
            'status' => $status,
            'id' => $this->id,
            'service' => [
                'serviceId' => $this->service->id,
                'title' => $this->service->title,
                'slug' => $this->service->slug,
            ],
            'company' => [
                'companyId' => $this->company->id,
                'companyName' => $this->company->title,
            ],
            'brandName' => $this->company_name,
            'companyFormat' => $this->company_format,
            'ownerName' => $this->owner_name,
            'email' => $this->email,
            'phoneNumber' => $this->phone_number,
            'identity_card' => $this->identity_card,
            'taxNumber' => $this->tax_number,
            'companyRegistration' => $this->company_registration,
            'websiteUrl' => $this->website_url,
            'instagramUrl' => $this->instagram_url,
            'facebookUrl' => $this->facebook_url,
            'youtubeUrl' => $this->youtube_url,
            'tiktokUrl' => $this->tiktok_url,
            'isComplete' => $this->status == 1 ? true : false,
        ];
    }
}
