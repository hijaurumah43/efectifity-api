<?php

namespace App\Http\Resources\Client\Partner\Bank;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MemberBankCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MemberBankResource::collection($this->collection)
        ];
    }
}
