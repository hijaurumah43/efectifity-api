<?php

namespace App\Http\Resources\Client\Partner\Bank;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberBankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bank' => [
                'bankId' => $this->bank->id,
                'bankTitle' => $this->bank->title,
            ],
            'address' => $this->address,
            'accountHolder' => $this->account_holder,
            'accountNumber' => $this->account_number,
        ];
    }
}
