<?php

namespace App\Http\Resources\Client\Partner\Branch;

use App\Models\Common\Branch\BranchRequire;
use Illuminate\Http\Resources\Json\JsonResource;

class PlatformRequireResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $status = false;
        $branchId = (int) request()->get('branchId');

        $BranchRequire = BranchRequire::where([
            ['question_category_id', $this->id],
            ['branch_id', $branchId]
        ])->first();

        if($BranchRequire) {
            if($BranchRequire->status == 1) {
                $status = true;   
            } else {
                $status = false;
            }
        }

        return [
            'id' => $this->id,
            'title' => $this->title,
            'status' => $status,
        ];
    }
}
