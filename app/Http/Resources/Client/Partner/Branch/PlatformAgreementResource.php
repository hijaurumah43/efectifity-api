<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class PlatformAgreementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "version" => $this->version,
            "content" => $this->content,
            "isDraft" => $this->is_draft == 1 ? true : false,
            "status" => $this->status == 1 ? 'active' : 'inactive',
        ];
    }
}
