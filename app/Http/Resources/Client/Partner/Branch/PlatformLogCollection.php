<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PlatformLogCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PlatformLogResource::collection($this->collection)
        ];
    }
}
