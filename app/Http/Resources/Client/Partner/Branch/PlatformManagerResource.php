<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class PlatformManagerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'member_id' => $this->member->member_id,
            'email' => $this->member->user->email,
            'name' => $this->member->name,
            'avatar' => $this->member->avatar,
            'status' => statusStaff($this->status)
        ];
    }
}
