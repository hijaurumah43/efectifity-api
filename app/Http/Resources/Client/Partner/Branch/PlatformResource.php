<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class PlatformResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'service' => [
                'serviceId' => $this->service->id,
                'serviceTtitle' => $this->service->title,
                'url' => 'https://' . $this->service->slug . '.efectifity.com'
            ],
            'branchName' => $this->title,
            'manager' => 'Manager',
            'status' => count($this->statusLog) > 0 ? $this->statusLog->last()->status->title : 'error',
            'address' => $this->address,
            'city' => $this->region,
            'postalCode' => $this->postal_code,
            'logo' => $this->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $this->logo : null,
            'thumbnail' => $this->thumbnail != null ? ENV('CDN') . '/' . config('cdn.branchThumbnail') . $this->thumbnail : null,
            'location' => [
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
            ],
            'phoneNumber' => $this->phone_number,
            'photo' => [
                'frontSide' => [
                    'title' => $this->front_side,
                    'link' => $this->front_side != null ? ENV('CDN') . '/' . config('cdn.branchGallery') . $this->front_side : "",
                ],
                'inside' => [
                    'title' => $this->inside,
                    'link' => $this->inside != null ? ENV('CDN') . '/' . config('cdn.branchGallery') . $this->inside : "",
                ],
            ],
            'website' => $this->website,
            'instagram' => $this->instagram,
            'facebook' => $this->facebook,
            'youtube' => $this->youtube,
            'tiktok' => $this->tiktok,
            'membershipRequirements' =>  $this->required->map(function ($m) {
                return [
                    "id" => $m->id,
                    "title" => $m->questionCategory->title,
                    "status" => $m->status == 1 ? true : false
                ];
            }),
        ];
    }
}
