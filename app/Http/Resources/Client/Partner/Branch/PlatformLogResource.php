<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PlatformLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $text = '';
        $name = 'ADMIN';

        if (isset($this->user)) {

            $name = ucwords($this->user->member->name . ' ' . $this->user->member->last_name);
        }

        switch ($this->status->id) {
            case '2':
                $text = 'Request to review by FitPartner';
                break;

            case '3':
                $text = 'In-review by Efectifity ' . $name;
                break;

            case '4':
                $text = 'Rejected by Efectifity Team ' . $name;;
                break;

            case '5':
                $text = 'Approved by Efectifity Team ' . $name;;
                break;

            case '6':
                $text = 'Active';
                break;

            default:
                $text = '';
                break;
        }

        return [
            'id' => $this->id,
            'status' => $this->status->title,
            'text' => $text,
            'note' => $this->note,
            'createdAt' => Carbon::parse($this->created_at)->format('d M, Y - H:s'),
        ];
    }
}
