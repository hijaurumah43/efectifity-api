<?php

namespace App\Http\Resources\Client\Partner\Branch;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PlatformRefundPolicyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
