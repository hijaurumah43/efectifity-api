<?php

namespace App\Http\Resources\Client\Partner;

use App\Models\Client\Member\Member;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Resources\Json\JsonResource;

class PartnerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $default = '123456789';
        $password = true;

        if (Hash::check($default, $this->user->password) == true) {
            $password = false;
        }

        return [
            'id' => $this->id,
            'membership' => $this->member_id,
            'name' => $this->name,
            'lastName' => $this->last_name,
            'avatar' => Member::avatarStorage($this->avatar),
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'scope' => isset($this->user->roles[0]) ? $this->user->roles[0]->name : 'no scope',
            'verified' => [
                'phoneNumber' => $this->user->phone_verified != 0 ? true : false,
                'email' => $this->user->email_verified_at != '' ? true : false,
                'pin' => $this->user->pin != '' ? true : false,
                'password' => $password,
            ],
            'company' => [
                'id' => $this->user->company->id,
                'brandName' => $this->user->company->title,
                'image' => $this->user->company->image,
                'address' => $this->user->company->address,
                'postalCode' => $this->user->company->postal_code,
                'city' => [
                    'cityId' => $this->user->company->cities->id,
                    'title' => $this->user->company->cities->title,
                ],
                'legalDocumentVerified' => [
                    'fitClub' => $this->user->companyDocument->where('service_id', 1)->count() > 0 ? true : false,
                    'fitKitchen' => $this->user->companyDocument->where('service_id', 2)->count() > 0 ? true : false,
                ]
            ],
        ];
    }
}
