<?php

namespace App\Http\Resources\Client\Staff\Attendance;

use Carbon\Carbon;
use Sujip\Ipstack\Ipstack;
use App\Models\Common\Branch\Branch;
use Illuminate\Http\Resources\Json\JsonResource;

class ClassAttendanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $isDisabled = true;
        $expired = false;

        $now = date('Y-m-d H:i:s');

        // CEK CLASS
        $typeRule = 1;
        $class = $this->scheduleDetail->class;
        if ($class->is_online == 1) {
            $typeRule = 2;
        }

        // GET RULES
        $branchRules = Branch::rules($this->branch->id, $typeRule);
        // NOTE:: SET TIME
        $maxScanTime = '';
        $delayTime = '';

        foreach ($branchRules as $rule) {
            if ($rule['id'] == 5 || $rule['id'] == 15) {
                $maxScanTime = $rule['duration'];
                $maxScanTimePeriode = strtolower(ruleNamePeriode($rule['period']));
            }

            if ($rule['id'] == 4 || $rule['id'] == 14) {
                $delayTime = $rule['duration'];
                $delayTimePeriode = strtolower(ruleNamePeriode($rule['period']));
            }
        }

        // Convert to date format of resource time
        $classTime = $this->scheduleDetail->start_time;
        $classTimeFormat = Carbon::parse((date('Y-m-d ' . $classTime . '.00')))->format('Y-m-d H:i:s');

        // Convert to date format
        $maxScanTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '-' . $maxScanTime . ' ' . $maxScanTimePeriode));
        $delayTime = date("Y-m-d H:i:s", strtotime($classTimeFormat . '+' . $delayTime . ' ' . $delayTimePeriode));

        // Disabled class
        if ($now >= $maxScanTime && $now <= $delayTime) {
            $isDisabled = false;
        }

        // Expired
        if ($now > $delayTime) {
            $expired = true;
        }

        return [
            'id' => $this->id,
            'time' => $classTime,
            'branch' => [
                'id' => $this->branch->id,
                'title' => $this->branch->title,
            ],
            'scannedAt' => $this->scan_time != null ? 'Check In at Class : ' . Carbon::parse($this->scan_time)->format('H.i') : null,
            'isDisabled' => $isDisabled,
            'expired' => $expired,
            // 'status' => Self::statusClassAttendance($this->class_attendance_status_id)
        ];
    }
}
