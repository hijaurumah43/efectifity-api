<?php

namespace App\Http\Resources\Client\Staff\Schedule;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class StaffScheduleDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'time' => Carbon::parse($this->start_time)->format('H.i').' - '.Carbon::parse($this->end_time)->format('H.i'),
            'description' => $this->description,
        ];
    }
}
