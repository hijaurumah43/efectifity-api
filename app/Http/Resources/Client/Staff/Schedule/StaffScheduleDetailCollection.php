<?php

namespace App\Http\Resources\Client\Staff\Schedule;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StaffScheduleDetailCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => StaffScheduleDetailResource::collection($this->collection)
        ];
    }
}
