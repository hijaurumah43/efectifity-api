<?php

namespace App\Http\Resources\Client\Staff\Schedule;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StaffScheduleCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => StaffScheduleResource::collection($this->collection)
        ];
    }
}
