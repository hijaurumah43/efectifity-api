<?php

namespace App\Http\Resources\Client\Staff\Schedule;

use Illuminate\Http\Resources\Json\JsonResource;

class StaffScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'collection' => StaffScheduleDetailResource::collection($this->detail),
        ];
    }
}
