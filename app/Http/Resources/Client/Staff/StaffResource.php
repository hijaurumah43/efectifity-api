<?php

namespace App\Http\Resources\Client\Staff;

use App\Models\PermissionRole;
use App\Models\Client\Member\Member;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\Client\Member\Programs\MemberPackage;

class StaffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $default = '123456789';
        $password = true;
        $healthQuestionnaire = false;
        $bodyMassIndex = false;
        $foodAllergies = false;

        foreach ($this->personalInformation as $row) {
            switch ($row->question->question_category_id) {
                case '1':
                    $healthQuestionnaire = true;
                    break;

                case '2':
                    $bodyMassIndex = true;
                    break;

                case '3':
                    $foodAllergies = true;
                    break;

                default:
                    $healthQuestionnaire = false;
                    $bodyMassIndex = false;
                    $foodAllergies = false;
                    break;
            }
        }

        if (Hash::check($default, $this->user->password) == true) {
            $password = false;
        }

        $company = NULL;
        $memberBranch = MemberBranch::where([
            'member_id' => $this->id,
            'is_active' => 1,
            'is_published' => 1
        ])->first();

        if ($memberBranch) {
            $company = [
                'id' => $memberBranch->branch->company->id,
                'brandName' => $memberBranch->branch->company->title,
                'image' => isset($memberBranch->branch) ? ENV('CDN') . '/' . config('cdn.branchLogo') . $memberBranch->branch->logo : null,
            ];
        }

        $newUser = false;
        $memberPackage = MemberPackage::where('member_id', $this->id)->first();
        if (!$memberPackage) {
            $newUser = true;
        }

        // NOTE:: TAB
        $isWork = false;
        $checkMemberBranch = MemberBranch::where([
            ['member_id', $this->id],
            ['status', 2],
            // ['type', '!=', 1],
            ['is_active', 1],
            ['is_published', 1],
        ])
            ->whereNull('type')
            ->count();

        if ($checkMemberBranch > 0) {
            $isWork = true;
        }

        return [
            'id' => $this->id,
            'membership' => $this->member_id,
            'username' => $this->user->username,
            'name' => $this->name,
            'lastName' => $this->last_name != NULL ? $this->last_name : '',
            'avatar' => Member::avatarStorage($this->avatar),
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'isWork' => $isWork,
            'scope' => isset($this->user->roles[0]) ? $this->user->roles[0]->name : 'no scope',
            'roles' => $this->user->roles->map(function ($q) {
                $permissions = PermissionRole::where('role_id', $q->id)->get();
                return [
                    'name' => $q->name,
                    'menu' => $permissions->map(function ($p) {
                        return $p->permission->display_name;
                    })
                ];
            }),
            'newUser' => $newUser,
            'verified' => [
                'phoneNumber' => $this->user->phone_verified != 0 ? true : false,
                'email' => $this->user->email_verified_at != '' ? true : false,
                'healthQuestionnaire' => $healthQuestionnaire,
                'bodyMassIndex' => $bodyMassIndex,
                'foodAllergies' => $foodAllergies,
                'emergancyContact' => count($this->emergencyContact->where('is_published', 1)) != 0 ? true : false,
                'deliveryAddress' => count($this->deliveryAddress->where('is_published', 1)) != 0 ? true : false,
                'pin' => $this->user->pin != '' ? true : false,
                'password' => $password,
            ],
            'company' => $company,
        ];
    }
}
