<?php

namespace App\Http\Resources\Feature\Fitclub\PersonalTrainer;

use App\Models\Client\Member\Member;
use App\Models\Client\Staff\PersonalTrainerMember;
use Illuminate\Http\Resources\Json\JsonResource;

class PersonalTrainerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $temp = [];
        $tempInactive = [];
        // $collections = 0;

        $countUsage = PersonalTrainerMember::where('personal_trainer_id', $this->id)
            ->leftJoin('member_packages', 'personal_trainer_member.member_package_id', '=', 'member_packages.id')
            // ->where('member_packages.status', 1)->where('pt_session_remaining', '>', 0)->count();
            ->where('member_packages.status', 1)->count();

        if ($request->url() == route('fc.pt.show', [$this->branch_id, $this->id])) {

            $query = PersonalTrainerMember::where('personal_trainer_id', $this->id)
                ->leftJoin('member_packages', 'personal_trainer_member.member_package_id', '=', 'member_packages.id')
                // ->where('member_packages.status', 1)->where('pt_session_remaining', '>', 0);
                ->where('member_packages.status', 1);

            if ($request->has('member')) {
                $query->whereHas('member', function ($q) use ($request) {
                    $service = strtolower($request->get('member'));
                    return $q->where('name', 'like', '%' . $service . '%');
                });
            }

            $collections = $query->get();

            foreach ($collections as $collection) {
                $member = $collection->member;
                $package = $collection->memberPackage;
                $temp[] =  [
                    'member' => [
                        'id' => $member->id,
                        'avatar' => Member::avatarStorage($member->avatar),
                        'firstName' => ucwords($member->name),
                        'lastName' => ucwords($member->last_name)
                    ],
                    'package' => [
                        'id' => isset($package) ? $package->id : null,
                        'title' => isset($package) ? $package->package->title : null,
                        'usage' => isset($package) ? $package->pt_session - $package->pt_session_remaining : null,
                        'total' => isset($package) ? $package->pt_session : null,
                        'remaining' => isset($package) ? $package->pt_session_remaining : null,
                    ]
                ];
            }

            $packageInactives = PersonalTrainerMember::where('personal_trainer_id', $this->id)
                ->leftJoin('member_packages', 'personal_trainer_member.member_package_id', '=', 'member_packages.id')
                ->where('member_packages.status', 0);

            if ($request->has('member')) {
                $packageInactives->whereHas('member', function ($q) use ($request) {
                    $service = strtolower($request->get('member'));
                    return $q->where('name', 'like', '%' . $service . '%');
                });
            }

            $items = $packageInactives->get();

            foreach ($items as $item) {
                $member = $item->member;
                $package = $item->memberPackage;
                $tempInactive[] =  [
                    'member' => [
                        'id' => $member->id,
                        'avatar' => Member::avatarStorage($member->avatar),
                        'firstName' => ucwords($member->name),
                        'lastName' => ucwords($member->last_name)
                    ],
                    'package' => [
                        'id' => isset($package) ? $package->id : null,
                        'title' => isset($package) ? $package->package->title : null,
                        'usage' => isset($package) ? $package->pt_session - $package->pt_session_remaining : null,
                        'total' => isset($package) ? $package->pt_session : null,
                        'remaining' => isset($package) ? $package->pt_session_remaining : null,
                    ]
                ];
            }
        }


        return [
            'member' => [
                'memberID' => $this->member->id,
                'avatar' => Member::avatarStorage($this->member->avatar),
                'firstName' => ucwords($this->member->name),
                'lastName' => ucwords($this->member->last_name),
                'email' => ucwords($this->member->user->email),
                'telp' => ucwords($this->member->user->phone),
            ],
            'personalTrainer' => [
                'ptID' => $this->id,
                'usage' => $countUsage,
                'quota' => $this->quota,
                'duration' => $this->duration,
                'image' => $this->image != NULL ? ENV('CDN') . '/' . config('cdn.pt') . $this->image : NULL,
                'status' => $this->is_published == 1 ? true : false,
                'attributes' => $this->when($request->url() == route('fc.pt.show', [$this->branch_id, $this->id]), [
                    'qualifications' => $this->qualifications,
                    'specialities' => $this->specialities,
                    'member' => $temp,
                    'memberInactive' => $tempInactive,
                ])
            ]
        ];
    }
}
