<?php

namespace App\Http\Resources\Feature\Fitclub\PersonalTrainer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PersonalTrainerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PersonalTrainerResource::collection($this->collection)
        ];
    }
}
