<?php

namespace App\Http\Resources\Feature\Fitclub\Promo;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PromoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'original' => [
                'id' => $this->package->id,
                'title' => ucwords($this->package->title),
            ],
            'promo' => [
                'id' => $this->id,
                'name' => ucwords($this->title),
                'type' => [
                    'id' => $this->type,
                    'name' => $this->type == 1 ? 'club' : 'global',
                ],
                'memberType' => [
                    'id' => $this->member_type,
                    'name' => ucwords(promoteMemberType($this->member_type))
                ],
                'startTime' => Carbon::parse($this->start_time)->toDateString(),
                'endTime' => Carbon::parse($this->end_time)->toDateString(),
                'status' => [
                    'id' => $this->status,
                    'name' => ucwords(promoteStatus($this->status))
                ],
                'duration' => $this->duration,
                'periode' => $this->periode,
                'gymAccess' => $this->gym_access == 1 ? true : false,
                'isUnlimited' => $this->is_unlimited == 1 ? true : false,
                'session' => $this->session,
                'ptSession' => $this->pt_session,
                'price' => $this->price,
                'startTime' => $this->start_time,
                'endTime' => $this->end_time,
            ],
        ];
    }
}
