<?php

namespace App\Http\Resources\Feature\Fitclub\Branch;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'logo' => $this->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $this->logo : null,
            'region' => $this->region,
        ];
    }
}
