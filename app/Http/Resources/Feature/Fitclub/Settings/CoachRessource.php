<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class CoachRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => ucwords($this->name),
        ];
    }
}
