<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;

class SettingsPersonalTrainerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $ptTemp = null;
        if ($this->member->pt) {
            $pt = $this->member->pt->where('branch_id', $this->branch_id)->where('member_id', $this->member->id)->first();
            if ($pt) {
                $ptTemp = [
                    'ptID' => $pt->id,
                    'remaining' => $pt->ptMember->count(),
                    'quota' => $pt->quota,
                    'duration' => $pt->duration,
                    'image' => $pt->image != NULL ? ENV('CDN') . '/' . config('cdn.pt') . $pt->image : NULL,
                    'status' => $pt->status == 1 ? true : false,
                    'qualifications' => $pt->qualifications,
                    'specialities' => $pt->specialities,
                ];
            }
        }

        return [
            'member' => [
                'memberID' => $this->member->id,
                'avatar' => Member::avatarStorage($this->member->avatar),
                'firstName' => ucwords($this->member->name),
                'lastName' => ucwords($this->member->last_name),
            ],
            'personalTrainer' => $ptTemp
        ];
    }
}
