<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Carbon\Carbon;
use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;

class ShiftResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user;
        return [
            'id' => $this->id,
            'branch_id' => $this->branch_id,
            'title' => $this->title,
            'clockIn' => Carbon::parse($this->start_time)->format('H.i'),
            'clockOut' => Carbon::parse($this->end_time)->format('H.i'),
            'createdBy' => [
                'name' => $user->member->name . ' ' . $user->member->last_name,
                'avatar' => Member::avatarStorage($user->member->avatar),
                'position' => 'Manager',
            ],
            'isActive' => $this->status == 1 ? true : false,
        ];
    }
}
