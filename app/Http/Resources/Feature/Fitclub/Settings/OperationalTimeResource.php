<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;

class OperationalTimeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user;
        return [

            'id' => $this->id,
            'title' => ucwords($this->title),
            'date' => $this->month . '/' . $this->year,
            'createdBy' => [
                'name' => $user->member->name . ' ' . $user->member->last_name,
                'avatar' => Member::avatarStorage($user->member->avatar),
                'position' => 'Manager',
            ],
            'status' => $this->year . '-' . $this->month < date("Y-m") ? Self::statusSchedule(2) : Self::statusSchedule($this->status),
        ];
    }

    private function statusSchedule($status)
    {
        switch ($status) {
            case '0':
                return [
                    'status' => $status,
                    'text' => strtolower('draft'),
                ];
                break;

            case '1':
                return [
                    'status' => $status,
                    'text' => strtolower('published'),
                ];
                break;

            case '2':
                return [
                    'status' => $status,
                    'text' => strtolower('ended'),
                ];
                break;

            default:
                return [
                    'status' => null,
                    'text' => null,
                ];
                break;
        }
    }
}
