<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        // $session = $this->session == 0 && strtolower($this->periode) == 'month' ? 'unlimited' : $this->session;

        return [
            'id' => $this->id,
            'title' => ucwords($this->title),
            'gymAccess' => $this->gym_access == 1 ? 'Yes' : 'No',
            'classSession' => $this->session,
            'ptSession' => $this->pt_session != NULL ? $this->pt_session : 0,
            'isPromo' => $this->is_promo == 1 ? true : false,
            'duration' => $this->duration,
            'period' => strtolower($this->periode),
            'price' => $this->price,
            'status' => $this->is_published == 1 ? true : false,
            'isUnlimited' => $this->is_unlimited == 1 ? true : false,
        ];
    }
}
