<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OperationalTimeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => OperationalTimeResource::collection($this->collection)
        ];
    }
}
