<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Carbon\Carbon;
use App\Models\Client\Member\Member;
use App\Models\Client\Staff\Schedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Client\Staff\StaffScheduleShift;
use Illuminate\Http\Resources\Json\JsonResource;

class StaffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $schedule = [];
        $schedulePeriode = NULL;
        if ($request->url() == route('fc.staff-schedule.show', [$this->branch_id, $this->id])) {

            $getSchedule = Schedule::where([
                ['branch_id', $this->branch_id],
                ['status', 1],
            ])
                ->orderBy('year', 'DESC')
                ->orderBy('month', 'DESC')
                ->first();

            if ($getSchedule) {
                $schedulePeriode = $getSchedule->title;


                $detailSchedule =  StaffScheduleDetail::where([
                    ['member_branch_id', $this->id],
                    ['schedule_id', $getSchedule->id]
                ])->distinct()->select('shift_id')->get();

                if (count($detailSchedule) > 0) {
                    foreach ($detailSchedule as $val) {

                        $temp = [];
                        $dayCollection = [];

                        $shifts =  StaffScheduleDetail::where([
                            ['member_branch_id', $this->id],
                            ['schedule_id', $getSchedule->id],
                            ['shift_id', $val->shift_id]
                        ])->get();

                        foreach ($shifts as $item) {
                            $day = Carbon::parse($item->start_time)->format('l');
                            $temp[] = $day;
                        }

                        // position for days true or false
                        $dayCollection =  $this->checkTheDay($temp);

                        $schedule[] = [
                            'shiftID' => $shifts[0]->shift->id,
                            'title' => $shifts[0]->shift->title,
                            'startTime' => Carbon::parse($shifts[0]->shift->start_time)->format('H.i'),
                            'endTime' => Carbon::parse($shifts[0]->shift->end_time)->format('H.i'),
                            'days' => $dayCollection,
                        ];
                    }
                }
            }
        }

        // relationship member
        $member = $this->member;

        // Get Position
        $position = [];
        if ($this->member->user) {
            $user = $this->member->user;
            foreach ($user->roles as $data) {
                $position[] = $data->name;
            }
        }

        // status
        // $isActive = $this->is_published == 1 && $this->is_active == 1 ? true : false;
        $isActive = $this->status == 2 ? true : false;

        return [
            'employee' => [
                'id' => $this->id,
                'memberId' => $member->member_id,
                'firstName' => $member->name,
                'lastName' => $member->last_name,
                'avatar' => Member::avatarStorage($member->avatar),
                'isActive' => $isActive,
                'status' => statusStaff($this->status),
            ],
            'role' => $position,
            'startingDate' => Carbon::parse($this->created_at)->format('d F Y'),
            'contact' => [
                'mobilePhone' => $member->user->phone,
                'email' => $member->user->email,
            ],
            'attributes' => $this->when($request->url() == route('fc.staff-schedule.show', [$this->branch_id, $this->id]), [
                'schedulePeriode' => $schedulePeriode,
                'schedule' => $schedule,
            ])
        ];
    }

    public function checkTheDay($days)
    {
        $temp = [];
        $collections = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ];

        foreach ($collections as $item) {

            if (in_array($item, $days) == 1) {
                $temp[] = true;
            } else {
                $temp[] = false;
            }
        }

        return $temp;
    }
}
