<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => ucwords($this->title),
            'thumbnail' => ENV('CDN') . '/' . config('cdn.classes') . $this->image,
            'level' => [
                'id' => $this->level->id,
                'title' => $this->level->title,
            ],
            'category' => [
                'id' => $this->category->id,
                'title' => $this->category->title
            ],
            'session' => 1,
            'description' => $this->description,
            'isOnline' => $this->is_online == 1 ? true : false,
            'isActive' => $this->is_published == 1 ? true : false,
        ];
    }
}
