<?php

namespace App\Http\Resources\Feature\Fitclub\Settings;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PackageCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PackageResource::collection($this->collection)
        ];
    }
}
