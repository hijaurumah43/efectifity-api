<?php

namespace App\Http\Resources\Feature\Fitclub\Member;

use App\Models\Client\Member\Log\MemberLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Staff\Attendance\MemberAttendance;

class MemberResource extends JsonResource
{
    public $data;
    public $branch_id;

    public function __construct($data, $branch_id)
    {
        $this->data = $data;
        $this->branch_id = $branch_id;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // $branch_id = $this->branch_id;
        $branch_id = $request->get('attributes');
        $status = true;
        $message = 'active membership';

        $checKMembership = MemberPackage::where([
            ['member_id', $this->data->id],
            // ['session_remaining', '>', 0],
            ['expiry_date', '>=', date('Y-m-d')],
        ])
            ->orWhere([
                ['member_id', $this->data->id],
                // ['pt_session_remaining', '>', 0],
                ['expiry_date', '>=', date('Y-m-d')],
            ])
            ->count();

        if ($checKMembership < 1) {

            $status = false;
            $message = 'inactive membership';
        }

        $programs = [];
        $collectMemberPackage = MemberPackage::where('member_id', $this->data->id)->where('branch_id', $branch_id)->get();
        if (count($collectMemberPackage) > 0) {
            $i = 0;
            foreach ($collectMemberPackage as $m) {

                $sessionText = '';
                if($m->package->is_unlimited == 1) {
                    $sessionText = 'unlimited';
                } else {
                    $sessionText = $m->session_remaining . ' of ' . $m->package->session;
                }

                $programs[] = [
                    'tab' => 'PROGRAM ' . ++$i,
                    'id' => $m->id,
                    'package' => $m->package->title,
                    'validity' => $m->created_at->format('d F Y') . ' until ' . Carbon::parse($m->expiry_date)->format('d F Y'),
                    'benefit' => [
                        'gymAccess' => $m->package->gym_access == 1 ? "Yes" : "",
                        'classSessions' => $sessionText,
                        'ptSessions' => $m->pt_session_remaining . ' of ' . $m->pt_session,
                    ]
                ];
            };
        }

        $todayActivity = [];
        $totalDuration = 0;
        $checkMemberLog = MemberLog::where('member_id', $this->data->id)->whereDate('created_at', date('Y-m-d'))->where('branch_id', $branch_id)->get();
        if (count($checkMemberLog) > 0) {
            foreach ($checkMemberLog as $item) {
                $todayActivity[] =  [
                    'id' => $item->id,
                    'time' => ConvertToTimestamp($item->created_at),
                    'message' => $item->message,
                    'label' => Self::statusLog($item->status),
                ];
            }

            // Duration
            $firstTime = MemberLog::where('member_id', $this->data->id)->whereDate('created_at', date('Y-m-d'))->where('branch_id', $branch_id)->orderBy('created_at', 'ASC')->first();
            $lastTime = MemberLog::where('member_id', $this->data->id)->whereDate('created_at', date('Y-m-d'))->where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->first();
            if ($firstTime->id == $lastTime->id) {
                $to = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
            } else {
                $to = Carbon::createFromFormat('Y-m-d H:i:s', $lastTime->created_at);
            }
            $from = Carbon::createFromFormat('Y-m-d H:i:s', $firstTime->created_at);
            $totalDuration = $to->diff($from)->format('%H Hours %i Minutes');
        }

        $lastCheckedIn = NULL;
        $memberAttendance = MemberAttendance::where([
            ['branch_id', 4],
            ['member_id', $this->data->id]
        ])->first();

        if ($memberAttendance) {
            $lastCheckedIn = Carbon::parse($memberAttendance->time)->format('Y-m-d H:i');
        }

        return [
            'id' => $this->data->id,
            'fitID' => $this->data->member_id,
            'firstName' => $this->data->name,
            'lastName' => $this->data->last_name,
            'phoneNumber' => $this->data->user->phone,
            'avatar' => Member::avatarStorage($this->data->avatar),
            'membership' => [
                'status' => $status,
                'message' => $message,
            ],
            'lastCheckedIn' => $lastCheckedIn,
            'attributes' => $this->when($request->url() == route('fc.members.show', [$branch_id, $this->data->id]), [
                'programs' => $programs,
                'totalDuration' => $totalDuration,
                'todayActivity' => $todayActivity,
            ])
        ];
    }

    private function statusLog($status)
    {
        switch ($status) {
            case '1':
                $status = '[CHECK-IN]'; // Check in
                break;

            case '2':
                $status = '[CHECK-OUT]'; // Check out
                break;

            case '3':
                $status = '[CLASS][-1]'; // Booked class
                break;

            case '4':
                $status = '[CLASS]'; // Attend class
                break;

            case '5':
                $status = '[CLASS][+1]'; // Cancel by branch
                break;

            case '6':
                $status = '[CLASS][+1]'; // Cancel by self
                break;

            case '7':
                $status = '[CLASS]'; // Not Show
                break;

            default:
                $status = '[NOT FOUND]';
                break;
        }

        return $status;
    }
}
