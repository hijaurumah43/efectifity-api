<?php

namespace App\Http\Resources\Feature\Fitclub\Member;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberPackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $discount = 0;
        if ($this->transaction->voucher_id != NULL) {
            $potongan = $this->transaction->voucher->discount;
            $price = $this->package->price;
            $discount = ($price * $potongan) / 100;
        }

        $status = true;
        $now = date('Y-m-d H:i:s');
        if ($now > $this->expiry_date) {
            $status = false;
        }

        return [
            'id' => $this->id,
            'program' => $this->package->title,
            'price' => number_format($this->transaction->total_payment, 0),
            'discount' => number_format($discount, 0),
            'datetime' => $this->created_at->format('Y-m-d H.i'),
            'isActive' => $status,
        ];
    }
}
