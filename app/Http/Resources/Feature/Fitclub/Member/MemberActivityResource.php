<?php

namespace App\Http\Resources\Feature\Fitclub\Member;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $program = isset($this->classAttendance) ? $this->classAttendance->memberPackage->package->title : NULL;

        $classes = isset($this->classAttendance) ? $this->classAttendance->scheduleDetail->class->title : NULL;

        $scannedBy = isset($this->classAttendance->user) ? $this->classAttendance->user->member->name : NULL;

        $scannedBy = isset($this->memberAttendance) ? $this->memberAttendance->user->member->name : NULL;

        return [
            'dateTime' => $this->created_at->format('Y-m-d H.i'),
            'activity' => $this->statusLog($this->status),
            'program' => $program,
            'detail' => $classes,
            'scanner' => $scannedBy,
        ];
    }

    private function statusLog($status)
    {
        switch ($status) {
            case '1':
                $status = '[CHECK-IN]'; // Check in
                break;

            case '2':
                $status = '[CHECK-OUT]'; // Check out
                break;

            case '3':
                $status = '[CLASS][-1]'; // Booked class
                break;

            case '4':
                $status = '[CLASS]'; // Attend class
                break;

            case '5':
                $status = '[CLASS][+1]'; // Cancel by branch
                break;

            case '6':
                $status = '[CLASS][+1]'; // Cancel by self
                break;

            case '7':
                $status = '[CLASS]'; // Not Show
                break;

            default:
                $status = '[NOT FOUND]';
                break;
        }

        return $status;
    }
}
