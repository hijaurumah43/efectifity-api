<?php

namespace App\Http\Resources\Feature\Fitclub\Member;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Feature\Fitclub\Member\MemberResource;

class MemberCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MemberResource::collection($this->collection)
        ];
    }
}
