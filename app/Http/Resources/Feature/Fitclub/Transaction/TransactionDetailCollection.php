<?php

namespace App\Http\Resources\Feature\Fitclub\Transaction;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TransactionDetailCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => TransactionDetailResource::collection($this->collection)
        ];
    }
}
