<?php

namespace App\Http\Resources\Feature\Fitclub\Transaction;

use App\Models\Client\Staff\Transaction\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $amountType = '+';
        if ($this->transaction_type_id != 1) {
            $amountType = '-';
        }

        $status = 'pending';
        if ($this->status == 1) {
            $status = 'completed';
        }

        if ($this->is_failed == 1) {
            $status = 'failed';
        }

        $accountNumber = NULL;
        if (isset($this->transaction)) {
            if ($this->transaction->paymentType->methode == 1) {
                $accountNumber = $this->transaction->action_to;
            }
        }

        return [
            'id' => $this->id,
            'order_number' => $this->order_number,
            'transactionType' => $this->type->title,
            'status' => $status,
            'amount' => $amountType . number_format($this->amount),
            'balance' => number_format($this->balanceLog($this->id, $this->branch_id)),
            // NOTE:: CONVERT_DT
            // 'createdAt' => Carbon::parse($this->created_at)->format('d M Y, H:i'),
            'createdAt' => ConvertToTimestamp($this->created_at),
            'channel' => isset($this->transaction) ? $this->transaction->paymentType->paymentMethod->title : NULL,
            'account' => [
                'type' => isset($this->transaction) ? $this->transaction->paymentType->title : NULL,
                'accountNumber' => $accountNumber,
            ],
        ];
    }

    private function balanceLog($id, $branch_id)
    {
        $balance = 0;

        $data = TransactionDetail::where('id', '<', $id)->where('branch_id', $branch_id)->where('is_failed', 0)->take(1)->first();
        if ($data) {

            $collections = TransactionDetail::where('id', '<=', $id)->where('branch_id', $branch_id)->where('is_failed', 0)->orderBy('created_at', 'DESC')->get();
            foreach ($collections as $collection) {
                if ($collection->transaction_type_id == 2 || $collection->transaction_type_id == 3 || $collection->transaction_type_id == 4) {
                    $balance -= $collection->amount;
                } else {
                    $balance += $collection->amount;
                }
            }
        } else {

            $transaction =  TransactionDetail::where('id', $id)->where('is_failed', 0)->first();
            $balance = $transaction->amount;
        }

        return $balance;
    }
}
