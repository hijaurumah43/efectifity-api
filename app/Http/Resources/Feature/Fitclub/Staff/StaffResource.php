<?php

namespace App\Http\Resources\Feature\Fitclub\Staff;

use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;

class StaffResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => Member::avatarStorage($this->member->avatar),

            'firstName' => $this->member->name,
            'lastName' => $this->member->lastName,
            'fitID' => $this->member->member_id,
            'position' => $this->member->user->roles->map(function ($q) {
                return $q->name;
            }),
            'isActive' => $this->status == 2 ? true : false,
        ];
    }
}
