<?php

namespace App\Http\Resources\Feature\Fitclub\Staff;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StaffCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => StaffResource::collection($this->collection)
        ];
    }
}
