<?php

namespace App\Http\Resources\Feature\Fitclub\Classes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClassCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ClassResource::collection($this->collection)
        ];
    }
}
