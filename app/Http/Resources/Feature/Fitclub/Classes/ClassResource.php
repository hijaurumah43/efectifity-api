<?php

namespace App\Http\Resources\Feature\Fitclub\Classes;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Client\Member\Attendances\ClassAttendance;

class ClassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $coaches = [];
        if ($this->class->branchCoach) {
            foreach ($this->class->branchCoach as $data) {
                if ($data->member) {
                    $coaches[] = $data->member->name;
                }
            }
        }

        $duration = 0;
        $from = $this->schedule->date . ' ' . $this->start_time . ':00';
        $to = $this->schedule->date . ' ' . $this->end_time . ':00';
        $from = Carbon::createFromFormat('Y-m-d H:i:s', $from);
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $to);
        $duration = $to->diff($from)->format('%HH %iM');

        $quota = 0;
        $quota = ClassAttendance::where([
            ['class_attendance_status_id', '!=', 3],
            ['branch_schedule_detail_id', $this->id]
        ])->count();

        $isFullBooked = false;
        if ($quota >= $this->max_quota) {
            $isFullBooked = true;
        }

        $isCancelled = false;
        if ($this->is_cancelled == 1) {
            $isCancelled = true;
        }

        return [
            'id' => $this->id,
            'date' => $this->schedule->date,
            'start' => $this->start_time,
            'end' => $this->end_time,
            'duration' => $duration,
            'quota' => $quota . '/' . $this->max_quota,
            'isFullBooked' => $isFullBooked,
            'isCancelled' => $isCancelled,
            'classes' => [
                'id' => $this->class->id,
                'title' => $this->class->title,
                'thumbnail' => ENV('CDN') . '/' . config('cdn.classes') . $this->class->image,
            ],
            'coaches' => $coaches
        ];
    }
}
