<?php

namespace App\Http\Resources\Feature\Fitclub\Classes;

use App\Models\Client\Member\Member;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ClassScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $canceledBy = NULL;
        $scannedBy = NULL;
        $message = NULL;

        // Cancel
        if ($this->class_attendance_status_id == 3) {
            if ($this->note != '') {
                $canceledBy = $this->scheduleDetail->class->branch->title;
                $message = 'This reservation has been cancelled by ' . $canceledBy . ' on ' . $this->cancel_time . ' with reason : ' . $this->note;
            } else {
                $canceledBy = $this->member->name;
                $message = 'This reservation has been cancelled by ' . $canceledBy . ' on ' . $this->cancel_time;
            }
        }

        // Attend
        if ($this->class_attendance_status_id == 2) {
            $scannedBy = $this->user->member->name;
            $message = "You're attend the Class on " . $this->scan_time . ' and scanned by ' . $scannedBy;
        }

        // Passed
        if ($this->class_attendance_status_id == 4) {
            $message = "You're not attend the class";
        }

        // Class Cancel
        $isCancelled = false;
        if ($this->class_attendance_status_id == 3) {
            $isCancelled = true;
        }

        // NOTE:: CONVERT_DT
        return [
            'id' => $this->id,
            'dateTime' => ConvertToTimestamp($this->created_at),
            'scanTime' => $this->scan_time != NULL ? ConvertToTimestamp($this->scan_time) : NULL,
            'cancelTime' => $this->cancel_time != NULL ? ConvertToTimestamp($this->cancel_time) : NULL,
            'scannedBy' => $scannedBy,
            'cancelBy' => $canceledBy,
            'isCancelled' => $isCancelled,
            'member' => [
                'id' => $this->member->id,
                'firstName' => $this->member->name,
                'lastName' => $this->member->last_name,
                'avatar' => Member::avatarStorage($this->avatar),
            ]
        ];

        /* return [
            'id' => $this->id,
            'scanTime' => $this->scan_time != NULL ? Carbon::parse($this->scan_time)->format('Y-m-d H.i') : NULL,
            'cancelTime' => $this->cancel_time != NULL ? Carbon::parse($this->cancel_time)->format('Y-m-d H.i') : NULL,
            'scannedBy' => $scannedBy,
            'cancelBy' => $canceledBy,
            'isCancelled' => $isCancelled,
            'member' => [
                'id' => $this->member->id,
                'firstName' => $this->member->name,
                'lastName' => $this->member->last_name,
                'avatar' => Member::avatarStorage($this->avatar),
            ]
        ]; */
    }
}
