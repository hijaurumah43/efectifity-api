<?php

namespace App\Http\Resources\Internal\Question;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuestionCategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
