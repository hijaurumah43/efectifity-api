<?php

namespace App\Http\Resources\Internal\Question;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuestionGroupCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => QuestionGroupResource::collection($this->collection)
        ];
    }
}
