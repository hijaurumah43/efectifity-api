<?php

namespace App\Http\Resources\Internal\Question;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Internal\Question\QuestionGroupResource;

class QuestionCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => ucwords($this->title),
            'section' => QuestionGroupResource::collection($this->groups)
        ];
    }
}
