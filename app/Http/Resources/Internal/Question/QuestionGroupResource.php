<?php

namespace App\Http\Resources\Internal\Question;

use App\Models\Internal\Question\Question;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Client\Member\Questions\QuestionAnswerResource;

class QuestionGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'collection' => QuestionAnswerResource::collection(Question::where('question_group_id', $this->id)->get())
        ];
    }
}
