<?php

namespace App\Http\Resources\Internal\Branch;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Internal\Branch\DocumentLogResource;

class DocumentLogCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => DocumentLogResource::collection($this->collection)
        ];
    }
}
