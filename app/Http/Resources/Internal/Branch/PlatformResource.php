<?php

namespace App\Http\Resources\Internal\Branch;

use App\Http\Resources\Client\Partner\Branch\PlatformLogResource;
use App\Models\Client\Partner\Branch\BranchStatusLog;
use App\Models\Internal\Branch\BranchFee;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PlatformResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // status
        $status = '';
        $latestStatusDate = '';
        $latestReviewer = '';
        $reviewer = [];

        if ($request->has('status')) {

            $statusLog = $request->get('status');
            $branchLog = BranchStatusLog::where([
                ['branch_id', $this->id],
                ['branch_status_id', $statusLog]
            ])->orderBy('created_at', 'DESC')->first();
        } else {
            $branchLog = BranchStatusLog::where('branch_id', $this->id)->orderBy('id', 'DESC')->first();
        }

        if ($branchLog) {
            $status = ucwords($branchLog->status->title);
            $latestStatusDate = Carbon::parse($branchLog->status->created_at)->format('Y-m-d H:i');
            $latestReviewer =  ucwords($branchLog->user->member->name);

            // NOTE:: status 3 == review
            if ($branchLog->branch_status_id == 2 || $branchLog->branch_status_id == 3 || $branchLog->branch_status_id == 4) {
                $reviewer = [
                    'statusLogID' => $branchLog->branch_status_id,
                    'note' => $branchLog->note
                ];
            }
        }

        $fee = ENV('FEE');
        $branchFee = BranchFee::where([
            ['branch_id', $this->id],
            ['is_active', 1]
        ])->orderBy('created_at', 'DESC')->first();
        if ($branchFee) {
            $fee = $branchFee->fee;
        }

        $legal = $this->company->document->where('service_id', $this->service_id)->first();
        
        return [
            "id" => $this->id,
            "branch" => ucwords($this->title),
            "type" => ucwords($this->service->title),
            "partnerName" => ucwords($this->company->title),
            "latestReviewer" => $latestReviewer,
            "status" => $status,
            "latestStatusDate" => $latestStatusDate,
            "submitDate" => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
            'fee' => $fee . '%',
            'attributes' => $this->when($request->url() ==  route('internal.platforms.show', $this->id), [
                'service' => [
                    'serviceId' => $this->service->id,
                    'serviceTtitle' => $this->service->title,
                    'url' => 'https://' . $this->service->slug . '.efectifity.com'
                ],
                'branchName' => $this->title,
                'address' => $this->address,
                'city' => $this->region,
                'postalCode' => $this->postal_code,
                'logo' => $this->logo != null ? ENV('CDN') . '/' . config('cdn.branchLogo') . $this->logo : null,
                'thumbnail' => $this->thumbnail != null ? ENV('CDN') . '/' . config('cdn.branchThumbnail') . $this->thumbnail : null,
                'location' => [
                    'latitude' => $this->latitude,
                    'longitude' => $this->longitude,
                ],
                'phoneNumber' => $this->phone_number,
                'photo' => [
                    'frontSide' => [
                        'title' => $this->front_side,
                        'link' => $this->front_side != null ? ENV('CDN') . '/' . config('cdn.branchGallery') . $this->front_side : "",
                    ],
                    'inside' => [
                        'title' => $this->inside,
                        'link' => $this->inside != null ? ENV('CDN') . '/' . config('cdn.branchGallery') . $this->inside : "",
                    ],
                ],
                'website' => $this->website,
                'instagram' => $this->instagram,
                'facebook' => $this->facebook,
                'youtube' => $this->youtube,
                'tiktok' => $this->tiktok,
                'reviewer' => $reviewer,
                'fee' => $fee . '%',
                'companyName' => isset($legal) ? $legal->company_name : "",
                'companyFormat' => isset($legal) ? $legal->company_format : "",
                'owner' => isset($legal) ? ucwords($legal->owner_name) : "",
                'email' => isset($legal) ? $legal->email : "",
                'phone' => isset($legal) ? $legal->phone_number : "",
                'identityCard' => $legal->identity_card != null ? ENV('CDN') . '/' . config('cdn.companyDocIdentityCard') . $legal->identity_card : "",
                'taxNumber' => $legal->tax_number != null ? ENV('CDN') . '/' . config('cdn.companyDocTaxNumber') . $legal->tax_number : "",
                'companyRegistration' => $legal->company_registration != null ? ENV('CDN') . '/' . config('cdn.companyDocReg') . $legal->company_registration : "",
                'log' => PlatformLogResource::collection($this->statusLog)
            ])
        ];
    }
}
