<?php

namespace App\Http\Resources\Internal\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Internal\Branch\DocumentLogResource;
use App\Models\Internal\Company\Document\DocumentStatusLog;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        {
            // status
            $status = '';
            $latestStatusDate = '';
            $latestReviewer = '';
            $reviewer = [];
    
            if ($request->has('status')) {
    
                $statusLog = $request->get('status');
                $resource = DocumentStatusLog::where([
                    ['company_document_id', $this->id],
                    ['document_status_id', $statusLog]
                ])->orderBy('created_at', 'DESC')->first();
            } else {
                $resource = DocumentStatusLog::where('company_document_id', $this->id)->orderBy('id', 'DESC')->first();
            }
    
            if ($resource) {
                $status = ucwords($resource->status->title);
                $latestStatusDate = Carbon::parse($resource->status->created_at)->format('Y-m-d H:i');
                $latestReviewer =  ucwords($resource->user->member->name);
    
                // NOTE:: status 3 == review
                if ($resource->document_status_id == 2 || $resource->document_status_id == 3 || $resource->document_status_id == 4) {
                    $reviewer = [
                        'statusLogID' => $resource->document_status_id,
                        'note' => $resource->note
                    ];
                }
            }
    
            return [
                "id" => $this->id,
                "type" => ucwords($this->service->title),
                "companyName" => ucwords($this->company->title),
                "latestReviewer" => $latestReviewer,
                "status" => $status,
                "latestStatusDate" => $latestStatusDate,
                "submitDate" => Carbon::parse($this->created_at)->format('Y-m-d H:i'),
                'attributes' => $this->when($request->url() ==  route('internal.documents.show', $this->id), [
                    'service' => [
                        'serviceId' => $this->service->id,
                        'serviceTtitle' => $this->service->title,
                        'url' => 'https://' . $this->service->slug . '.efectifity.com'
                    ],
                    'companyFormat' => $this->company_format,
                    'ownerName' => $this->owner_name,
                    'email' => $this->email,
                    'phoneNumber' => $this->phone_number,
                    'identityCard' => $this->identity_card != null ? ENV('CDN') . '/' . config('cdn.companyDocIdentityCard') . $this->identity_card : "",
                    'taxNumber' => $this->tax_number != null ? ENV('CDN') . '/' . config('cdn.companyDocTaxNumber') . $this->tax_number : "",
                    'companyRegistration' => $this->company_registration != null ? ENV('CDN') . '/' . config('cdn.companyDocReg') . $this->company_registration : "",
                    'website' => $this->website_url,
                    'instagram' => $this->instagram_url,
                    'facebook' => $this->facebook_url,
                    'youtube' => $this->youtube_url,
                    'tiktok' => $this->tiktok_url,
                    'reviewer' => $reviewer,
                    'log' => DocumentLogResource::collection($this->statusLog)
                ])
            ];
        }
    }
}
