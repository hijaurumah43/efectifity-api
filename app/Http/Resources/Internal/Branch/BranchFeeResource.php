<?php

namespace App\Http\Resources\Internal\Branch;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchFeeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'branchID' => $this->branch_id,
            'period' => Carbon::parse($this->start_date)->format('Y-m-d') . ' to ' . Carbon::parse($this->end_date)->format('Y-m-d'),
            'fee' => $this->fee,
            'isActive' => $this->is_active == 1 ? true : false,
        ];
    }
}
