<?php

namespace App\Http\Resources\Internal\Company;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyTempCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => CompanyTempResource::collection($this->collection)
        ];
    }
}
