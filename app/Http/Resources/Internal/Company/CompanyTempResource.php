<?php

namespace App\Http\Resources\Internal\Company;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyTempResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'agent' => $this->agent,
            'city' => [
                'cityId' => $this->cities->id,
                'cityTitle' => $this->cities->title,
            ],
            'is_activated' => $this->is_activated == 1 ? 'Email Sent' : 'Pending',
        ];
    }
}
