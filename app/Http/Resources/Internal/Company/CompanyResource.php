<?php

namespace App\Http\Resources\Internal\Company;

use Illuminate\Http\Resources\Json\JsonResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'phone' => $this->phone,
            'address' => $this->address,
            'agent' => $this->agent,
            'postalCode' => $this->postal_code,
            'city' => [
                'cityId' => $this->cities->id,
                'cityTitle' => $this->cities->title,
            ],
            'isPublished' => $this->is_published == 1 ? 'active' : 'false',
            'owner' => [
                'name' => isset($this->user) ? $this->user->member->name : '',
                'email' => isset($this->user) ? $this->user->email : '' ,
                'phone' => isset($this->user) ? $this->user->phone : '' ,
            ],
        ];
    }
}
