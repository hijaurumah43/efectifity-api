<?php

namespace App\Http\Resources\Internal\Admin;

use App\Models\Client\Member\Member;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'membership' => $this->member_id,
            'name' => $this->name,
            'lastName' => $this->last_name,
            'avatar' => Member::avatarStorage($this->avatar),
            'phone' => $this->user->phone,
            'email' => $this->user->email,
            'scope' => isset($this->user->roles[0]) ? $this->user->roles[0]->name : 'no scope',
        ];
    }
}
