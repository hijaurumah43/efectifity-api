<?php

namespace App\Http\Resources\Internal\Settings\Club;

use Illuminate\Http\Resources\Json\JsonResource;

class ClubInformationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image != NULL ? ENV('CDN') . '/' . config('cdn.clubSettingInformation') . $this->image : null,
            'isPublished' => $this->is_published,
        ];
    }
}
