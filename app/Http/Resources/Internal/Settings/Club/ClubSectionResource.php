<?php

namespace App\Http\Resources\Internal\Settings\Club;

use Illuminate\Http\Resources\Json\JsonResource;

class ClubSectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'isStatus' => $this->status,
        ];
    }
}
