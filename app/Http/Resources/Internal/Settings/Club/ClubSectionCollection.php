<?php

namespace App\Http\Resources\Internal\Settings\Club;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClubSectionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ClubSectionResource::collection($this->collection)
        ];
    }
}
