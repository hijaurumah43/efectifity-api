<?php

namespace App\Http\Resources\Internal\Settings\Club;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClubServiceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ClubServiceResource::collection($this->collection)
        ];
    }
}
