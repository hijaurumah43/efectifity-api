<?php

namespace App\Http\Resources\Internal\Settings\Classes;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ClassCategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ClassCategoryResource::collection($this->collection)
        ];
    }
}
