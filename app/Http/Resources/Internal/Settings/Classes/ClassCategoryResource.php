<?php

namespace App\Http\Resources\Internal\Settings\Classes;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'thumbnail' => $this->thumbnail != NULL ? ENV('CDN') . '/' . config('cdn.classSettingCategory') . $this->thumbnail : null,
            'isPublished' => $this->is_published,
        ];
    }
}
