<?php

namespace App\Http\Resources\Internal\Settings\Classes;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassLevelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'isActive' => $this->is_active,
            'createdAt' => $this->created_at
        ];
    }
}
