<?php

namespace App\Services;

use App\Models\Client\Member\Classes\MemberBranch;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;

class MemberBranchService {
    private $memberBranchRepository;
    
    /**
     * @param MemberBranchRepository $memberBranchRepository
     */
    public function __construct(MemberBranchRepository $memberBranchRepository)
    {
        $this->memberBranchRepository = $memberBranchRepository;        
    }

    public function update(MemberBranch $memberBranch, array $data)
    {
        $this->memberBranchRepository->update($memberBranch, $data);
    }
}