<?php

namespace App\Services;

use App\Contracts\LoginHistoryRepository;
use App\Models\User;

class LoginHistoryService
{
    private $loginHistoryRepository;

    public function __construct(LoginHistoryRepository $loginHistoryRepository)
    {
        $this->loginHistoryRepository = $loginHistoryRepository;
    }

    public function store(User $user, array $data)
    {
        $this->loginHistoryRepository->store($data);
    }
}
