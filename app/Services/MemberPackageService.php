<?php

namespace App\Services;

use App\Models\Client\Member\Programs\MemberPackage;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;

class MemberPackageService 
{
    private $memberPackageRepository;

    /**
     * @param MemberPackageRepository $memberPackageRepository
     */
    public function __construct(MemberPackageRepository $memberPackageRepository)
    {
        $this->memberPackageRepository = $memberPackageRepository;        
    }

    /**
     * Increment session remaining of resource
     *
     * @param MemberPackage $memberPackage
     */
    public function incrementSession(MemberPackage $memberPackage)
    {
        $memberPackage->increment('session_remaining', 1);
        $memberPackage->save();    
    }

    /**
     * Decrement session remaining of resource
     *
     * @param MemberPackage $memberPackage
     */
    public function decrementSession(MemberPackage $memberPackage)
    {
        $memberPackage->decrement('session_remaining', 1);
        $memberPackage->save();    
    }
}
