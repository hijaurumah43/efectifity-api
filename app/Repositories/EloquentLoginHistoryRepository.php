<?php

namespace App\Repositories;

use App\Contracts\LoginHistoryRepository;

class EloquentLoginHistoryRepository extends EloquentRepository implements LoginHistoryRepository
{
}
