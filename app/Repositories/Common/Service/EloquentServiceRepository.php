<?php

namespace App\Repositories\Common\Service;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Service\ServiceRepository;

class EloquentServiceRepository extends EloquentRepository implements ServiceRepository
{
    
}