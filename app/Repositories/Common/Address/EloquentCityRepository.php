<?php

namespace App\Repositories\Common\Address;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Address\CityRepository;

class EloquentCityRepository extends EloquentRepository implements CityRepository
{
    
}
