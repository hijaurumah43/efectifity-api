<?php

namespace App\Repositories\Common\Branch;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Branch\BranchClassRepository;

class EloquentBranchClassRepository extends EloquentRepository implements BranchClassRepository
{
    
}
