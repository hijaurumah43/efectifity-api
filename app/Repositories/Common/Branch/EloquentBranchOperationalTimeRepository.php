<?php

namespace App\Repositories\Common\Branch;

use Spatie\QueryBuilder\QueryBuilder;
use App\Repositories\EloquentRepository;
use App\Models\Common\Branch\BranchOperationalTime;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Common\Branch\BranchOperationalTimeRepository;

class EloquentBranchOperationalTimeRepository extends EloquentRepository implements BranchOperationalTimeRepository
{
    private $defaultSort = 'id';

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(BranchOperationalTime::class)
            ->defaultSort($this->defaultSort)
            ->where($criteria)
            ->paginate($perPage);
    }
}