<?php

namespace App\Repositories\Common\Branch;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Common\Branch\BranchSchedule;
use App\Contracts\Common\Branch\BranchScheduleRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentBranchScheduleRepository extends EloquentRepository implements BranchScheduleRepository
{
    private $defaultSort = 'date';

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(BranchSchedule::class)
            ->allowedFilters([
                AllowedFilter::scope('date'),
                AllowedFilter::scope('fromTo'),
            ])
            ->defaultSort($this->defaultSort)
            ->where($criteria)
            ->where('is_published', 1)
            ->paginate($perPage);
    }
}