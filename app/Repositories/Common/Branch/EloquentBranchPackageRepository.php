<?php

namespace App\Repositories\Common\Branch;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Branch\BranchPackageRepository;

class EloquentBranchPackageRepository extends EloquentRepository implements BranchPackageRepository
{
    
}
