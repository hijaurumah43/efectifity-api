<?php

namespace App\Repositories\Common\Branch;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Branch\BranchScheduleDetailRepository;

class EloquentBranchScheduleDetailRepository extends EloquentRepository implements BranchScheduleDetailRepository
{
    
}
