<?php

namespace App\Repositories\Common\Branch;

use App\Models\Common\Branch\Branch;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Contracts\Common\Branch\BranchRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentBranchRepository extends EloquentRepository implements BranchRepository
{
    private $defaultSort = 'id';

    private $allowedSorts = [
        'title',
        'created_at',
    ];

    private $defaultSelect = [
       
    ];

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(Branch::class)
            ->allowedFilters([
                AllowedFilter::scope('services'),
                AllowedFilter::scope('location'),
                AllowedFilter::scope('club'),
                AllowedFilter::scope('price'),
                AllowedFilter::scope('budget'),
                AllowedFilter::scope('offer'),
                AllowedFilter::scope('rating'),
                AllowedFilter::scope('hours'),
            ])
            ->allowedSorts($this->allowedSorts)
            ->defaultSort($this->defaultSort)
            ->where($criteria)
            ->where('is_published', 1)
            ->paginate($perPage);
    }
}
