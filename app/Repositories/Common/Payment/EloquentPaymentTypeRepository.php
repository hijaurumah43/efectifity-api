<?php

namespace App\Repositories\Common\Payment;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Payment\PaymentTypeRepository;

class EloquentPaymentTypeRepository extends EloquentRepository implements PaymentTypeRepository
{

}
