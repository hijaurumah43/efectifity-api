<?php

namespace App\Repositories\Common\Payment;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Payment\PaymentMethodRepository;

class EloquentPaymentMethodRepository extends EloquentRepository implements PaymentMethodRepository
{
    
}

