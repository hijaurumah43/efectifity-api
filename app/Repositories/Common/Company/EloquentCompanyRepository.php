<?php

namespace App\Repositories\Common\Company;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Company\CompanyRepository;

class EloquentCompanyRepository extends EloquentRepository implements CompanyRepository
{
    
}
