<?php

namespace App\Repositories\Common\Company;

use App\Repositories\EloquentRepository;
use App\Contracts\Common\Company\CompanyServiceRepository;

class EloquentCompanyServiceRepository extends EloquentRepository implements CompanyServiceRepository
{
    
}
