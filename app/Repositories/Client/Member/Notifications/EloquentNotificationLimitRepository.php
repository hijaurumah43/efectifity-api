<?php

namespace App\Repositories\Client\Member\Notifications;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\Notifications\NotificationLimitRepository;

class EloquentNotificationLimitRepository extends EloquentRepository implements NotificationLimitRepository
{
    
}
