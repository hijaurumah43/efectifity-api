<?php

namespace App\Repositories\Client\Member\Transactions;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Client\Member\Transactions\Transaction;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Client\Member\Transactions\TransactionRepository;

class EloquentTransactionRepository extends EloquentRepository implements TransactionRepository
{
    private $defaultSort = '-created_at';

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(Transaction::class)
            ->allowedFilters([
                AllowedFilter::scope('status'),
            ])
            ->defaultSort($this->defaultSort)
            ->where($criteria)
            ->where('is_published', 1)
            ->paginate($perPage);
    }
}
