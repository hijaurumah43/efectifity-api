<?php

namespace App\Repositories\Client\Member\Transactions;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\Transactions\TransactionStatusRepository;

class EloquentTransactionStatusRepository extends EloquentRepository implements TransactionStatusRepository
{
    
}
