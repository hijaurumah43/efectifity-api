<?php

namespace App\Repositories\Client\Member\Classes;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\Classes\MemberBranchRepository;

class EloquentMemberBranchRepository extends EloquentRepository implements MemberBranchRepository
{
    
}
