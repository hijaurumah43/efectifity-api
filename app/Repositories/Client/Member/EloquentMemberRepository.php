<?php

namespace App\Repositories\Client\Member;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\MemberRepository;

class EloquentMemberRepository extends EloquentRepository implements MemberRepository
{
    
}
