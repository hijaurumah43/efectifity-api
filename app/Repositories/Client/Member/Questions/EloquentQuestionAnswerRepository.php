<?php

namespace App\Repositories\Client\Member\Questions;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Client\Member\Questions\QuestionAnswer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Client\Member\Questions\QuestionAnswerRepository;

class EloquentQuestionAnswerRepository extends EloquentRepository implements QuestionAnswerRepository
{
    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(QuestionAnswer::class)
            ->allowedFilters([
                AllowedFilter::scope('category'),
            ])
            ->where($criteria)
            ->where('question_answers.is_active', 1)
            ->paginate($perPage);
    }
}