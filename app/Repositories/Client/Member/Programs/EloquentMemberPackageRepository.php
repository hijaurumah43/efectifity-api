<?php

namespace App\Repositories\Client\Member\Programs;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Client\Member\Programs\MemberPackageRepository;

class EloquentMemberPackageRepository extends EloquentRepository implements MemberPackageRepository
{
    private $defaultSort = 'id';

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $user = auth()->user();
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(MemberPackage::class)
            ->allowedFilters([
                AllowedFilter::scope('status'),
                AllowedFilter::scope('branch'),
            ])
            ->defaultSort($this->defaultSort)
            ->where('member_id', $user->member->id)
            ->paginate($perPage);
    }
}