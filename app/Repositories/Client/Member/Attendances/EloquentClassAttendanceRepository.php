<?php

namespace App\Repositories\Client\Member\Attendances;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Client\Member\Attendances\ClassAttendance;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Client\Member\Attendances\ClassAttendanceRepository;

class EloquentClassAttendanceRepository extends EloquentRepository implements ClassAttendanceRepository
{
    protected $defaultSort = '-created_at';

    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(ClassAttendance::class)
            ->allowedFilters([
                AllowedFilter::scope('status'),
            ])
            ->defaultSort($this->defaultSort)
            ->where($criteria)
            ->paginate($perPage);
    }
}
