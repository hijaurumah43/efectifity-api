<?php

namespace App\Repositories\Client\Member\Settings;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\Settings\DeliveryAddressRepository;

class EloquentDeliveryAddressRepository extends EloquentRepository implements DeliveryAddressRepository
{

}
