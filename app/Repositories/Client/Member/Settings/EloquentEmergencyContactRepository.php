<?php

namespace App\Repositories\Client\Member\Settings;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Member\Settings\EmergencyContactRepository;

class EloquentEmergencyContactRepository extends EloquentRepository implements EmergencyContactRepository
{
    
}
