<?php

namespace App\Repositories\Client\Partner\Company;

use App\Contracts\Client\Partner\Company\CompanyDocumentRepository;
use App\Repositories\EloquentRepository;

class EloquentCompanyDocumentRepository extends EloquentRepository implements CompanyDocumentRepository
{
    
}