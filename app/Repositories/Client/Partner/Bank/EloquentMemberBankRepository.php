<?php

namespace App\Repositories\Client\Partner\Bank;

use App\Contracts\Client\Partner\Bank\MemberBankRepository;
use App\Repositories\EloquentRepository;

class EloquentMemberBankRepository extends EloquentRepository implements MemberBankRepository
{
    
}
