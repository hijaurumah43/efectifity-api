<?php

namespace App\Repositories\Client\Staff\Attendance;

use App\Contracts\Client\Staff\Attendance\StaffAttendanceRepository;
use App\Repositories\EloquentRepository;

class EloquentStaffAttendanceRepository extends EloquentRepository implements StaffAttendanceRepository
{
    
}
