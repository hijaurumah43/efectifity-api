<?php

namespace App\Repositories\Client\Staff\Attendance;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Staff\Attendance\MemberAttendanceRepository;

class EloquentMemberAttendanceRepository extends EloquentRepository implements MemberAttendanceRepository
{
    
}