<?php

namespace App\Repositories\Client\Staff;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleRepository;

class EloquentStaffScheduleRepository extends EloquentRepository implements StaffScheduleRepository
{
    
}
