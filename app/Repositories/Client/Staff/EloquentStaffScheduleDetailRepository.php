<?php

namespace App\Repositories\Client\Staff;

use App\Repositories\EloquentRepository;
use App\Contracts\Client\Staff\Schedule\StaffScheduleDetailRepository;

class EloquentStaffScheduleDetailRepository extends EloquentRepository implements StaffScheduleDetailRepository
{
    
}
