<?php

namespace App\Repositories\Internal\Question;

use App\Repositories\EloquentRepository;
use App\Contracts\Internal\Question\QuestionDetailRepository;

class EloquentQuestionDetailRepository extends EloquentRepository implements QuestionDetailRepository
{
    
}
