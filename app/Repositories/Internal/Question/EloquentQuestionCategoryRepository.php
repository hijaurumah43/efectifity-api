<?php

namespace App\Repositories\Internal\Question;

use Spatie\QueryBuilder\QueryBuilder;
use App\Repositories\EloquentRepository;
use App\Models\Internal\Question\QuestionCategory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Contracts\Internal\Question\QuestionCategoryRepository;

class EloquentQuestionCategoryRepository extends EloquentRepository implements QuestionCategoryRepository
{
    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(QuestionCategory::class)
            ->allowedFilters(['id'])
            ->where($criteria)
            ->where('is_published', 1)
            ->paginate($perPage);
    }
}