<?php

namespace App\Repositories\Internal\Question;

use App\Repositories\EloquentRepository;
use App\Contracts\Internal\Question\QuestionTypeRepository;

class EloquentQuestionTypeRepository extends EloquentRepository implements QuestionTypeRepository
{
    
}
