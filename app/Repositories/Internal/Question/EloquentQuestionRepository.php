<?php

namespace App\Repositories\Internal\Question;

use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use App\Repositories\EloquentRepository;
use App\Models\Internal\Question\Question;
use App\Contracts\Internal\Question\QuestionRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentQuestionRepository extends EloquentRepository implements QuestionRepository
{
    public function findByFilters(array $criteria = NULL): LengthAwarePaginator
    {
        $perPage = (int)request()->get('limit');
        $perPage = $perPage >= 1 && $perPage < 100 ? $perPage : 20;

        return QueryBuilder::for(Question::class)
            ->allowedFilters([
                AllowedFilter::scope('category'),
            ])
            ->where($criteria)
            ->where('is_published', 1)
            ->paginate($perPage);
    }
}
