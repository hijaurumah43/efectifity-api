<?php

namespace App\Repositories\Internal\Question;

use App\Repositories\EloquentRepository;
use App\Contracts\Internal\Question\QuestionGroupRepository;

class EloquentQuestionGroupRepository extends EloquentRepository implements QuestionGroupRepository
{
    
}
