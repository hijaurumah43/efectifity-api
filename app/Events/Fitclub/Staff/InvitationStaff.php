<?php

namespace App\Events\Fitclub\Staff;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InvitationStaff
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $data;
    public $messageNotification;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $messageNotification)
    {
        $this->data = $data;
        $this->messageNotification = $messageNotification;
    }
}
