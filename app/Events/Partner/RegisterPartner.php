<?php

namespace App\Events\Partner;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RegisterPartner
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $companyTemp;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($companyTemp)
    {
        $this->companyTemp = $companyTemp;
    }
}
