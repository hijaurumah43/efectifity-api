<?php

namespace App\Models\Common\Payment;

use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Payment\PaymentType;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaymentMethod extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'is_active',
    ];

    public function paymentType()
    {
        return $this->hasMany(PaymentType::class, 'methode');
    }
}
