<?php

namespace App\Models\Common\Payment;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    use HasFactory;

    protected $fillable = [
        'methode',
        'title',
        'channel_code',
        'vendor_id',
        'image',
        'is_published',
    ];

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'methode');
    }
}
