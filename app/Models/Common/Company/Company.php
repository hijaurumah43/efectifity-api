<?php

namespace App\Models\Common\Company;

use App\Models\Client\Partner\Company\CompanyDocument;
use App\Models\Common\Address\City;
use App\Models\Common\Branch\Branch;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'phone',
        'address',
        'agent',
        'postal_code',
        'city_id',
        'is_published',
        'is_active',
    ];

    public function cities()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function branch()
    {
        return $this->hasMany(Branch::class, 'company_id');
    }

    public function document()
    {
        return $this->hasMany(CompanyDocument::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
