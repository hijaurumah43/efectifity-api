<?php

namespace App\Models\Common\Branch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchClassCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'thumbnail',
        'is_published',
    ];
}
