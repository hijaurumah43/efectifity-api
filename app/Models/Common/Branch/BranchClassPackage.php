<?php

namespace App\Models\Common\Branch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchClassPackage extends Model
{
    use HasFactory;
}
