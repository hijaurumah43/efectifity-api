<?php

namespace App\Models\Common\Branch;

use Carbon\Carbon;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Common\Branch\BranchScheduleDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchSchedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'class_schedule_id',
        'date',
        'timestamp',
        'is_published',
    ];

    public function detail()
    {
        return $this->hasMany(BranchScheduleDetail::class, 'branch_schedule_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function scopeFromTo(Builder $query, $start_at, $end_at = ''): Builder
    {
        if($end_at != '') {
            
            return $query->whereBetween('date', [$start_at, $end_at]);
            
        } else {
            return $query->whereDate('date', '=', Carbon::parse($start_at));
        }
    }

    public function scopeDate(Builder $query, $date): Builder
    {
        if($date != '') {
            
            $query = $query->where('date', $date);
            
        }

        return $query;
    }
}
