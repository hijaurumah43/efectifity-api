<?php

namespace App\Models\Common\Branch;

use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Branch\BranchClassCoach;
use App\Models\Common\Branch\BranchClassLevel;
use App\Models\Common\Branch\BranchClassCategory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchClass extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'branch_id',
        'title',
        'image',
        'branch_class_category_id',
        'delay_time',
        'max_book_time',
        'max_cancel_time',
        'branch_class_level_id',
        'is_online',
        'description',
        'is_published',
    ];

    public function branchCoach()
    {
        return $this->hasMany(BranchClassCoach::class, 'branch_class_id');
    }

    public function level()
    {
        return $this->belongsTo(BranchClassLevel::class, 'branch_class_level_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function category()
    {
        return $this->belongsTo(BranchClassCategory::class, 'branch_class_category_id');
    }
}
