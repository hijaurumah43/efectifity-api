<?php

namespace App\Models\Common\Branch;

use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Branch\BranchClass;
use App\Models\Common\Branch\BranchSchedule;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchScheduleDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_schedule_id',
        'branch_class_id',
        'start_time',
        'end_time',
        'start_timestamp',
        'end_timestamp',
        'max_quota',
        'is_quota',
        'url_class',
        'password_class',
        'is_cancelled',
        'is_published',
    ];

    public function class()
    {
        return $this->belongsTo(BranchClass::class, 'branch_class_id');
    }

    public function schedule()
    {
        return $this->belongsTo(BranchSchedule::class, 'branch_schedule_id');
    }
}
