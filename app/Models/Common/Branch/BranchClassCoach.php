<?php

namespace App\Models\Common\Branch;

use App\Models\Client\Member\Member;
use App\Models\Common\Coach\Coach;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchClassCoach extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_class_id',
        'coach_id',
        'member_id',
        'is_published',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class, 'coach_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
