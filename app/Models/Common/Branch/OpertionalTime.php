<?php

namespace App\Models\Common\Branch;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class OpertionalTime extends Model
{
    use HasFactory;

    protected $table = 'operational_times';

    protected $fillable = [
        'branch_id',
        'title',
        'month',
        'year',
        'created_by',
        'status',
    ];
    
    public function user() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function detail()
    {
        return $this->hasMany(BranchOperationalTime::class, 'operational_time_id');
    }
}
