<?php

namespace App\Models\Common\Branch;

use Illuminate\Support\Facades\DB;
use App\Models\Common\Company\Company;
use App\Models\Common\Service\Service;
use App\Models\Client\Staff\Rules\Rule;
use Illuminate\Database\Eloquent\Model;
use App\Models\Internal\Voucher\Voucher;
use App\Models\Common\Branch\BranchClass;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Common\Branch\BranchGallery;
use App\Models\Common\Branch\BranchPackage;
use App\Models\Common\Branch\BranchRequire;
use App\Models\Common\Branch\BranchCategory;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Client\Staff\Rules\BranchRule;
use App\Models\Common\Service\ServiceCategory;
use App\Contracts\Common\Service\ServiceRepository;
use App\Models\Common\Branch\BranchOperationalTime;
use App\Models\Client\Partner\Branch\BranchStatusLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Branch extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'company_id',
        'service_id',
        'title',
        'slug',
        'phone_number',
        'address',
        'logo',
        'thumbnail',
        'front_side',
        'inside',
        'website',
        'whatsapp',
        'instagram',
        'facebook',
        'youtube',
        'tiktok',
        'latitude',
        'longitude',
        'description',
        'postal_code',
        'region',
        'is_published',
        'is_active',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function packages()
    {
        return $this->hasMany(BranchPackage::class);
    }

    public function scopeBudget(Builder $query)
    {
        $query =  $this->packages()->min('price')->whereBetween('price', [10, 100]);

        return $query;
    }

    public function operationalTimes()
    {
        return $this->hasMany(BranchOperationalTime::class, 'branch_id');
    }

    public function branchCategories()
    {
        return $this->hasMany(BranchCategory::class, 'branch_id');
    }

    public function branchClass()
    {
        return $this->hasMany(BranchClass::class, 'branch_id');
    }

    public function branchGalleries()
    {
        return $this->hasMany(BranchGallery::class, 'branch_id');
    }

    public function schdules()
    {
        return $this->hasMany(BranchSchedule::class, 'branch_id');
    }

    public function required()
    {
        return $this->hasMany(BranchRequire::class, 'branch_id');
    }

    public function statusLog()
    {
        return $this->hasMany(BranchStatusLog::class, 'branch_id');
    }

    public function branchAmenities()
    {
        return $this->hasMany(BranchAmenity::class, 'branch_id');
    }

    public function barnchAdditionalInformation()
    {
        return $this->hasMany(BranchAdditionalInformation::class, 'branch_id');
    }

    public function vouchers()
    {
        return $this->hasMany(Voucher::class, 'branch_id');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('branches.is_published', 1);
    }

    public function scopeServices(Builder $query, $args = ''): Builder
    {
        $service = app(ServiceRepository::class);
        $collect = $service->findOneBy(['slug' => $args]);

        if ($collect) {
            $query = $query->where('service_id', $collect->id);
        }

        return $query;
    }

    public function scopeLocation(Builder $query, $latitude, $longitude = '', $rangeDistance = 50): Builder
    {
        return $query->select(
            DB::raw('*, ( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(latitude) ) ) ) AS distance')
        )->having('distance', '<', $rangeDistance); //->orderBy('distance', 'ASC')
    }

    static function setDistance($latitude, $longitude, $branch_id)
    {
        $data = DB::select('
            SELECT
                ( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians(latitude) ) ) ) AS distance 
            FROM
                branches
            WHERE
                id = ' . $branch_id . '
            LIMIT 1
        ');

        $data = count($data) > 0 ? (float) $data[0]->distance : 0;

        return $data;
    }

    /* public function scopeOrderPrice(Builder $query): Builder
    {
        return $query->select(
            DB::raw('*, MIN(branch_packages.price) as price')
        )->orderBy('price', 'ASC');
    } */

    public function scopeClub(Builder $query)
    {
        return $this->branchCategories->whereIn('service_category_id', 1);
    }

    static function rules($branchID, $type = NULL)
    {
        $data = [];
        if ($type != NULL) {
            $collections = BranchRule::where('type', $type)->where('branch_id', $branchID)->get();

            if (count($collections) < 1) {
                $collections = Rule::where('type', $type)->get();
            }

            foreach ($collections as $collection) {

                $data[] = [
                    'id' => isset($collection->rule) ? $collection->rule_id : $collection->id,
                    'title' => isset($collection->title) ? $collection->title : $collection->rule->title,
                    'duration' => $collection->duration,
                    'period' => $collection->period,
                ];
            }
        } else {
            $collections = BranchRule::where('branch_id', $branchID)->get();

            if (count($collections) < 1) {
                $collections = Rule::all();
            }

            foreach ($collections as $collection) {

                $data[] = [
                    'id' => isset($collection->rule) ? $collection->rule_id : $collection->id,
                    'title' => isset($collection->title) ? $collection->title : $collection->rule->title,
                    'duration' => $collection->duration,
                    'period' => $collection->period,
                ];
            }
        }

        return $data;
    }
}
