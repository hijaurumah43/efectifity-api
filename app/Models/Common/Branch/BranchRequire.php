<?php

namespace App\Models\Common\Branch;

use App\Models\Internal\Question\QuestionCategory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchRequire extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'question_category_id',
        'status',
    ];

    public function questionCategory()
    {
        return $this->belongsTo(QuestionCategory::class, 'question_category_id');
    }
}
