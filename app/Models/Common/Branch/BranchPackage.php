<?php

namespace App\Models\Common\Branch;

use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchPackage extends Model
{
    use HasFactory;

    protected $fillable  = [
        'id',
        'branch_id',
        'title',
        'gym_access',
        'duration',
        'periode',
        'pt_session',
        'session',
        'description',
        'about',
        'benefit',
        'policy',
        'price',
        'is_published',
        'is_unlimited',
        'is_promo',
        'promo_id',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
