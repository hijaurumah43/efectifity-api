<?php

namespace App\Models\Common\Branch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchOperationalTime extends Model
{
    use HasFactory;

    protected $fillable = [
        'operational_time_id',
        'branch_id',
        'title',
        'date',
        'note',
        'start_time',
        'end_time',
        'status'
    ];

    public function operationalTime()
    {
        return $this->belongsTo(OpertionalTime::class, 'operational_time_id');
    }
}
