<?php

namespace App\Models\Common\Branch;

use App\Models\Internal\Additional\AdditionalInformation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchAdditionalInformation extends Model
{
    use HasFactory;

    protected $table = 'branch_additional_informations';
    protected $fillable = [
        'id',
        'branch_id',
        'additional_information_id',
        'is_published',
    ];

    public function additionalInformation()
    {
        return $this->belongsTo(AdditionalInformation::class);
    }
}
