<?php

namespace App\Models\Common\Branch;

use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Service\ServiceCategory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'service_category_id',
    ];

    public function serviceCategories()
    {
        return $this->belongsTo(ServiceCategory::class);
    }
}
