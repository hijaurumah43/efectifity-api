<?php

namespace App\Models\Common\Branch;

use App\Models\Internal\Amenity\Amenity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchAmenity extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'branch_id',
        'amenity_id',
        'is_published',
    ];

    public function amenity()
    {
        return $this->belongsTo(Amenity::class);
    }
}
