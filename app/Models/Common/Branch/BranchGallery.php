<?php

namespace App\Models\Common\Branch;

use App\Models\Client\Staff\Settings\Section;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchGallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'section_id',
        'title',
        'name',
        'type',
        'member_id',
        'is_active',
    ];

    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }
}
