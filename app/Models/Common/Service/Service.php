<?php

namespace App\Models\Common\Service;

use App\Models\Client\Partner\Company\CompanyDocument;
use Illuminate\Database\Eloquent\Model;
use App\Models\Common\Service\ServiceCategory;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'is_published',
        'is_active',
    ];

    public function categories()
    {
        return $this->hasMany(ServiceCategory::class, 'service_id');
    }

    public function companyDocument()
    {
        return $this->hasMany(CompanyDocument::class, 'service_id');
    }
}
