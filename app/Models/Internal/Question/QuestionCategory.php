<?php

namespace App\Models\Internal\Question;

use Illuminate\Database\Eloquent\Model;
use App\Models\Internal\Question\Question;
use App\Models\Internal\Question\QuestionGroup;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QuestionCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'type',
        'is_published',
        'is_active',
    ];

    public function groups()
    {
        return $this->hasMany(QuestionGroup::class, 'question_category_id');
    }

    public function question()
    {
        return $this->hasMany(Question::class, 'question_category_id');
    }
}
