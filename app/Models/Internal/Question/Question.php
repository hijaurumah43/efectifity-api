<?php

namespace App\Models\Internal\Question;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Internal\Question\QuestionType;
use App\Models\Internal\Question\QuestionDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'question_category_id',
        'question_type_id',
        'title',
        'with_note',
        'is_published',
        'is_active',
    ];

    public function details()
    {
        return $this->hasMany(QuestionDetail::class, 'question_id');
    }

    public function type()
    {
        return $this->belongsTo(QuestionType::class, 'question_type_id');
    }

    public function scopeCategory(Builder $query, $args = ''): Builder
    {
        if(isset($args)) {
            return $query->where('question_category_id', $args);
        } else {
            return $query;
        }
    }
}
