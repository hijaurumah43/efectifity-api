<?php

namespace App\Models\Internal\Question;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'question_id',
        'title',
        'is_published',
    ];
}
