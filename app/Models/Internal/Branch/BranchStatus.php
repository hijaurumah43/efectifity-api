<?php

namespace App\Models\Internal\Branch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'is_active'
    ];
}
