<?php

namespace App\Models\Internal\Branch;

use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchFee extends Model
{
    use HasFactory;

    protected $table = 'branch_fee';
    protected $fillable = [
        'branch_id',
        'fee',
        'start_date',
        'end_date',
        'is_active',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
