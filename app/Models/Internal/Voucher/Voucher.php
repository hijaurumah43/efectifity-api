<?php

namespace App\Models\Internal\Voucher;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'title',
        'image',
        'type',
        'branch_id',
        'qty',
        'discount',
        'expired',
        'is_published',
    ];
}
