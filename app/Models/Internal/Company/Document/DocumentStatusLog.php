<?php

namespace App\Models\Internal\Company\Document;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Internal\Company\Document\DocumentStatus;

class DocumentStatusLog extends Model
{
    use HasFactory;

    protected $table = 'company_document_logs';

    protected $fillable = [
        'id',
        'company_document_id',
        'document_status_id',
        'user_id',
        'note',
    ];

    public function status()
    {
        return $this->belongsTo(DocumentStatus::class, 'document_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
