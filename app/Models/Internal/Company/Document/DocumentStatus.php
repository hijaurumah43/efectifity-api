<?php

namespace App\Models\Internal\Company\Document;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentStatus extends Model
{
    use HasFactory;

    protected $table = 'document_statuses';

    protected $fillable = [
        'title',
        'is_active'
    ];
}
