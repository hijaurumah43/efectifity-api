<?php

namespace App\Models\Internal\Additional;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdditionalInformation extends Model
{
    use HasFactory;

    protected $table = 'additional_informations';
    protected $fillable = [
        'id',
        'title',
        'image',
        'description',
        'is_published',
    ];
}
