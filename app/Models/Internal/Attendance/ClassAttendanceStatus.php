<?php

namespace App\Models\Internal\Attendance;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassAttendanceStatus extends Model
{
    use HasFactory;
}
