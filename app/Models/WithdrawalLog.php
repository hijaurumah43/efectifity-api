<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawalLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'order_number',
        'account_holder_name',
        'account_number',
        'bank_code',
        'amount',
        'old_values',
        'callback_values',
        'callback_at',
        'is_callback',
        'is_published',
    ];
}
