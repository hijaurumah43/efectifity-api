<?php

namespace App\Models;

use Laratrust\Models\LaratrustPermission;

class Permission extends LaratrustPermission
{
    public $guarded = [];
    public $incrementing = false;

    protected $fillable = [
        'id',
        'name',
        'display_name',
        'description'
    ];
}
