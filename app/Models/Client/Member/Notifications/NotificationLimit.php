<?php

namespace App\Models\Client\Member\Notifications;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationLimit extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'value'
    ];
}
