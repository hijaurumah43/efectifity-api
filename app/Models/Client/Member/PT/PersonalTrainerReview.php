<?php

namespace App\Models\Client\Member\PT;

use App\Models\Client\Member\Member;
use App\Models\Client\Staff\PersonalTrainer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalTrainerReview extends Model
{
    use HasFactory;

    protected $fillable = [
        'personal_trainer_id',
        'member_id',
        'star',
        'review',
        'status',
    ];

    public function pt()
    {
        return $this->belongsTo(PersonalTrainer::class, 'personal_trainer_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
