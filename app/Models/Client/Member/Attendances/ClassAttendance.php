<?php

namespace App\Models\Client\Member\Attendances;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Common\Branch\BranchSchedule;
use App\Models\Common\Branch\BranchScheduleDetail;
use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Internal\Attendance\ClassAttendanceStatus;

class ClassAttendance extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'member_id',
        'member_package_id',
        'branch_id',
        'branch_schedule_id',
        'branch_schedule_detail_id',
        'class_attendance_status_id',
        'scan_time',
        'cancel_time',
        'note',
        'is_active',
        'is_cancellation',
        'user_id',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function scheduleDetail()
    {
        return $this->belongsTo(BranchScheduleDetail::class, 'branch_schedule_detail_id');
    }

    public function schedule()
    {
        return $this->belongsTo(BranchSchedule::class, 'branch_schedule_id');
    }

    public function attendanceStatus()
    {
        return $this->belongsTo(ClassAttendanceStatus::class, 'class_attendance_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function memberPackage()
    {
        return $this->belongsTo(MemberPackage::class, 'member_package_id');
    }

    public function scopeStatus(Builder $query, $args = ''): Builder
    {
        switch ($args) {
            case 'active':
                $query = $query->where([
                    ['class_attendance_status_id', 1],
                    ['is_active', 1]
                ]);
                break;

            case 'past':
                $query = $query->where([
                    ['class_attendance_status_id', '>', 1],
                    // ['is_active', 0]
                ]);
                break;

            default:
                $query;
                break;
        }

        return $query;
    }

    static function quotaCancelled($user, $memberPackage)
    {
        $isBlock = 0;
        $maxCancelled = 3;
        $maxBloacked = 7; // TODO:: MANUAL

        if ($user->member->block_reservation_at != NULL) {

            $subDays = Carbon::parse($user->member->block_reservation_at)->subMinutes(3)->format('Y-m-d H:i:s');
            $addDays = Carbon::parse($user->member->block_reservation_at)->addDays($maxBloacked)->format('Y-m-d H:i:s');

            if (date('Y-m-d H:i:s') <= $addDays) {

                $classAttendance = ClassAttendance::where([
                    ['member_id', $user->member->id],
                    ['branch_id', $memberPackage->branch_id],
                    ['class_attendance_status_id', 3],
                    ['is_cancellation', 0],
                ])
                    ->where('cancel_time', '>=', $subDays)
                    ->where('cancel_time', '<=', $addDays)
                    ->count();

                if ($classAttendance >= $maxCancelled) {
                    $isBlock = 1;
                }
            }
        }
        /* else {

            $subDays = Carbon::now()->subDays(7)->format('Y-m-d H:i:s');

            $classAttendance = ClassAttendance::where([
                ['member_id', $user->member->id],
                ['branch_id', $memberPackage->branch_id],
                ['class_attendance_status_id', 3],
            ])
                ->where('created_at', '>=', $subDays)
                ->count();

            if ($classAttendance >= $maxCancelled) {
                $isBlock = 1;
            }
        } */

        /* if ($isBlock == 1) {
            $user = Member::where('id', $user->member->id)->first();
            $user->block_reservation_at = date('Y-m-d H:i:s');
            $user->save();
        } */

        return $isBlock;
    }
}
