<?php

namespace App\Models\Client\Member\Questions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Internal\Question\Question;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class QuestionAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'question_id',
        'response',
        'note',
        'is_published',
        'is_active',
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function scopeCategory(Builder $query, $args = ''): Builder
    {
        if(isset($args)) {
            return $query->rightJoin('questions', 'questions.id', '=', 'question_answers.question_id')->where('questions.question_category_id', $args);
        } else {
            return $query;
        }
    }
}
