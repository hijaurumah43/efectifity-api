<?php

namespace App\Models\Client\Member\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryAddress extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'label',
        'receiver',
        'phone_number',
        'address',
        'zip_code',
        'latitude',
        'longitude',
        'is_published',
        'is_active',
    ];
}
