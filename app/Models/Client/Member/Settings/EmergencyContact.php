<?php

namespace App\Models\Client\Member\Settings;

use App\Models\Client\Member\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmergencyContact extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'member_id',
        'label',
        'name',
        'phone_number',
        'relation',
        'is_published',
        'is_active',
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
