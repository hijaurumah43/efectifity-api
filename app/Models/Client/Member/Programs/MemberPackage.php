<?php

namespace App\Models\Client\Member\Programs;

use App\Models\Client\Member\Member;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Client\Partner\Branch\BranchAgreement;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MemberPackage extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'id',
        'member_id',
        'transaction_id',
        'expiry_date',
        'branch_id',
        'branch_package_id',
        'branch_agreement_id',
        'session',
        'session_remaining',
        'pt_session',
        'pt_session_remaining',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function package()
    {
        return $this->belongsTo(BranchPackage::class, 'branch_package_id');
    }

    public function agreement()
    {
        return $this->belongsTo(BranchAgreement::class, 'branch_agreement_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function scopeStatus(Builder $query, $args = ''): Builder
    {
        switch ($args) {
            case 'active':
                $query = $query->where('expiry_date', '>=', date('Y-m-d H:i:s'));
                break;

            case 'expired':
                $query = $query->where('expiry_date', '<', date('Y-m-d H:i:s'));
                break;

            default:
                return $query;
                break;
        }

        return $query;
    }

    public function scopeBranch(Builder $query, $args = ''): Builder
    {
        return $query->where('branch_id', $args);
    }
}
