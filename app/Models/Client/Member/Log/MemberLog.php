<?php

namespace App\Models\Client\Member\Log;

use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Client\Staff\Attendance\MemberAttendance;
use App\Models\Client\Member\Attendances\ClassAttendance;
use App\Models\Client\Staff\PersonalTrainerAttendance;

class MemberLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'branch_id',
        'personal_trainer_attendance_id',
        'class_attendance_id',
        'member_package_id',
        'member_attendance_id',
        'status',
        'message',
        'is_published',
    ];

    public function classAttendance()
    {
        return $this->belongsTo(ClassAttendance::class, 'class_attendance_id');
    }

    public function personalAttendance()
    {
        return $this->belongsTo(PersonalTrainerAttendance::class, 'personal_trainer_attendance_id');
    }

    public function memberAttendance()
    {
        return $this->belongsTo(MemberAttendance::class, 'member_attendance_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
