<?php

namespace App\Models\Client\Member;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Models\Client\Member\Log\MemberLog;
use App\Models\Client\Member\Programs\MemberPackage;
use App\Models\Client\Member\Questions\QuestionAnswer;
use App\Models\Client\Member\Settings\DeliveryAddress;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Client\Member\Settings\EmergencyContact;
use App\Models\Client\Staff\PersonalTrainer;

class Member extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'user_id',
        'member_id',
        'name',
        'last_name',
        'avatar',
        'block_reservation_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function deliveryAddress()
    {
        return $this->hasMany(DeliveryAddress::class, 'member_id');
    }

    public function emergencyContact()
    {
        return $this->hasMany(EmergencyContact::class, 'member_id');
    }

    public function personalInformation()
    {
        return $this->hasMany(QuestionAnswer::class, 'member_id');
    }

    public function memberPackage()
    {
        return $this->hasMany(MemberPackage::class, 'member_id');
    }

    public function memberLog()
    {
        return $this->hasMany(MemberLog::class, 'member_id');
    }

    public function pt()
    {
        return $this->hasMany(PersonalTrainer::class, 'member_id');
    }

    public function location($branch_id, $latitude, $longitude)
    {
        return DB::select("
            SELECT 
                ( 6371 * acos( cos( radians('.$latitude.') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians(latitude) ) ) ) AS distance
            FROM 
                branches 
            WHERE 
                id = '.$branch_id.'
            HAVING distance < 10
        ");
    }

    static function avatarStorage($file)
    {
        if(substr($file, 0, 4) == 'http') {
            
            return $file;    

        } else if ($file != null) {

            return ENV('CDN') . '/avatars/' . $file;
            
        } else {
            return null;
        }
    }
}
