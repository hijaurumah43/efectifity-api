<?php

namespace App\Models\Client\Member\Transactions;

use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Service\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Common\Payment\PaymentType;
use App\Models\Common\Branch\BranchPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Client\Member\Transactions\TransactionStatus;
use App\Contracts\Client\Member\Transactions\TransactionStatusRepository;
use App\Models\Client\Staff\PersonalTrainer;
use App\Models\Internal\Voucher\Voucher;

class Transaction extends Model
{
    use HasFactory;

    public $incrementing = false;

    protected $fillable = [
        'id',
        'order_number',
        'member_id',
        'service_id',
        'branch_id',
        'branch_agreement_id',
        'branch_package_id',
        'transaction_status_id',
        'payment_type_id',
        'branch_schedule_detail_id',
        'voucher_id',
        'personal_trainer_id',
        'total_payment',
        'channels',
        'action_to',
        'old_values',
        'callback_values',
        'callback_at',
        'is_callback',
        'is_published',
        'is_active',
    ];
    
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function pt()
    {
        return $this->belongsTo(PersonalTrainer::class, 'personal_trainer_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    public function branchPackage()
    {
        return $this->belongsTo(BranchPackage::class);
    }

    public function transactionStatus()
    {
        return $this->belongsTo(TransactionStatus::class);
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class);
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    public function agreement()
    {
        return $this->belongsTo(BranchAgreement::class, 'branch_agreement_id');
    }

    public function scopeStatus(Builder $query, $args = ''): Builder
    {
        $service = app(TransactionStatusRepository::class);
        $collect = $service->findOneBy(['title' => strtolower($args)]);
        
        if($collect) {
            $query = $query->where('transaction_status_id', $collect->id);
        } 

        return $query;
    }
}
