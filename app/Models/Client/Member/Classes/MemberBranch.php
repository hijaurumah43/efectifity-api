<?php

namespace App\Models\Client\Member\Classes;

use App\Models\Client\Member\Member;
use App\Models\Client\Staff\StaffSchedule;
use App\Models\Client\Staff\StaffScheduleDetail;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MemberBranch extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'branch_id',
        'checkin',
        'checkout',
        'status',
        'type',
        'is_published',
        'is_active',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    public function schedule()
    {
        return $this->hasMany(StaffSchedule::class, 'member_branch_id');
    }

    public function scheduleDetail()
    {
        return $this->hasMany(StaffScheduleDetail::class, 'member_branch_id');
    }
}
