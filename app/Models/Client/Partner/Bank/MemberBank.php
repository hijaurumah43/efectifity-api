<?php

namespace App\Models\Client\Partner\Bank;

use App\Models\Internal\Bank\Bank;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MemberBank extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'bank_id',
        'address',
        'account_holder',
        'account_number',
        'is_published',
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }
}
