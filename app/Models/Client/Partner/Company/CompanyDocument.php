<?php

namespace App\Models\Client\Partner\Company;

use App\Models\Common\Company\Company;
use App\Models\Common\Service\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Internal\Company\Document\DocumentStatusLog;

class CompanyDocument extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'company_documents';

    protected $fillable = [
        'id',
        'user_id',
        'service_id',
        'company_id',
        'company_name',
        'company_format',
        'owner_name',
        'email',
        'phone_number',
        'identity_card',
        'tax_number',
        'company_registration',
        'website_url',
        'instagram_url',
        'facebook_url',
        'youtube_url',
        'tiktok_url',
        'status',
        'is_published',
        'is_active',
    ];

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function statusLog()
    {
        return $this->hasMany(DocumentStatusLog::class, 'company_document_id');
    }
}
