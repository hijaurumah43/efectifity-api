<?php

namespace App\Models\Client\Partner\Temporary;

use App\Models\Common\Address\City;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CompanyTemp extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'id',
        'title',
        'name',
        'email',
        'phone',
        'address',
        'agent',
        'city_id',
        'is_activated',
    ];

    public function cities()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
