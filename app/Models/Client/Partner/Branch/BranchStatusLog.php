<?php

namespace App\Models\Client\Partner\Branch;

use App\Models\Internal\Branch\BranchStatus;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchStatusLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'branch_id',
        'branch_status_id',
        'user_id',
        'note',
    ];

    public function status()
    {
        return $this->belongsTo(BranchStatus::class, 'branch_status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
