<?php

namespace App\Models\Client\Partner\Branch;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchAgreement extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'branch_id',
        'version',
        'content',
        'is_draft',
        'is_published',
    ];
}
