<?php

namespace App\Models\Client\Staff\Settings;

use App\Models\Common\Branch\BranchGallery;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchGallerySection extends Model
{
    use HasFactory;

    protected $fillable  = [
        'title',
        'status',
    ];
}
