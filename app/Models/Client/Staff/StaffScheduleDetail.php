<?php

namespace App\Models\Client\Staff;

use App\Models\Client\Member\Classes\MemberBranch;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffScheduleDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'member_branch_id',
        'schedule_id',
        'shift_id',
        'staff_schedule_id',
        'title',
        'start_time',
        'end_time',
        'start_timestamp',
        'end_timestamp',
        'description',
        'is_published',
    ];

    public function shift()
    {
        return $this->belongsTo(Shift::class, 'shift_id');
    }

    public function staffSchedule()
    {
        return $this->belongsTo(StaffSchedule::class, 'staff_schedule_id');
    }

    public function memberBranch()
    {
        return $this->belongsTo(MemberBranch::class, 'member_branch_id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
