<?php

namespace App\Models\Client\Staff;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffScheduleShift extends Model
{
    use HasFactory;

    protected $fillable = [
        'shift_id',
        'staff_schedule_id',
        'title',
        'start_time',
        'end_time',
        'is_active'
    ];

    public function scheduleDetail()
    {
        return $this->hasMany(StaffScheduleDetail::class, 'staff_schedule_shift_id');
    }
}
