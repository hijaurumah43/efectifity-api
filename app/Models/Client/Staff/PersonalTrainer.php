<?php

namespace App\Models\Client\Staff;

use App\Models\Client\Member\Member;
use App\Models\Client\Member\PT\PersonalTrainerReview;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalTrainer extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'member_id',
        'quota',
        'duration',
        'qualifications',
        'specialities',
        'image',
        'status',
        'is_published',
    ];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function ptMember()
    {
        return $this->hasMany(PersonalTrainerMember::class, 'personal_trainer_id');
    }

    public function reviews()
    {
        return $this->hasMany(PersonalTrainerReview::class, 'personal_trainer_id');
    }
}
