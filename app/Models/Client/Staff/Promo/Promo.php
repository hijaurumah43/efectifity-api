<?php

namespace App\Models\Client\Staff\Promo;

use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Branch\BranchPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'type',
        'branch_id',
        'branch_package_id',
        'member_type',
        'title',
        'duration',
        'periode',
        'gym_access',
        'session',
        'pt_session',
        'price',
        'start_time',
        'end_time',
        'status',
        'is_unlimited',
        'member_id',
    ];

    public function package()
    {
        return $this->belongsTo(BranchPackage::class, 'branch_package_id');
    }

    public function memebr()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
