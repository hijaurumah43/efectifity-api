<?php

namespace App\Models\Client\Staff;

use App\Models\User;
use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Client\Staff\PersonalTrainerAttendanceStatus;

class PersonalTrainerAttendance extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $fillable = [
        'personal_trainer_id',
        'member_id',
        'member_package_id',
        'branch_id',
        'date',
        'start_time',
        'end_time',
        'start_timestamp',
        'end_timestamp',
        'personal_trainer_attendance_status_id',
        'scan_time',
        'cancel_time',
        'note',
        'is_active',
        'user_id',
    ];

    public function pt()
    {
        return $this->belongsTo(PersonalTrainer::class, 'personal_trainer_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function memberPackage()
    {
        return $this->belongsTo(MemberPackage::class, 'member_package_id');
    }

    public function status()
    {
        return $this->belongsTo(PersonalTrainerAttendanceStatus::class, 'personal_trainer_attendance_status_id');
    }

    public function scopeStatus(Builder $query, $args = ''): Builder
    {
        switch ($args) {
            case 'active':
                $query = $query->where([
                    ['personal_trainer_attendance_status_id', 1],
                    ['is_active', 1]
                ]);
                break;

            case 'past':
                $query = $query->where([
                    ['personal_trainer_attendance_status_id', '>', 1],
                    ['is_active', 0]
                ]);
                break;

            default:
                $query;
                break;
        }

        return $query;
    }
}
