<?php

namespace App\Models\Client\Staff;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffAttendance extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'member_branch_id',
        'time',
        'status',
    ];
}
