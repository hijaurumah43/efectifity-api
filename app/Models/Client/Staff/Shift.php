<?php

namespace App\Models\Client\Staff;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Shift extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'title',
        'start_time',
        'end_time',
        'created_by',
        'status',
    ];

    public function user() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function scheduleDetail() {
        return $this->hasMany(StaffScheduleDetail::class, 'shift_id');
    }

    public function scopeStatus(Builder $query, $status) {
        
        if($status == 1) {
            $query =  $query->where('status', 1);
        } else if($status == 0) {
            $query =  $query->where('status', 0);
        } else {
            return $query;
        }

    }
}
