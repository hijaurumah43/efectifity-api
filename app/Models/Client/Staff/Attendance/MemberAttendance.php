<?php

namespace App\Models\Client\Staff\Attendance;

use App\Models\Client\Member\Member;
use App\Models\Common\Branch\Branch;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MemberAttendance extends Model
{
    use HasFactory;

    protected $fillable = [
        'member_id',
        'branch_id',
        'time',
        'status',
        'scanned_by',
    ];

    protected $dates = ['time'];

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'scanned_by');
    }
}
