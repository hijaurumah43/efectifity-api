<?php

namespace App\Models\Client\Staff;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    use HasFactory;

    protected $table = 'staffs';
    public $incrementing = false;

    protected $fillable = [
        'name',
        'user_id',
    ];
}
