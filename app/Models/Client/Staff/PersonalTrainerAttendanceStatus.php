<?php

namespace App\Models\Client\Staff;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalTrainerAttendanceStatus extends Model
{
    use HasFactory;

    protected $table = 'personal_trainer_attendance_statuses';
    
}
