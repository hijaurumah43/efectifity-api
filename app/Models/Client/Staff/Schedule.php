<?php

namespace App\Models\Client\Staff;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'title',
        'month',
        'year',
        'created_by',
        'status'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function detail()
    {
        return $this->hasMany(StaffScheduleDetail::class, 'schedule_id');
    }
}
