<?php

namespace App\Models\Client\Staff;

use App\Models\Client\Member\Classes\MemberBranch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaffSchedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'schedule_id',
        'member_branch_id',
        'date',
        'is_published'
    ];

    public function detail()
    {
        return $this->hasMany(StaffScheduleDetail::class, 'staff_schedule_id');
    }

    public function memberBranch()
    {
        return $this->belongsTo(MemberBranch::class, 'member_branch_id');
    }
}
