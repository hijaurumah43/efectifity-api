<?php

namespace App\Models\Client\Staff;

use App\Models\Client\Member\Member;
use App\Models\Client\Member\Programs\MemberPackage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalTrainerMember extends Model
{
    use HasFactory;

    protected $table = 'personal_trainer_member';
    protected $fillable = [
        'id',
        'personal_trainer_id',
        'member_id',
        'member_package_id',
        'status'
    ];

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function pt()
    {
        return $this->belongsTo(PersonalTrainer::class, 'personal_trainer_id');
    }

    public function memberPackage()
    {
        return $this->belongsTo(MemberPackage::class, 'member_package_id');
    }
}
