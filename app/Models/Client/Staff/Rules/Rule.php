<?php

namespace App\Models\Client\Staff\Rules;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'sequence',
        'duration',
        'period',
        'type',
        'description',
        'is_active',
        'is_show',
        'is_published',
    ];
}
