<?php

namespace App\Models\Client\Staff\Rules;

use App\Models\Common\Branch\Branch;
use App\Models\Client\Staff\Rules\Rule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BranchRule extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'rule_id',
        'duration',
        'period',
        'type',
        'is_active',
        'is_show',
        'is_published',
    ];

    public function rule()
    {
        return $this->belongsTo(Rule::class, 'rule_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
