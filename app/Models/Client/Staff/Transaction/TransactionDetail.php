<?php

namespace App\Models\Client\Staff\Transaction;

use App\Models\Client\Member\Member;
use App\Models\Client\Member\Transactions\Transaction;
use App\Models\Common\Branch\Branch;
use App\Models\Common\Company\Company;
use App\Models\Common\Service\Service;
use App\Models\WithdrawalLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id',
        'order_number',
        'member_id',
        'branch_id',
        'transaction_type_id',
        'status',
        'amount',
        'is_failed',
        'is_published',
        'service_id',
        'withdrawal_log_id',
    ];

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function type()
    {
        return $this->belongsTo(TransactionType::class, 'transaction_type_id');
    }

    public function wd()
    {
        return $this->belongsTo(WithdrawalLog::class, 'withdrawal_log_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }

    static function balance($branch_id)
    {
        $balance = 0;

        $income = TransactionDetail::where([
            ['branch_id', $branch_id],
            ['status', 1]
        ])
            ->whereIn('transaction_type_id', [1])
            ->sum('amount');

        $outcome = TransactionDetail::where('branch_id', $branch_id)->whereIn('transaction_type_id', [2, 3, 4, 5])->where('is_failed', 0)->sum('amount');

        $balance = $income - $outcome;
        if ($balance < 1) {
            $balance = 0;
        }
        return $balance;
    }

    static function balanceCompany()
    {
        $balance = 0;
        $user = auth()->user();
        $company = Company::where('user_id', $user->id)->first();
        if (!$company) {
            return $balance;
        }

        $branches = Branch::where('company_id', $company->id)->select('id')->get();
        $branchID = [];
        foreach ($branches as $branch) {
            $branchID[] = $branch->id;
        }

        $income = TransactionDetail::whereIn('branch_id', [$branchID])->where([
            ['status', 1]
        ])
            ->whereIn('transaction_type_id', [1])
            ->sum('amount');

        $outcome = TransactionDetail::whereIn('branch_id', [$branchID])->whereIn('transaction_type_id', [2, 3, 4, 5])->where('is_failed', 0)->sum('amount');

        $balance = $income - $outcome;
        if ($balance < 1) {
            $balance = 0;
        }
        return $balance;
    }
}
