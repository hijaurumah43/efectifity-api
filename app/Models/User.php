<?php

namespace App\Models;

use OwenIt\Auditing\Auditable;
use App\Models\Client\Member\Member;
use App\Models\Client\Partner\Company\CompanyDocument;
use App\Models\Common\Company\Company;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use OwenIt\Auditing\Contracts\Auditable as OwenItAuditable;

class User extends Authenticatable implements JWTSubject, OwenItAuditable, MustVerifyEmail
{
    use Auditable;
    use LaratrustUserTrait;
    use HasFactory, Notifiable;

    public $incrementing = false;

    protected $keyType = 'string';

    protected $fillable = [
        'username',
        'email',
        'email_token_confirmation',
        'email_token_disable_account',
        'email_verified_at',
        'password',
        'provider',
        'pin',
        'pin_token_confirmation',
        'phone_token_confirmation',
        'phone',
        'phone_verified',
        'username_change_at',
        'fcm_token',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'id' => 'string',
        'email_verified_at' => 'datetime',
        'is_active' => 'boolean',
    ];

    /**
     * {@inheritdoc}
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function notifications()
    {
        return $this->morphMany(DatabaseNotification::class, 'notifiable')
            ->whereNotNull('read_at')
            ->orderByDesc('created_at');
    }

    public function member()
    {
        return $this->hasOne(Member::class, 'user_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'user_id');
    }

    public function companyDocument()
    {
        return $this->hasMany(CompanyDocument::class, 'user_id');
    }

    public function roleUser()
    {
        return $this->hasMany(RoleUser::class, 'user_id');
    }
}
