<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    public $guarded = [];
    public $incrementing = false;

    public const SUPERADMIN = 'superadministrator';
    public const ADMIN = 'administrator';
    public const OWNER = 'owner';
    public const PARTNER = 'partner';
    public const MANAGER = 'manager';
    public const STAFF = 'staff';
    public const MEMBER = 'member';
}
