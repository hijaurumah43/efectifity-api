<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model implements \OwenIt\Auditing\Contracts\Audit
{
    use \OwenIt\Auditing\Audit;

    public const UPDATED_AT = null;

    protected static $unguarded = true;

    public $incrementing = false;

    protected $keyType = 'string';

    protected $casts = [
        'id'         => 'string',
        'old_values' => 'json',
        'new_values' => 'json',
    ];

    public function getTable(): string
    {
        return 'audits';
    }

    public function getConnectionName()
    {
        return config('database.default');
    }
}
