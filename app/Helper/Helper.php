<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

function getIp()
{
    $ip = geoip()->getLocation($ip = request()->ip())->timezone;
    return $ip;
}

function ConvertToTimestamp($datetime)
{
    $dt = Carbon::parse($datetime);
    // return $dt->timestamp;
    return intdiv((int) $dt->format('Uu'), 1000);
}

function ConvertToTimeServer($datetime, $type = NULL)
{
    /* https://www.py4u.net/discuss/24187
    I am trying to get a formatted date, including the microseconds from a UNIX timestamp specified in milliseconds.
    The only problem is I keep getting 000000, e.g. */

    $dt = Carbon::parse($datetime / 1000);
    switch ($type) {
        case '1':
            $result = $dt->toDateString();
            break;

        case '2':
            $result = $dt->toDateTimeString();
            break;

        default:
            $result = $dt->toDateTimeString();
            break;
    }
    return $result;
}

function ConvertToUTC($dateTime)
{
    // $tz = $tt->created_at;  // "2019-01-16 18:21:31" (UTC Time)
    $date = Carbon::createFromFormat('Y-m-d H:i:s', $dateTime, 'UTC');
    return $date;
}

function statusStaff($status)
{
    switch ($status) {
        case '1':
            $status = 'pending';
            break;

        case '2':
            $status = 'accepted';
            break;

        case '3':
            $status = 'rejected';
            break;

        case '4':
            $status = 'cancelled';
            break;

        case '5':
            $status = 'expired';
            break;

        case '6':
            $status = 'inactive';
            break;

        default:
            $status = 'not found';
            break;
    }

    return $status;
}

function sendPhoneNotification($to, $content)
{
    $resStatus = 'false';
    // Change to +62
    if (substr($to, 0, 1) == '0') {
        $from = '/' . preg_quote(substr($to, 0, 1), '/') . '/';
        $to = preg_replace($from, '62', $to, 1);
    } else if (substr($to, 0, 2) == '62') {
        $to = $to;
    } else {
        $to = '62' . $to;
    }

    // $to       = str_replace('+', '', $to);
    $from     = "EFECTIFITY"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
    $message  = "Halo Fitters! Jangan informasikan angka berikut ini kepada siapapun : " . $content . " - Layanan CS : 021-29385381";
    $getUrl   = "https://api.tcastsms.net/api/v2/SendSMS?";
    $apiUrl   = $getUrl . 'SenderId=' . $from . '&Message=' . rawurlencode($message) . '&MobileNumbers=' . $to . '&ApiKey=' . ENV('TCASTAPIKEY') . '&ClientId=' . ENV('TCASTCLIENTID');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json',
            'Accept:application/json'
        )
    );

    $response = curl_exec($ch);
    $responseBody = json_decode($response, true);
    curl_close($ch);

    $data = $responseBody['Data'];
    if ($data[0]['MessageId'] != "") {
        $resStatus = 'true';
    };

    return $resStatus;
}

function ptAttendanceStatus($id)
{
    switch ($id) {
        case '1':
            $status = 'upcoming';
            break;

        case '2':
            $status = 'attended';
            break;

        case '3':
            $status = 'not attended';
            break;

        case '4':
            $status = 'reschedule';
            break;

        case '5':
            $status = 'cancelled';
            break;

        case '6':
            $status = 'started';
            break;

        case '7':
            $status = 'pending';
            break;

        case '8':
            $status = 'rejected';
            break;

        case '9':
            $status = 'expired';
            break;

        default:
            $status = 'not found';
            break;
    }

    return $status;
}

function attendanceStatus($id)
{
    switch ($id) {
        case '1':
            $status = 'upcoming';
            break;

        case '2':
            $status = 'attended';
            break;

        case '3':
            $status = 'cancelled';
            break;

        case '4':
            $status = 'not attended';
            break;

        default:
            $status = 'not found';
            break;
    }

    return $status;
}

function promoteMemberType($id)
{
    switch ($id) {
        case '1':
            $status = 'all member';
            break;

        case '2':
            $status = 'new member';
            break;

        case '3':
            $status = 'return member';
            break;

        default:
            $status = 'not found';
            break;
    }

    return $status;
}

function promoteStatus($id)
{
    switch ($id) {
        case '0':
            $status = 'draft';
            break;

        case '1':
            $status = 'active';
            break;

        case '2':
            $status = 'inactive';
            break;

        default:
            $status = 'not found';
            break;
    }

    return $status;
}

function ruleNamePeriode($id)
{
    $name = NULL;
    switch ($id) {
        case '1':
            $name = 'Minute';
            break;

        case '2':
            $name = 'Hour';
            break;

        case '3':
            $name = 'Day';
            break;

        case '4':
            $name = 'Month';
            break;

        case '5':
            $name = 'Year';
            break;

        default:
            $name = 'Times';
            break;
    }

    return $name;
}

function sendNotificationFirebase($customerFcmToken, $body, $dataBody)
{
    $headers = array(
        'Authorization: key=' . ENV('FIREBASETOKEN'),
        'Content-Type: application/json'
    );

    $data = array(
        "to" => $customerFcmToken,
        'notification' => [
            "title" => 'EFECTIFITY',
            "body" => $body,
        ],
        "data" => $dataBody,
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    $result = curl_exec($ch);
    curl_close($ch);
}

function uploadAssets($file, $path)
{
    $imageFileName = NULL;
    if ($file) {
        $imageFileName  = microtime(true) . '.' . $file->getClientOriginalExtension();
        Storage::disk('sftp')
            ->put(
                config($path) . $imageFileName,
                file_get_contents($file->getRealPath())
            );
        Storage::disk('sftp')->setVisibility(config($path), 'public');
    }

    return $imageFileName;
}
