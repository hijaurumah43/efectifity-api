<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RegisterPartnerEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $companyTemp;
    private $token;

    public function __construct($companyTemp, $token)
    {
        $this->companyTemp = $companyTemp;
        $this->token = $token;
        $this->onQueue('notifications');
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->markdown('emails.partner.register', [
                'data'   => $this->companyTemp,
                'token'  => $this->token,
            ])
            ->from("no-reply@efectifity.com", "EFECTIFITY")
            ->subject('Konfirmasi FitPartner / FitPartner Confirmation');
    }
        
}
