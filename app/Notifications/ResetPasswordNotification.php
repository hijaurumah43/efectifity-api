<?php

namespace App\Notifications;

use App\Mail\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as Notification;

class ResetPasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(string $token)
    {
        parent::__construct($token);
        $this->onQueue('notifications');
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * {@inheritdoc}
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->markdown('emails.member.resetPassword', [
                'data'   => $this->token,
                'email'  => $notifiable->email,
            ])
            ->from("no-reply@efectifity.com", "EFECTIFITY")
            ->subject('Reset Password / Password Reset');
    }
}
