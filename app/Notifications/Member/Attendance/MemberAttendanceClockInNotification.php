<?php

namespace App\Notifications\Member\Attendance;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MemberAttendanceClockInNotification extends Notification
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'member-attendance-clockin',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'member_attendance_id' => $this->data->member_attendance_id,
                'status' => 1,
                'label' => '[CHECK-IN]',
                'message' => $this->data->message,
            ],
            'slug' => NULL,
            'param' => NULL,
        ];
    }
}
