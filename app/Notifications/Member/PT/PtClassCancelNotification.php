<?php

namespace App\Notifications\Member\PT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PtClassCancelNotification extends Notification
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'pt-class-cancel',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'class_attendance_id' => $this->data->personal_trainer_attendance_id,
                'status' => 5,
                'label' => '[PT CLASS][+1]',
                'message' => $this->data->message,
            ],
            'slug' => 'upcoming',
            'param' => [
                'key' => 'status',
                'value' => 'expired',
            ],
        ];
    }
}
