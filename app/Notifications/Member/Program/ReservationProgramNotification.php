<?php

namespace App\Notifications\Member\Program;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReservationProgramNotification extends Notification
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'program-pending',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'transaction_id' => $this->data->id,
                'label' => 'transaction',
                'message' => 'You have awaiting transaction for IDR ' . $this->data->total_payment . ' - tap to finish the payment',
            ],
            'slug' => 'transaction',
            'param' => [
                'key' => 'transantion-status',
                'value' => $this->data->transactionStatus->title
            ],
        ];
    }
}
