<?php

namespace App\Notifications\Fitclub\PT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PTReservationNotification extends Notification
{
    use Queueable;

    public $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'pt-reservation',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'personal_trainer_attendance_id' => $this->data->personal_trainer_attendance_id,
                'status' => 7,
                'label' => '[PT]',
                'message' => $this->data->message,
            ],
            'slug' => 'pt-reservation',
            'param' => [
                'key' => 'pt-reservation-id',
                'value' => $this->data->personal_trainer_attendance_id,
            ],
        ];
    }
}
