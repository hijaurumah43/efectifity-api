<?php

namespace App\Notifications\Fitclub\Staff;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvitationStaffNotification extends Notification
{
    use Queueable;

    public $data;
    public $messageNotification;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data, $messageNotification)
    {
        $this->data = $data;
        $this->messageNotification = $messageNotification;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'staff-invitation',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'member_branch_id' => $this->data->id,
                'label' => 'invitation',
                'message' => $this->messageNotification,
            ]
        ];
    }
}
