<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->onQueue('notifications');
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $data = $this->user;
        $name = (string)request()->get('name');
        $data['name'] = $name != '' ? $name : "FITTERS";

        return (new MailMessage())
            ->markdown('emails.member.register', [
                'data'   => $data,
            ])
            ->from("no-reply@efectifity.com", "EFECTIFITY")
            ->subject('Verifkasi Email / Email Verification');
    }
        
}
