<?php

namespace App\Notifications\Partner;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ForgotPasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $user;
    private $token;

    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
        $this->onQueue('notifications');
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->markdown('emails.partner.resetPassword', [
                'data'   => $this->user,
                'token'  => $this->token,
            ])
            ->from("no-reply@efectifity.com", "EFECTIFITY")
            ->subject('Reset Password / Password Reset');
    }
        
}
