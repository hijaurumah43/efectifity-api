<?php

namespace App\Notifications\PT;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SessionDeductNotification extends Notification
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => 'pt-session-deduct',
            'data' => [
                'branch_id' => $this->data->branch_id,
                'member_id' => $this->data->member_id,
                'client_attendance_id' => $this->data->id,
                'status' => '',
                'label' => '[PT][OTP]',
                'message' => 'Share this OTP ' . $this->data->pin . ' to your Personal Trainer only. This action will deduct your PT Session.',
            ],
            'slug' => '',
            'param' => [
                'key' => '',
                'value' => '',
            ],
        ];
    }
}
