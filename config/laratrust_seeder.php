<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => true,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'gilgamesh' => [
            'fitclub' => '1,2,3,4,5,6',
            'app' => 'a1,a2,a3'
        ],
        'superadmin' => [
            'fitclub' => '1,2,3,4,5,6',
            'app' => 'a1,a2,a3'
        ],
        'admin' => [
            'fitclub' => '',
        ],
        'partner' => [
            'fitclub' => '1,2,3,4,5,6',
            'app' => 'a1,a2,a3'
        ],
        'manager' => [
            'fitclub' => '1,2,3,4,5,6',
            'app' => 'a1,a2,a3'
        ],
        'customer_service' => [
            'fitclub' => '1,2,3,4',
            'app' => 'a1,a2,a3',
        ],
        'sales_marketing' => [
            'fitclub' => '2',
            'app' => 'a1,a3',
        ],
        'finance_accounting' => [
            'fitclub' => '4,5',
            'app' => 'a1,a3',
        ],
        'personal_trainer' => [
            'app' => 'a1,a2,a3',
        ],
        'coach' => [
            'app' => 'a1,a2,a3',
        ],
        'staff' => [
            'fitclub' => '1,2,3,4',
            'app' => 'a1,a2,a3',
        ],
        'member' => [
            'fitclub' => '',
        ]
    ],

    'permissions_map' => [
        '1' => 'dashboard',
        '2' => 'member',
        '3' => 'class',
        '4' => 'staff',
        '5' => 'transaction',
        '6' => 'settings',
        'a1' => 'attendance',
        'a2' => 'scanning',
        'a3' => 'schedule',
    ]
];
