<?php

return [
    'memberAvatar' => 'avatars/',
    'classes' => 'classes/',
    'pt' => 'pt/',
    'companyDocIdentityCard' => 'company/doc/identity/',
    'companyDocTaxNumber' => 'company/doc/tax/',
    'companyDocReg' => 'company/doc/register/',
    'branchGallery' => 'branch/gallery/',
    'branchLogo' => 'branch/logo/',
    'branchThumbnail' => 'branch/thumbnail/',
    'classSettingCategory' => 'assets/classes/category/',
    'clubSettingService' => 'assets/club/service/',
    'clubSettingAmenity' => 'assets/club/amenity/',
    'clubSettingInformation' => 'assets/club/information/',
];
